/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getUser = /* GraphQL */ `
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      username
      email
      name
      surname
      idno
      dob
      phone_number
      emergency_name
      emergency_number
      chronic
      chronic_name
      wheelchair
      gender
      completeP
      imageUrl
      smoke
      carName
      model
      plate
      carImage
      balance
      account
      bank
      branch
      onBoard
      orders {
        items {
          id
          createdAt
          type
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          userId
          carId
          updatedAt
        }
        nextToken
      }
      car {
        id
        type
        latitude
        longitude
        heading
        isActive
        orders {
          nextToken
        }
        userId
        user {
          id
          username
          email
          name
          surname
          idno
          dob
          phone_number
          emergency_name
          emergency_number
          chronic
          chronic_name
          wheelchair
          gender
          completeP
          imageUrl
          smoke
          carName
          model
          plate
          carImage
          balance
          account
          bank
          branch
          onBoard
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        username
        email
        name
        surname
        idno
        dob
        phone_number
        emergency_name
        emergency_number
        chronic
        chronic_name
        wheelchair
        gender
        completeP
        imageUrl
        smoke
        carName
        model
        plate
        carImage
        balance
        account
        bank
        branch
        onBoard
        orders {
          nextToken
        }
        car {
          id
          type
          latitude
          longitude
          heading
          isActive
          userId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCar = /* GraphQL */ `
  query GetCar($id: ID!) {
    getCar(id: $id) {
      id
      type
      latitude
      longitude
      heading
      isActive
      orders {
        items {
          id
          createdAt
          type
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          userId
          carId
          updatedAt
        }
        nextToken
      }
      userId
      user {
        id
        username
        email
        name
        surname
        idno
        dob
        phone_number
        emergency_name
        emergency_number
        chronic
        chronic_name
        wheelchair
        gender
        completeP
        imageUrl
        smoke
        carName
        model
        plate
        carImage
        balance
        account
        bank
        branch
        onBoard
        orders {
          nextToken
        }
        car {
          id
          type
          latitude
          longitude
          heading
          isActive
          userId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listCars = /* GraphQL */ `
  query ListCars(
    $filter: ModelCarFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCars(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        type
        latitude
        longitude
        heading
        isActive
        orders {
          nextToken
        }
        userId
        user {
          id
          username
          email
          name
          surname
          idno
          dob
          phone_number
          emergency_name
          emergency_number
          chronic
          chronic_name
          wheelchair
          gender
          completeP
          imageUrl
          smoke
          carName
          model
          plate
          carImage
          balance
          account
          bank
          branch
          onBoard
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCashOut = /* GraphQL */ `
  query GetCashOut($id: ID!) {
    getCashOut(id: $id) {
      id
      userId
      email
      name
      surname
      amount
      progress
      createdAt
      updatedAt
    }
  }
`;
export const listCashOuts = /* GraphQL */ `
  query ListCashOuts(
    $filter: ModelCashOutFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCashOuts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userId
        email
        name
        surname
        amount
        progress
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getOrder = /* GraphQL */ `
  query GetOrder($id: ID!) {
    getOrder(id: $id) {
      id
      createdAt
      type
      status
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      userId
      user {
        id
        username
        email
        name
        surname
        idno
        dob
        phone_number
        emergency_name
        emergency_number
        chronic
        chronic_name
        wheelchair
        gender
        completeP
        imageUrl
        smoke
        carName
        model
        plate
        carImage
        balance
        account
        bank
        branch
        onBoard
        orders {
          nextToken
        }
        car {
          id
          type
          latitude
          longitude
          heading
          isActive
          userId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      carId
      car {
        id
        type
        latitude
        longitude
        heading
        isActive
        orders {
          nextToken
        }
        userId
        user {
          id
          username
          email
          name
          surname
          idno
          dob
          phone_number
          emergency_name
          emergency_number
          chronic
          chronic_name
          wheelchair
          gender
          completeP
          imageUrl
          smoke
          carName
          model
          plate
          carImage
          balance
          account
          bank
          branch
          onBoard
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      updatedAt
    }
  }
`;
export const listOrders = /* GraphQL */ `
  query ListOrders(
    $filter: ModelOrderFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listOrders(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        createdAt
        type
        status
        originLatitude
        originLongitude
        destLatitude
        destLongitude
        userId
        user {
          id
          username
          email
          name
          surname
          idno
          dob
          phone_number
          emergency_name
          emergency_number
          chronic
          chronic_name
          wheelchair
          gender
          completeP
          imageUrl
          smoke
          carName
          model
          plate
          carImage
          balance
          account
          bank
          branch
          onBoard
          createdAt
          updatedAt
        }
        carId
        car {
          id
          type
          latitude
          longitude
          heading
          isActive
          userId
          createdAt
          updatedAt
        }
        updatedAt
      }
      nextToken
    }
  }
`;
export const getTodo = /* GraphQL */ `
  query GetTodo($id: ID!) {
    getTodo(id: $id) {
      id
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const listTodos = /* GraphQL */ `
  query ListTodos(
    $filter: ModelTodoFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTodos(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        description
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getRides = /* GraphQL */ `
  query GetRides($id: ID!) {
    getRides(id: $id) {
      id
      driverID
      name
      surname
      imageUrl
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      price
      tripFee
      status
      active
      startDate
      totalSeats
      availSeats
      willTaxi
      willTaxiPrice
      carName
      model
      plate
      carImage
      driverId
      luggageMax
      message
      request {
        items {
          id
          userId
          name
          surname
          imageUrl
          active
          taxi
          rideId
          tripFee
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          originDesc
          originSec
          destDesc
          destSec
          startDate
          luggage
          driverId
          seats
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listRides = /* GraphQL */ `
  query ListRides(
    $filter: ModelRidesFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listRides(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        driverID
        name
        surname
        imageUrl
        originLatitude
        originLongitude
        destLatitude
        destLongitude
        originDesc
        originSec
        destDesc
        destSec
        price
        tripFee
        status
        active
        startDate
        totalSeats
        availSeats
        willTaxi
        willTaxiPrice
        carName
        model
        plate
        carImage
        driverId
        luggageMax
        message
        request {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getRequests = /* GraphQL */ `
  query GetRequests($id: ID!) {
    getRequests(id: $id) {
      id
      userId
      name
      surname
      imageUrl
      active
      taxi
      rideId
      tripFee
      status
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      startDate
      luggage
      driverId
      seats
      rides {
        id
        driverID
        name
        surname
        imageUrl
        originLatitude
        originLongitude
        destLatitude
        destLongitude
        originDesc
        originSec
        destDesc
        destSec
        price
        tripFee
        status
        active
        startDate
        totalSeats
        availSeats
        willTaxi
        willTaxiPrice
        carName
        model
        plate
        carImage
        driverId
        luggageMax
        message
        request {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listRequests = /* GraphQL */ `
  query ListRequests(
    $filter: ModelRequestsFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listRequests(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userId
        name
        surname
        imageUrl
        active
        taxi
        rideId
        tripFee
        status
        originLatitude
        originLongitude
        destLatitude
        destLongitude
        originDesc
        originSec
        destDesc
        destSec
        startDate
        luggage
        driverId
        seats
        rides {
          id
          driverID
          name
          surname
          imageUrl
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          originDesc
          originSec
          destDesc
          destSec
          price
          tripFee
          status
          active
          startDate
          totalSeats
          availSeats
          willTaxi
          willTaxiPrice
          carName
          model
          plate
          carImage
          driverId
          luggageMax
          message
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getParcelRequests = /* GraphQL */ `
  query GetParcelRequests($id: ID!) {
    getParcelRequests(id: $id) {
      id
      userId
      name
      surname
      imageUrl
      type
      contents
      qty
      breakage
      flame
      recName
      recSurname
      recNumber
      recCode
      active
      totalSeats
      availSeats
      rideId
      tripFee
      status
      pickupDate
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      startDate
      luggage
      driverId
      seats
      mode
      createdAt
      updatedAt
    }
  }
`;
export const listParcelRequests = /* GraphQL */ `
  query ListParcelRequests(
    $filter: ModelParcelRequestsFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listParcelRequests(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userId
        name
        surname
        imageUrl
        type
        contents
        qty
        breakage
        flame
        recName
        recSurname
        recNumber
        recCode
        active
        totalSeats
        availSeats
        rideId
        tripFee
        status
        pickupDate
        originLatitude
        originLongitude
        destLatitude
        destLongitude
        originDesc
        originSec
        destDesc
        destSec
        startDate
        luggage
        driverId
        seats
        mode
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getParcelOrder = /* GraphQL */ `
  query GetParcelOrder($id: ID!) {
    getParcelOrder(id: $id) {
      id
      userId
      name
      surname
      imageUrl
      carName
      model
      plate
      carImage
      type
      contents
      qty
      originLongitude
      pickupDate
      pickupTime
      createdAt
      originLatitude
      destLatitude
      destLongitude
      price
      driverId
      status
      active
      originDesc
      originSec
      destDesc
      destSec
      mode
      drLat
      drLng
      drPOLat
      drPOLng
      drPDLat
      drPDLng
      drPODesc
      drPOSec
      drPDDesc
      drPDSec
      drPDate
      drPSize
      updatedAt
    }
  }
`;
export const listParcelOrders = /* GraphQL */ `
  query ListParcelOrders(
    $filter: ModelParcelOrderFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listParcelOrders(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userId
        name
        surname
        imageUrl
        carName
        model
        plate
        carImage
        type
        contents
        qty
        originLongitude
        pickupDate
        pickupTime
        createdAt
        originLatitude
        destLatitude
        destLongitude
        price
        driverId
        status
        active
        originDesc
        originSec
        destDesc
        destSec
        mode
        drLat
        drLng
        drPOLat
        drPOLng
        drPDLat
        drPDLng
        drPODesc
        drPOSec
        drPDDesc
        drPDSec
        drPDate
        drPSize
        updatedAt
      }
      nextToken
    }
  }
`;
export const getGoodsOrder = /* GraphQL */ `
  query GetGoodsOrder($id: ID!) {
    getGoodsOrder(id: $id) {
      id
      userId
      type
      contents
      qty
      breakage
      flame
      recName
      recSurname
      recNumber
      recCode
      originLongitude
      pickupDate
      pickupTime
      createdAt
      originLatitude
      destLatitude
      destLongitude
      price
      driverId
      status
      active
      originDesc
      originSec
      destDesc
      destSec
      drLat
      drLng
      drPOLat
      drPOLng
      drPDLat
      drPDLng
      drPODesc
      drPOSec
      drPDDesc
      drPDSec
      drPDate
      drPSize
      updatedAt
    }
  }
`;
export const listGoodsOrders = /* GraphQL */ `
  query ListGoodsOrders(
    $filter: ModelGoodsOrderFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listGoodsOrders(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userId
        type
        contents
        qty
        breakage
        flame
        recName
        recSurname
        recNumber
        recCode
        originLongitude
        pickupDate
        pickupTime
        createdAt
        originLatitude
        destLatitude
        destLongitude
        price
        driverId
        status
        active
        originDesc
        originSec
        destDesc
        destSec
        drLat
        drLng
        drPOLat
        drPOLng
        drPDLat
        drPDLng
        drPODesc
        drPOSec
        drPDDesc
        drPDSec
        drPDate
        drPSize
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLd = /* GraphQL */ `
  query GetLd($id: ID!) {
    getLd(id: $id) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
export const listLds = /* GraphQL */ `
  query ListLds($filter: ModelLdFilterInput, $limit: Int, $nextToken: String) {
    listLds(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        balance
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLdBlack = /* GraphQL */ `
  query GetLdBlack($id: ID!) {
    getLdBlack(id: $id) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
export const listLdBlacks = /* GraphQL */ `
  query ListLdBlacks(
    $filter: ModelLdBlackFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLdBlacks(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        balance
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLdTemp = /* GraphQL */ `
  query GetLdTemp($id: ID!) {
    getLdTemp(id: $id) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
export const listLdTemps = /* GraphQL */ `
  query ListLdTemps(
    $filter: ModelLdTempFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLdTemps(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        balance
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
