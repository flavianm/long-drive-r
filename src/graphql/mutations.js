/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createUser = /* GraphQL */ `
  mutation CreateUser(
    $input: CreateUserInput!
    $condition: ModelUserConditionInput
  ) {
    createUser(input: $input, condition: $condition) {
      id
      username
      email
      name
      surname
      idno
      dob
      phone_number
      emergency_name
      emergency_number
      chronic
      chronic_name
      wheelchair
      gender
      completeP
      imageUrl
      smoke
      carName
      model
      plate
      carImage
      balance
      account
      bank
      branch
      onBoard
      orders {
        items {
          id
          createdAt
          type
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          userId
          carId
          updatedAt
        }
        nextToken
      }
      car {
        id
        type
        latitude
        longitude
        heading
        isActive
        orders {
          nextToken
        }
        userId
        user {
          id
          username
          email
          name
          surname
          idno
          dob
          phone_number
          emergency_name
          emergency_number
          chronic
          chronic_name
          wheelchair
          gender
          completeP
          imageUrl
          smoke
          carName
          model
          plate
          carImage
          balance
          account
          bank
          branch
          onBoard
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateUser = /* GraphQL */ `
  mutation UpdateUser(
    $input: UpdateUserInput!
    $condition: ModelUserConditionInput
  ) {
    updateUser(input: $input, condition: $condition) {
      id
      username
      email
      name
      surname
      idno
      dob
      phone_number
      emergency_name
      emergency_number
      chronic
      chronic_name
      wheelchair
      gender
      completeP
      imageUrl
      smoke
      carName
      model
      plate
      carImage
      balance
      account
      bank
      branch
      onBoard
      orders {
        items {
          id
          createdAt
          type
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          userId
          carId
          updatedAt
        }
        nextToken
      }
      car {
        id
        type
        latitude
        longitude
        heading
        isActive
        orders {
          nextToken
        }
        userId
        user {
          id
          username
          email
          name
          surname
          idno
          dob
          phone_number
          emergency_name
          emergency_number
          chronic
          chronic_name
          wheelchair
          gender
          completeP
          imageUrl
          smoke
          carName
          model
          plate
          carImage
          balance
          account
          bank
          branch
          onBoard
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteUser = /* GraphQL */ `
  mutation DeleteUser(
    $input: DeleteUserInput!
    $condition: ModelUserConditionInput
  ) {
    deleteUser(input: $input, condition: $condition) {
      id
      username
      email
      name
      surname
      idno
      dob
      phone_number
      emergency_name
      emergency_number
      chronic
      chronic_name
      wheelchair
      gender
      completeP
      imageUrl
      smoke
      carName
      model
      plate
      carImage
      balance
      account
      bank
      branch
      onBoard
      orders {
        items {
          id
          createdAt
          type
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          userId
          carId
          updatedAt
        }
        nextToken
      }
      car {
        id
        type
        latitude
        longitude
        heading
        isActive
        orders {
          nextToken
        }
        userId
        user {
          id
          username
          email
          name
          surname
          idno
          dob
          phone_number
          emergency_name
          emergency_number
          chronic
          chronic_name
          wheelchair
          gender
          completeP
          imageUrl
          smoke
          carName
          model
          plate
          carImage
          balance
          account
          bank
          branch
          onBoard
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createCar = /* GraphQL */ `
  mutation CreateCar(
    $input: CreateCarInput!
    $condition: ModelCarConditionInput
  ) {
    createCar(input: $input, condition: $condition) {
      id
      type
      latitude
      longitude
      heading
      isActive
      orders {
        items {
          id
          createdAt
          type
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          userId
          carId
          updatedAt
        }
        nextToken
      }
      userId
      user {
        id
        username
        email
        name
        surname
        idno
        dob
        phone_number
        emergency_name
        emergency_number
        chronic
        chronic_name
        wheelchair
        gender
        completeP
        imageUrl
        smoke
        carName
        model
        plate
        carImage
        balance
        account
        bank
        branch
        onBoard
        orders {
          nextToken
        }
        car {
          id
          type
          latitude
          longitude
          heading
          isActive
          userId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateCar = /* GraphQL */ `
  mutation UpdateCar(
    $input: UpdateCarInput!
    $condition: ModelCarConditionInput
  ) {
    updateCar(input: $input, condition: $condition) {
      id
      type
      latitude
      longitude
      heading
      isActive
      orders {
        items {
          id
          createdAt
          type
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          userId
          carId
          updatedAt
        }
        nextToken
      }
      userId
      user {
        id
        username
        email
        name
        surname
        idno
        dob
        phone_number
        emergency_name
        emergency_number
        chronic
        chronic_name
        wheelchair
        gender
        completeP
        imageUrl
        smoke
        carName
        model
        plate
        carImage
        balance
        account
        bank
        branch
        onBoard
        orders {
          nextToken
        }
        car {
          id
          type
          latitude
          longitude
          heading
          isActive
          userId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteCar = /* GraphQL */ `
  mutation DeleteCar(
    $input: DeleteCarInput!
    $condition: ModelCarConditionInput
  ) {
    deleteCar(input: $input, condition: $condition) {
      id
      type
      latitude
      longitude
      heading
      isActive
      orders {
        items {
          id
          createdAt
          type
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          userId
          carId
          updatedAt
        }
        nextToken
      }
      userId
      user {
        id
        username
        email
        name
        surname
        idno
        dob
        phone_number
        emergency_name
        emergency_number
        chronic
        chronic_name
        wheelchair
        gender
        completeP
        imageUrl
        smoke
        carName
        model
        plate
        carImage
        balance
        account
        bank
        branch
        onBoard
        orders {
          nextToken
        }
        car {
          id
          type
          latitude
          longitude
          heading
          isActive
          userId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createCashOut = /* GraphQL */ `
  mutation CreateCashOut(
    $input: CreateCashOutInput!
    $condition: ModelCashOutConditionInput
  ) {
    createCashOut(input: $input, condition: $condition) {
      id
      userId
      email
      name
      surname
      amount
      progress
      createdAt
      updatedAt
    }
  }
`;
export const updateCashOut = /* GraphQL */ `
  mutation UpdateCashOut(
    $input: UpdateCashOutInput!
    $condition: ModelCashOutConditionInput
  ) {
    updateCashOut(input: $input, condition: $condition) {
      id
      userId
      email
      name
      surname
      amount
      progress
      createdAt
      updatedAt
    }
  }
`;
export const deleteCashOut = /* GraphQL */ `
  mutation DeleteCashOut(
    $input: DeleteCashOutInput!
    $condition: ModelCashOutConditionInput
  ) {
    deleteCashOut(input: $input, condition: $condition) {
      id
      userId
      email
      name
      surname
      amount
      progress
      createdAt
      updatedAt
    }
  }
`;
export const createOrder = /* GraphQL */ `
  mutation CreateOrder(
    $input: CreateOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    createOrder(input: $input, condition: $condition) {
      id
      createdAt
      type
      status
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      userId
      user {
        id
        username
        email
        name
        surname
        idno
        dob
        phone_number
        emergency_name
        emergency_number
        chronic
        chronic_name
        wheelchair
        gender
        completeP
        imageUrl
        smoke
        carName
        model
        plate
        carImage
        balance
        account
        bank
        branch
        onBoard
        orders {
          nextToken
        }
        car {
          id
          type
          latitude
          longitude
          heading
          isActive
          userId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      carId
      car {
        id
        type
        latitude
        longitude
        heading
        isActive
        orders {
          nextToken
        }
        userId
        user {
          id
          username
          email
          name
          surname
          idno
          dob
          phone_number
          emergency_name
          emergency_number
          chronic
          chronic_name
          wheelchair
          gender
          completeP
          imageUrl
          smoke
          carName
          model
          plate
          carImage
          balance
          account
          bank
          branch
          onBoard
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      updatedAt
    }
  }
`;
export const updateOrder = /* GraphQL */ `
  mutation UpdateOrder(
    $input: UpdateOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    updateOrder(input: $input, condition: $condition) {
      id
      createdAt
      type
      status
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      userId
      user {
        id
        username
        email
        name
        surname
        idno
        dob
        phone_number
        emergency_name
        emergency_number
        chronic
        chronic_name
        wheelchair
        gender
        completeP
        imageUrl
        smoke
        carName
        model
        plate
        carImage
        balance
        account
        bank
        branch
        onBoard
        orders {
          nextToken
        }
        car {
          id
          type
          latitude
          longitude
          heading
          isActive
          userId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      carId
      car {
        id
        type
        latitude
        longitude
        heading
        isActive
        orders {
          nextToken
        }
        userId
        user {
          id
          username
          email
          name
          surname
          idno
          dob
          phone_number
          emergency_name
          emergency_number
          chronic
          chronic_name
          wheelchair
          gender
          completeP
          imageUrl
          smoke
          carName
          model
          plate
          carImage
          balance
          account
          bank
          branch
          onBoard
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      updatedAt
    }
  }
`;
export const deleteOrder = /* GraphQL */ `
  mutation DeleteOrder(
    $input: DeleteOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    deleteOrder(input: $input, condition: $condition) {
      id
      createdAt
      type
      status
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      userId
      user {
        id
        username
        email
        name
        surname
        idno
        dob
        phone_number
        emergency_name
        emergency_number
        chronic
        chronic_name
        wheelchair
        gender
        completeP
        imageUrl
        smoke
        carName
        model
        plate
        carImage
        balance
        account
        bank
        branch
        onBoard
        orders {
          nextToken
        }
        car {
          id
          type
          latitude
          longitude
          heading
          isActive
          userId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      carId
      car {
        id
        type
        latitude
        longitude
        heading
        isActive
        orders {
          nextToken
        }
        userId
        user {
          id
          username
          email
          name
          surname
          idno
          dob
          phone_number
          emergency_name
          emergency_number
          chronic
          chronic_name
          wheelchair
          gender
          completeP
          imageUrl
          smoke
          carName
          model
          plate
          carImage
          balance
          account
          bank
          branch
          onBoard
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      updatedAt
    }
  }
`;
export const createTodo = /* GraphQL */ `
  mutation CreateTodo(
    $input: CreateTodoInput!
    $condition: ModelTodoConditionInput
  ) {
    createTodo(input: $input, condition: $condition) {
      id
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const updateTodo = /* GraphQL */ `
  mutation UpdateTodo(
    $input: UpdateTodoInput!
    $condition: ModelTodoConditionInput
  ) {
    updateTodo(input: $input, condition: $condition) {
      id
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const deleteTodo = /* GraphQL */ `
  mutation DeleteTodo(
    $input: DeleteTodoInput!
    $condition: ModelTodoConditionInput
  ) {
    deleteTodo(input: $input, condition: $condition) {
      id
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const createRides = /* GraphQL */ `
  mutation CreateRides(
    $input: CreateRidesInput!
    $condition: ModelRidesConditionInput
  ) {
    createRides(input: $input, condition: $condition) {
      id
      driverID
      name
      surname
      imageUrl
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      price
      tripFee
      status
      active
      startDate
      totalSeats
      availSeats
      willTaxi
      willTaxiPrice
      carName
      model
      plate
      carImage
      driverId
      luggageMax
      message
      request {
        items {
          id
          userId
          name
          surname
          imageUrl
          active
          taxi
          rideId
          tripFee
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          originDesc
          originSec
          destDesc
          destSec
          startDate
          luggage
          driverId
          seats
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateRides = /* GraphQL */ `
  mutation UpdateRides(
    $input: UpdateRidesInput!
    $condition: ModelRidesConditionInput
  ) {
    updateRides(input: $input, condition: $condition) {
      id
      driverID
      name
      surname
      imageUrl
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      price
      tripFee
      status
      active
      startDate
      totalSeats
      availSeats
      willTaxi
      willTaxiPrice
      carName
      model
      plate
      carImage
      driverId
      luggageMax
      message
      request {
        items {
          id
          userId
          name
          surname
          imageUrl
          active
          taxi
          rideId
          tripFee
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          originDesc
          originSec
          destDesc
          destSec
          startDate
          luggage
          driverId
          seats
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteRides = /* GraphQL */ `
  mutation DeleteRides(
    $input: DeleteRidesInput!
    $condition: ModelRidesConditionInput
  ) {
    deleteRides(input: $input, condition: $condition) {
      id
      driverID
      name
      surname
      imageUrl
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      price
      tripFee
      status
      active
      startDate
      totalSeats
      availSeats
      willTaxi
      willTaxiPrice
      carName
      model
      plate
      carImage
      driverId
      luggageMax
      message
      request {
        items {
          id
          userId
          name
          surname
          imageUrl
          active
          taxi
          rideId
          tripFee
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          originDesc
          originSec
          destDesc
          destSec
          startDate
          luggage
          driverId
          seats
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createRequests = /* GraphQL */ `
  mutation CreateRequests(
    $input: CreateRequestsInput!
    $condition: ModelRequestsConditionInput
  ) {
    createRequests(input: $input, condition: $condition) {
      id
      userId
      name
      surname
      imageUrl
      active
      taxi
      rideId
      tripFee
      status
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      startDate
      luggage
      driverId
      seats
      rides {
        id
        driverID
        name
        surname
        imageUrl
        originLatitude
        originLongitude
        destLatitude
        destLongitude
        originDesc
        originSec
        destDesc
        destSec
        price
        tripFee
        status
        active
        startDate
        totalSeats
        availSeats
        willTaxi
        willTaxiPrice
        carName
        model
        plate
        carImage
        driverId
        luggageMax
        message
        request {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateRequests = /* GraphQL */ `
  mutation UpdateRequests(
    $input: UpdateRequestsInput!
    $condition: ModelRequestsConditionInput
  ) {
    updateRequests(input: $input, condition: $condition) {
      id
      userId
      name
      surname
      imageUrl
      active
      taxi
      rideId
      tripFee
      status
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      startDate
      luggage
      driverId
      seats
      rides {
        id
        driverID
        name
        surname
        imageUrl
        originLatitude
        originLongitude
        destLatitude
        destLongitude
        originDesc
        originSec
        destDesc
        destSec
        price
        tripFee
        status
        active
        startDate
        totalSeats
        availSeats
        willTaxi
        willTaxiPrice
        carName
        model
        plate
        carImage
        driverId
        luggageMax
        message
        request {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteRequests = /* GraphQL */ `
  mutation DeleteRequests(
    $input: DeleteRequestsInput!
    $condition: ModelRequestsConditionInput
  ) {
    deleteRequests(input: $input, condition: $condition) {
      id
      userId
      name
      surname
      imageUrl
      active
      taxi
      rideId
      tripFee
      status
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      startDate
      luggage
      driverId
      seats
      rides {
        id
        driverID
        name
        surname
        imageUrl
        originLatitude
        originLongitude
        destLatitude
        destLongitude
        originDesc
        originSec
        destDesc
        destSec
        price
        tripFee
        status
        active
        startDate
        totalSeats
        availSeats
        willTaxi
        willTaxiPrice
        carName
        model
        plate
        carImage
        driverId
        luggageMax
        message
        request {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createParcelRequests = /* GraphQL */ `
  mutation CreateParcelRequests(
    $input: CreateParcelRequestsInput!
    $condition: ModelParcelRequestsConditionInput
  ) {
    createParcelRequests(input: $input, condition: $condition) {
      id
      userId
      name
      surname
      imageUrl
      type
      contents
      qty
      breakage
      flame
      recName
      recSurname
      recNumber
      recCode
      active
      totalSeats
      availSeats
      rideId
      tripFee
      status
      pickupDate
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      startDate
      luggage
      driverId
      seats
      mode
      createdAt
      updatedAt
    }
  }
`;
export const updateParcelRequests = /* GraphQL */ `
  mutation UpdateParcelRequests(
    $input: UpdateParcelRequestsInput!
    $condition: ModelParcelRequestsConditionInput
  ) {
    updateParcelRequests(input: $input, condition: $condition) {
      id
      userId
      name
      surname
      imageUrl
      type
      contents
      qty
      breakage
      flame
      recName
      recSurname
      recNumber
      recCode
      active
      totalSeats
      availSeats
      rideId
      tripFee
      status
      pickupDate
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      startDate
      luggage
      driverId
      seats
      mode
      createdAt
      updatedAt
    }
  }
`;
export const deleteParcelRequests = /* GraphQL */ `
  mutation DeleteParcelRequests(
    $input: DeleteParcelRequestsInput!
    $condition: ModelParcelRequestsConditionInput
  ) {
    deleteParcelRequests(input: $input, condition: $condition) {
      id
      userId
      name
      surname
      imageUrl
      type
      contents
      qty
      breakage
      flame
      recName
      recSurname
      recNumber
      recCode
      active
      totalSeats
      availSeats
      rideId
      tripFee
      status
      pickupDate
      originLatitude
      originLongitude
      destLatitude
      destLongitude
      originDesc
      originSec
      destDesc
      destSec
      startDate
      luggage
      driverId
      seats
      mode
      createdAt
      updatedAt
    }
  }
`;
export const createParcelOrder = /* GraphQL */ `
  mutation CreateParcelOrder(
    $input: CreateParcelOrderInput!
    $condition: ModelParcelOrderConditionInput
  ) {
    createParcelOrder(input: $input, condition: $condition) {
      id
      userId
      name
      surname
      imageUrl
      carName
      model
      plate
      carImage
      type
      contents
      qty
      originLongitude
      pickupDate
      pickupTime
      createdAt
      originLatitude
      destLatitude
      destLongitude
      price
      driverId
      status
      active
      originDesc
      originSec
      destDesc
      destSec
      mode
      drLat
      drLng
      drPOLat
      drPOLng
      drPDLat
      drPDLng
      drPODesc
      drPOSec
      drPDDesc
      drPDSec
      drPDate
      drPSize
      updatedAt
    }
  }
`;
export const updateParcelOrder = /* GraphQL */ `
  mutation UpdateParcelOrder(
    $input: UpdateParcelOrderInput!
    $condition: ModelParcelOrderConditionInput
  ) {
    updateParcelOrder(input: $input, condition: $condition) {
      id
      userId
      name
      surname
      imageUrl
      carName
      model
      plate
      carImage
      type
      contents
      qty
      originLongitude
      pickupDate
      pickupTime
      createdAt
      originLatitude
      destLatitude
      destLongitude
      price
      driverId
      status
      active
      originDesc
      originSec
      destDesc
      destSec
      mode
      drLat
      drLng
      drPOLat
      drPOLng
      drPDLat
      drPDLng
      drPODesc
      drPOSec
      drPDDesc
      drPDSec
      drPDate
      drPSize
      updatedAt
    }
  }
`;
export const deleteParcelOrder = /* GraphQL */ `
  mutation DeleteParcelOrder(
    $input: DeleteParcelOrderInput!
    $condition: ModelParcelOrderConditionInput
  ) {
    deleteParcelOrder(input: $input, condition: $condition) {
      id
      userId
      name
      surname
      imageUrl
      carName
      model
      plate
      carImage
      type
      contents
      qty
      originLongitude
      pickupDate
      pickupTime
      createdAt
      originLatitude
      destLatitude
      destLongitude
      price
      driverId
      status
      active
      originDesc
      originSec
      destDesc
      destSec
      mode
      drLat
      drLng
      drPOLat
      drPOLng
      drPDLat
      drPDLng
      drPODesc
      drPOSec
      drPDDesc
      drPDSec
      drPDate
      drPSize
      updatedAt
    }
  }
`;
export const createGoodsOrder = /* GraphQL */ `
  mutation CreateGoodsOrder(
    $input: CreateGoodsOrderInput!
    $condition: ModelGoodsOrderConditionInput
  ) {
    createGoodsOrder(input: $input, condition: $condition) {
      id
      userId
      type
      contents
      qty
      breakage
      flame
      recName
      recSurname
      recNumber
      recCode
      originLongitude
      pickupDate
      pickupTime
      createdAt
      originLatitude
      destLatitude
      destLongitude
      price
      driverId
      status
      active
      originDesc
      originSec
      destDesc
      destSec
      drLat
      drLng
      drPOLat
      drPOLng
      drPDLat
      drPDLng
      drPODesc
      drPOSec
      drPDDesc
      drPDSec
      drPDate
      drPSize
      updatedAt
    }
  }
`;
export const updateGoodsOrder = /* GraphQL */ `
  mutation UpdateGoodsOrder(
    $input: UpdateGoodsOrderInput!
    $condition: ModelGoodsOrderConditionInput
  ) {
    updateGoodsOrder(input: $input, condition: $condition) {
      id
      userId
      type
      contents
      qty
      breakage
      flame
      recName
      recSurname
      recNumber
      recCode
      originLongitude
      pickupDate
      pickupTime
      createdAt
      originLatitude
      destLatitude
      destLongitude
      price
      driverId
      status
      active
      originDesc
      originSec
      destDesc
      destSec
      drLat
      drLng
      drPOLat
      drPOLng
      drPDLat
      drPDLng
      drPODesc
      drPOSec
      drPDDesc
      drPDSec
      drPDate
      drPSize
      updatedAt
    }
  }
`;
export const deleteGoodsOrder = /* GraphQL */ `
  mutation DeleteGoodsOrder(
    $input: DeleteGoodsOrderInput!
    $condition: ModelGoodsOrderConditionInput
  ) {
    deleteGoodsOrder(input: $input, condition: $condition) {
      id
      userId
      type
      contents
      qty
      breakage
      flame
      recName
      recSurname
      recNumber
      recCode
      originLongitude
      pickupDate
      pickupTime
      createdAt
      originLatitude
      destLatitude
      destLongitude
      price
      driverId
      status
      active
      originDesc
      originSec
      destDesc
      destSec
      drLat
      drLng
      drPOLat
      drPOLng
      drPDLat
      drPDLng
      drPODesc
      drPOSec
      drPDDesc
      drPDSec
      drPDate
      drPSize
      updatedAt
    }
  }
`;
export const createLd = /* GraphQL */ `
  mutation CreateLd($input: CreateLdInput!, $condition: ModelLdConditionInput) {
    createLd(input: $input, condition: $condition) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
export const updateLd = /* GraphQL */ `
  mutation UpdateLd($input: UpdateLdInput!, $condition: ModelLdConditionInput) {
    updateLd(input: $input, condition: $condition) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
export const deleteLd = /* GraphQL */ `
  mutation DeleteLd($input: DeleteLdInput!, $condition: ModelLdConditionInput) {
    deleteLd(input: $input, condition: $condition) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
export const createLdBlack = /* GraphQL */ `
  mutation CreateLdBlack(
    $input: CreateLdBlackInput!
    $condition: ModelLdBlackConditionInput
  ) {
    createLdBlack(input: $input, condition: $condition) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
export const updateLdBlack = /* GraphQL */ `
  mutation UpdateLdBlack(
    $input: UpdateLdBlackInput!
    $condition: ModelLdBlackConditionInput
  ) {
    updateLdBlack(input: $input, condition: $condition) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
export const deleteLdBlack = /* GraphQL */ `
  mutation DeleteLdBlack(
    $input: DeleteLdBlackInput!
    $condition: ModelLdBlackConditionInput
  ) {
    deleteLdBlack(input: $input, condition: $condition) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
export const createLdTemp = /* GraphQL */ `
  mutation CreateLdTemp(
    $input: CreateLdTempInput!
    $condition: ModelLdTempConditionInput
  ) {
    createLdTemp(input: $input, condition: $condition) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
export const updateLdTemp = /* GraphQL */ `
  mutation UpdateLdTemp(
    $input: UpdateLdTempInput!
    $condition: ModelLdTempConditionInput
  ) {
    updateLdTemp(input: $input, condition: $condition) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
export const deleteLdTemp = /* GraphQL */ `
  mutation DeleteLdTemp(
    $input: DeleteLdTempInput!
    $condition: ModelLdTempConditionInput
  ) {
    deleteLdTemp(input: $input, condition: $condition) {
      id
      balance
      createdAt
      updatedAt
    }
  }
`;
