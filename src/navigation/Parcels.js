import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
/* import HomeScreen from "../screens/HomeScreen";
import DestinationSearch from "../screens/DestinationSearch";
import SearchResult from "../screens/SearchResults";
import { DatePickerPick } from "../screens/DatePick";
import OrderScreen from "../screens/OrderScreen";
import { Chat } from "../screens/ChatScreen";
import OptionScreen from "../screens/Option"; */
import { Posttrip } from "../screens/Posttrip";
import PayScreen from "../screens/PayScreen";
import CompleteScreen from "../screens/CompleteScreen";
import PostSearch from "../screens/PostSearch";
/*import EmptyPage from "../screens/EmptyScreen";
import SeatOrder from "../screens/SeatOrders";
import SecScreen from "../screens/SecScreen";
import FindSearch from "../screens/Findtrip";
import ParcelCustomer from "../screens/ParcelCustomer";
import ParcelOrders from "../screens/ParcelOrders";
import ParcelOrderInfoCust from "../screens/ParcelOrderInfoCust";
import ParcelOrdersDA from "../screens/ParcelOrdersDA";
import ParcelOrderInfoDA from "../screens/ParcelOrderInfoDA";
import PDAO from "../screens/ParcelDAO";
import ParcelDAOInfo from "../screens/ParcelDAOInfo"; */
import ProgressScreen from "../screens/ProgressScreen";
import UserProfile from "../screens/UserProfileScreen";
//import ParcelDriver from "../screens/ParcelDriver";
import HomeOptParcels from "../components/HomeOptParcels";
import ParcelsMoreNavigator from "./ParcelsMore";
//import ParcelHomeScreen from "../screens/ParcelHomeScreen";
import DriverMore from "../screens/DriverMore";
/*import ParcelDestDriver from "../screens/ParcelDestDriver";
//import ParcelOrdersDriver from "../screens/ParcelOrdersDriver";
import ParcelSearchCustResult from "../screens/ParcelSearchCustResult";
import ParcelCustSearch from "../screens/ParcelCustSearch";
import ParcelOrderInfoDSet from '../screens/ParcelOrderInfoDSet';
import ParcelOrderInfoDSetBBB from "../screens/ParcelOrderInfoDSetBBB" */;

const Stack = createStackNavigator();

const ParcelsNavigator = (props) =>{

   return (
      <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
      initialRouteName={'Parcels'}
      >
        <Stack.Screen name="Parcels"  component={ParcelHomeScreen}/>
        <Stack.Screen name="ParcelsMenu"  component={ParcelsMoreNavigator}/>
        <Stack.Screen name="DriverMore"  component={DriverMore}/>
        <Stack.Screen name="ParcelDestDriver"  component={ParcelDestDriver}/>
        <Stack.Screen name="ParcelDriver"  component={ParcelDriver}/>
        <Stack.Screen name="ParcelOrdersDriver"  component={ParcelOrdersDriver}/>
        <Stack.Screen name="Trip"  component={Posttrip}/>
        <Stack.Screen name="Pay"  component={PayScreen}/>
        <Stack.Screen name="Complete"  component={CompleteScreen}/>
        <Stack.Screen name="PostSearch"  component={PostSearch}/>
        <Stack.Screen name="ParcelSearchCustResult"  component={ParcelSearchCustResult}/>
        <Stack.Screen name="ParcelCustSearch"  component={ParcelCustSearch}/>
        <Stack.Screen name="ParcelOrderInfoDSetBBB"  component={ParcelOrderInfoDSetBBB}/>
        <Stack.Screen name="ParcelCustomer"  component={ParcelCustomer}/>
        <Stack.Screen name="ParcelOrders"  component={ParcelOrders}/>
        <Stack.Screen name="ParcelOrderInfoCust"  component={ParcelOrderInfoCust}/>
        <Stack.Screen name="ParcelOrdersDA"  component={ParcelOrdersDA}/>
        <Stack.Screen name="ParcelOrderInfoDA"  component={ParcelOrderInfoDA}/>
        <Stack.Screen name="PDAO"  component={PDAO}/>
        <Stack.Screen name="ParcelDAOInfo"  component={ParcelDAOInfo}/>
        <Stack.Screen name="ProgressScreen"  component={ProgressScreen}/>
        <Stack.Screen name="ParcelOrderInfoDSet"  component={ParcelOrderInfoDSet}/>
      </Stack.Navigator>
  );
};

export default ParcelsNavigator;