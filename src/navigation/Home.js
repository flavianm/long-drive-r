import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from "../screens/HomeScreen";
import DestinationSearch from "../screens/DestinationSearch";
import SearchResult from "../screens/SearchResults";
//import { DatePickerPick } from "../screens/DatePick";
import OrderScreen from "../screens/OrderScreen";
//import { Chat } from "../screens/ChatScreen";
import OptionScreen from "../screens/Option";
import { Posttrip } from "../screens/Posttrip";
import PayScreen from "../screens/PayScreen";
import CompleteScreen from "../screens/CompleteScreen";
import PostSearch from "../screens/PostSearch";
//import EmptyPage from "../screens/EmptyScreen";
import SeatOrder from "../screens/SeatOrders";
import SecScreen from "../screens/SecScreen";
import FindSearch from "../screens/Findtrip";
/*import ParcelCustomer from "../screens/ParcelCustomer";
import ParcelOrders from "../screens/ParcelOrders";
import ParcelOrderInfoCust from "../screens/ParcelOrderInfoCust";
import ParcelOrdersDA from "../screens/ParcelOrdersDA";
import ParcelOrderInfoDA from "../screens/ParcelOrderInfoDA";
import PDAO from "../screens/ParcelDAO";
import ParcelDAOInfo from "../screens/ParcelDAOInfo"; */
import ProgressScreen from "../screens/ProgressScreen";
import UserProfile from "../screens/UserProfileScreen";
//import ParcelDriver from "../screens/ParcelDriver";
//import ParcelSearchCustResult from "../screens/ParcelSearchCustResult";
//import ParcelCustSearch from "../screens/ParcelCustSearch";
import SecScreenParc from "../screens/SecScreenParc";
//import ParcelDest from "../screens/ParcelDest";
import DriverMore from "../screens/DriverMore";
//import ParcelDestDriver from "../screens/ParcelDestDriver";
//import ParcelOrdersDriver from "../screens/ParcelOrdersDriver";
import SecScreenGoods from "../screens/SecScreenGoods";
/*import ParcelOrderInfoDSet from '../screens/ParcelOrderInfoDSet';
import ParcelOrderInfoDSetBBB from "../screens/ParcelOrderInfoDSetBBB";
import DriverMoreGoods from "../screens/DriverMoreGoods";
import GoodsDestDriver from "../screens/GoodsDestDriver";
import GoodsDriver from "../screens/GoodsDriver";
import GoodsOrdersDA from "../screens/GoodsOrdersDA";
import GoodsOrderInfoDA from "../screens/GoodsOrderInfoDA";
import GoodsOrdersDriver from '../screens/GoodsOrdersDriver';
import GoodsOrderInfoDSetBBB from '../screens/GoodsOrderInfoDSetBBB' ;
import GoodsOrderInfoDSet from "../screens/GoodsOrderInfoDSet";
import GoodsDAO from "../screens/GoodsDAO";
import GoodsDAOInfo from "../screens/GoodsDAOInfo";
import GoodsSearchCustResult from "../screens/GoodsSearchCustResult";
import GoodsCustSearch from "../screens/GoodsCustSearch";
import GoodsDest from "../screens/GoodsDest";
import GoodsCustomer from "../screens/GoodsCustomer";
import GoodsOrders from "../screens/GoodsOrders";
import GoodsOrderInfoCust from "../screens/GoodsOrderInfoCust" */;
import BankingDetails from "../screens/BankingDetails";
/*import ParcelOrderCustSearchInfo from '../screens/ParcelOrderCustSearchInfo';
import ParcelCustomerSearch from '../screens/ParcelCustomerSearch';
import GoodsOrderCustSearchInfo from "../screens/GoodsOrderCustSearchInfo";
import GoodsCustomerSearch from "../screens/GoodsCustomerSearch" */;
import DriverMoreRides from "../screens/DriverMoreRides";
//import RiderDestDriver from "../screens/RiderDestDriver";
//import RiderDriver from "../screens/RiderDriver";
import RiderSearchCustResult from "../screens/RiderSearchCustResult";
import RidesOrderCustSearchInfo from "../screens/RidesOrderCustSearchInfo";
import RidesCustomerSearch from "../screens/RidesCustomerSearch";
import RidesOrdersDriver from "../screens/RidesOrdersDriver";
import RidesOrderInfoDSetBBB from "../screens/RidesOrderInfoDSetBBB";
import RidesOrderInfoDSet from "../screens/RidesOrderInfoDSet";
import RidesDAO from "../screens/RidesDAO";
import RidesDAOInfo from "../screens/RidesDAOInfo";
import RidersOrders from "../screens/RidersOrders";
import RidesOrderInfoCust from "../screens/RidesOrderInfoCust";
import RefundForm from "../screens/RefundForm";
//import ParcelDSFR from "../screens/ParcelDSFR";
// import CarProfileScreen from "../screens/CarProfileScreen";

const Stack = createStackNavigator();

const HomeNavigator = (props) =>{

   return (
      <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
      initialRouteName={'Home'}
      >
        <Stack.Screen name="Home"  component={SecScreen}/>
         <Stack.Screen name="SecScreenGoods"  component={SecScreenGoods}/>
        <Stack.Screen name="DestinationSearch"  component={DestinationSearch}/>
        <Stack.Screen name="DriverMoreGoods"  component={DriverMoreGoods}/>
        <Stack.Screen name="DriverMoreRides"  component={DriverMoreRides}/>
        <Stack.Screen name="SearchResult"  component={SearchResult}/>
        <Stack.Screen name="BankingDetails"  component={BankingDetails}/>
        <Stack.Screen name="RefundForm"  component={RefundForm}/>
        <Stack.Screen name="DriverMore"  component={DriverMore}/>
         <Stack.Screen name="ParcelDestDriver"  component={ParcelDestDriver}/>
        <Stack.Screen name="ParcelDriver"  component={ParcelDriver}/>
        <Stack.Screen name="ParcelOrdersDriver"  component={ParcelOrdersDriver}/>
        <Stack.Screen name="SecScreenParc"  component={SecScreenParc}/>
        <Stack.Screen name="DatePick"  component={DatePickerPick}/>
        <Stack.Screen name="OrderPage"  component={OrderScreen}/>
        <Stack.Screen name="Chat"  component={Chat}/>
        <Stack.Screen name="ParcelDest"  component={ParcelDest}/>
        <Stack.Screen name="ParcelCustSearch"  component={ParcelCustSearch}/>
        <Stack.Screen name="ParcelOrderInfoDSet"  component={ParcelOrderInfoDSet}/>
        <Stack.Screen name="ParcelOrderInfoDSetBBB"  component={ParcelOrderInfoDSetBBB}/>
        <Stack.Screen name="ParcelSearchCustResult"  component={ParcelSearchCustResult}/>
        <Stack.Screen name="Trip"  component={Posttrip}/>
        <Stack.Screen name="ParcelOrderCustSearchInfo"  component={ParcelOrderCustSearchInfo}/>
        <Stack.Screen name="ParcelCustomerSearch"  component={ParcelCustomerSearch}/>
        <Stack.Screen name="Complete"  component={CompleteScreen}/>
        <Stack.Screen name="PostSearch"  component={PostSearch}/>
        <Stack.Screen name="Empty"  component={EmptyPage}/>
        <Stack.Screen name="SeatOrder"  component={SeatOrder}/>
        <Stack.Screen name="Letspick"  component={HomeScreen}/>
        <Stack.Screen name="FindTrip"  component={FindSearch}/>
        <Stack.Screen name="ParcelCustomer"  component={ParcelCustomer}/>
        <Stack.Screen name="ParcelOrders"  component={ParcelOrders}/>
        <Stack.Screen name="ParcelOrderInfoCust"  component={ParcelOrderInfoCust}/>
        <Stack.Screen name="ParcelOrdersDA"  component={ParcelOrdersDA}/>
        <Stack.Screen name="ParcelOrderInfoDA"  component={ParcelOrderInfoDA}/>
        <Stack.Screen name="ParcelDSFR"  component={ParcelDSFR}/>
        <Stack.Screen name="PDAO"  component={PDAO}/>
        <Stack.Screen name="ParcelDAOInfo"  component={ParcelDAOInfo}/>
        <Stack.Screen name="ProgressScreen"  component={ProgressScreen}/>
        <Stack.Screen name="UserProfile"  component={UserProfile}/>
        <Stack.Screen name="GoodsOrdersDA"  component={GoodsOrdersDA}/>
        <Stack.Screen name="GoodsDestDriver"  component={GoodsDestDriver}/>
        <Stack.Screen name="GoodsDriver"  component={GoodsDriver}/>
        <Stack.Screen name="GoodsOrderInfoDSet"  component={GoodsOrderInfoDSet}/>
        <Stack.Screen name="GoodsOrdersDriver"  component={GoodsOrdersDriver}/>
        <Stack.Screen name="GoodsOrderInfoDA"  component={GoodsOrderInfoDA}/>
        <Stack.Screen name="GoodsOrderInfoDSetBBB"  component={GoodsOrderInfoDSetBBB}/>
        <Stack.Screen name="GoodsDAO"  component={GoodsDAO}/>
        <Stack.Screen name="GoodsDAOInfo"  component={GoodsDAOInfo}/>
        <Stack.Screen name="GoodsCustSearch"  component={GoodsCustSearch}/>
        <Stack.Screen name="GoodsSearchCustResult"  component={GoodsSearchCustResult}/>
        <Stack.Screen name="GoodsDest"  component={GoodsDest}/>
        <Stack.Screen name="GoodsCustomer"  component={GoodsCustomer}/>
        <Stack.Screen name="GoodsOrders"  component={GoodsOrders}/>
        <Stack.Screen name="GoodsOrderInfoCust"  component={GoodsOrderInfoCust}/>
         <Stack.Screen name="GoodsCustomerSearch"  component={GoodsCustomerSearch}/>
        <Stack.Screen name="GoodsOrderCustSearchInfo"  component={GoodsOrderCustSearchInfo}/>
        {/* <Stack.Screen name="RiderDestDriver"  component={RiderDestDriver}/> */}
        {/* <Stack.Screen name="RiderDriver"  component={RiderDriver}/> */}
        <Stack.Screen name="RiderSearchCustResult"  component={RiderSearchCustResult}/>
        <Stack.Screen name="RidesOrderCustSearchInfo"  component={RidesOrderCustSearchInfo}/>
         <Stack.Screen name="RidesCustomerSearch"  component={RidesCustomerSearch}/>
        {/* <Stack.Screen name="RidesOrdersDriver"  component={RidesOrdersDriver}/>
        <Stack.Screen name="RidesOrderInfoDSetBBB"  component={RidesOrderInfoDSetBBB}/>
        <Stack.Screen name="RidesOrderInfoDSet"  component={RidesOrderInfoDSet}/>
        <Stack.Screen name="RidesDAOInfo"  component={RidesDAOInfo}/>
        <Stack.Screen name="RidesDAO"  component={RidesDAO}/>
        <Stack.Screen name="RidersOrders"  component={RidersOrders}/>
        <Stack.Screen name="RidesOrderInfoCust"  component={RidesOrderInfoCust}/> */}
        {/* <Stack.Screen name="CarProfileScreen"  component={CarProfileScreen}/> */}

      </Stack.Navigator>
  );
};

export default HomeNavigator;