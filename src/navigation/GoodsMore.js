import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from "../screens/HomeScreen";
import DestinationSearch from "../screens/DestinationSearch";
import SearchResult from "../screens/SearchResults";
import { DatePickerPick } from "../screens/DatePick";
import OrderScreen from "../screens/OrderScreen";
import { Chat } from "../screens/ChatScreen";
import OptionScreen from "../screens/Option";
import { Posttrip } from "../screens/Posttrip";
import PayScreen from "../screens/PayScreen";
import CompleteScreen from "../screens/CompleteScreen";
import PostSearch from "../screens/PostSearch";
import EmptyPage from "../screens/EmptyScreen";
import SeatOrder from "../screens/SeatOrders";
import SecScreen from "../screens/SecScreen";
import FindSearch from "../screens/Findtrip";
import ParcelCustomer from "../screens/ParcelCustomer";
import ParcelOrders from "../screens/ParcelOrders";
import ParcelOrderInfoCust from "../screens/ParcelOrderInfoCust";
import ParcelOrdersDA from "../screens/ParcelOrdersDA";
import ParcelOrderInfoDA from "../screens/ParcelOrderInfoDA";
import PDAO from "../screens/ParcelDAO";
import ParcelDAOInfo from "../screens/ParcelDAOInfo";
import ProgressScreen from "../screens/ProgressScreen";
import UserProfile from "../screens/UserProfileScreen";
import ParcelDriver from "../screens/ParcelDriver";
import HomeOptParcels from "../components/HomeOptParcels";
import ParcelHomeScreen from "../screens/ParcelHomeScreen";
import ParcCustmoreopt from "../screens/ParcCustmoreopt";
import ParcelDest from "../screens/ParcelDest";
import GoodsDest from '../screens/GoodsDest'
import ParcelCustSearch from "../screens/ParcelCustSearch";
import ParcelCustSearchPlus from '../screens/ParcelCustSearchPlus';
import ParcelOrderCustSearchInfo from '../screens/ParcelOrderCustSearchInfo';
import ParcelCustomerSearch from '../screens/ParcelCustomerSearch';
import ParcelOrderInfoDSet from '../screens/ParcelOrderInfoDSet';
import ParcelOrderInfoDSetBBB from "../screens/ParcelOrderInfoDSetBBB";
import GoodsCustmoreopt from "../screens/GoodsCustmoreopt";
import GoodsCustSearch from "../screens/GoodsCustSearch";
import GoodsSearchCustResult from "../screens/GoodsSearchCustResult";
import GoodsOrderCustSearchInfo from "../screens/GoodsOrderCustSearchInfo";
import GoodsOrders from "../screens/GoodsOrders";
import GoodsOrderInfoCust from "../screens/GoodsOrderInfoCust";
import GoodsOrdersDA from "../screens/GoodsOrdersDA";
const Stack = createStackNavigator();

const GoodsMoreNavigator = (props) =>{

   return (
      <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
      initialRouteName={'GoodsMoreNavigator'}
      >
        <Stack.Screen name="GoodsMore"  component={GoodsCustmoreopt}/>
        <Stack.Screen name="GoodsOrders"  component={GoodsOrders}/>
        <Stack.Screen name="GoodsOrderInfoCust"  component={GoodsOrderInfoCust}/>
        <Stack.Screen name="ParcelDest"  component={ParcelDest}/>
        <Stack.Screen name="GoodsOrdersDA"  component={GoodsOrdersDA}/>
        <Stack.Screen name="OrderPage"  component={OrderScreen}/>
        <Stack.Screen name="GoodsCustSearch"  component={GoodsCustSearch}/>
        <Stack.Screen name="GoodsOrderCustSearchInfo"  component={GoodsOrderCustSearchInfo}/>
        <Stack.Screen name="Pay"  component={PayScreen}/>
        <Stack.Screen name="ParcelCustSearchPlus"  component={ParcelCustSearchPlus}/>
        <Stack.Screen name="ParcelOrderCustSearchInfo"  component={ParcelOrderCustSearchInfo}/>
        <Stack.Screen name="GoodsSearchCustResult"  component={GoodsSearchCustResult}/>
        <Stack.Screen name="ParcelOrderInfoDSetBBB"  component={ParcelOrderInfoDSetBBB}/>
        <Stack.Screen name="ParcelCustomerSearch"  component={ParcelCustomerSearch}/>
        <Stack.Screen name="ParcelOrderInfoDSet"  component={ParcelOrderInfoDSet}/>
        <Stack.Screen name="ParcelCustomer"  component={ParcelCustomer}/>
        <Stack.Screen name="ParcelOrders"  component={ParcelOrders}/>
        <Stack.Screen name="ParcelOrderInfoCust"  component={ParcelOrderInfoCust}/>
        <Stack.Screen name="ParcelOrdersDA"  component={ParcelOrdersDA}/>
        <Stack.Screen name="ParcelOrderInfoDA"  component={ParcelOrderInfoDA}/>
        <Stack.Screen name="PDAO"  component={PDAO}/>
        <Stack.Screen name="ParcelDAOInfo"  component={ParcelDAOInfo}/>
        <Stack.Screen name="ProgressScreen"  component={ProgressScreen}/>
        <Stack.Screen name="UserProfile"  component={UserProfile}/>
        <Stack.Screen name="ParcelDriver"  component={ParcelDriver}/>
      </Stack.Navigator>
  );
};

export default GoodsMoreNavigator;