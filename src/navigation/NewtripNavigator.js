import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import RiderDestDriver from "../screens/RiderDestDriver";
import RiderDriver from "../screens/RiderDriver";

const Stack = createStackNavigator();


const NewtripNavigator = (props) =>{

    return (
        <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
      initialRouteName={'home'}
      >
    <Stack.Screen name="home"  component={RiderDestDriver}/>
    <Stack.Screen name="RiderDriver"  component={RiderDriver}/>

      </Stack.Navigator>
    )
}

export default NewtripNavigator;