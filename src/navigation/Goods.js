import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import PayScreen from "../screens/PayScreen";
import CompleteScreen from "../screens/CompleteScreen";
import PostSearch from "../screens/PostSearch";
/*import ParcelCustomer from "../screens/ParcelCustomer";
import ParcelOrders from "../screens/ParcelOrders";
import ParcelOrderInfoCust from "../screens/ParcelOrderInfoCust";
import ParcelOrdersDA from "../screens/ParcelOrdersDA";
import ParcelOrderInfoDA from "../screens/ParcelOrderInfoDA";
import PDAO from "../screens/ParcelDAO";
import ParcelDAOInfo from "../screens/ParcelDAOInfo" */;
import ProgressScreen from "../screens/ProgressScreen";
//import ParcelDriver from "../screens/ParcelDriver";
import GoodsMoreNavigator from "./GoodsMore";
//import GoodsHomeScreen from "../screens/GoodsHomeScreen";
//import DriverMore from "../screens/DriverMore";
//
// import ParcelDestDriver from "../screens/ParcelDestDriver";
import ParcelOrdersDriver from "../screens/ParcelOrdersDriver";
//import ParcelSearchCustResult from "../screens/ParcelSearchCustResult";
//import ParcelCustSearch from "../screens/ParcelCustSearch";
//import ParcelOrderInfoDSet from '../screens/ParcelOrderInfoDSet';
//import ParcelOrderInfoDSetBBB from "../screens/ParcelOrderInfoDSetBBB";
//import GoodsCustSearch from "../screens/GoodsCustSearch";
//import GoodsDest from "../screens/GoodsDest";
//import GoodsCustomer from '../screens/GoodsCustomer'
//import DriverMoreGoods from '../screens/DriverMoreGoods'
/*import GoodsDestDriver from "../screens/GoodsDestDriver";
import GoodsDriver from "../screens/GoodsDriver";
import GoodsOrdersDA from "../screens/GoodsOrdersDA";
import GoodsOrderInfoDA from "../screens/GoodsOrderInfoDA";
import GoodsOrdersDriver from '../screens/GoodsOrdersDriver';
import GoodsOrderInfoDSetBBB from '../screens/GoodsOrderInfoDSetBBB';
import GoodsOrderInfoDSet from "../screens/GoodsOrderInfoDSet";
import GoodsDAO from "../screens/GoodsDAO";
import GoodsDAOInfo from "../screens/GoodsDAOInfo"; */

const Stack = createStackNavigator();

const GoodsNavigator = (props) =>{

   return (
      <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
      initialRouteName={'Goods'}
      >
        <Stack.Screen name="Goods"  component={GoodsHomeScreen}/>
        <Stack.Screen name="GoodsMenu"  component={GoodsMoreNavigator}/>
        <Stack.Screen name="DriverMoreGoods"  component={DriverMoreGoods}/>
        <Stack.Screen name="GoodsOrdersDA"  component={GoodsOrdersDA}/>
        <Stack.Screen name="GoodsDestDriver"  component={GoodsDestDriver}/>
        <Stack.Screen name="GoodsDriver"  component={GoodsDriver}/>
        <Stack.Screen name="GoodsOrderInfoDSet"  component={GoodsOrderInfoDSet}/>
        <Stack.Screen name="GoodsOrdersDriver"  component={GoodsOrdersDriver}/>
        <Stack.Screen name="GoodsOrderInfoDA"  component={GoodsOrderInfoDA}/>
        <Stack.Screen name="GoodsOrderInfoDSetBBB"  component={GoodsOrderInfoDSetBBB}/>
        <Stack.Screen name="ParcelOrdersDriver"  component={ParcelOrdersDriver}/>
        <Stack.Screen name="Pay"  component={PayScreen}/>
        <Stack.Screen name="Complete"  component={CompleteScreen}/>
        <Stack.Screen name="GoodsDAO"  component={GoodsDAO}/>
        <Stack.Screen name="GoodsDAOInfo"  component={GoodsDAOInfo}/>
        <Stack.Screen name="GoodsCustSearch"  component={GoodsCustSearch}/>
        <Stack.Screen name="ParcelOrderInfoDSetBBB"  component={ParcelOrderInfoDSetBBB}/>
        <Stack.Screen name="ParcelCustomer"  component={ParcelCustomer}/>
        <Stack.Screen name="ParcelOrders"  component={ParcelOrders}/>
        <Stack.Screen name="ParcelOrderInfoCust"  component={ParcelOrderInfoCust}/>
        <Stack.Screen name="ParcelOrdersDA"  component={ParcelOrdersDA}/>
        <Stack.Screen name="ParcelOrderInfoDA"  component={PacelOrderInfoDA}/>
        <Stack.Screen name="PDAO"  component={PDAO}/>
        <Stack.Screen name="ParcelDAOInfo"  component={ParcelDAOInfo}/>
        <Stack.Screen name="ProgressScreen"  component={ProgressScreen}/>
        <Stack.Screen name="ParcelOrderInfoDSet"  component={ParcelOrderInfoDSet}/>
        <Stack.Screen name="GoodsDest"  component={GoodsDest}/>
        <Stack.Screen name="GoodsCustomer"  component={GoodsCustomer}/>
      </Stack.Navigator>
  );
};

export default GoodsNavigator;