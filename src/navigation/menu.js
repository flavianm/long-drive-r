import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import WalletHome from "../screens/Wallet";
import BankingDetails from "../screens/BankingDetails";
import PayScreen from "../screens/PayScreen";
import cashout from "../screens/cashout"
import TabTwoScreen from "../screens/Profile";
import UserProfile from "../screens/UserProfileScreen";
import CarProfileScreen from "../screens/CarProfileScreen";



const Stack = createStackNavigator();

const MenuNavigator = (props) =>{
     return (
         <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
      initialRouteName={'Menu'}
      >
        <Stack.Screen name="home"  component={TabTwoScreen}/>
        <Stack.Screen name="BankingDetails"  component={BankingDetails}/>
        <Stack.Screen name="Pay"  component={PayScreen}/>
        <Stack.Screen name="cashout"  component={cashout}/>
        <Stack.Screen name="WalletHome"  component={WalletHome}/>
        <Stack.Screen name="UserProfile"  component={UserProfile}/>
        <Stack.Screen name="CarProfileScreen"  component={CarProfileScreen}/>




      </Stack.Navigator>
     )
}

export default MenuNavigator;