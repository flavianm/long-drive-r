import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import WalletHome from "../screens/Wallet";
import MyTrips from "../screens/mytrips";
import RiderDestDriver from "../screens/RiderDestDriver";
import RiderDriver from "../screens/RiderDriver";
import RiderSearchCustResult from "../screens/RiderSearchCustResult";
import RidesOrderCustSearchInfo from "../screens/RidesOrderCustSearchInfo";
import RidesCustomerSearch from "../screens/RidesCustomerSearch";
import RidesOrdersDriver from "../screens/RidesOrdersDriver";
import RidesOrderInfoDSetBBB from "../screens/RidesOrderInfoDSetBBB";
import RidesOrderInfoDSet from "../screens/RidesOrderInfoDSet";
import RidesDAOInfo from "../screens/RidesDAOInfo";
import RidesDAO from "../screens/RidesDAO";
import RidersOrders from "../screens/RidersOrders";
import RidesOrderInfoCust from "../screens/RidesOrderInfoCust";

const Stack = createStackNavigator();

const TripsNavigator = (props) =>{
     return (
         <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
      initialRouteName={'Trips'}
      >
          <Stack.Screen name="home"  component={MyTrips}/>
          <Stack.Screen name="RiderDestDriver"  component={RiderDestDriver}/>
        <Stack.Screen name="RiderDriver"  component={RiderDriver}/>
        <Stack.Screen name="RiderSearchCustResult"  component={RiderSearchCustResult}/>
        <Stack.Screen name="RidesOrderCustSearchInfo"  component={RidesOrderCustSearchInfo}/>
        <Stack.Screen name="RidesCustomerSearch"  component={RidesCustomerSearch}/>
        <Stack.Screen name="RidesOrdersDriver"  component={RidesOrdersDriver}/>
        <Stack.Screen name="RidesOrderInfoDSetBBB"  component={RidesOrderInfoDSetBBB}/>
        <Stack.Screen name="RidesOrderInfoDSet"  component={RidesOrderInfoDSet}/>
        <Stack.Screen name="RidesDAOInfo"  component={RidesDAOInfo}/>
        <Stack.Screen name="RidesDAO"  component={RidesDAO}/>
        <Stack.Screen name="RidersOrders"  component={RidersOrders}/>
        <Stack.Screen name="RidesOrderInfoCust"  component={RidesOrderInfoCust}/>





      </Stack.Navigator>
     )
}

export default TripsNavigator;