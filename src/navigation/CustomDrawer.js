import React from "react";
import { Text, View, Pressable } from 'react-native';
import { DrawerContentScrollView, DrawerItemList } from "@react-navigation/drawer";
import {Auth} from 'aws-amplify';

const CustomDrawer = (props) =>{

   return (
    <DrawerContentScrollView {...props}>
        <View style={{backgroundColor: '#212121', padding: 15}}>

        {/*User Row*/}

        <View style={{
            flexDirection: 'row',
            alignItems: 'center'
             
        }}>
            <View style={{backgroundColor: '#cacaca', width: 50, height: 50, borderRadius: 35, marginRight: 10}}/>
                <View>
                    <Text style={{color: 'white', fontSize: 24 }}>Siso Hlatshwayo</Text>
                    <Text style={{color: 'lightgrey'}}> 5.00 *</Text>
                </View>
            
        </View>

        {/*Messages Row*/}
        <View style={{borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#919191', paddingVertical: 5, marginVertical: 10}}>
             <Pressable
                onPress={() => {console.warn('Messages')}}>
                <Text style={{color: 'white', paddingVertical: 5}}>Messages</Text>
            </Pressable>
        </View>
       
        {/*Do more*/}
         <Pressable
          onPress={() => {console.warn('More account')}}>
            <Text style={{color: '#dddddd', paddingVertical: 5}}>Do more with your account</Text>
        </Pressable>
         {/*Make Money*/}
        <Pressable onPress={() => {console.warn('Money driving')}}>
            <Text style={{color: 'white', paddingVertical: 5}}>Make Money Driving</Text>
        </Pressable>
        </View>
        <DrawerItemList {...props} />

        <Pressable onPress={() => {Auth.signOut}}>
            <Text style={{padding: 5, paddingLeft: 20, paddingTop: 20 }}>SignOut</Text>
        </Pressable>

    </DrawerContentScrollView>
  );
};

export default CustomDrawer;