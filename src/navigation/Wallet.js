import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import WalletHome from "../screens/Wallet";
import BankingDetails from "../screens/BankingDetails";
import PayScreen from "../screens/PayScreen";
import cashout from "../screens/cashout";

const Stack = createStackNavigator();

const WalletNavigator = (props) =>{
     return (
         <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
      initialRouteName={'Wallet'}
      >
          <Stack.Screen name="home"  component={WalletHome}/>
         <Stack.Screen name="BankingDetails"  component={BankingDetails}/>
          <Stack.Screen name="Pay"  component={PayScreen}/>
          <Stack.Screen name="cashout"  component={cashout}/>





      </Stack.Navigator>
     )
}

export default WalletNavigator;