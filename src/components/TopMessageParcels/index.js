import React, {useState, useEffect} from "react";
import {View, Text} from 'react-native';
import HomeMap from "../HomeMap";
import styles from "./styles";
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

import Toast from 'react-native-toast-message';
import AnimatedLoader from "react-native-animated-loader";

const TopMessageParcels = () =>{

 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

 const [visible, setVisible] = useState(true);


     useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 5000);
  }, [])


   if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{


    return (
        <View style={styles.container}>
            {/* <Text style={{fontFamily: 'Manrope_700Bold', paddingLeft: 15, fontSize: 25}}>Send your package in peace...</Text> */}
          
        </View>
    )
    }
};

export default TopMessageParcels;