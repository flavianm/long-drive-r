import { Feather, AntDesign, MaterialIcons, Entypo } from "@expo/vector-icons";
import React from "react";
import {View, Text, ScrollView, Pressable, Image} from 'react-native';
import HomeMap from "../HomeMap";
// import styles from "../CovidMessage/styles";
import styles from "./styles";
import { useNavigation } from "@react-navigation/core";

const GoodsOptParcels = () =>{

    const navigation = useNavigation();

    const dateSearch = () =>{
       navigation.navigate('GoodsMenu');
    }

    const posttrip = () =>{
       navigation.navigate('DriverMoreGoods');
    }

    return (
            <View style={{backgroundColor:'white', marginBottom: 100}}>

            <Pressable style={styles.row} 
             onPress={dateSearch}
            >
                <View style={{width: 100, height: 100}}>
                   <Image
                   source={require('../../../src/assets/book.png')}
                   style={{width: 100, height: 100}}
                    />

                </View>
                   <Text style={styles.destinationText}> Customer </Text>
                <View>
                   <Text> Need to send and track large Goods ?</Text>
                </View>
            </Pressable>

       <Pressable style={styles.row} 
             onPress={posttrip}
            >
                <View style={{width: 100, height: 100}}>
                   <Image
                   source={require('../../../src/assets/goods.png')}
                   style={{width: 100, height: 100}}
                    />
                   
                </View>
                
                   <Text style={styles.destinationText}> Driver </Text>
                <View>
                   <Text>Looking to carry large Goods ?</Text>
                </View>
            </Pressable>



        </View>
    
        
    )
};

export default GoodsOptParcels;