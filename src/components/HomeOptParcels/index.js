import { Feather, AntDesign, MaterialIcons, Entypo } from "@expo/vector-icons";
import React from "react";
import {View, Text, ScrollView, Pressable, Image} from 'react-native';
import HomeMap from "../HomeMap";
// import styles from "../CovidMessage/styles";
import styles from "./styles";
import { useNavigation } from "@react-navigation/core";

const HomeOptParcels = () =>{

    const navigation = useNavigation();


    const dateSearch = () =>{
       navigation.navigate('ParcelsMenu');
    }

    const posttrip = () =>{
       navigation.navigate('DriverMore');
    }

    return (
            <View style={{backgroundColor:'white', marginBottom: 100}}>
            
           
            <Pressable style={styles.row} 
             onPress={dateSearch}
            >
                <View style={{width: 100, height: 100}}>
                   <Image
                   source={require('../../../src/assets/book.png')}
                   style={{width: 100, height: 100}}
                    />
                   
                </View>
                   <Text style={styles.destinationText}> Customer </Text>
                <View>
                   <Text> Need to send and track a parcel ?</Text>
                </View>
            </Pressable>




            

       <Pressable style={styles.row} 
             onPress={posttrip}
            >
                <View style={{width: 100, height: 100}}>
                   <Image
                   source={require('../../../src/assets/top.png')}
                   style={{width: 100, height: 100}}
                    />
                   
                </View>
                
                   <Text style={styles.destinationText}> Driver </Text>
                <View>
                   <Text> Going somewhere and looking to carry a parcel ?</Text>
                </View>
            </Pressable>



        </View>
    
        
    )
};

export default HomeOptParcels;