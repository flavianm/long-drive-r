import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#fff',
        alignContent: 'center',
        alignItems: 'center',
        height: 45,
        marginTop: 10

    },
    title:{
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    text:{
        color: '#bed9ff',
        fontSize: 15,
        marginBottom: 10,
    },
    learnmore:{
        color: '#fff',
        fontSize: 15,
        fontWeight: 'bold',
    }
});

export default styles;