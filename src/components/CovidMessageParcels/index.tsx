import { Feather, FontAwesome5 } from "@expo/vector-icons";
import React from "react";
import {View, Text, Pressable} from 'react-native';
import HomeMap from "../../components/HomeMap";
import styles from "./styles";
import { useNavigation, useRoute } from "@react-navigation/core";
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

import { TouchableOpacity } from "react-native-gesture-handler";


const CovidMessageParcels = () =>{
    const navigation = useNavigation();

let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });


    if (!fontsLoaded) {
            return(
                <View>
                    <Text>Loading...</Text>
                </View>
            )
        } else{
 return (
        <View style={[styles.container, {flexDirection: 'row'}]}>
             {/* cars */}
           <TouchableOpacity
           onPress={()=> navigation.navigate('Home')}
           style={{marginLeft: 5, width: 170, height: 40, borderRadius: 5, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', alignContent: 'center', alignItems: 'center'}}>
                    <View>
                        <FontAwesome5 name='car' size={15} color={'grey'}/>
                    </View>
                    <View>
                        <Text style={{color: 'grey', fontWeight: '400', fontSize: 14, marginLeft: 10, fontFamily: 'Manrope_500Medium'}}>Car Ride</Text>
                    </View>

                </View>
           </TouchableOpacity>
            <Pressable
            
            style={{width: 185, height: 40, backgroundColor: '#41afa9', borderRadius: 5, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', alignContent: 'center', alignItems: 'center'}}>
                    <View>
                        <FontAwesome5 name='box-open' size={15} color={'white'}/>
                    </View>
                    <View>
                        <Text style={{color: 'white', fontWeight: '400', fontSize: 15, marginLeft: 10, fontFamily: 'Manrope_600SemiBold'}}>Parcels</Text>
                    </View>

                </View>
           </Pressable >
           


        </View>
    )
        }


   
};

export default CovidMessageParcels;