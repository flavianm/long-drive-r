import React from "react";
import {Image, FlatList} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps'; 
import {API, graphqlOperation} from 'aws-amplify';
import { listCars } from "../../graphql/queries";

// import cars from "../../assets/data/cars";



 const getImage = (type) => {
        if (type === 'UberX'){
            return require('../../assets/images/top-UberX.png')
        }
        if (type === 'Comfort'){
            return require('../../assets/images/top-Comfort.png')
        }
        return require('../../assets/images/top-UberXL.png')
    }

const OrderMap = ({car}) =>{
    
    return (
            <MapView
                style={{height: '100%', width: '100%'}} 
                provider={PROVIDER_GOOGLE}
                showsUserLocation={true}
                initialRegion={{
                    latitude: -26.192910,
                    longitude: 28.035990,
                    latitudeDelta: 0.0222,
                    longitudeDelta: 0.0121,
                }}>

              {car && (<Marker
                key={car.id} 
                coordinate={{latitude : car.latitude, longitude : car.longitude}}>
                
                    <Image
                    style={{width: 60, height: 60, resizeMode: 'contain', 
                    transform:[{
                        rotate: `${car.heading}deg`
                    }]
                        }} 
                    source={getImage(car.type)}
                />

               </Marker>)}
              
              
            </MapView>
        
    );
};

export default OrderMap;