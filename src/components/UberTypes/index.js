import React from "react";
import {View, Text, Button, Pressable} from 'react-native';
import styles from "./styles";
import UberTypeRow from "../UberTypeRow";
// import typesData from "../../assets/data/types";
import types from "../../data/types";

const UberTypes = ({typeState, onSubmit}) =>{

    const [selectedType, setSelectedType] = typeState;

    const confirm = () =>{
        console.warn('confirmed')
    }

    return (
        <View style={{marginTop: 10}} >
             <Pressable onPress={onSubmit} 
            style={{ 
            backgroundColor: '#E15F25', 
            padding: 10, margin: 10, borderRadius: 5, marginBottom: 0,
            alignItems:'center'}}>
                <Text
                style={{color:'white', fontWeight: 'bold'}}
                >Book Ride</Text>
            </Pressable>

            {types.map(type => <UberTypeRow
            type={type}
            key={type.id} // change the type.id to uniqueId or number plate as it will be unique. can also use a flatlist here
            isSelected={type.type === selectedType}
            onPress={() => setSelectedType(type.type)}
            />)}

           

        </View>
    )
};

export default UberTypes;