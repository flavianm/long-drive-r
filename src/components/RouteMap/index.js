import React from "react";
import {Image, FlatList} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps'; 
import MapViewDirections from "react-native-maps-directions";

const GOOGLE_MAPS_APIKEY = 'AIzaSyAj8LKvEnwzBhZRycGM1yJJ96j7AA2snGE';


const RouteMap = ({origin, destination}) =>{

    const originLoc = {
        latitude:  origin.details.geometry.location.lat,
        longitude: origin.details.geometry.location.lng
    };

    const destinationLoc = {
      latitude:  destination.details.geometry.location.lat,
      longitude: destination.details.geometry.location.lng
    }

    return (
            <MapView
                style={{height: '100%', width: '100%'}} 
                provider={PROVIDER_GOOGLE}
                // initialRegion={{
                //     latitude: 28.450627,
                //     longitude: -16.263045,
                //     latitudeDelta: 0.0222,
                //     longitudeDelta: 0.0121,
                // }}
                initialRegion={{
                    latitude: originLoc.latitude,
                    longitude: originLoc.longitude,
                    latitudeDelta: 0.0222,
                    longitudeDelta: 0.0121,
                }}
            >
                <MapViewDirections
                    origin={originLoc}
                    destination={destinationLoc}
                    apikey={GOOGLE_MAPS_APIKEY}
                    strokeWidth={4}
                    strokeColor="black"
                />
                <Marker 
                    coordinate={originLoc}
                    title={'Pick-Up'}
                    description={'You will be picked up here'}
                />
                <Marker 
                    coordinate={destinationLoc}
                    title={'Destination'}
                    description={'You will be dropped off'}
                />
              
            </MapView>
        
    )
};

export default RouteMap;