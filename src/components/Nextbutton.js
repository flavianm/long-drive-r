import React, {useEffect, useRef} from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Animated } from 'react-native';
import Svg, {G, Circle} from 'react-native-svg';
import { AntDesign } from '@expo/vector-icons';
import { Value } from 'react-native-reanimated';

export default Nextbutton = ({percentage, scrollTo}) => {

    const size = 80;
    const strokeWidth = 2;
    const center = size / 2
    const radius = size / 2 - strokeWidth / 2;
    const circumference = 2 * Math.PI * radius;

    const ProgressAnimation = useRef(new Animated.Value(0)).current;
    const progressRef = useRef(null);

    const animation = (toValue) => {
        return Animated.timing(ProgressAnimation, {
            toValue,
            duration: 250,
            useNativeDriver: true
        }).start()
    }

    useEffect(() => {
        animation(percentage)
       
    }, [percentage]);

    useEffect(() => {

        ProgressAnimation.addListener((value) =>{
            const strokeDashoffset = circumference - (circumference * value.value) / 100;

            if(progressRef?.current){

                progressRef.current.setNativeProps({
                    strokeDashoffset
                })

            }

        },[percentage]);

         return () =>{
            ProgressAnimation.removeAllListeners()
        };


    }, []);


   


    return (
        <View style={styles.container}>

           

                <Svg width={size} height={size} >
                     <G rotation={"-90"} origin={center} >
                        <Circle  stroke={"#E6E7E8"} cx={center} cy={center} r={radius} strokeWidth={strokeWidth}
                        />

                        <Circle 
                            ref={progressRef}
                            stroke={'orange'}
                            cx={center}
                            cy={center}
                            r={radius}
                            strokeWidth={strokeWidth}
                            strokeDasharray={circumference}
                            // strokeDashoffset={circumference - (circumference * 60) / 100}
                        />
                </G>
                </Svg>
                <TouchableOpacity onPress={scrollTo} style={styles.button} activeOpacity={0.6} >
                    <AntDesign name='arrowright' size={32} color={"#FFF"} />
                </TouchableOpacity>
            
        </View>
    )
}



const styles = StyleSheet.create({
container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

button: {
    position: 'absolute',
    backgroundColor: 'orange',
    borderRadius: 100,
    padding: 20,
}
})
