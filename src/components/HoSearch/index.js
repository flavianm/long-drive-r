import { Feather, Octicons, Ionicons, AntDesign, MaterialIcons, Entypo, FontAwesome5, MaterialCommunityIcons, FontAwesome } from "@expo/vector-icons";
import React, {useState, useEffect, useRef, useMemo, useCallback} from "react";
import {View, Text, ScrollView, Pressable, Dimensions, Image, TextInput, Button, TouchableOpacity, KeyboardAvoidingView} from 'react-native';
// import styles from "../CovidMessage/styles";
import styles from "./styles";
import PlaceRow from "./PlaceRow";
import { useNavigation } from "@react-navigation/core";
import Modal from "react-native-modal";
import FindSearch from "../../screens/Findtrip";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import LottieView from 'lottie-react-native';
import Toast from 'react-native-toast-message';
import {LocaleConfig} from 'react-native-calendars';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import BottomSheet from '@gorhom/bottom-sheet';

import {Auth} from 'aws-amplify';
// import * as mutations from './graphql/mutations';
import * as mutations from '../../graphql/mutations'
import * as queries from '../../graphql/queries';
import {API, graphqlOperation} from 'aws-amplify';
import { getParcelOrder, getParcelRequests, getUser } from '../../graphql/queries';
import { S3Image } from 'aws-amplify-react-native';
import { MotiView, MotiText  } from 'moti'

import HomeMap from "../HomeMap";
import CovidMessage from "../CovidMessage";

const GOOGLE_PLACES_API_KEY = 'AIzaSyAj8LKvEnwzBhZRycGM1yJJ96j7AA2snGE';

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';
import { color } from "react-native-reanimated";

const HoSearch = () =>{

  const bottomSheetRef = useRef(BottomSheet);

   const snapPoints = useMemo(() => ['60%', '30%'], []);
  const handleSheetChanges = useCallback((index = number) => {
    console.log('handleSheetChanges', index);
  }, []);

   let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });


    const navigation = useNavigation();


    // after editing set to true
    const [fullLoad, setFullLoad] = useState(true);


     const [originPlace, setOriginPlace] = useState(null);
    const [destinationPlace, setDestinationPlace] = useState(null);

    const [originDesc, setOriginDesc] = useState(null);
    const [destDesc, setDestDesc] = useState(null);

    const [originSec, setOriginSec] = useState(null);
    const [destSec, setDestSec] = useState(null);

   const [isModalVisible, setModalVisible] = useState(false);

    const [userDet, setUserDet] = useState();

    const [pipo, setPipo] = useState(false);
    const [desti, setDesti] = useState(false);

    const [calShow, setCalShow] = useState(false);
    const [seatShow, setSeatShow] = useState(false);
    const [date, setDate] = useState('Date');
    const [seats, setSeats] = useState('1');
    const [noSeats, setNoSeats] = useState('3');
    const [sts, setSts] = useState('0');
    


  const toggleModal = () => {
    setModalVisible(!isModalVisible);
    
  };

    const FindTRip = () =>{
       navigation.navigate('FindTrip');
    }

    const posttrip = () =>{
       navigation.navigate('Trip');
    }

  

    const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            // setuser(userSub);
            // console.log(userSub)

            const todoData = await API.graphql(graphqlOperation(getUser, { id: userSub}))
            const todos = todoData.data.getUser
            setUserDet(todos)
            
    }

    useEffect(() => {
     
     setInterval(() => {
      setFullLoad(!fullLoad)
    }, 8000);
  }, [])



    useEffect(() => {
     fetchUsers()
    }, [])

    if(fullLoad === true){

  return(
    <View style={{alignItems: 'center', alignContent: 'center', justifyContent:'center', marginTop: 200}}>
        
         {/* <LottieView style={{width: 100, height: 100, }} source={require('./src/assets/9809-loading.json')} autoPlay loop /> */}
         <LottieView style={{width: 100, height: 100, }} source={require('../../../src/assets/9809-loading.json')} autoPlay loop />

    </View>

  )
}

    if (pipo === true){

        return(
            <View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 15}}>

                    <TouchableOpacity onPress={()=> setPipo(false)}>
                        <MaterialIcons name="arrow-back-ios" size={20} color="black" />
                    </TouchableOpacity>

                    <View>
                         <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 15}}>Pick Up</Text>
                    </View>
                    
                </View>

                 <View style={styles.container}>
                <View>
                       
                        
                    </View>

                   <GooglePlacesAutocomplete 

                        placeholder="Pick Up From ?"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za',
                        }}
                        enablePoweredByContainer={false}
                        suppressDefaultStyles
                        currentLocation={true}
                        currentLocationLabel='Current Location'
                        enableHighAccuracyLocation={true}
                        styles={{
                            textInput: styles.textInput,
                            container: styles.autoCompleteContainer,
                            listView: styles.listView,
                            separator: styles.separator,
                            placeholder: [{color: 'black'}]

                        }}
                        onPress={(data, details = null) =>{
                            setOriginPlace({data, details});
                            setOriginDesc(data.description);
                            setOriginSec(data.structured_formatting.secondary_text);
                            setPipo(false);
                        }}
                        textInputProps={{
                            leftIcon: {}
                        }}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                        renderRow={(data) => <PlaceRow data={data} />}
                        renderDescription={(data) => data.description || data.vicinity}
                        
                        // predefinedPlaces={[homePlace, workPlace]}
                    />

          
                    <View>

                     
                    </View>

                    
                       
            </View>







            </View>

        )
    
    }

    if (desti === true){

        return(
            <View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 15}}>

                    <TouchableOpacity onPress={()=> setDesti(false)}>
                       <MaterialIcons name="arrow-back-ios" size={20} color="black" />
                    </TouchableOpacity>

                    <View>
                         <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 15}}>DropOff</Text>
                    </View>

                    
                </View>

                 <View style={styles.container}>
                <View>
                       
                        
                    </View>

                <GooglePlacesAutocomplete 

                        placeholder="Your Destination ?"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za'
                        }}
                        suppressDefaultStyles
                        styles={{
                            textInput: styles.textInput,
                            container:{
                                ...styles.autoCompleteContainer,
                                top: 0,
                                
                            },
                            
                            separator: styles.separator
                        }}
                        onPress={(data, details = null) =>{
                            console.log(originDesc)
                            setDestinationPlace({data, details});
                            setDestDesc(data.description);
                            setDestSec(data.structured_formatting.secondary_text);
                            setDesti(false);
                        }}
                        enablePoweredByContainer={false}
                        renderRow={(data) => <PlaceRow
                            data={data} />}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                    />

          
                    <View>

                     
                    </View>

                    
                       
            </View>







            </View>

        )
    
    }


    const SeatssOp = () => {


if (noSeats === '1'){

    if(sts === '0'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>
     
        </View>
      )
    }


    if(sts === '1'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>
        </View>
      )
    }


}

if (noSeats === '2'){

    if(sts === '0'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

     
        </View>
      )
    }




    if(sts === '1'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
          </TouchableOpacity>

        </View>
      )
    }
    
    if(sts === '2'){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange"/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>
        </View>
      )
    }
}

if (noSeats === '3'){

   if(sts === '0'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        {/* thr */}
          <TouchableOpacity onPress={()=> setSts('3')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>


        </View>
      )
    }
   
   if(sts === '1'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey"  style={{marginRight: 30}}/>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=> setSts('3')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        </View>
      )
    }
    
    if(sts === '2'){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>

          <TouchableOpacity onPress={()=> setSts('3')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>
        </View>
      )
    }
   
    if(sts === '3'){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        {/* thr */}
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>


        </View>
      )
    }

}


  }


    if (calShow === true){

        return(
            <View style={{margin: 10}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                     <TouchableOpacity onPress={()=> setCalShow(false)}>
                        <MaterialIcons name="arrow-back-ios" size={20} color="black" />
                     </TouchableOpacity>

                     
                    
                    
                     <View>
                         <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 15}}>Pick your travel date</Text>
                    </View>
                </View>


               

                <View style={{alignContent: 'center', alignItems: 'center', justifyContent: 'center', marginTop: 30}}>
                   <Text>{date || `Date`}</Text>
                </View>


                <View style={{marginLeft: 30, marginRight: 30, marginTop: 10}}>
                 <Calendar
  // Initially visible month. Default = Date()
  current={Date()}
  // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
  minDate={Date()}
  // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
  maxDate={'2022-05-30'}
  // Handler which gets executed on day press. Default = undefined
//   onDayPress={(day) => {console.log('selected day', day)}}
//   onDayPress={(day) => setDate(day.dateString)}
  onDayPress={(day) =>  {setDate(day.dateString)}}
  // Handler which gets executed on day long press. Default = undefined
  onDayLongPress={(day) => {console.log('selected day', day)}}
  // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
  monthFormat={'yyyy MM'}
  // Handler which gets executed when visible month changes in calendar. Default = undefined
  onMonthChange={(month) => {console.log('month changed', month)}}
  // Hide month navigation arrows. Default = false
  hideArrows={false}
  // Replace default arrows with custom ones (direction can be 'left' or 'right')
//   renderArrow={(direction) => (<Arrow/>)}
  // Do not show days of other months in month page. Default = false
  hideExtraDays={true}
  // If hideArrows = false and hideExtraDays = false do not switch month when tapping on greyed out
  // day from another month that is visible in calendar page. Default = false
  disableMonthChange={true}
  // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday
  firstDay={1}
  // Hide day names. Default = false
  hideDayNames={true}
  // Show week numbers to the left. Default = false
  showWeekNumbers={true}
  // Handler which gets executed when press arrow icon left. It receive a callback can go back month
  onPressArrowLeft={subtractMonth => subtractMonth()}
  // Handler which gets executed when press arrow icon right. It receive a callback can go next month
  onPressArrowRight={addMonth => addMonth()}
  // Disable left arrow. Default = false
  disableArrowLeft={false}
  // Disable right arrow. Default = false
  disableArrowRight={false}
  // Disable all touch events for disabled days. can be override with disableTouchEvent in markedDates
  disableAllTouchEventsForDisabledDays={true}
  // Replace default month and year title with custom one. the function receive a date as parameter
  renderHeader={(date) => {(date.dateString)}}
  // Enable the option to swipe between months. Default = false
  enableSwipeMonths={true}
//   markedDates={
   
//     {
//             better: {selected: true, marked: true, selectedColor: 'blue'},
//   }}
/>
            </View>


            </View>

        )
    }

    if (seatShow === true){
        return(
            <View style={{margin: 10}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                     <TouchableOpacity onPress={()=> setSeatShow(false)}>
                        <MaterialIcons name="arrow-back-ios" size={20} color="black" />
                     </TouchableOpacity>

                   
                    
                     <View>
                         <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 15}}>Seats you need</Text>
                        
                    </View>
                </View>

                  
               

            


                <View style={{marginTop: 60, marginLeft: 30, marginRight: 30}}>
                <View>
                    {SeatssOp()}
                </View>
            </View>


            </View>

        )
    }


     const Press =()=>{
      
        if(originPlace && destinationPlace){
            navigation.navigate('RiderSearchCustResult', {
                originPlace,
                destinationPlace,
                destDesc,
                originDesc,
                originSec,
                destSec,
                datee: date,
                stsa: sts
            });
        } else{
           Toast.show({
      type: 'error',
      text1: 'Locations',
      text2: 'Enter Both Locations',
      visibilityTime: 4000,

    });
        }
    }

     if (!fontsLoaded) {
            return(
                <Text>Loading...</Text>
            )
        } else{

    return (
            <View style={{backgroundColor:'white'}}>
        
      
            <View style={{height: Dimensions.get('window').height - 0}}>
                <HomeMap/>
           </View>


             {/* <View >
                <HomeMap/>
           </View>             */}

    {/* <CovidMessage/> */}

            <BottomSheet
              ref={bottomSheetRef}
              index={1}
              snapPoints={snapPoints}
              onChange={handleSheetChanges}
              enablePanDownToClose={false}
            >

   
                <MotiView 
                from={{
                opacity: 0,
                scale: 0.5,
              }}
              animate={{
                opacity: 1,
                scale: 1,
                
              }}
              transition={{
                type: 'timing',
                duration: 350,
              }}
                style={{margin: 20}} 
                >
                    {/* <View style={{flexDirection: 'row', marginBottom: 30, justifyContent: 'space-between', alignContent: 'center', alignItems: 'center'}}>
                        
                        <View style={{width: 150}}>
                        <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20}}>Hey {userDet?.name || `Anonymous`}...</Text>

                        </View>
                        <View style={{alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                        <Image
                            source={{uri: userDet?.imageUrl}}
                            style={{width: 50, height: 50, borderRadius: 50, borderWidth: 1, borderColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}
                        />
                        </View>

                        
                        


                    </View> */}

                              

                    <View >

                      <TouchableOpacity onPress={()=> setPipo(true)} style={{flexDirection: 'row', backgroundColor: '#f5f5f5', marginRight: 0, borderBottomWidth: 0, borderColor: 'grey', borderRadius: 20, alignContent: 'center', alignItems: 'center'}}>
                            <View style={{marginLeft: 12}}>
                                <FontAwesome name="search" size={23} color="orange" />
                            </View>

                    
                            <View style={{width: 270, marginTop: 10, marginBottom: 10}}>
                                <Text style={{fontFamily: 'Manrope_600SemiBold', marginLeft: 15, fontSize: 20}}>Pickup Point</Text>
                                <Text style={{fontFamily: 'Manrope_400Regular', marginLeft: 15, fontSize: 10}}>{originDesc || `Click To Set Location...`}</Text>
                            </View>   

                    
                            <View style={{marginLeft: 10, marginTop: 15,  width: 15, height: 15,  borderColor: 'grey',  borderRadius: 50, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                                {/* <Text style={{fontSize: 8, color: '#5e675d', fontWeight: '500'}}>X</Text> */}
                            </View>    



                    </TouchableOpacity> 
                    
                    

                    <TouchableOpacity onPress={()=> setDesti(true)} style={{flexDirection: 'row', marginTop: 25, marginRight: 0, backgroundColor: '#f5f5f5', borderRadius: 20, alignContent: 'center', alignItems: 'center'}}>
                            <View style={{}}>
                                <LottieView style={{width: 40, height: 40, marginLeft: 1}} source={require('../../assets/31078-pin-animation.json')} autoPlay loop />

                            </View>

                    
                            <View style={{width: 270, marginTop: 10, marginBottom: 10}}>
                                <Text style={{fontFamily: 'Manrope_600SemiBold', marginLeft: 15, fontSize: 12}}>DropOff Point</Text>
                                <Text style={{fontFamily: 'Manrope_400Regular', marginLeft: 15, fontSize: 10}}>{destDesc || `Click To Set Location...`}</Text>
                            </View>   

                    
                            <View style={{marginLeft: 10, marginTop: 15,  width: 15, height: 15, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                                {/* <Text style={{fontSize: 8, color: '#5e675d', fontWeight: '500'}}>X</Text> */}
                            </View>    



                    </TouchableOpacity>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>

                    <TouchableOpacity onPress={()=> setCalShow(true)} style={{flexDirection: 'row', marginTop: 20, marginLeft: 10}}>
                        <Octicons name="calendar" size={20} color="black" />
                        <Text style={{marginLeft: 20, fontFamily: 'Manrope_500Medium', fontSize: 13}}>{date || `Select to pick date`}</Text>
                    </TouchableOpacity>

                    

                    <TouchableOpacity onPress={()=> setSeatShow(true)} style={{flexDirection: 'row', marginTop: 20, marginLeft: 10}}>
                        <MaterialIcons name="event-seat" size={20} color="black" />
                        <Text style={{marginLeft: 20, fontFamily: 'Manrope_500Medium', fontSize: 13}}>{sts || `Select Seats`} Seats</Text>
                    </TouchableOpacity>
                    
                    </View>
                    </View>      

                </MotiView>



                <View style={{alignContent: 'center', alignItems: 'center'}}>


                <TouchableOpacity
          onPress={()=> Press()}
          style={{backgroundColor: 'orange', justifyContent: 'space-around', 
          alignItems:'center', width: 315, height: 45, marginTop: 15, 
          borderRadius: 5}}>
                      <View style={{justifyContent:'space-around', flexDirection:'row', alignContent: 'center', alignItems: 'center'}}>
                        <Feather name={'search'} size={20} color={'white'}/>
                        <Text style={{fontWeight: '500', color: 'white', marginLeft: 10}}>Find Ride</Text>
                      </View>
                </TouchableOpacity>

                </View>

              </BottomSheet> 

                
        </View>
    )
                }
};

export default HoSearch;