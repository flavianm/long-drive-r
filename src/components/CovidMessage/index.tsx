import { Feather, FontAwesome5 } from "@expo/vector-icons";
import React from "react";
import {View, Text, Pressable, Touchable} from 'react-native';
import HomeMap from "../../components/HomeMap";
import styles from "./styles";
import { useNavigation, useRoute } from "@react-navigation/core";
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';
import { TouchableOpacity } from "react-native-gesture-handler";
const CovidMessage = () =>{
    const navigation = useNavigation();

    let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

       const warn = () =>{
       console.warn('pressed')
    }


    if (!fontsLoaded) {
            return(
                <View>
                    <Text style={{alignContent: 'center', alignItems: 'center'}}>Loading...</Text>
                </View>
            )
        } else{


    return (
        <View style={[styles.container, {flexDirection: 'row', backgroundColor: '#1f66e0', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}]}>
          
           <View style={{}}>
               <Text style={{color: 'white', fontFamily: 'Manrope_500Medium', fontSize: 12}}>Please ensure that you keep safe during this COVID Season. For emergencies call 087785</Text>
           </View>
      


        </View>
    )

        }
};

export default CovidMessage;