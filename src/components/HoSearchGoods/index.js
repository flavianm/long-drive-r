import { Feather, Octicons, Ionicons, AntDesign, MaterialIcons, Entypo, FontAwesome5, MaterialCommunityIcons } from "@expo/vector-icons";
import React, {useState} from "react";
import {View, Text, ScrollView, Pressable, Image, TextInput, Button} from 'react-native';
import HomeMap from "../HomeMap";
// import styles from "../CovidMessage/styles";
import styles from "./styles";
import PlaceRow from "./PlaceRow";
import { useNavigation } from "@react-navigation/core";
import Modal from "react-native-modal";
import Toast from 'react-native-toast-message';
import FindSearch from "../../screens/Findtrip";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

const GOOGLE_PLACES_API_KEY = 'AIzaSyAj8LKvEnwzBhZRycGM1yJJ96j7AA2snGE';


const HoSearchGoods = () =>{

    const navigation = useNavigation();

     const [originPlace, setOriginPlace] = useState(null);
    const [destinationPlace, setDestinationPlace] = useState(null);

    const [originDesc, setOriginDesc] = useState(null);
    const [destDesc, setDestDesc] = useState(null);

    const [originSec, setOriginSec] = useState(null);
    const [destSec, setDestSec] = useState(null);

   const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
    
  };

    const FindTRip = () =>{
       navigation.navigate('FindTrip');
    }

    const posttrip = () =>{
       navigation.navigate('Trip');
    }

     const Press =()=>{
      
        if(originPlace && destinationPlace){
            navigation.navigate('GoodsSearchCustResult', {
                originPlace,
                destinationPlace,
                destDesc,
                originDesc,
                originSec,
                destSec
            });
        } else{
            Toast.show({
      type: 'error',
      text1: 'Locations Missing',
      text2: 'Please enter both locations'
    });
        }
    }

    return (
            <View style={{backgroundColor:'white'}}>
                     {/* <Button title="Show modal" onPress={toggleModal} /> */}
         <Modal isVisible={isModalVisible}
         swipeDirection={'down'}
         onSwipeComplete={()=> setModalVisible(false)}
         style={{ backgroundColor: 'white', marginLeft: 0, marginRight: 0}}>
        <View style={{ flex: 1 }}>
            <View style={{width: '100%', marginTop: 20, alignContent: 'center', alignItems: 'center'}}> 
                 <Pressable onPress={toggleModal}>
               <View style={{flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
                   <Ionicons name="checkmark-done-circle" size={40} color="green" />
                   <Text style={{fontWeight: '500'}}>Done</Text>
               </View>
           </Pressable>
            </View>
          
          
         

         <View style={styles.container}>
                <View>
                        {/* <Pressable style={{backgroundColor: 'orange', borderRadius: 10, width: '100%', height: 40, marginTop: 500, alignContent: 'center', alignItems: 'center', justifyContent:'space-evenly',}}
                        onPress={Press}
                        >
                       <View style={{flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center'}}>
                                <Text style={{ color: 'black', fontWeight: '500', marginRight: 10}}>Search</Text>
                                <Octicons name='search' size= {20}/>
                            </View>
                        </Pressable> */}
                        
                    </View>

                   <GooglePlacesAutocomplete 

                        placeholder="Pick Up From ?"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za',
                        }}
                        enablePoweredByContainer={false}
                        suppressDefaultStyles
                        
                        currentLocation={true}
                        currentLocationLabel='Current Location'
                        enableHighAccuracyLocation={true}
                        styles={{
                            textInput: styles.textInput,
                            container: styles.autoCompleteContainer,
                            listView: styles.listView,
                            separator: styles.separator

                        }}
                        onPress={(data, details = null) =>{
                            setOriginPlace({data, details});
                            setOriginDesc(data.description);
                            setOriginSec(data.structured_formatting.secondary_text);
                        }}
                        textInputProps={{
                            leftIcon: {}
                        }}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                        renderRow={(data) => <PlaceRow data={data} />}
                        renderDescription={(data) => data.description || data.vicinity}
                        
                        // predefinedPlaces={[homePlace, workPlace]}
                    />

                    <GooglePlacesAutocomplete 

                        placeholder="Your Destination ?"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za'
                        }}
                        suppressDefaultStyles
                        styles={{
                            textInput: styles.textInput,
                            //  container:{
                            //     position: 'absolute',
                            //     top: 85,
                            //     left: 10,
                            //     right: 10,
                            // },
                            container:{
                                ...styles.autoCompleteContainer,
                                top: 85,
                                
                            },
                            // listView:{...styles.listView,
                            // top: 70},
                            separator: styles.separator
                        }}
                        onPress={(data, details = null) =>{
                            console.log(originDesc)
                            setDestinationPlace({data, details});
                            setDestDesc(data.description);
                            setDestSec(data.structured_formatting.secondary_text);
                        }}
                        enablePoweredByContainer={false}
                        renderRow={(data) => <PlaceRow
                            data={data} />}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                    />

                    {/* cricle near origin input */}
                        <View style={styles.circle}>
                            <Ionicons name='ios-location-outline' size={20}/>
                        </View>
                    {/* long line connecting cirlces */}
                        {/* <View style={styles.line} /> */}

                    {/* square near destintaion input */}
                        <View style={styles.square}>
                            <Ionicons name='ios-location-sharp' size={20} color={'red'}/>
                        </View>

                        
                    <View>

                        {/* <Image
                        style={{marginLeft: 30, height: 300, width: 300, marginTop: 250, alignContent: 'center', alignItems:'center'}}
                        source={require('../../../assets/air.jpg')}
                        /> */}
                    </View>

                    
                       
            </View>


        </View>
      </Modal>




            <View style={{margin: 20}} 
            >
                {/* navigations */}
                <View style={{ marginBottom: 40, flexDirection:'row', justifyContent: 'space-evenly'}}>
                    {/* my orders */}
                      <Pressable
                      onPress={()=> navigation.navigate('GoodsOrders')}
                      style={{justifyContent: 'center', alignContent: 'center', alignItems: 'center'}}>
                        <View style={{width: 40, height: 40, backgroundColor: '#0e609e', borderRadius: 10, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                                <Feather name='list' size={20} color='white' />
                        </View>
                        <Text style={{fontWeight: '500', marginTop: 10}}>My Trips</Text>
                    

                      </Pressable>

                         {/* new trip */}
                      <View style={{justifyContent: 'center',  alignContent: 'center', alignItems: 'center'}}>
                      <Pressable
                       onPress={()=> navigation.navigate('GoodsDest')}
                      style={{width: 40, height: 40, backgroundColor: '#0e6c9e', borderRadius: 10, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                            <Feather name='plus' size={20} color='white' />
                      </Pressable>
                      <Text style={{fontWeight: '500', marginTop: 10}}>Request</Text>
                    

                      </View>
                        
                    {/* driver */}
                      <View style={{justifyContent: 'center', alignContent: 'center', alignItems: 'center'}}>
                      <Pressable
                      onPress={()=> navigation.navigate('DriverMoreGoods')}
                      style={{width: 40, height: 40, backgroundColor: '#0aa893', borderRadius: 10, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                            <MaterialCommunityIcons name="car-side" size={20} color="white" />
                      </Pressable>
                      <Text style={{fontWeight: '500', marginTop: 10}}>Driver</Text>
                    

                      </View>
                        
                    


                </View>

                <Pressable onPress={()=> toggleModal()}>


                <View onPress={()=> console.log('top press')}
                style={{flexDirection: 'row', marginLeft: 20, marginRight: 20, justifyContent: 'center', alignContent: 'center', alignItems: 'center'}}  >
               
                  <View style={{ height: 15, width: 15, borderRadius: 50, borderWidth: 3, marginRight: 10}}>

                  </View>
                  <View style={{ height: 40, width: '100%', borderRadius: 10, borderWidth: 1, alignContent: 'center', justifyContent: 'center'}}>
                        <TextInput
                        placeholder='Pick Up Location'
                        editable={false}
                        placeholderTextColor={'black'}
                        style={{ backgroundColor: 'white', marginLeft: 20, width: 250}}>
                              <Text style={{fontSize: 13}}>{originDesc}</Text>
                        </TextInput>
                  </View>
                </View>

                
               
                            <View onPress={()=> console.log('top press')}
                                style={{flexDirection: 'row', marginTop: 15, marginLeft: 20, marginRight: 20, justifyContent: 'center', alignContent: 'center', alignItems: 'center'}}  >
                    
                                <View style={{ height: 15, width: 15, borderRadius: 50, backgroundColor: 'red', marginRight: 10}}>
                                    </View>
                                <View style={{ height: 40, width: '100%', borderRadius: 10, borderWidth: 1, alignContent: 'center', justifyContent: 'center'}}>
                                        <TextInput
                                        placeholder='Drop off location'
                                        editable={false}
                                        placeholderTextColor={'black'}
                                        style={{ backgroundColor: 'white', marginLeft: 20, width: 250}}>
                                            <Text style={{fontSize: 13}}>{destDesc}</Text>
                                        </TextInput>
                                </View>
                            </View>
             

                 </Pressable>        





                  <View style={{ marginLeft: 30, marginRight: 30, height: 30, marginTop: 30, flexDirection: 'row', justifyContent: 'space-between'}}>

                     <View style={{flexDirection: 'row', borderRadius: 10, width: 100, height: 40, alignItems: 'center', backgroundColor: 'grey', justifyContent: 'space-around'}} >
                        <FontAwesome5 name={'calendar-alt'} size={20} color={'white'} />
                        <Text style={{fontWeight: '500', color: 'white'}}> Today </Text>
                     </View>

                     <View style={{flexDirection: 'row', borderRadius: 10, width: 100, height: 40, alignItems: 'center', backgroundColor: 'grey', justifyContent: 'space-around'}} >
                        <FontAwesome5 name={'box-open'} size={20} color={'white'} />
                        <Text style={{fontWeight: '500', color: 'white'}}>S</Text>
                        <MaterialIcons name="keyboard-arrow-down" size={20} color="white" />
                     </View>

                    

                  </View>


            </View>

            

         <Pressable
         onPress={()=> Press()}
         style={{backgroundColor: '#E15F25', justifyContent: 'space-around', 
         alignItems:'center', width: 295, height: 45, marginTop: 10, 
         borderRadius: 10,
         marginLeft: 40,
         marginRight: 40,}}>
                     <View style={{justifyContent:'space-around', flexDirection:'row', alignContent: 'center', alignItems: 'center'}}>
                        <Feather name={'search'} size={20} color={'white'}/>
                        <Text style={{fontWeight: '500', color: 'white', marginLeft: 10}}>Find Ride</Text>
                     </View>
                </Pressable>


        </View>
    
        
    )
};

export default HoSearchGoods;