import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
inputBox:{
    backgroundColor:'#e7e7e7',
    margin: 10,
    padding: 10,
    // flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
},
inputText:{
    fontSize: 20,
    fontWeight: '600',
    color: '#6e6e6e',


},
timeContainer:{
flexDirection:'row',
width: 100,
justifyContent:'space-between',
padding:10,
backgroundColor: '#fff',
borderRadius: 50
},

destinationText:{
    marginLeft: 10,
    fontWeight: '600',
    fontSize: 21,
    marginTop: 10,
    marginBottom: 10
},

 container:{
        padding: 10,
        height:'100%',
        
    },

    textInput:{
    margin: 10,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 42,
    borderWidth: 1,
    borderRadius: 10,
},
row:{
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10
},
iconContainer:{
    backgroundColor: '#a2a2a2',
    padding: 5,
    borderRadius: 50,
    marginRight: 15

},
locationText:{

},
separator:{
    backgroundColor: '#efefef',
    height: 1
      },
listView:{
    position: 'absolute',
    top: 123,
    },
 autoCompleteContainer:{
    position: 'absolute',
    top: 30,
    left: 10,
    right: 10,
},
circle:{
     width: 20,
    height: 20,
    position: 'absolute',
    top: 50,
    left: 22,
    borderRadius: 5
},
// line: {
//     height: 51,
//     width: 2,
//     backgroundColor: '#919191',
//     position: 'absolute',
//     top: 67,
//     left: 23.5,
// },
square:{
     width: 20,
    height: 20,
    position: 'absolute',
    top: 100,
    left: 22

}





});

export default styles;