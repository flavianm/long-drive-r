import React, { useState } from 'react'
import { SafeAreaView, StyleSheet, Text, TextInput, View } from 'react-native'

const SignIn = () => {

    const [Name, setName] = useState('null');



    return (
        <SafeAreaView  style={{alignContent: 'center', alignItems: 'center'}}>
            <View style={{alignContent: 'center', alignItems: 'center'}}>
                <Text>Sign In Component</Text>
            </View>
            <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setName(val)}
                    placeholder={'First Name'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
        </SafeAreaView>
        
    )
}

export default SignIn

const styles = StyleSheet.create({})
