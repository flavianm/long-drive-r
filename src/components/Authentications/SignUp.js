import React, { useState } from 'react'
import { SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';
import { Auth } from 'aws-amplify';
const SignUp = () => {

    let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

    const [username, setUsername] = useState('null');
    const [password, setPassword] = useState('null');
    const [phone_number, setPhone_number] = useState('null');
     const [email, setEmail] = useState('null');
  const [confirmationCode, setConfirmationCode] = useState('null');
  const [confirmationCodeLog, setConfirmationCodeLog] = useState('null');
    const [userr, setUserr] = useState('null');
    const [isAuthenticated, setIsAuthenticated] = useState(false);




   async function authenticate(isAuthenticated){
        setIsAuthenticated(isAuthenticated)

    }

    render = () => {
        if(isAuthenticated){
            console.log('Auth: ', Auth)
            return(
                <View>
                    <Text>Authenticated </Text>
                </View>
            )
        }
    }


 async function signUp() {
    Auth.signUp({
        username: username,
        password: password,
        attributes: {
            email: email,
            phone_number: phone_number,
        }
    }).then(()=> console.log('Registered Successfully')).catch(err => console.log('SignUp error !:', err))
  }

async function confirmSignUp(){

    Auth.confirmSignUp(username, confirmationCode)
    .then(()=> console.log('Confirmed Successfully')).catch(err => console.log('Confirm error !:', err))
  }

async function signIn() {
    Auth.signIn( username, password).then(user => {
        setUserr({user})
    console.log('Successfully Logged In')
    setIsAuthenticated(true)

    }).catch(err => console.log('Login error !:', err))
    
            
            
  }

async function confirmSignIn(){

    Auth.confirmSignIn(username, confirmationCodeLog)
    .then(()=> {  
        console.log('Success ')     
    setIsAuthenticated(true)
        console.log('authenticated true ')     

}).catch(err => console.log('Confirm error !:', err))
  }







if (!fontsLoaded) {
            return(
                <View style={{alignContent: 'center', justifyContent: 'space-around'}}> 
                   <Text>Please wait...</Text>
                </View>
            )
        } else{

    return (
        <SafeAreaView  style={{alignContent: 'center', alignItems: 'center', backgroundColor: 'white'}}>

            <ScrollView  showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false}>



          


            <View style={{alignContent: 'center', alignItems: 'center', marginBottom: 30, marginTop: 10}}>
                <Text>Sign Up</Text>
            </View>
            <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setUsername(val)}
                    placeholder={'E-mail Address'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setPassword(val)}
                    secureTextEntry={true}
                    placeholder={'Password'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
                 <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setPhone_number(val)}
                    placeholder={'Phone Number'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setEmail(val)}
                    placeholder={'Confirm E-mail'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>


            <View style={{width: 250, marginTop: 20}}>
                <TouchableOpacity onPress={()=> signUp()} style={{width: '100%', borderRadius: 5, height: 45, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Sign Up</Text>
                </TouchableOpacity>
            </View>


<View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginTop: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setConfirmationCode(val)}
                    placeholder={'Confirmation Code'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

<View style={{width: 250, marginTop: 20}}>
             <TouchableOpacity onPress={()=> confirmSignUp()} style={{width: '100%', borderRadius: 5, height: 45, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Confirm Code</Text>
                </TouchableOpacity>
            </View>
            




            <View style={{marginTop: 30, marginBottom: 30}}>
                <Text>Sign In</Text>
            </View>



            <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setUsername(val)}
                    placeholder={'E-mail Address'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

                 <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setPassword(val)}
                    secureTextEntry={true}
                    placeholder={'Password'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

                <View style={{width: 250, marginTop: 20}}>
             <TouchableOpacity onPress={()=> signIn()} style={{width: '100%', borderRadius: 5, height: 45, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Sign In</Text>
                </TouchableOpacity>
            </View>



            <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginTop: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setConfirmationCodeLog(val)}
                    placeholder={'Confirmation Code'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

<View style={{width: 250, marginTop: 20}}>
             <TouchableOpacity onPress={()=> confirmSignIn()} style={{width: '100%', borderRadius: 5, height: 45, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Confirm Code</Text>
                </TouchableOpacity>
            </View>
            
               


            

             </ScrollView>
        </SafeAreaView>
        
    )
        }
}

export default SignUp

const styles = StyleSheet.create({})
