import React, {useState, useEffect} from 'react'
import { ScrollView, StyleSheet, Text, View, TextInput, Pressable, Image, SafeAreaView, Platform , TouchableOpacity } from 'react-native'
import { useNavigation, useRoute } from "@react-navigation/core";
import { AntDesign, Entypo, MaterialIcons } from '@expo/vector-icons';import { Feather, FontAwesome, MaterialCommunityIcons, Fontisto  } from '@expo/vector-icons';
import { API, graphqlOperation } from "aws-amplify";
import {Auth, Storage} from 'aws-amplify';
import { updateUser, createUser } from '../graphql/mutations';
import { getUser } from '../graphql/queries';
import Toast from 'react-native-toast-message';
import AnimatedLoader from "react-native-animated-loader";
import LottieView from 'lottie-react-native';
import * as ImagePicker from 'expo-image-picker';

import * as mutations from '../graphql/mutations';
import * as queries from '../graphql/queries';

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

//   <Toast ref={(ref) => Toast.setRef(ref)} />
const OnBProfile = () => {

 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

 const [visible, setVisible] = useState(true);

    useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 5000);
  }, [])

  

const [user, setUser] = useState([])

const [champUser, setChampUser] = useState();

const [image, setImage] = useState(null);

const [name, setName] = useState(null);
const [surname, setSurname] = useState(null);
const [idNo, setIdNo] =useState(null);
const [gender, setGender] =useState('null');

      


//   const navigation = useNavigation();

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

  if (!result.cancelled) {
      setImage(result.uri);
    }
};

const fetchUsers = async() =>{

        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setUser(userData.attributes);
            console.log(userData.attributes.email)
            // setInput('userId', userSub)


            // input new user

           


        // fetch balance amount

            const userBalance = await API.graphql(graphqlOperation(getUser, { id: userSub}))
            const userBalanceInfo = userBalance.data.getUser
            setChampUser(userBalanceInfo)
            const zeBalance = userBalanceInfo.balance
            // setCurrentBalance(zeBalance)
            console.log('USER BALANCE  ::' + zeBalance)


            const bankupdate = {
                id: userSub,
                balance: '0.01'
            }


            if (zeBalance === null){
                console.log('null value here')
                await API.graphql(graphqlOperation(updateUser, {input: bankupdate}))
                console.log('done')
            } else{
                console.log('balance is :' + zeBalance )
            }


            
    }

useEffect(() => {
   fetchUsers();
              
            },[])

const uploadImage = async () =>{
    try {
        const response = await fetch(image);

        const blob = await response.blob();


        const urlParts = image.split('.')


        const extension = urlParts[urlParts.length - 1];

        const frnt = champUser?.id + 'profile'

        const key =   `${frnt}.${extension}`; 
        
        await Storage.put(key, blob);

        return key;



    }catch(e){
        console.log(e)
    }

    return '';
}


const [todos, setTodos] = useState([])

    function setInput(key, value) {
    setFormState({ ...formState, [key]: value })
  }
   

const todoDetails = {
        id: user.sub,
        status: 'dcancelled',
        active: 'no'
    };

     async function updateTodo() {

       

        if(idNo === null || idNo === ' ' || idNo=== '   '){
            
            return;
        }

        if(name === null || name === ' ' || name=== '   '){
          
            return;
        }

        if(surname === null || surname === ' ' || surname=== '   '){
                 
            
            return;
        }

        if(gender === 'null' || gender === ' ' || gender=== '   '){
                 
            return;
        }


        const userData = await Auth.currentAuthenticatedUser();
        const userSub = userData.attributes.sub;


        const bluza = {
            id: userSub,
            name: name, 
            surname: surname, 
            idno: idNo, 
            gender: gender, 
            completeP: 'Yes', 
            imageUrl: 'https://thumbs.dreamstime.com/b/default-avatar-profile-icon-social-media-user-vector-default-avatar-profile-icon-social-media-user-vector-portrait-176194876.jpg', 
        }

    try {
    
      await API.graphql(graphqlOperation(updateUser, {input: bluza}))
 
    } catch (err) {
      console.log('error update User:', err)
      
    }

  }

  const gend = () =>{
    if (gender === 'null'){
        return(
            <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>

            <TouchableOpacity onPress={()=> setGender('male')} style={{width: 85, borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'grey', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13}}>Male</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> setGender('female')} style={{width: 85, borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'grey', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13}}>Female</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> setGender('other')} style={{width: 85, borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'grey', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13}}>Other</Text>
            </TouchableOpacity>
            </View>
           
        )
    }

   if (gender === 'male'){
        return(
            <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>

            <TouchableOpacity onPress={()=> setGender('null')} style={{width: 85,  borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13, color: 'orange'}}>Male</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> setGender('female')} style={{width: 85, borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'grey', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13}}>Female</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> setGender('other')} style={{width: 85, borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'grey', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13}}>Other</Text>
            </TouchableOpacity>
            </View>
           
        )
    }

    if (gender === 'female'){
        return(
            <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>

            <TouchableOpacity onPress={()=> setGender('male')} style={{width: 85,  borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'grey', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13}}>Male</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> setGender('null')} style={{width: 85, borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13, color: 'orange'}}>Female</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> setGender('other')} style={{width: 85, borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'grey', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13}}>Other</Text>
            </TouchableOpacity>
            </View>
           
        )
    }

    if (gender === 'other'){
        return(
            <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>

            <TouchableOpacity onPress={()=> setGender('male')} style={{width: 85,  borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'grey', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13}}>Male</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> setGender('female')} style={{width: 85, borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'grey', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13}}>Female</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=> setGender('null')} style={{width: 85, borderRadius: 5, height: 45, borderWidth: 0.5, borderColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13, color: 'orange'}}>Other</Text>
            </TouchableOpacity>
            </View>
           
        )
    }


}
  const zeImage = ()=>{

    if (image === null){


        return(
            <TouchableOpacity  onPress={pickImage} style={{width: 100, height: 100, borderRadius: 5, backgroundColor: '#e1e7e8', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
             <Image  source={{ uri: champUser?.imageUrl}}
            style={{ width: 100, height: 100, marginBottom: -94.5, borderRadius: 5 }}
            />
            
             <LottieView style={{width: 75, height: 75, marginBottom: 20}} source={require('../assets/warning/9948-camera-pop-up.json')} autoPlay loop />
            </TouchableOpacity>

        )
    } else{
        return(
            <TouchableOpacity onPress={pickImage} >

            <Image  source={{ uri: image}}
            style={{ width: 100, height: 100, borderRadius: 5 }}
            />

            </TouchableOpacity>
            

            )
    }


     
  }

  const beImage = ()=>{

        return(
            <TouchableOpacity style={{borderRadius: 5, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
            
             <LottieView style={{width: 280, height: 280, marginBottom: 20}} source={require('../assets/warning/prok.json')} autoPlay loop />
           
            </TouchableOpacity>

        )
    


     
  }

if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{


    return (
        <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>
            <ScrollView
            showsVerticalScrollIndicator={false}
            >

                <View>
                    <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10}}>

                        {/* <Text style={{paddingLeft: 15, fontSize: 25, color: 'black', marginTop: 30, fontFamily: 'Manrope_700Bold'}}>
                        My Profile
                        </Text>

                    <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    /> */}

                    {/* <View style={{alignItems: 'center', alignContent: 'center', justifyContent: 'center'}}>
                            { beImage()}
                    </View> */}

                        <Text style={{paddingLeft: 15, fontSize: 25, color: 'black', marginTop: 30, fontFamily: 'Manrope_700Bold'}}>
                        Set Up Your Profile
                        </Text>

                        <View style={{margin: 20}}>







                        



                    <View style={{marginLeft: 10, marginBottom: 20, marginTop: 30, justifyContent: 'space-evenly'}}>
               
                <View>
                        <Text style={{ marginBottom: 5, fontFamily: 'Manrope_500Medium', fontSize: 13}} >First Name</Text>

                </View>
                <View style={{borderColor:'grey', borderWidth: 0.5,  marginBottom: 5, height: 45, width: '100%', borderRadius: 5}}>
                    <TextInput style={{color:'black', fontSize: 13, paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium'}}
                    onChangeText={val => setName(val)}
                    placeholder= {'First Name'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

           
           
           
           
           
                    </View>

                    <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontFamily: 'Manrope_500Medium', fontSize: 13, marginBottom: 5}} >Last Name</Text>

                </View>
                {/* TextInput */}
                <View style={{borderColor:'grey', borderWidth: 0.5, marginBottom: 5, height: 45, width: '100%', borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setSurname(val)}
                    // value={formState.surname}
                    placeholder= {'Surname'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

                    </View>

                    <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{ marginBottom: 5, fontFamily: 'Manrope_500Medium', fontSize: 13}} > SA ID Number / Passport</Text>

                </View>
                {/* TextInput */}
                <View style={{borderWidth: 0.5, borderColor: 'grey',  marginBottom: 5, height: 45, width: '100%', borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setIdNo(val)}
                    placeholder= {'ID Number / Passport'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

           <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', marginLeft: 20, marginBottom: 5, fontFamily: 'Manrope_500Medium', fontSize: 13}} >Gender</Text>

                </View>

                 <View style={{ marginBottom: 5, marginTop: 10 }}>
                        {gend()}
                    </View>        


                {/* TextInput */}
                {/* <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                   
                    
                   
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setGender( val)}
                    placeholder= {'Male | Female'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View> */}

            </View>

            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontFamily: 'Manrope_500Medium', fontSize: 13, marginBottom: 5}} >Your Phone Number</Text>

                </View>
                {/* TextInput */}
                <View style={{justifyContent: 'space-evenly', borderColor:'grey', borderWidth: 0.5, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <Text style={{color:'#7d7d7d', paddingLeft: 10, paddingTop: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    >
                        {user.phone_number}
                </Text>
                </View>

            </View>

                    </View>
                </View>



                <View style={{ marginTop: 20, marginBottom: 40, marginLeft: 30, marginRight: 30,  backgroundColor: 'orange', borderRadius: 5, alignItems:'center', alignContent: 'center'}}>
                    <TouchableOpacity style={{width: '100%', height: 45, marginLeft: 50, marginRight: 50, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
                    // onPress={addTodo} 
                    // onPress={ ()=> updateTodo} 
                    onPress={()=> updateTodo()}
                    >
                        <Text style={{fontWeight: '500', color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Finish</Text>
                    </TouchableOpacity>
                </View>


            </View>
            </ScrollView>
        </SafeAreaView>
    )
        }
}

export default OnBProfile;
