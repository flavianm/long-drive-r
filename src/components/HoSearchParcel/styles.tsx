import { StyleSheet } from "react-native";
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const styles = StyleSheet.create({
inputBox:{
    backgroundColor:'#e7e7e7',
    margin: 10,
    padding: 10,
    // flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
},
inputText:{
    fontSize: 20,
    fontWeight: '600',
    color: '#6e6e6e',
    fontFamily: 'Manrope_500Medium'


},
timeContainer:{
flexDirection:'row',
width: 100,
justifyContent:'space-between',
padding:10,
backgroundColor: '#fff',
borderRadius: 50
},

destinationText:{
    marginLeft: 10,
    fontWeight: '600',
    fontSize: 21,
    marginTop: 10,
    marginBottom: 10
},

 container:{
        padding: 10,
        height:'100%',
        
    },

    textInput:{
    margin: 10,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 42,
    borderRadius: 5,
    backgroundColor: '#e0e0e0',
    height: 45,
    fontFamily: 'Manrope_500Medium'

},
row:{
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10
},
iconContainer:{
    backgroundColor: '#a2a2a2',
    padding: 5,
    borderRadius: 50,
    marginRight: 15

},
locationText:{

},
separator:{
    backgroundColor: '#efefef',
    height: 1
      },
listView:{
    position: 'absolute',
    top: 123,
    },
 autoCompleteContainer:{
    position: 'absolute',
    top: 30,
    left: 10,
    right: 10,
},
circle:{
     width: 20,
    height: 20,
    position: 'absolute',
    top: 50,
    left: 22,
    borderRadius: 5
},
// line: {
//     height: 51,
//     width: 2,
//     backgroundColor: '#919191',
//     position: 'absolute',
//     top: 67,
//     left: 23.5,
// },
square:{
     width: 20,
    height: 20,
    position: 'absolute',
    top: 100,
    left: 22

}





});

export default styles;