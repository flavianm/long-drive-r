import { Feather, Octicons, Ionicons, AntDesign, MaterialIcons, Entypo, FontAwesome5, MaterialCommunityIcons } from "@expo/vector-icons";
import React, {useState} from "react";
import {View, Text, ScrollView, TouchableOpacity, Pressable, Image, TextInput, Button, SafeAreaView} from 'react-native';
import HomeMap from "../HomeMap";
// import styles from "../CovidMessage/styles";
import styles from "./styles";
import PlaceRow from "./PlaceRow";
import { useNavigation } from "@react-navigation/core";
import Modal from "react-native-modal";
import { SwipeablePanel } from 'rn-swipeable-panel';
import Toast from 'react-native-toast-message';
import LottieView from 'lottie-react-native';

// import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';



import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';


import FindSearch from "../../screens/Findtrip";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

const GOOGLE_PLACES_API_KEY = 'AIzaSyAj8LKvEnwzBhZRycGM1yJJ96j7AA2snGE';


const HoSearchParcel = () =>{

    const navigation = useNavigation();

     const [originPlace, setOriginPlace] = useState(null);
    const [destinationPlace, setDestinationPlace] = useState(null);

    const [originDesc, setOriginDesc] = useState(null);
    const [destDesc, setDestDesc] = useState(null);

    const [originSec, setOriginSec] = useState(null);
    const [destSec, setDestSec] = useState(null);

    const [switching, setSwitching] = useState('Parcels')

   const [isModalVisible, setModalVisible] = useState(false);

   let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
    
  };

    const FindTRip = () =>{
       navigation.navigate('FindTrip');
    }

    const posttrip = () =>{
       navigation.navigate('Trip');
    }

     const [panelProps, setPanelProps] = useState({
    fullWidth: true,
    showCloseButton: true,
    onClose: () => closePanel(),
    onPressCloseButton: () => closePanel(),
    // ...or any prop you want
  });
  const [isPanelActive, setIsPanelActive] = useState(false);

  const openPanel = () => {
    setIsPanelActive(true);
  };

  const closePanel = () => {
    setIsPanelActive(false);
  };


const switchingOptions = () =>{

    if(switching === 'Parcels'){
        return(
           <View style={{ flexDirection: 'row',  marginTop: 10, marginLeft: 25, marginBottom: 10, justifyContent: 'space-evenly' }}>

                        <Pressable onPress={()=> console.warn('parcel pressed')} style={{width: 100, height: 40, marginRight: 20, backgroundColor: '#3A92DA', borderRadius: 5, flexDirection: 'row', justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
                            <FontAwesome5 name="box-open" size={15} color="white" />
                            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'white'}}>Parcels</Text>
                        </Pressable>

    

                        <View style={{width: 20, marginRight: 10, height: 40, borderRadius: 5, flexDirection: 'row', justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
                             <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'black', fontSize: 10}}>OR</Text>
                        </View>

                        <Pressable onPress={()=> setSwitching('Goods')} style={{width: 100, height: 40, borderRadius: 5, flexDirection: 'row', justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
                            <FontAwesome5 name="box-open" size={15} color="grey" />
                            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>Goods</Text>
                        </Pressable>



                </View>
        )
    } 

    if(switching === 'Goods'){
        return(
           <View style={{ flexDirection: 'row',  marginTop: 10, marginLeft: 25, marginBottom: 10, justifyContent: 'space-evenly' }}>

                        <Pressable onPress={()=> setSwitching('Parcels')} style={{width: 100, height: 40, marginRight: 20, borderRadius: 5, flexDirection: 'row', justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
                            <FontAwesome5 name="box-open" size={15} color="grey" />
                            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>Parcels</Text>
                        </Pressable>

                        <View style={{width: 20, marginRight: 20, height: 40, borderRadius: 5, flexDirection: 'row', justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
                             <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'black', fontSize: 10}}>OR</Text>
                        </View>

                        <Pressable style={{width: 100, height: 40, borderRadius: 5, backgroundColor: '#3A92DA', flexDirection: 'row', justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
                            <FontAwesome5 name="box-open" size={15} color="white" />
                            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'white'}}>Goods</Text>
                        </Pressable>



                </View>
        )
    }



   

}




     const Press =()=>{
      
        if(originPlace && destinationPlace){
            navigation.navigate('ParcelSearchCustResult', {
                originPlace,
                destinationPlace,
                destDesc,
                originDesc,
                originSec,
                destSec,
                switching
            });
        } else{
           Toast.show({
      type: 'error',
      text1: 'Locations',
      text2: 'Enter Both Locations',
      visibilityTime: 5000,

    });
        }
    }

const theImage = '../../assets/lyftleft.jpg' 

     if (!fontsLoaded) {
            return(
                <Text>Loading...</Text>
            )
        } else{

return (
    <View style={{backgroundColor:'white'}} 
    >
                     {/* <Button title="Show modal" onPress={toggleModal} /> */}
         <Modal isVisible={isModalVisible}
         swipeDirection={'down'}
         onSwipeComplete={()=> setModalVisible(false)}
         style={{ backgroundColor: 'white', marginLeft: 0, marginRight: 0}}>
        <View style={{ flex: 1 }}>
            <View style={{width: '100%', marginTop: 20, alignContent: 'center', alignItems: 'center'}}> 
                 <Pressable onPress={toggleModal}>
               <View style={{flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
                   <Ionicons name="checkmark-done-circle" size={40} color="green" />
                   <Text style={{fontWeight: '500', fontFamily: 'Manrope_600SemiBold'}}>Done</Text>
               </View>
           </Pressable>
            </View>
          
          
         

         <View style={styles.container}>
                <View>
                      
                        
                    </View>

                   <GooglePlacesAutocomplete 

                        placeholder="PickUp Point"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za',
                        }}
                        enablePoweredByContainer={false}
                        suppressDefaultStyles
                        
                        currentLocation={true}
                        currentLocationLabel='Current Location'
                        enableHighAccuracyLocation={true}
                        styles={{
                            textInput: styles.textInput,
                            container: styles.autoCompleteContainer,
                            listView: styles.listView,
                            separator: styles.separator

                        }}
                        onPress={(data, details = null) =>{
                            setOriginPlace({data, details});
                            setOriginDesc(data.description);
                            setOriginSec(data.structured_formatting.secondary_text);
                        }}
                        textInputProps={{
                            leftIcon: {}
                        }}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                        renderRow={(data) => <PlaceRow data={data} />}
                        renderDescription={(data) => data.description || data.vicinity}
                        
                        // predefinedPlaces={[homePlace, workPlace]}
                    />

                    <GooglePlacesAutocomplete 

                        placeholder="DropOff Point"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za'
                        }}
                        suppressDefaultStyles
                        styles={{
                            textInput: styles.textInput,
                            //  container:{
                            //     position: 'absolute',
                            //     top: 85,
                            //     left: 10,
                            //     right: 10,
                            // },
                            container:{
                                ...styles.autoCompleteContainer,
                                top: 85,
                                
                            },
                            // listView:{...styles.listView,
                            // top: 70},
                            separator: styles.separator
                        }}
                        onPress={(data, details = null) =>{
                            console.log(originDesc)
                            setDestinationPlace({data, details});
                            setDestDesc(data.description);
                            setDestSec(data.structured_formatting.secondary_text);
                        }}
                        enablePoweredByContainer={false}
                        renderRow={(data) => <PlaceRow
                            data={data} />}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                    />

                    {/* cricle near origin input */}
                        <View style={styles.circle}>
                            <Ionicons name='ios-location-outline' size={20}/>
                        </View>
                    {/* long line connecting cirlces */}
                        {/* <View style={styles.line} /> */}

                    {/* square near destintaion input */}
                        <View style={styles.square}>
                              <LottieView style={{width: 30, height: 30, marginLeft: -2}} source={require('../../assets/31078-pin-animation.json')} autoPlay loop />

                        </View>

                        
                    <View>

                        {/* <Image
                        style={{marginLeft: 30, height: 300, width: 300, marginTop: 250, alignContent: 'center', alignItems:'center'}}
                        source={require('../../../assets/air.jpg')}
                        /> */}
                    </View>

                    
                       
            </View>


        </View>
      </Modal>


            <ScrollView showsHorizontalScrollIndicator={false} 
            showsVerticalScrollIndicator={false}
            style={{marginTop: 20}} 
            >


                <View style={{marginBottom: 10, flexDirection: 'row', justifyContent: 'space-evenly'}}>

                       
                        
                        
                        
                        <TouchableOpacity
                      onPress={()=> navigation.navigate('ParcelOrders')} style={{backgroundColor: '#fff', width: 100, height: 100, borderRadius: 10, alignContent: 'center', alignItems: 'center' }}>

                           

                            <View style={{width: 45, height: 45, borderRadius: 50, backgroundColor: 'orange',  justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
                                <Feather name='list' size={22} color='#fff' />
                            </View>

                             <View style={{justifyContent: 'center', alignContent: 'center', alignItems: 'center', marginTop: 10}}>
                                <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 12, color: 'black'}}>My Trips</Text>
                            </View>

                        </TouchableOpacity>

                        <TouchableOpacity
                      onPress={()=> navigation.navigate('ParcelDest')} style={{backgroundColor: '#fff', width: 100, height: 100, borderRadius: 10, alignContent: 'center', alignItems: 'center' }}>

                           

                            <View style={{width: 45, height: 45, borderRadius: 50, backgroundColor: '#41afa9',  justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
                                <Feather name='plus' size={22} color='#fff' />
                            </View>

                             <View style={{justifyContent: 'center', alignContent: 'center', alignItems: 'center', marginTop: 10}}>
                                <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 12, color: 'black'}}>Requests</Text>
                            </View>

                        </TouchableOpacity>

                         <TouchableOpacity
                      onPress={()=> navigation.navigate('DriverMore')} style={{backgroundColor: '#fff', width: 100, height: 100, borderRadius: 10, alignContent: 'center', alignItems: 'center' }}>

                           

                            <View style={{width: 45, height: 45, borderRadius: 50, backgroundColor: '#41afa9',  justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
                                <MaterialCommunityIcons name="car-side" size={22} color="#fff" />
                            </View>

                             <View style={{justifyContent: 'center', alignContent: 'center', alignItems: 'center', marginTop: 10}}>
                                <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 12, color: 'black'}}>Driver</Text>
                            </View>

                        </TouchableOpacity>


                       
                        
                
                </View>

                <View>
                      <TouchableOpacity
                      onPress={()=> navigation.navigate('Home')} style={{backgroundColor: '#fff', width: 100, height: 100, borderRadius: 10, alignContent: 'center', alignItems: 'center', marginLeft: 18 }}>

                           

                            <View style={{width: 45, height: 45, borderRadius: 50, backgroundColor: '#3A92DA',  justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
                                {/* <MaterialCommunityIcons name="car-side" size={22} color="#fff" /> */}
                                                             <LottieView style={{width: 35, height: 35, marginLeft: 0}} source={require('../../assets/warning/78700-back-arrow.json')} autoPlay loop />

                            </View>

                             <View style={{justifyContent: 'center', alignContent: 'center', alignItems: 'center', marginTop: 10}}>
                                <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 12, color: 'black'}}>Rides</Text>
                            </View>

                        </TouchableOpacity>
                </View>


<View>
                       
                         
                   
               </View>    

            {/* </ScrollView> */}

                         <View style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 0, marginBottom: 20}}/>


           <View style={{marginLeft: 20}}
           
           >
               {/* <Pressable  onPress={()=> toggleModal()} > */}

                <Pressable  onPress={()=> toggleModal()} >


                <View style={{flexDirection: 'row', marginRight: 20, backgroundColor: '#f5f5f5', borderRadius: 5, alignContent: 'center', alignItems: 'center'}}>
                        {/* icon */}
                        <View style={{marginLeft: 10}}>
                            <Entypo name="location-pin" size={20} color="black" />
                        </View>

                        {/* locations */}
                
                        <View style={{width: 270, marginTop: 10, marginBottom: 10}}>
                            <Text style={{fontFamily: 'Manrope_600SemiBold', marginLeft: 15, fontSize: 12}}>Pickup Point</Text>
                            <Text style={{fontFamily: 'Manrope_400Regular', marginLeft: 15, fontSize: 10}}>{originDesc || `Click To Set Location...`}</Text>
                        </View>   

                        {/* clear */}
                
                        <View style={{marginLeft: 10, marginTop: 15,  width: 15, height: 15,  borderColor: 'grey',  borderRadius: 50, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontSize: 8, color: '#5e675d', fontWeight: '500'}}>X</Text>
                        </View>    



                </View> 

                <View style={{flexDirection: 'row', marginTop: 25, marginRight: 20, backgroundColor: '#f5f5f5', borderRadius: 5, alignContent: 'center', alignItems: 'center'}}>
                        {/* icon */}
                        <View style={{}}>
                            {/* <Entypo name="location-pin" size={20} color="red" /> */}
                            <LottieView style={{width: 30, height: 30, marginLeft: 1}} source={require('../../assets/31078-pin-animation.json')} autoPlay loop />

                        </View>

                        {/* locations */}
                
                        <View style={{width: 270, marginTop: 10, marginBottom: 10}}>
                            <Text style={{fontFamily: 'Manrope_600SemiBold', marginLeft: 15, fontSize: 12}}>DropOff Point</Text>
                            <Text style={{fontFamily: 'Manrope_400Regular', marginLeft: 15, fontSize: 10}}>{destDesc || `Click To Set Location...`}</Text>
                        </View>   

                        {/* clear */}
                
                        <View style={{marginLeft: 10, marginTop: 15,  width: 15, height: 15, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{fontSize: 8, color: '#5e675d', fontWeight: '500'}}>X</Text>
                        </View>    



                </View> 


              


               </Pressable>

                       <View style={{marginRight: 20}}>
                         <View style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: -5}}/>

                       </View>


           
                </View>  

                

              <View style={{flexDirection: 'row', width: '100%', marginLeft: 22, marginTop: 25, alignItems: 'center', alignContent: 'center'}}>
                  {/* <FontAwesome5 name="people-carry" size={15} color="black" /> */}
                   <LottieView style={{width: 45, height: 45, marginLeft: -6, marginBottom: 3}} source={require('../../assets/warning/8367-open-box.json')} autoPlay loop />

            {switchingOptions()}
            </View>          

             
              
         </ScrollView>

<View style={{}}>
                         <View style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: -5}}/>

                       </View>

<View style={{alignContent: 'center', alignItems: 'center'}}>





                <TouchableOpacity
         onPress={()=> Press()}
         style={{backgroundColor: '#5e675d', justifyContent: 'space-around', 
         alignItems:'center', width: 315, height: 45, marginTop: 30, 
         borderRadius: 5}}>
                     <View style={{justifyContent:'space-around', flexDirection:'row', alignContent: 'center', alignItems: 'center'}}>
                        <Feather name={'search'} size={20} color={'#fff'}/>
                        <Text style={{fontWeight: '500', color: '#fff', marginLeft: 10}}>Find Ride</Text>
                     </View>
                </TouchableOpacity>

                </View>


  
        </View>




           
    )
        }
    
};

export default HoSearchParcel;