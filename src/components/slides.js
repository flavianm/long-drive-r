export default [

    {
        id: '1',
        title: 'The Long Ride',
        description: 'A community of normal people going normal routes. Hitch a safe ride in a controlled space.',
        image: require('../assets/Ridesb.png'),
    },
    {
        id: '2',
        title: 'Quick Parcels',
        description: 'Send a parcel to where normal people travel. Transported by the everyday commuter and priced by the driver. You can send a parcel too..',
        image: require('../assets/deliveriesbb.png'),
    },
    {
        id: '3',
        title: 'Larger Goods',
        description: 'Send something bigger... A fridge maybe?',
        image: require('../assets/deliveriesb.png'),
    },
    {
        id: '4',
        title: 'Paid Easily',
        description: 'Make money and get paid easily. Earn per trip and keep 90% of your trip fees ',
        image: require('../assets/money.png'),
    },


]