import React, {useState, useEffect} from "react";
import {Image, FlatList, View, Text} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps'; 
import {API, graphqlOperation} from 'aws-amplify';
import { listCars } from "../../graphql/queries";
import { MaterialIcons } from "@expo/vector-icons";
import {Auth} from 'aws-amplify';
import { useNavigation } from "@react-navigation/core";
import { getParcelOrder, getParcelRequests, getUser } from '../../graphql/queries';

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const HomeMap = () =>{
    const [cars, setCars] = useState([]);
    const [userDet, setUserDet] = useState([]);

     let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

    const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            // setuser(userSub);
            // console.log(userSub)

            const todoData = await API.graphql(graphqlOperation(getUser, { id: userSub}))
            const todos = todoData.data.getUser
            setUserDet(todos)
            
    }

    useEffect(() => {
     fetchUsers()
    }, [])

    if (!fontsLoaded) {
            return(
                // <Text>Loading...</Text>
                <View>

                </View>
            )
        } else{

    return (
            <MapView
                style={{height: '100%', width: '100%', borderRadius: 10}} 
                provider={PROVIDER_GOOGLE}
                showsUserLocation={true}
                initialRegion={{
                    latitude: -26.192910,
                    longitude: 28.035990,
                    latitudeDelta: 0.0242,
                    longitudeDelta: 0.0141,
                }}
            >

                {/* <View style={{backgroundColor: 'orange', height: 50, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                    
                </View> */}
                <View style={{alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>

                    <View style={{backgroundColor: '#1f66e0', flexDirection: 'row', height: 40, top: 0, width: 200, borderBottomLeftRadius: 30, borderBottomRightRadius: 30, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                                                     
                               

                                 <View style={{alignContent: 'center', alignItems: 'center', justifyContent: 'center', marginRight: 10}}>
                            <Image
                                source={{uri: userDet?.imageUrl}}
                                style={{width: 25, height: 25, borderRadius: 50, borderWidth: 1, borderColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}
                            />
                            
                        </View>

                        <View>
                            <MaterialIcons name="keyboard-arrow-down" size={15} color="white" />
                        </View>

                        <View style={{marginLeft: 10, width: 55, height: 32, alignContent: 'center', justifyContent: 'center'}}>
                            <Text style={{color: 'white', fontFamily: 'Manrope_500Medium', fontSize: 12}}>{userDet?.name || `Loading..`}</Text>
                        </View>

                        


                </View>
                </View>
                
              
            </MapView>
        
    )}
};

export default HomeMap;