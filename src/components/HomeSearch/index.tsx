import { Feather, AntDesign, MaterialIcons, Entypo } from "@expo/vector-icons";
import React from "react";
import {View, Text, ScrollView, Pressable, Image} from 'react-native';
import HomeMap from "../../components/HomeMap";
// import styles from "../CovidMessage/styles";
import styles from "./styles";
import { useNavigation } from "@react-navigation/core";

const HomeSearch = () =>{

    const navigation = useNavigation();


    const dateSearch = () =>{
       navigation.navigate('DatePick');
    }

    const posttrip = () =>{
       navigation.navigate('Trip');
    }

    return (
            <View style={{backgroundColor:'white', marginBottom: 100}}>

            <Pressable style={styles.row} 
             onPress={posttrip}
            >
                <View style={{width: 100, height: 100}}>
                   <Image
                   source={require('../../../src/assets/top.png')}
                   style={{width: 100, height: 100}}
                    />
                   
                </View>
                
                   <Text style={styles.destinationText}> Set A Trip </Text>
                <View>
                   <Text> Going somewhere and need someone to join you on your trip. Let people book your seats</Text>
                </View>
            </Pressable>

            <Pressable style={styles.row} 
             onPress={dateSearch}
            >
                <View style={{width: 100, height: 100}}>
                   <Image
                   source={require('../../../src/assets/book.png')}
                   style={{width: 100, height: 100}}
                    />
                   
                </View>
                   <Text style={styles.destinationText}> Request A Seat </Text>
                <View>
                   <Text> You're looking to get somewhere and would like to book seat(s) on an available ride ?</Text>
                </View>
            </Pressable>

      


        </View>
    
        
    )
};

export default HomeSearch;