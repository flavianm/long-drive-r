import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
inputBox:{
    backgroundColor:'#e7e7e7',
    margin: 10,
    padding: 10,
    // flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
},
inputText:{
    fontSize: 20,
    fontWeight: '600',
    color: '#6e6e6e',


},
timeContainer:{
flexDirection:'row',
width: 100,
justifyContent:'space-between',
padding:10,
backgroundColor: '#fff',
borderRadius: 50
},
iconContainer:{
    backgroundColor:'#b3b3b3',
    padding: 10,
    borderRadius: 100,
    width: 80,
    height: 80
},
row:{
    // flexDirection: 'row',
    padding: 20,
    alignItems:'center',
    borderBottomWidth: 1,
    borderColor: '#dbdbdb',
    backgroundColor: '#ffffff'
},
destinationText:{
    marginLeft: 10,
    fontWeight: '600',
    fontSize: 21,
    marginTop: 10,
    marginBottom: 10
}
});

export default styles;