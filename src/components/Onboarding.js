import React, {useState, useRef, useEffect} from 'react'
import { FlatList, StyleSheet, Text, View, Animated } from 'react-native';
import slides from './slides';
import OnboardingItem from './OnboardingItem';
import Paginator from './Paginator';
import Nextbutton from './Nextbutton';

import {Auth} from 'aws-amplify';
import {API, graphqlOperation} from 'aws-amplify';
import * as mutations from '../graphql/mutations';
import { getParcelOrder, getParcelRequests, getUser } from '../graphql/queries';



export default Onboarding = () => {

    const [currentIndex, setCurrentIndex] = useState(0);

    const [boarded, setBoarded] = useState(null);
    const [userDet, setUserDet] = useState();
    const [userSubv, setUserSubv] = useState(null);


    const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setUserSubv(userSub)
            // setuser(userSub);
            // console.log(userSub)

            //  const tede = {
            //     id: userSub,
            //     email: userData.attributes.email,
            //     username: userSub,
            //     balance: '0.01'
                
            // }

            //  await API.graphql(graphqlOperation(mutations.createUser, { input: tede}));




            const todoData = await API.graphql(graphqlOperation(getUser, { id: userSub}))
            const todos = todoData.data.getUser
            setUserDet(todos)

            // console.warn(todos?.name)
            
    }

    useEffect(() => {
     fetchUsers()
    }, [])


    const scrollX = useRef(new Animated.Value(0)).current;

    const slidesRef = useRef(null)

    const viewableItemsChanged = useRef(({viewableItems}) => {
        setCurrentIndex(viewableItems[0].index);
    }).current;

    const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 50}).current;

 const onBUpdate = {
        id: userSubv,
        onBoard: 'Yes'
    };


    const blooza = async () =>{
            await API.graphql({ query: mutations.updateUser , variables: {input: onBUpdate}});

    }



    const scrollTo = async () => {

        if(currentIndex < slides.length - 1){
            slidesRef.current.scrollToIndex({index: currentIndex + 1 });
        } else{

            await API.graphql({ query: mutations.updateUser , variables: {input: onBUpdate}});



            try{

            }catch(err){
                console.log('err')
            }

        }

    }



    return (
        <View style={{alignItems: 'center', alignContent: 'center', justifyContent: 'center', flex: 1}}>
          

            <View style={{flex: 3}}>

                  <FlatList 
            data={slides}
             renderItem={({item}) => <OnboardingItem item={item} />} 
            horizontal
            showsHorizontalScrollIndicator={false}
            pagingEnabled
            bounces={false}
            keyExtractor={(item) => item.id}
            onScroll={Animated.event([{nativeEvent: {contentOffset: { x: scrollX}}}], {
                useNativeDriver: false,
            })}
            scrollEventThrottle={32}
            onViewableItemsChanged={viewableItemsChanged}
            viewabilityConfig={viewConfig}
            ref={slidesRef}
            
            />

            

            </View>
            {/* <Paginator data={slides} scrollX={scrollX} /> */}
            <Nextbutton scrollTo={scrollTo} percentage={(currentIndex + 1) * (100/ slides.length)} />
        </View>

    );
};



const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    }
});
