import { Feather, FontAwesome5 } from "@expo/vector-icons";
import React from "react";
import {View, Text, Pressable} from 'react-native';
import HomeMap from "../../components/HomeMap";
import styles from "./styles";
import { useNavigation, useRoute } from "@react-navigation/core";

const CovidMessageGoods = () =>{
    const navigation = useNavigation();

       const warn = () =>{
       console.warn('pressed')
    }

    return (
        <View style={[styles.container, {flexDirection: 'row'}]}>
             {/* cars */}
           <Pressable
           onPress={()=> navigation.navigate('Home')}
           style={{marginLeft: 5, width: 120, height: 40, borderRadius: 10, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                    <View>
                        <FontAwesome5 name='car' size={15} color={'white'}/>
                    </View>
                    <View>
                        <Text style={{color: 'white', fontWeight: '400', fontSize: 15, marginLeft: 10}}>Car Ride</Text>
                    </View>

                </View>
           </Pressable>
            <Pressable
            onPress={()=> navigation.navigate('SecScreenParc')}
            style={{width: 120, height: 40,  borderRadius: 10, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                    <View>
                        <FontAwesome5 name='box-open' size={15} color={'white'}/>
                    </View>
                    <View>
                        <Text style={{color: 'white', fontWeight: '400', fontSize: 15, marginLeft: 10}}>Parcels</Text>
                    </View>

                </View>
           </Pressable >
            <Pressable 
             style={{width: 120, height: 40, backgroundColor: '#ff6f00', borderRadius: 10, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                    <View>
                        <FontAwesome5 name='truck-moving' size={15} color={'white'}/>
                    </View>
                    <View>
                        <Text style={{color: 'white', fontWeight: '400', fontSize: 15, marginLeft: 10}}>Logistics</Text>
                    </View>

                </View>
           </Pressable>


        </View>
    )
};

export default CovidMessageGoods;