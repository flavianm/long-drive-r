import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
        padding: 15,
        alignContent: 'center',
        height: 100

    },
    title:{
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        marginBottom: 10,
        marginTop: 30
    },
    text:{
        color: '#bed9ff',
        fontSize: 15,
        marginBottom: 10,
    },
    learnmore:{
        color: '#fff',
        fontSize: 15,
        fontWeight: 'bold',
    }
});

export default styles;