import React from 'react';
import { View, StyleSheet, Image, TouchableHighlight } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import Swipeable from 'react-native-gesture-handler/Swipeable';

import AppText from './AppText';


const APP_COLORS = {
    primary: '#fc5c65',
    secondary: '#4ecdc4',
    black: '#000',
    white: '#fff',
    medium: '#6e6969',
    light: '#f8f4f4',
    dark : '#0c0c0c',
    danger: '#ff5252'
};

const ListItem = ({ image, title, subTitle, IconComponent, onPress, renderRightActions }) => {
    return (
        <Swipeable friction={2} rightThreshold={20} renderRightActions={renderRightActions}>
            <TouchableHighlight underlayColor={APP_COLORS.light} onPress={onPress}>
                <View style={styles.container}>
                    { IconComponent }
                    { !!image && <Image style={styles.image} source={image} />}
                    <View style={styles.detailsContainer}>
                        <AppText style={styles.title} numberOfLines={1}>{title}</AppText>
                        { !!subTitle && <AppText style={styles.subTitle} numberOfLines={2}>{subTitle}</AppText>}
                    </View>
                    <MaterialCommunityIcons name="chevron-right" color={APP_COLORS.medium} size={25}/>
                </View>
            </TouchableHighlight>
        </Swipeable>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 8,
        // backgroundColor: APP_COLORS.dark,
        backgroundColor: 'white',
        // borderBottomWidth: 0.1,
        // borderColor: 'white',
        // marginLeft: 10,
        // marginRight: 10,
        // borderRadius: 10,
        shadowColor: 'black',
        
       
    },
    detailsContainer: {
        flex: 1,
        justifyContent: 'center',
        marginLeft: 10,
        
        
    },
    image: {
        width: 50,
        height: 50,
        borderRadius: 35,
    },
    subTitle: {
        color: '#d6d6d6',
        fontSize: 13

    },
    title: {
        fontWeight: '500',
        color: '#666666',
        fontSize: 15
    }
});

export default ListItem;