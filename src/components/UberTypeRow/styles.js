import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        // justifyContent: 'space-between',
        padding: 20,
        height: 200,
        margin: 10,
        borderRadius: 10,
        justifyContent: 'space-between'

    },
    image:{
        height: 100,
        width: 100,
        resizeMode: 'contain',
        marginBottom:  -20
        
    },
    middleContainer:{
        marginHorizontal: 10,
        justifyContent: 'space-between'
       
    },
    rightContainer:{
        width: 80,
        justifyContent:'space-between',
    },
    type:{
        fontWeight:'bold',
        fontSize: 16,
        marginBottom: 5,

    },
    time:{
        color: '#5d5d5d'
    }, 
    
    price:{
        fontWeight:'bold',
        fontSize: 16,
        
    }

});

export default styles;