import React from "react";
import {View, Text, Image, Pressable} from 'react-native';
import UberTypes from "../UberTypes";
import HomeMap from "../HomeMap";
import styles from "./styles";
import { Ionicons } from "@expo/vector-icons";


const UberTypeRow = (props) =>{

    // console.log(props);
    const {type, onPress, isSelected} = props;

    const getImage = () => {
        if (type.type === 'Themba'){
            return require('../../assets/images/audi.jpg')
        }
        if (type.type === 'Muzi'){
            return require('../../assets/images/mahir.jpg')
        }
        if (type.type === 'Charles'){
            return require('../../assets/images/merc.jpg')
        }
        return require('../../assets/images/UberXL.jpeg')
    }

    const getUSerImage = ()=>{
        if (type.type === 'Themba'){
            return require('../../assets/images/other.jpg')
        }
        if (type.type === 'Muzi'){
            return require('../../assets/images/muzi.jpg')
        }
        if (type.type === 'Charles'){
            return require('../../assets/images/charles.jpg')
        }
        return require('../../assets/images/other.jpg')
    }

     const getCar = ()=>{
        if (type.type === 'Themba'){
            return (
                'Audi A4'
            )
        }
        if (type.type === 'Muzi'){
            return (
                'Mahindra M4'
            )
        }
        if (type.type === 'Charles'){
            return (
                'Mercedes C200'
            )
        }
        return (
                'Audi A4'
            )
    }

    const getDep= ()=>{
        if (type.type === 'Themba'){
            return (
                '08 : 03'
            )
        }
        if (type.type === 'Muzi'){
            return (
                '09 : 00'
            )
        }
        if (type.type === 'Charles'){
            return (
                 '11 : 00'
            )
        }
        return (
                '11 : 00'
            )
    }

     const getArrive= ()=>{
        if (type.type === 'Themba'){
            return (
                '11 : 30'
            )
        }
        if (type.type === 'Muzi'){
            return (
                '12 : 05'
            )
        }
        if (type.type === 'Charles'){
            return (
                 '13 : 00'
            )
        }
        return (
                '13 : 00'
            )
    }


    return (
        <Pressable  
        onPress={onPress} 
        style={[styles.container, {
            backgroundColor: isSelected? '#efefef': 'white'
        
        }]} >
            {/* image */}
            {/* <Image
            style={styles.image} 
            source={getImage()}
            /> */}

            <View style={{ width: 100}}>
                
                <View style={{flexDirection: 'row', justifyContent:'space-between', alignContent: 'center', alignItems: 'baseline'}}>
                    <Text style={styles.time}>
                            {getDep()}  
                    </Text>

                    <View style={{width: 10, height: 10, borderWidth: 1, borderColor: 'black', borderRadius: 50}}></View>

                </View>
                

                <View style={{flexDirection: 'row', justifyContent:'space-between', alignContent: 'center', alignItems: 'baseline'}}>
                        <Text style={[styles.time, {marginTop: 30}]}>
                        {getArrive()} 
                        </Text>
                        <View style={{width: 10, height: 10, backgroundColor: 'black', borderRadius: 50}}></View>
                </View>
                
                        <View>
                        <Image
                        style={[styles.image, {borderRadius: 100, height: 70, width: 70, marginTop: 35 }]} 
                        source={getUSerImage()}
                        />
                        </View>

            </View>

            <View style={styles.middleContainer}>
                <View>
                            <Text style={styles.time}>
                        24 Mandela Av  
                        </Text>
                        <Text style={[styles.time, {marginTop: 30}]}>
                        7th Hani St
                        </Text>
                </View>

                <View>
                    <View>
                        <Text>
                            {getCar()}
                        </Text>
                    </View>

                <View style={{flexDirection: 'row'}}>
                    <Text style={styles.type}
                        >{type.type}{'  '}  
                        <Ionicons name='person' size={16} />
                        3
                    </Text>
                </View>
                </View>
                
               
                
                
            
            </View>

            <View style={styles.rightContainer}>
                        <Text style={styles.price}>ZAR {type.price}</Text>
                        <Image
                        style={[styles.image, {borderRadius: 10, height: 70, width: 70, marginTop: 35, marginBottom: -2-5 }]} 
                        source={getImage()}
                        />
            </View>

            
        </Pressable>
    )
};

export default UberTypeRow;