import React, {useEffect, useState} from 'react'
import { StyleSheet, Image, Text, View, useWindowDimensions } from 'react-native'
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';
import AnimatedLoader from "react-native-animated-loader";





export default OnboardingItem = ({item}) => {

    let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

   useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
        }, 4000);
    }, [])

    const [visible, setVisible] = useState(true);


    const {width} = useWindowDimensions();

  if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{

    return (
        <View style={[styles.container, {width}]}>
            <Image source={item.image} style={[styles.image, {width, resizeMode: 'contain'}]} />

            <View style={{flex: 0.3}}>
                <Text style={styles.title}>{item.title}</Text>
                <Text style={styles.description}>{item.description}</Text>
            </View>


        </View>
    )
     }
}



const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        flex: 0.5,
        justifyContent: 'center',
        width: 150,
        height: 150
    },
    title: {
        fontWeight: '800',
        fontSize: 28,
        marginBottom: 10,
        color: 'orange',
        textAlign: 'center',
        fontFamily: 'Manrope_800ExtraBold', 
        marginTop: 30
    },
    description: {
        fontWeight: '300',
        color: '#62656b',
        textAlign: 'center',
        paddingHorizontal: 64,
        fontFamily: 'Manrope_500Medium'
    },
})
