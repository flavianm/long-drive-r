import React, {useState, useEffect, useCallback, useMemo, useRef } from 'react'
import { StyleSheet, ActivityIndicator, Text, View, TouchableOpacity, TextComponent, RefreshControl, FlatList, ScrollView, SafeAreaView, Pressable} from 'react-native'
import {Auth} from 'aws-amplify';
import {API, graphqlOperation} from 'aws-amplify';
import { listParcelOrders } from '../../graphql/queries';
import * as queries from '../../graphql/queries';
import { useNavigation, useRoute } from "@react-navigation/core";
import { onParcelOrderUpdated, onRequestsUpdated } from '../../graphql/subscriptions';
import { AntDesign, FontAwesome5, Ionicons, MaterialCommunityIcons, MaterialIcons, Zocial } from '@expo/vector-icons';
import { color } from 'react-native-reanimated';
import AnimatedLoader from "react-native-animated-loader";
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const RidersOrders = () => {

    const navigation = useNavigation();

    // STATES 
const [refreshing, setRefreshing] = useState(false);
  const [user, setuser] = useState(null);
  const [todos, setTodos] = useState([])  
  const [loading, setLoading] = useState('true');
  const [filterTodo, setFilterTodo] = useState([]);
  const [orderId, setOrderId] = useState([]);
  const [getStatus, setGetStatus] = useState([]);
    const [visible, setVisible] = useState(true);

// user information starts


 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });


  useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 3000);
  }, [])




    const onRefresh = useCallback(() => {


    setRefreshing(true);

                fetchUsers();
                 fetchTodos();
    wait(2000).then(() => setRefreshing(false));
  }, []);


  const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setuser(userSub);
            
            
    }

       async function fetchTodos() {
            const userplus = await Auth.currentAuthenticatedUser({bypassCache: true});

            const todoData = await API.graphql({query: queries.listRequests, variables:  {filter : { userId: {eq: 
                ( userplus.attributes.sub)
            }}} })
            const todos = todoData.data.listRequests.items
            setTodos(todos)
            setLoading('false')

            console.log(todos)
     }

     useEffect(() => {
            
                fetchUsers();
                fetchTodos();
            },[])
         
            const ItemView2 = ({item}) => {

                return(

              <Pressable style={{ backgroundColor: 'white'}} onPress={() => 
                    navigation.navigate('RidesOrderInfoCust',{driverId: item.driverId,  price: item.tripFee,  originSec: item.originDesc, destSec: item.destDesc,  type: item.seats, id: item.id, thestatus: item.status, date: item.startDate, luggageSize: item.luggage })
                }>

                    <View style={{flexDirection: 'row'}}>

                    </View>

                    <View style={{margin: 10}}>

                     <View style={{marginLeft: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                                
                                     <View style={{marginRight: 10}}>
                                        <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'green'}}/>
                                     </View>

                                    <Text style={{fontWeight: '500', marginLeft: 10, width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 13}}>{item.originDesc || `Loading...`} </Text>
                             </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                <View style={{marginRight: 10}}>
                                    <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'red'}}/>
                                </View>
                                <Text style={{fontWeight: '500', marginLeft: 10 , width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 13}}>{item.destDesc || `Loading...`}</Text>
                            </View>
                    </View>


                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name="date-range" size={13} color="white" />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.startDate}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>R {item.tripFee || `0.00`} Trip Total</Text>
                        </View>

                    </View>


                     <View style={{flexDirection: 'row', marginBottom: 10}}>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 7}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.seats || `0`} Seats </Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.status || `Loading...`}</Text>
                        </View>

                    </View>

                    </View>
                   
                    </Pressable>


              
                 )
            }

        const ItemSeperatorView = () => {
                return(
                    <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}}
                    />
                )
            }

if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{

    return (
        <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
            
          <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
            
        <ScrollView
        style={{backgroundColor: 'white', padding: 5}}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
        >

         <View style={{flexDirection: 'row'}}>
                    <Text style={{ paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30, fontFamily: 'Manrope_700Bold'}}>My Trips</Text>

            </View>

            <View 
                style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
            />

              <View>
                <Text style={{fontWeight: '500', marginLeft: 20, fontFamily: 'Manrope_700Bold'}}>TRIPS</Text>
            </View>

            <View 
                style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
            />

             <FlatList
                    data={todos}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView2}
                /> 

         
        </ScrollView>
        </SafeAreaView>
    )
        }
}

export default RidersOrders

const styles = StyleSheet.create({

    container: { flex: 1, justifyContent: 'center', padding: 20 },
  todo: {  backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20},
  input: { height: 50, backgroundColor: '#ddd', marginBottom: 10, padding: 8 },
  todoName: { fontSize: 15 },

})
