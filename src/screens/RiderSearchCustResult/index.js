import React, {useState, useEffect, useCallback, useMemo, useRef } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, TextComponent, FlatList, Pressable, ScrollView, Image, SafeAreaView, ActivityIndicator} from 'react-native'
import {Auth} from 'aws-amplify';
import {API, graphqlOperation} from 'aws-amplify';
import { listParcelOrders } from '../../graphql/queries';
import * as queries from '../../graphql/queries';
import { useNavigation, useRoute } from "@react-navigation/core";
import { onParcelOrderUpdated, onRidesOrderUpdated } from '../../graphql/subscriptions';
import { Feather, FontAwesome, FontAwesome5, Ionicons, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { getUser } from '../../graphql/queries';
import {Font} from 'expo';
import AnimatedLoader from "react-native-animated-loader";
import LottieView from 'lottie-react-native';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const RiderSearchCustResult = () => {

  
    let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

     


    const [visible, setVisible] = useState(true);


     useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 5000);
  }, [])


    const navigation = useNavigation();
    const route = useRoute();
    const {originPlace, destinationPlace, destDesc, originDesc, originSec, destSec, datee, stsa} = route.params

   const haversine = require('haversine')

const [loading, setLoading] = useState('true');
   const onRefresh = useCallback(() => {

    setRefreshing(true);

                 fetchUsers();
                fetchTodos();
           


    wait(2000).then(() => setRefreshing(false));
  }, []);

    // STATES 

  const [user, setuser] = useState(null);
  const [todos, setTodos] = useState([])  
  const [filterTodo, setFilterTodo] = useState([]);
  const [orderId, setOrderId] = useState([]);
  const [getStatus, setGetStatus] = useState([]);
  const [userDa, setUserDa] = useState();
  const [showFilter, setShowFilter] = useState(false);
  const [radius, setRadius] = useState('30');
  const [noSeats, setNoSeats] = useState('3');
  const [sts, setSts] = useState(stsa);
  const [date, setDate] = useState(datee);
  const [orKm, setOrKm] = useState(5);
  const [desKm, setDesKm] = useState(5);




// user information starts

  const fetchUsers = async() =>{

 
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setuser(userSub);


             const todoData = await API.graphql(graphqlOperation(getUser, { id: userSub}));
             const todos = todoData.data.getUser;
            setUserDa(todos)
            
            
    }


    async function fetchTodos() {

            const userplus = await Auth.currentAuthenticatedUser({bypassCache: true});

            const todoData = await API.graphql({query: queries.listRides, variables:  
                {filter :   { status: 
                        {eq: 
                            'driver'
                        },
                        }
                   
                 }
                
        })
            const todos = todoData.data.listRides.items
            setTodos(todos)

            console.log(todos)
             setLoading('false')

     }






     useEffect(() => {
            
                fetchUsers();
                fetchTodos();
            },[todos])
            // END USER INFORMATION

            // SUBSCRIPTION START

           


 let subsUpdate;
 function setUpSus(){
   

     subsUpdate = API.graphql(graphqlOperation(onRidesOrderUpdated)).subscribe( {next: (daraa) => {
         setTodos(daraa)
     }, }) 

 }

  useEffect(() =>{
        setUpSus();

        return() =>{
            subsUpdate.unsubscribe();
        };

    },[]);


const SeatssOp = () => {


if (noSeats === '1'){

    if(sts === '0'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>
     
        </View>
      )
    }


    if(sts === '1'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>
        </View>
      )
    }


}

if (noSeats === '2'){

    if(sts === '0'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

     
        </View>
      )
    }




    if(sts === '1'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
          </TouchableOpacity>

        </View>
      )
    }
    
    if(sts === '2'){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange"/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>
        </View>
      )
    }
}

if (noSeats === '3'){

   if(sts === '0'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        {/* thr */}
          <TouchableOpacity onPress={()=> setSts('3')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>


        </View>
      )
    }
   
   if(sts === '1'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey"  style={{marginRight: 30}}/>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=> setSts('3')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        </View>
      )
    }
    
    if(sts === '2'){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>

          <TouchableOpacity onPress={()=> setSts('3')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>
        </View>
      )
    }
   
    if(sts === '3'){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        {/* thr */}
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>


        </View>
      )
    }

}


  }

  const OrRadOp = () => {

      
   if(orKm === 5){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
             <Text style={{marginRight: 30, color: 'orange', fontFamily: 'Manrope_500Medium'}}>5 KM</Text>
          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setOrKm(15)} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <Text style={{marginRight: 30, fontFamily: 'Manrope_500Medium'}}>15 KM</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=> setOrKm(30)} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_500Medium'}}>30 KM</Text>
          </TouchableOpacity>

        </View>
      )
    }
    
    if(orKm === 15){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setOrKm(5)} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <Text style={{marginRight: 30, color: 'grey', fontFamily: 'Manrope_500Medium'}}>5 KM</Text>
          </TouchableOpacity>

          {/* tw */}
         <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
             <Text style={{ color: 'orange', fontFamily: 'Manrope_500Medium', marginRight: 30}}>15 KM</Text>
          </View>

         <TouchableOpacity onPress={()=> setOrKm(30)} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{color: 'grey', fontFamily: 'Manrope_500Medium'}}>30 KM</Text>
          </TouchableOpacity>
        </View>
      )
    }
   
    if(orKm === 30){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setOrKm(5)} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <Text style={{marginRight: 30, color: 'grey', fontFamily: 'Manrope_500Medium'}}>5 KM</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=> setOrKm(15)} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <Text style={{color: 'grey', marginRight: 30, fontFamily: 'Manrope_500Medium', marginRight: 30}}>15 KM</Text>
          </TouchableOpacity>

          {/* tw */}
         <View  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
             <Text style={{ color: 'orange', fontFamily: 'Manrope_500Medium'}}>30 KM</Text>
          </View>

         
        </View>
      )
    }




  }

  const destRadOp = () => {

      
   if(desKm === 5){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
             <Text style={{marginRight: 30, color: 'orange', fontFamily: 'Manrope_500Medium'}}>5 KM</Text>
          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setDesKm(15)} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <Text style={{marginRight: 30, fontFamily: 'Manrope_500Medium'}}>15 KM</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=> setDesKm(30)} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_500Medium'}}>30 KM</Text>
          </TouchableOpacity>

        </View>
      )
    }
    
    if(desKm === 15){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setDesKm(5)} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <Text style={{marginRight: 30, color: 'grey', fontFamily: 'Manrope_500Medium'}}>5 KM</Text>
          </TouchableOpacity>

          {/* tw */}
         <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
             <Text style={{ color: 'orange', fontFamily: 'Manrope_500Medium', marginRight: 30}}>15 KM</Text>
          </View>

         <TouchableOpacity onPress={()=> setDesKm(30)} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{color: 'grey', fontFamily: 'Manrope_500Medium'}}>30 KM</Text>
          </TouchableOpacity>
        </View>
      )
    }
   
    if(desKm === 30){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setDesKm(5)} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <Text style={{marginRight: 30, color: 'grey', fontFamily: 'Manrope_500Medium'}}>5 KM</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=> setDesKm(15)} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <Text style={{color: 'grey', marginRight: 30, fontFamily: 'Manrope_500Medium', marginRight: 30}}>15 KM</Text>
          </TouchableOpacity>

          {/* tw */}
         <View  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
             <Text style={{ color: 'orange', fontFamily: 'Manrope_500Medium'}}>30 KM</Text>
          </View>

         
        </View>
      )
    }




  }




    
        
        if(showFilter === true){
            return(
                // <View style={{backgroundColor: 'white', flex: 1, margin: 25}}> 
                //     <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>

                //         <TouchableOpacity onPress={()=> setShowFilter(false)}>
                //             <MaterialIcons name="arrow-back-ios" size={22} color="black" />
                //         </TouchableOpacity>

                //         <View>
                //             <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20}}>Filter Results</Text>
                //         </View>
                    
                //     </View>

                //     <Text>FILTER HERE</Text>
                
                
                // </View>
                <View style={{backgroundColor: 'white', flex: 1}}>

                <View style={{margin: 10, marginTop: 25, backgroundColor: 'white'}}>


                   

                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <TouchableOpacity onPress={()=> setShowFilter(false)}>
                                <MaterialIcons name="arrow-back-ios" size={22} color="black" />
                            </TouchableOpacity>

                            
                            
                            
                            <View>
                                <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20}}>Filter Results</Text>
                            </View>
                        </View>


                   <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>


                        <View style={{margin: 10}}>
                        

                            <Text style={{marginTop: 15, fontFamily: 'Manrope_500Medium'}}>Distance From You</Text>
                            <View style={{marginTop: 10}}>
                                 {OrRadOp()}
                            </View>

                           

                            <Text style={{marginTop: 15, fontFamily: 'Manrope_500Medium'}}>Distance To Where You Are Going</Text>
                                <View style={{marginTop: 10}}>
                                    {destRadOp()}
                                </View>
                                
                            <Text style={{marginTop: 15, fontFamily: 'Manrope_500Medium'}}>Seats</Text>
                            <View style={{marginTop: 10}}>
                                {SeatssOp()}
                            </View>

                            <Text style={{marginTop: 10, fontFamily: 'Manrope_500Medium'}}>Date</Text>

                            <View>
                                <View style={{alignContent: 'center', alignItems: 'center', justifyContent: 'center', marginTop: 30}}>
                        <Text>{date || `Date`}</Text>
                        </View>


                        <View style={{margin: 20}}>
                        <Calendar
        // Initially visible month. Default = Date()
        current={Date()}
        // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
        minDate={Date()}
        // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
        maxDate={'2022-05-30'}
        // Handler which gets executed on day press. Default = undefined
        //   onDayPress={(day) => {console.log('selected day', day)}}
        //   onDayPress={(day) => setDate(day.dateString)}
        onDayPress={(day) =>  {setDate(day.dateString)}}
        // Handler which gets executed on day long press. Default = undefined
        onDayLongPress={(day) => {console.log('selected day', day)}}
        // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
        monthFormat={'yyyy MM'}
        // Handler which gets executed when visible month changes in calendar. Default = undefined
        onMonthChange={(month) => {console.log('month changed', month)}}
        // Hide month navigation arrows. Default = false
        hideArrows={false}
        // Replace default arrows with custom ones (direction can be 'left' or 'right')
        //   renderArrow={(direction) => (<Arrow/>)}
        // Do not show days of other months in month page. Default = false
        hideExtraDays={true}
        // If hideArrows = false and hideExtraDays = false do not switch month when tapping on greyed out
        // day from another month that is visible in calendar page. Default = false
        disableMonthChange={true}
        // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday
        firstDay={1}
        // Hide day names. Default = false
        hideDayNames={true}
        // Show week numbers to the left. Default = false
        showWeekNumbers={true}
        // Handler which gets executed when press arrow icon left. It receive a callback can go back month
        onPressArrowLeft={subtractMonth => subtractMonth()}
        // Handler which gets executed when press arrow icon right. It receive a callback can go next month
        onPressArrowRight={addMonth => addMonth()}
        // Disable left arrow. Default = false
        disableArrowLeft={false}
        // Disable right arrow. Default = false
        disableArrowRight={false}
        // Disable all touch events for disabled days. can be override with disableTouchEvent in markedDates
        disableAllTouchEventsForDisabledDays={true}
        // Replace default month and year title with custom one. the function receive a date as parameter
        renderHeader={(date) => {(date.dateString)}}
        // Enable the option to swipe between months. Default = false
        enableSwipeMonths={true}
        //   markedDates={
        
        //     {
        //             better: {selected: true, marked: true, selectedColor: 'blue'},
        //   }}
        />
                    </View>
                            </View>

                        </View>
                </ScrollView>

            </View>
            </View>

            )
        }

    



    const theImage = 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/images/products/keyboard1.jpg'

     const ItemView= ({item}) => {

                // driver locations
                const start = {
                 latitude: item?.originLatitude,
                 longitude: item?.originLongitude   
                }
                
                const end = {
                    latitude: item?.destLatitude,
                 longitude: item?.destLongitude 
                }


                const startUser = {
                    latitude: originPlace?.details.geometry.location.lat,
                    longitude: originPlace?.details.geometry.location.lng
                }

                const endUser = {
                    latitude: destinationPlace?.details.geometry.location.lat,
                    longitude: destinationPlace?.details.geometry.location.lng
                }

                console.log(destinationPlace?.details.geometry.location.lat)

                const blu = haversine(startUser, start).toFixed(2)
                const toEnd =  haversine(endUser, end).toFixed(2)
                const seata = item?.availSeats
                const datea = item?.startDate

                if ((blu <= orKm && toEnd <= desKm) && (sts <= seata) && datea === date ) {

                   
                        return(
                  
                    <TouchableOpacity style={{ backgroundColor: 'white'}} onPress={() => 
                    navigation.navigate('RidesOrderCustSearchInfo',{id: item.id, blu, toEnd, dName: item.name, dSurname: item.surname,  imageUrl: item.imageUrl, carName: item.carName, model: item.model, carImage: item.carImage, thestatus: item.status, orSec: item.originSec, desSec: item.destSec, datedate: item.startDate, originPlace, destinationPlace, destDesc, originDesc, price: item.price, totalSeats: item.totalSeats, availSeats: item.availSeats, willTaxi: item.willTaxi, willTaxiPrice: item.willTaxiPrice, luggageMax: item.luggageMax, message: item.message, driverId: item.driverId, startDate: item.startDate })
                    }>


                    <View style={{margin: 10}}>

                         <View style={{ flexDirection: 'row', alignContent: 'center', alignItems: 'center', marginLeft: 10}}>

                    {/* image */}

                    <View>
                            <View style={{height: 50, marginRight: 20, justifyContent: 'space-around'}} >
                                <Image
                                    source={{uri: item.imageUrl}}
                                    style={{width: 40, height: 40, borderRadius: 10}}
                                />
                            </View>
                    </View>


                    {/* driven by */}
                    <View>
                        
                        <Text style={{fontWeight: '500', fontSize: 13, fontFamily: 'Manrope_600SemiBold'}}>{item.name || `Loading...`} {item.surname}</Text>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                            <FontAwesome5 name='car' color={'grey'} size={15} />
                            <Text style={{marginLeft: 10, color: 'grey', fontSize: 12, fontFamily: 'Manrope_500Medium'}}> {item.carName}, {item.model}  </Text>
                        </View>
                     
                        

                    </View>

                   </View>

                

                     <View style={{marginLeft: 10, marginTop: 10}}>
                         <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 0}}>
                                
                                     <View style={{marginRight: 10}}>
                                        <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'green'}}/>
                                     </View>

                                    <Text style={{fontWeight: '500', marginLeft: 10, width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>{item.originSec || `Loading...`} </Text>
                             </View>
                           

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                <View style={{marginRight: 10}}>
                                    <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'red'}}/>
                                </View>
                                <Text style={{fontWeight: '500', marginLeft: 10 , width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>{item.destSec || `Loading...`}</Text>
                            </View>
                    </View>


                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name="date-range" size={13} color="white" />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 11, fontFamily: 'Manrope_500Medium'}}>{item.startDate}</Text>
                        </View>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 11, fontFamily: 'Manrope_500Medium'}}>R {item.price || `0.00`} Per Seat</Text>
                        </View>

                    </View>

                     <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <FontAwesome5 name="car" size={13} color="white" />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 11, fontFamily: 'Manrope_500Medium'}}>{blu}KM Away</Text>
                        </View>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 11, fontFamily: 'Manrope_500Medium'}}>{item.availSeats} of {item.totalSeats} Avail. Seats</Text>
                        </View>

                    </View>

                    
                    
                   
                   
                    </View>


                   
                    </TouchableOpacity>



                        )
                    
                }

               
            };

    const ItemSeperatorView = () => {
                return(
                    <View>
                    <View style={{height: 0.1, width: '100%', backgroundColor: '#c8c8c8', marginBottom: 10}}
                    />
                    </View>
                )
            };

             if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{

    return (
        <SafeAreaView style={{ backgroundColor: 'white', flex: 1}}>
            
{/* 
        <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader> */}




             <ScrollView style={{backgroundColor: 'white', padding: 5, flex: 1}}
                 showsHorizontalScrollIndicator={false}
                 showsVerticalScrollIndicator={false}>
                     


             <View style={{flexDirection: 'row'}}>
                    <Text style={{ paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30, fontFamily: 'Manrope_700Bold'}}>Search Results</Text>

            </View>
            
                     <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />
                  
                <View style={{ justifyContent: 'space-around', backgroundColor: 'white', alignContent: 'center', alignItems: 'center',  height: 50, width: '100%',  flexDirection: 'row'}}>
                   {/* location left  */}
                    <View style={{width: 140}}>
                        <Text style={{fontSize: 10, fontFamily: 'Manrope_400Regular'}}>
                            {originDesc}
                        </Text>
                    </View>
                   {/* centre arrow */}
                   <View>
                     {/* <MaterialIcons name='arrow-forward-ios' size={20} color={'black'} style={{paddingRight: 10}}/> */}
                <LottieView style={{width: 50, height: 50, marginLeft: -3}} source={require('../../assets/warning/11515-swipe-right-arrows.json')} autoPlay loop />

                   </View>
                   

                   {/* location right */}
                        <Text style={{width: 140, fontSize: 10, fontFamily: 'Manrope_400Regular'}}>
                            {destDesc}
                        </Text>
                </View>

                
                     <View style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

                     <View style={{flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20, marginRight: 20}}>
                        <Text style={{fontWeight: '500', fontFamily: 'Manrope_600SemiBold'}}>Trips</Text>

                        <TouchableOpacity onPress={()=> setShowFilter(true)} style={{flexDirection: 'row'}}>
                            <Ionicons name="filter" size={20} color="black" />
                            <Text style={{marginLeft: 10}}>Filter</Text>
                        </TouchableOpacity>
                    
                    </View>


                    <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />



                <FlatList
                    data={todos}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView}
                />

            <View>

                <View style={{alignItems: 'center', marginTop: 10}}>
                    <Text style={{fontFamily: 'Manrope_400Regular', fontSize: 12}}>- No More Trips -</Text>
                    
                </View>
            </View>

             </ScrollView>

        </SafeAreaView>
    )
        }
}

export default RiderSearchCustResult

const styles = StyleSheet.create({

    container: { flex: 1, justifyContent: 'center', padding: 20 },
  todo: {  backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20},
  input: { height: 50, backgroundColor: '#ddd', marginBottom: 10, padding: 8 },
  todoName: { fontSize: 15 },

})
