import * as React from 'react';
import { FlatList, Pressable, StyleSheet, Image } from 'react-native';

// import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../../components/Themed';
import ListItem from '../../components/ListItem';
import AppIcon from '../../components/AppIcon';
import AppScreen from '../../components/AppScreen';

import { useNavigation } from "@react-navigation/core";


import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const APP_COLORS = {
    primary: '#fc5c65',
    secondary: '#4ecdc4',
    black: '#000',
    white: '#fff',
    dark : '#0c0c0c',
};

const ListItemSeperator = () => {
    return (
        <View style={styles.seperator}></View>
    );
};



const MENU_ITEMS = [
    {
        title: 'Create Trip',
        icon: {
            name: 'plus',
            backgroundColor: APP_COLORS.primary,
        },
        targetScreen: 'ParcelDestDriver',
    },
    // {
    //     title: 'Search Orders',
    //     icon: {
    //         name: 'map-search-outline',
    //         backgroundColor: APP_COLORS.primary,
    //     },
    //     targetScreen: 'ParcelDSFR',
    // },
    // {
    //     title: 'My Posted Orders',
    //     icon: {
    //         name: 'format-list-bulleted',
    //         backgroundColor: APP_COLORS.primary,
    //     },
    //     targetScreen: 'ParcelOrdersDriver',
        
    // },
    // {
    //     title: 'Active Orders',
    //     icon: {
    //         name: 'car-connected',
    //         backgroundColor: APP_COLORS.primary,
    //     },
    //     targetScreen: 'PDAO',
    // },
   
];
export default function DriverMore() {

      let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });


   const navigation = useNavigation();

   if (!fontsLoaded) {
            return(
                <View>
                  <Text style={{alignContent: 'center', alignItems: 'center', fontFamily: 'Manrope_600SemiBold'}}>Loading...</Text>
                </View>
            )
        } else{

  return (
  
    <AppScreen style={styles.screen}>
      

            <View style={styles.container}>
                <FlatList
                    data={MENU_ITEMS}
                    keyExtractor={item => item.title}
                    ItemSeparatorComponent={ListItemSeperator}
                    renderItem={({ item }) => {
                        return (
                            <ListItem
                                title={item.title}
                                IconComponent={<AppIcon name={item.icon.name} backgroundColor={item.icon.backgroundColor}/>}
                                onPress={() => {
                                    navigation.navigate(item.targetScreen)}}
                            />
                        );
                    }}
                />
            </View>

    </AppScreen>


  );
                }
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 10,
        borderRadius: 15,
        backgroundColor:'white'
    },
   seperator: {
        width: '100%',
        height: 1,
        color: 'white',
        backgroundColor: '#d6d6d6'

    },
      screen: {
        // borderRadius: 15,
        backgroundColor: 'white'
    }
});
