import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container:{
        padding: 10,
        height:'100%',
        
    },

    textInput:{
    backgroundColor:'#e7e7e7',
    margin: 10,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 42
    
},
row:{
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10
},
iconContainer:{
    backgroundColor: '#a2a2a2',
    padding: 5,
    borderRadius: 50,
    marginRight: 15

},
locationText:{

},
separator:{
    backgroundColor: '#efefef',
    height: 1
      },
listView:{
    position: 'absolute',
    top: 123,
    },
 autoCompleteContainer:{
    position: 'absolute',
    top: 30,
    left: 10,
    right: 10,
},
circle:{
    width: 5,
    height: 5,
    backgroundColor: 'black',
    position: 'absolute',
    top: 60,
    left: 22,
    borderRadius: 5
},
line: {
    height: 51,
    width: 2,
    backgroundColor: '#919191',
    position: 'absolute',
    top: 67,
    left: 23.5,
},
square:{
    width: 5,
    height: 5,
    backgroundColor: 'black',
    position: 'absolute',
    top: 120,
    left: 22

}

});

export default styles;