import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, ScrollView, SafeAreaView, Image } from 'react-native'
import { useNavigation, useRoute } from "@react-navigation/core";
import * as mutations from '../../graphql/mutations'
import { FontAwesome, FontAwesome5, Fontisto, Ionicons, MaterialCommunityIcons, MaterialIcons, Zocial } from '@expo/vector-icons';
import { getUser } from '../../graphql/queries';
import { API, graphqlOperation } from "aws-amplify";
import Toast from 'react-native-toast-message';
import AnimatedLoader from "react-native-animated-loader";

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const RidesOrderInfoCust = (props) => {

const navigation = useNavigation();

let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

  useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 3000);
  }, [])


    const typeState = useState(null);
    const route = useRoute();
        const [status, setStatus] = useState([]);
        const [driver, setDriver] = useState([]);
        const [loading, setLoading] = useState('true');
    const [visible, setVisible] = useState(true);

    const { driverId, id, thestatus, type, originSec, contents, destSec, date, luggageSize, price } = route.params


 async function fetchDriver() {

            const todoData = await API.graphql(graphqlOperation(getUser, { id: driverId}))
              const todos = todoData.data.getUser
            setDriver(todos)
            console.log(todos)
            setLoading('false')
        
     }



    useEffect(() => {
                fetchDriver();
                setStatus(thestatus)
            },[])


 const restart = {
        id: id,
        status: 'looking',
        active: 'no'
    };

    const collect = {
        id: id,
        status: 'Collect'
    };

    const delivered = {
        id: id,

        status: 'Delivered'
    };

            const whenCancelled = async ()=>{
           await API.graphql({ query: mutations.updateGoodsOrder, variables: {input: restart}});
           setStatus('looking')

           Toast.show({
      type: 'success',
      text1: 'Re-Post',
      text2: 'Your trip has been successfully re-posted your trip',
      visibilityTime: 5000,

    });     
           navigation.navigate('GoodsOrders')

        }

        const whenCollect = async ()=>{
           await API.graphql({ query: mutations.updateGoodsOrder, variables: {input: restart}});
           setStatus('uCancel')
           navigation.navigate('RidersOrders')
           // write logic to deduct for cancelation + 50% of order, pay driver 25% of order fee
        console.log('done')
        }

        const whenLooking = async ()=>{
           await API.graphql({ query: mutations.updateGoodsOrder, variables: {input: restart}});
           setStatus('cancelled')
           Toast.show({
      type: 'success',
      text1: 'Logistics',
      text2: 'Your trip has been successfully cancelled',
      visibilityTime: 5000,

    });
           navigation.navigate('RidersOrders')
           // no cancellation fee. write logic to delete order
        console.log('done')
        }

         const whenAccepted = async ()=>{
           await API.graphql({ query: mutations.updateGoodsOrder, variables: {input: restart}});
           setStatus('cancelled')
           Toast.show({
      type: 'success',
      text1: 'Logistics',
      text2: 'Your trip has been successfully cancelled, 50% will be refunded',
      visibilityTime: 5000,

    });
           navigation.navigate('RidersOrders')
           // write logic to deduct amount for cancelation only
        console.log('done')
        }
    


     const StatusStatus = () =>{

            
             if(status === 'Accepted'){
            return(
                 <TouchableOpacity onPress={
                 ()=>
                 whenAccepted()
                } 
                style={{margin: 15, backgroundColor: 'red', height: 35, width: 130, borderRadius: 10, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}> 
                 <Text style={{fontSize: 15, fontWeight: '500', color: 'white'}}>
                    Cancel Trip
                   </Text>
                </TouchableOpacity>

            )
        }
            
            

         if(status === 'cancelled'){
            return(
               <TouchableOpacity onPress={
                 ()=>
                 whenCancelled()
                } 
                style={{margin: 15, backgroundColor: 'green', height: 35, width: 130, borderRadius: 10, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}> 
                 <Text style={{fontSize: 15, fontWeight: '500', color: 'white'}}>
                    RE-BOOK
                   </Text>
                </TouchableOpacity>
                )}

                if(status === 'pending'){
            return(
               <TouchableOpacity onPress={
                 ()=>
                 whenLooking()
                } 
                style={{margin: 15, backgroundColor: 'red', height: 35, width: 130, borderRadius: 10, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}> 
                 <Text style={{fontSize: 15, fontWeight: '500', color: 'white'}}>
                    Cancel Order
                   </Text>
                </TouchableOpacity>
                )}

        }

        if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{


    return (
         <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>

    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>

       
        <ScrollView style={{marginTop: 20}}
         showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
         style={{backgroundColor: 'white'}}
        
        >
            <View style={{flexDirection: 'row'}}>
                    <Text style={{ paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30}}>Trip Details</Text>

            </View>


            <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />


              <View style={{ backgroundColor: 'white', borderRadius: 10, padding: 20}}>

             <View>
                    <Text style={{fontWeight: '500'}}>ROUTE</Text>
            </View>


                     <View>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10, backgroundColor:'green', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{ fontWeight: '500', width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 13}}>{originSec || `Loading...`}</Text>
                                </View>
                            </View>
                     </View>

                    <View>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10,backgroundColor:'red', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{fontWeight: '500', width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 13}}>{destSec || `Loading...`}</Text>
                                </View>
                            </View>
                     </View>

                    <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

                <View>
                    <Text style={{fontWeight: '500', fontFamily: 'Manrope_600SemiBold', fontFamily: 'Manrope_600SemiBold'}}>TRIP ITINERARY</Text>
                </View>

                  <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Fontisto name='date' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{date}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='shopping-bag' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{luggageSize || `Small`}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>R{ (parseFloat(price) + (parseFloat(price) *  parseFloat('0.035') )).toFixed(2)  || `0.00`}</Text>
                        </View>

                        

                    </View>


                <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{type} Seats</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{ marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{status}</Text>
                        </View>

                
                    </View>


                       <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

                <View>
                    <Text style={{fontWeight: '500', fontFamily: 'Manrope_600SemiBold'}}>DRIVER</Text>
                </View>  

                
                 <View style={{ flexDirection: 'row', marginLeft: 20, alignContent: 'center', alignItems: 'center'}}>

                        <View>
                                <View style={{height: 100, marginRight: 20, justifyContent: 'space-around'}} >
                                    <Image
                                        source={{uri: driver?.imageUrl}}
                                        style={{width: 50, height: 50, borderRadius: 10}}
                                    />
                                </View>
                        </View>

                        <View>
                                
                                <Text style={{fontWeight: '500', fontSize: 15, fontFamily: 'Manrope_600SemiBold'}}>{driver?.name || `Loading`} {driver?.surname || `... `}</Text>
                                <View style={{flexDirection: 'row', marginTop: 5, alignContent: 'center', alignItems: 'center'}}>
                                    <FontAwesome5 name='car' color='grey' size={15} />
                                    <Text style={{marginLeft: 10, color: 'grey', fontSize: 12, fontFamily: 'Manrope_500Medium'}}> {driver?.carName}, {driver?.model}</Text>
                                </View>
                                <View style={{flexDirection: 'row', marginTop: 5, alignContent: 'center', alignItems: 'center'}}>
                                    <MaterialCommunityIcons name='gender-male-female' color={'grey'} size={15} />
                                    <Text style={{marginLeft: 10, color: 'grey', fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{driver?.gender}</Text>
                                </View>
                                

                            </View>

            </View>    
                       <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 20}} />


             {/* {StatusStatus()} */}


         </View>





            <View style={{margin: 15, backgroundColor: 'white', borderRadius: 10, padding: 20}}>
               

              

            </View>
            
                   {/* {StatusStatus()} */}
        </ScrollView>
    
          </SafeAreaView>
    )
        }
}

export default RidesOrderInfoCust

const styles = StyleSheet.create({})
