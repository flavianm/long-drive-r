import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const OptionScreen = () => {
    return (
        <View style={styles.a} >
            <Text>Option Screen</Text>
        </View>
    )
}

export default OptionScreen

const styles = StyleSheet.create({
    a:{
        fontSize: 10,
        alignContent: 'center',
        alignItems: 'center'
    }
})
