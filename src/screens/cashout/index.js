import React, {useState, useEffect  } from 'react'
import {View, Text, FlatList, Pressable, StyleSheet, Image, TextInput, SafeAreaView, TouchableOpacity} from 'react-native'
import {Auth} from 'aws-amplify';
import { getUser } from '../../graphql/queries';
import { API, graphqlOperation } from "aws-amplify";
import { updateUser, createCashOut } from '../../graphql/mutations';
import Modal from "react-native-modal";
import { useNavigation, useRoute } from "@react-navigation/core";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';


const cashout = () => {


        const route = useRoute();

     const navigation = useNavigation();

 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

        const {userId} = route.params


    const initialState = {id: userId , bank: '', account: '', branch: ''}
    const [formState, setFormState] = useState(initialState)
const [todos, setTodos] = useState([])
const [userDetails, setUserDetails] = useState([]);
const [balancebf, setBalancebf] = useState('0');
const [amDed, setAmDed] = useState('0')


function setInput(key, value) {
    setFormState({ ...formState, [key]: value })
  }

const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser({bypassCache: true});
            const userSub = userData.attributes;
            const userId = userSub.sub;

            const todoData = await API.graphql(graphqlOperation(getUser, { id: userId}))
            const todos = todoData.data.getUser
            setUserDetails(todos)
            setBalancebf(todos.balance)
            // console.log('info ::' + todos.balance)

          
            
    }

useEffect(() => {
        fetchUsers();
       
    }, [])



              async function updateTodo() {


const userData = await Auth.currentAuthenticatedUser();
        const userSub = userData.attributes.sub;
        const userDee = userData.attributes;


        const deduction = {
            id: userSub,
            balance: parseFloat(balancebf) - parseFloat(amDed)

        }
        
        const newInp = {
            name: userDee.name,
            surname: userDee.surname,
            email: userDee.email,
            userId: userSub,
            amount: amDed,
            progress: 'pending'
        }

    try {
      const todo = { ...formState }
      console.log('1')
      setTodos([...todos, todo])
       console.log('2')
      setFormState(initialState)
       console.log('3')


        if(balancebf > amDed){
      await API.graphql(graphqlOperation(updateUser, {input: deduction}));

        } else{
           Toast.show({
      type: 'error',
      text1: 'Cash Out',
      text2: 'Please enter an amount less than ' + 'R' + parseFloat(balancebf).toFixed(0)
    });
    return;
        }

      await API.graphql(graphqlOperation(updateUser, {input: deduction}));
      await API.graphql(graphqlOperation(createCashOut, {input: newInp}));  

    //    await API.graphql(graphqlOperation(updateUser, todo));
      
      Toast.show({
      type: 'success',
      text1: 'Cash Out',
      text2: 'You have submitted your cashout request. EFT payment can take upto 72Hours'
    });


      navigation.navigate('home');
    } catch (err) {
      console.log('error update User:', err)
      Toast.show({
      type: 'error',
      text1: 'Cash Out',
      text2: 'Failed to cash out'
    });
    }

  }

  

    

 


    return (
      <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>
        <View>
             <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black', marginTop: 30, marginBottom: 15, fontFamily: 'Manrope_700Bold'}}>
               Cash Out
                </Text>

                <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

           {/* <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black'}}>{user.email}</Text> */}
                 <View style={{ marginTop: 20, marginBottom: 10, justifyContent: 'center', alignContent: 'center', alignItems: 'center', flexDirection: 'row' }}>

               
                <View style={{ marginBottom: 10, justifyContent: 'center'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontSize: 13, marginLeft: 20, marginBottom: 5, fontFamily: 'Manrope_500Medium'}} >Amount</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium'}}
                    onChangeText={val => setAmDed(val)}
                    // value={formState.bank}
                    keyboardType='numeric'
                    placeholder='Enter Amount'
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

                
                

            </View>

           

               
            </View>


             <View style={{marginTop: 10, marginLeft: 20, marginRight: 20}}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={{fontFamily: 'Manrope_600SemiBold'}}>Current Balance</Text>
                <Text style={{fontFamily: 'Manrope_500Medium'}}>R{parseFloat(balancebf).toFixed(2)}</Text>
              </View>

            {/* <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={{fontFamily: 'Manrope_600SemiBold'}}>Available Balance</Text>
                <Text style={{fontFamily: 'Manrope_500Medium'}}>R{(balancebf.toFixed(2) - (parseFloat(balancebf) * 0.10)).toFixed(2) }</Text>
              </View> */}


            </View>

            {/* <View style={{marginLeft: 15, alignContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 10, justifyContent: 'center', flexDirection: 'row' }}>

                <View style={{width: 25, height: 25,  borderRadius: 50, backgroundColor:'#a2a3a3', flexDirection: 'row', marginTop: 10, justifyContent:'space-evenly', alignItems: 'center'}}>
                   
                        <MaterialCommunityIcons name="account-cash-outline" size={15} color="white" />
                    
                </View>
                <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
                <View>
                        <Text style={{fontWeight: '400', fontSize: 13, marginLeft: 20, marginBottom: 5, fontFamily: 'Manrope_500Medium'}} >Account Number</Text>

                </View>
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium'}}
                    onChangeText={val => setInput('account', val)}
                    value={formState.account}
                    placeholder={userDetails?.account?.toString()}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
                

            </View>

               
            </View> */}

            {/* <View style={{marginLeft: 15, alignContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 10, justifyContent: 'center', flexDirection: 'row' }}>

                <View style={{width: 25, height: 25,  borderRadius: 50, backgroundColor:'#a2a3a3', flexDirection: 'row', marginTop: 10, justifyContent:'space-evenly', alignItems: 'center'}}>
                   
                        <MaterialCommunityIcons name="source-branch-plus" size={15} color="white" />
                    
                </View>
                <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
                <View>
                        <Text style={{fontWeight: '400', fontSize: 13, marginLeft: 20, marginBottom: 5, fontFamily: 'Manrope_500Medium'}} >Branch Code</Text>

                </View>
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium'}}
                    onChangeText={val => setInput('branch', val)}
                    value={formState.branch}
                    placeholder={userDetails.branch}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
                

            </View>

               
            </View> */}

        
<View style={{marginLeft: 15, marginTop: 20, marginBottom: 10, justifyContent: 'center', flexDirection: 'row' }}>

                {/* medium items */}
              

               
            </View>
<View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />


             <View style={{ marginTop: 20, marginLeft: 40, width: 300, height: 45, backgroundColor: 'green', borderRadius: 5, alignItems:'center', alignContent: 'center', justifyContent:'center'}}>
                    <TouchableOpacity style={{ alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
                   
                    onPress={()=> updateTodo()}
                    >
                        <Text style={{fontWeight: '500', color: 'white'}}>CashOut</Text>
                    </TouchableOpacity>
            </View>


        </View>

      </SafeAreaView>
        
    )
}

export default cashout

const styles = StyleSheet.create({})
