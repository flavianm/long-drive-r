import React, {useState, useEffect, useCallback, useMemo, useRef } from 'react'
import {SafeAreaView, ActivityIndicator, RefreshControl, Image, ScrollView, StyleSheet, Text, View, TouchableOpacity, TextComponent, FlatList, Pressable} from 'react-native'
import {Auth} from 'aws-amplify';
import {API, graphqlOperation} from 'aws-amplify';
import { listParcelOrders } from '../../graphql/queries';
import * as queries from '../../graphql/queries';
import { useNavigation, useRoute } from "@react-navigation/core";
import { onParcelOrderUpdated, onRidesOrderUpdated, onRequestsUpdated } from '../../graphql/subscriptions';
import { Feather, FontAwesome, FontAwesome5, Ionicons, MaterialCommunityIcons, MaterialIcons, Zocial } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';
const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const RidesOrdersDriver = () => {

    const navigation = useNavigation();

     const onRefresh = useCallback(() => {

    setRefreshing(true);

                fetchUsers();
                fetchTodos();
                fetchPending();

                // pop up that sauys "you are upto date"
           


    wait(2000).then(() => setRefreshing(false));
  }, []);

    // STATES 

  const [user, setuser] = useState(null);
  const [todos, setTodos] = useState([])  
  const [Pending, setPending] = useState([]);
  const [filterTodo, setFilterTodo] = useState([]);
  const [orderId, setOrderId] = useState([]);
  const [getStatus, setGetStatus] = useState([]);
  const [loading, setLoading] = useState('true'); 
  const [refreshing, setRefreshing] = useState(false);
  


 const [visible, setVisible] = useState(true);

// user information starts


 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });


  useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 3000);
  }, [])







// user information starts

  const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setuser(userSub);
            
            
    }

       async function fetchTodos() {

            const userplus = await Auth.currentAuthenticatedUser({bypassCache: true});

            const todoData = await API.graphql({query: queries.listRides, variables:  
                {filter : 
                   {and: [{ driverId: 
                        {eq: 
                            ( userplus.attributes.sub)
                        }
                        },
                        { status: 
                        {eq: 
                            'driver'
                        }
                        }
                    ]
                 }
                } 
        })
            const todos = todoData.data.listRides.items
            setTodos(todos)

            setLoading('false')

     }

      async function fetchPending() {

            const userplus = await Auth.currentAuthenticatedUser({bypassCache: true});

            const todoData = await API.graphql({query: queries.listRequests, variables:  
                {filter : 
                   {and: [{ driverId: 
                        {eq: 
                            ( userplus.attributes.sub)
                        }
                        },
                        { status: 
                        {eq: 
                            'pending'
                        }
                        }
                    ]
                 }, 
                } 
        })
            const pend = todoData.data.listRequests.items
            setPending(pend)


     }

     useEffect(() => {
            
                fetchUsers();
                fetchTodos();
                // fetchPending();
            },[todos])

     useEffect(() => {
            
            
                fetchPending();
            },[Pending])
   
            // END USER INFORMATION

            // SUBSCRIPTION START


            let subsUpdate;
            let reqUpdate;
            function setUpSus(){
   
                subsUpdate = API.graphql(graphqlOperation(onRidesOrderUpdated)).subscribe( {next: (daraa) => {
                    setTodos(daraa)
                }, }) 

                 reqUpdate = API.graphql(graphqlOperation(onRequestsUpdated)).subscribe( {next: (bruu) => {
                    setPending(bruu)

                }, }) 


            }

            useEffect(() =>{
                setUpSus();

                return() =>{
                    subsUpdate.unsubscribe();
                    reqUpdate.unsubscribe();   
                };

            },[]);


            const ItemView= ({item}) => {

                return(

              

              <Pressable style={{ backgroundColor: 'white'}} onPress={() => 
                    navigation.navigate('RidesOrderInfoDSet',{id: item?.id, thestatus: item?.status})
                }>
            <View style={{margin: 10}}>

                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <FontAwesome5 name="user-alt" size={12} color="white" />
                                </View>
                        <Text style={{fontWeight: '500', marginLeft: 10, fontSize: 14, fontFamily: 'Manrope_600SemiBold'}}>{item?.name || `Loading...`} {item?.surname}</Text>
                        </View>



                     <View style={{marginLeft: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                
                                     <View style={{marginRight: 10}}>
                                        <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'green'}}/>
                                     </View>

                                    <Text style={{fontWeight: '500', marginLeft: 10, width: 280, fontSize: 13, fontFamily: 'Manrope_600SemiBold'}}>{item.originSec || `Loading...`} </Text>
                             </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                <View style={{marginRight: 10}}>
                                    <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'red'}}/>
                                </View>
                                <Text style={{fontWeight: '500', marginLeft: 10 , width: 280, fontSize: 13, fontFamily: 'Manrope_600SemiBold'}}>{item.destSec || `Loading...`}</Text>
                            </View>
                    </View>


                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name="date-range" size={13} color="white" />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.startDate}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>R{item.tripFee || `0.00`} P/S</Text>
                        </View>

                    </View>


                     <View style={{flexDirection: 'row'}}>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 7}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.seats || `0`} Seats Booked</Text>
                        </View>


                        <View style={{flexDirection: 'row', marginTop: 15}}>
                                    <View style={{alignItems: 'center', marginRight: 10, justifyContent: 'center', width: 80, height: 30, backgroundColor: 'green', borderRadius: 5}}>
                                            <Text style={{fontWeight: '500', color: 'white', fontSize: 12, fontFamily: 'Manrope_600SemiBold'}}>Accept</Text>
                                        </View>
                                        <View style={{alignItems: 'center', justifyContent: 'center', width: 80, height: 30, backgroundColor: 'red', borderRadius: 5}}>
                                            <Text style={{fontWeight: '500', color: 'white', fontSize: 12, fontFamily: 'Manrope_600SemiBold'}}>Decline</Text>
                                        </View>
                    </View>

                    </View>


                    </View>


                   
                    </Pressable>


              
                 )
            }

            const ItemView2= ({item}) => {

                return(

              

              <Pressable style={{ backgroundColor: 'white'}} onPress={() => 
                    navigation.navigate('RidesOrderInfoDSetBBB',{id: item?.id, thestatus: item?.status})
                }>

                    <View style={{flexDirection: 'row'}}>



                    </View>

                    <View style={{margin: 10}}>

                     <View style={{marginLeft: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                                
                                     <View style={{marginRight: 10}}>
                                        <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'green'}}/>
                                     </View>

                                    <Text style={{fontWeight: '500', marginLeft: 10, width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 13}}>{item.originDesc || `Loading...`} </Text>
                             </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                <View style={{marginRight: 10}}>
                                    <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'red'}}/>
                                </View>
                                <Text style={{fontWeight: '500', marginLeft: 10 , width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 13}}>{item.destDesc || `Loading...`}</Text>
                            </View>
                    </View>


                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name="date-range" size={13} color="white" />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.startDate}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>R {item.tripFee || `0.00`} Trip Total</Text>
                        </View>

                    </View>


                     <View style={{flexDirection: 'row'}}>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 7}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.availSeats || `0`} of {item.totalSeats || `0`} Seats Left</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.status || `Loading...`}</Text>
                        </View>

                    </View>

                    </View>
                   
                    </Pressable>


              
                 )
            }


            const ItemSeperatorView = () => {
                return(
                    <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}}
                    />
                )
            }


if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{

    return (
         <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
            
            <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
            
             <ScrollView
             showsVerticalScrollIndicator={false}
             showsHorizontalScrollIndicator={false}
             style={{backgroundColor: 'white', padding: 5}}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
             >

        
        <View>

             <View style={{flexDirection: 'row'}}>
                    <Text style={{ paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30, fontFamily: 'Manrope_700Bold'}}>My Trips</Text>

            </View>


                 <FlatList
                    data={Pending}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView}
                />


                {/* Pending Trips */}
                    <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

                    <View>
                        <Text style={{fontWeight: '500', marginLeft: 20, fontFamily: 'Manrope_700Bold'}}>ACTIVE TRIPS</Text>
                    </View>

                    <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

                <FlatList
                    data={todos}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView2}
                />
            
                {/* Normal Trips */}

            

        </View>
           </ScrollView>
        </SafeAreaView>
    )
            }
}
 

export default RidesOrdersDriver

const styles = StyleSheet.create({

    container: { flex: 1, justifyContent: 'center', padding: 20 },
  todo: {  backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20},
  input: { height: 50, backgroundColor: '#ddd', marginBottom: 10, padding: 8 },
  todoName: { fontSize: 15 },

})
