import React, {Component} from 'react';
import {TouchableHighlight, TextInput, StyleSheet, Text, View} from 'react-native';
import { Auth } from 'aws-amplify'
import Icon from 'react-native-vector-icons/Ionicons'
import { colors, typography, dimensions } from '../../../src/theme'
import BaseHeader from '../../../src/BaseHeader'

export default class Profile extends Component {
  state = {
    username: 'Username',
    email: 'info@longdrive.co.za',
    twitter: '@Mutswari',
    github: '@Mutswari_M',
    isEditing: false
  }
  async componentDidMount() {
    const user = await Auth.currentAuthenticatedUser()
    const { signInUserSession: { idToken: { payload }}} = user
    this.setState({
      username: user.username,
      email: payload.email
    })
  }
  toggleForm = () => this.setState({ isEditing: !this.state.isEditing })
  onChange = (key, value) => this.setState({ [key]: value })
  signOut = () => {
    Auth.signOut()
      .then(() => {
         this.props.screenProps.onStateChange('signedOut', null);
      })
      .catch(err => {
          console.log('err: ', err)
      })
  }
  render() {
    const buttonText = this.state.isEditing ? 'Save' : 'Edit Profile'
    return (
      <View style={styles.container}>
        <BaseHeader />
        <View style={styles.profileContainer}>
          <Text style={styles.username}>{this.state.username}</Text>
          <Text style={styles.email}>{this.state.email}</Text>
          {
            !this.state.isEditing ? (
              <Social
                twitter={this.state.twitter}
                github={this.state.github}
              />
            ) : (
              <Form
                onChange={this.onChange}
                twitter={this.state.twitter}
                github={this.state.github}
              />
            )
          }
          <View style={styles.buttonContainer}>
            <TouchableHighlight
              onPress={this.toggleForm}
              underlayColor='transparent'
            >
              <View style={styles.button}>
                <Text style={styles.buttonText}>
                  {buttonText}
                </Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={this.signOut}
              underlayColor='transparent'
            >
              <View style={styles.button}>
                <Text style={styles.buttonText}>
                  Sign out
                </Text>
              </View>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }
}

const Social = ({ twitter, github}) => (
  <View>
    <View style={styles.iconContainer}>
      <Icon
        color='black'
        name='logo-twitter'
        size={29}
      />
      <Text style={styles.twitterHandle}>{twitter}</Text>
    </View>
    <View style={[styles.iconContainer, styles.twitter]}>
      <Icon
        color='black'
        name='logo-github'
        size={29}
      />
      <Text style={styles.twitterHandle}>{github}</Text>
    </View>
  </View>
)

const Form = ({ onChange, twitter, github }) => (
  <View>
    <View>
      <TextInput
        onChangeText={value => onChange('twitter', value)}
        style={styles.input}
        value={twitter}
        autoCapitalize='none'
        autoCorrect={false}
      />
    </View>
    <View>
      <TextInput
        onChangeText={value => onChange('github', value)}
        style={styles.input}
        value={github}
        autoCapitalize='none'
        autoCorrect={false}
      />
    </View>
  </View>
)

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    alignContent: 'center',
    
  },
  button: {
    width: 110,
    backgroundColor: 'red',
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
    marginLeft: -1,
    marginRight: 15,
    borderRadius: 5,
    bottom: -250,
  },
  buttonText: {
    color: '#fff',
    marginTop: 3,
    fontSize: 13
  },
  input: {
    height: 45,
    width: dimensions.width - 30,
    borderBottomWidth: 2,
    marginBottom: 8,
    fontSize: 18,
    borderBottomColor: '#000',
  },
  twitterHandle: {
    color: 'black',
    // fontFamily: typography.primary,
    fontSize: 18,
    marginLeft: 15
  },
  iconContainer: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  gitHub: {
    marginTop: 15
  },
  container: {
    
  },
  profileContainer: {
  },

  username: {
    fontSize: 20,
    marginBottom: 3,
  },
  email: {
    fontSize: 18,
    marginBottom: 10,
    color: '#000'
  }
});
