import React, {useState, useEffect} from 'react'
import { ScrollView, StyleSheet, Text, View, TextInput, Pressable, SafeAreaView, TouchableOpacity } from 'react-native'
import { useNavigation, useRoute } from "@react-navigation/core";
import { AntDesign, Entypo, MaterialIcons } from '@expo/vector-icons';import { Feather, FontAwesome, MaterialCommunityIcons, Fontisto  } from '@expo/vector-icons';
import { API, graphqlOperation } from "aws-amplify";
import {Auth} from 'aws-amplify'
import { updateUser } from '../../graphql/mutations';
import { getUser } from '../../graphql/queries';
import Toast from 'react-native-toast-message';
import AnimatedLoader from "react-native-animated-loader";
import LottieView from 'lottie-react-native';

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';


const CarProfileScreen = () => {

 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

 const [visible, setVisible] = useState(true);


     useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 5000);
  }, [])


const [user, setUser] = useState([])

const [champUser, setChampUser] = useState();

const [carName, setCarName] = useState(champUser?.carName);
const [model, setModel] =useState(champUser?.model);
const [plate, setPlate] =useState(champUser?.plate);

 const route = useRoute();
  const {userId} = route.params;

  

      const navigation = useNavigation();

//     React.useEffect(() => {
//     Toast.show({
//       type: 'success',
//       text1: 'Hello',
//       text2: 'This is some something 👋'
//     });
//   }, []);

const fetchUsers = async() =>{

        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setUser(userData.attributes);
            console.log(userData.attributes.email)
            // setInput('userId', userSub)


        // if balance is equal to null, put in a 0.01 value

        // fetch balance amount

            const userBalance = await API.graphql(graphqlOperation(getUser, { id: userSub}))
            const userBalanceInfo = userBalance.data.getUser
            setChampUser(userBalanceInfo)
            const zeBalance = userBalanceInfo.balance
            // setCurrentBalance(zeBalance)
            console.log('USER BALANCE  ::' + zeBalance)


            const bankupdate = {
                id: userSub,
                balance: '0.01'
            }


            if (zeBalance === null){
                console.log('null value here')
                await API.graphql(graphqlOperation(updateUser, {input: bankupdate}))
                console.log('done')
            } else{
                console.log('balance is :' + zeBalance )
            }


            
    }

 useEffect(() => {

   fetchUsers();
              
            },[])







const initialState = {id: userId, plate: '', model: '', carName: '', carImage: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/images/products/keyboard1.jpg', smoke: ''}
const [formState, setFormState] = useState(initialState)
const [todos, setTodos] = useState([])

    function setInput(key, value) {
    setFormState({ ...formState, [key]: value })
  }
   

const todoDetails = {
        id: user.sub,
        status: 'dcancelled',
        active: 'no'
    };


  

     async function updateTodo() {

        const userData = await Auth.currentAuthenticatedUser();
        const userSub = userData.attributes.sub;

         const bluza = {
            id: userSub,
            carName: carName, 
            model: model, 
            plate: plate, 
            imageUrl: 'https://thumbs.dreamstime.com/b/default-avatar-profile-icon-social-media-user-vector-default-avatar-profile-icon-social-media-user-vector-portrait-176194876.jpg', 
        }





    try {
      const todo = { ...formState }
      console.log('1')
      setTodos([...todos, todo])
       console.log('2')
      setFormState(initialState)
       console.log('3')
      await API.graphql(graphqlOperation(updateUser, {input: bluza}))
    //   console.warn('Successful')

       Toast.show({
      type: 'success',
      text1: 'Profile',
      text2: 'Profile is Set'
    }); 

    navigation.navigate('Home')



    } catch (err) {
      console.log('error update User:', err)
      Toast.show({
      type: 'error',
      text1: 'Profile',
      text2: 'Profile Update Failed'
    }); 
    }

  }


   if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{

    return (

         <SafeAreaView style={{ backgroundColor: 'white', flex: 1}}>


             {/* <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader> */}

        <ScrollView
         showsVerticalScrollIndicator={false}
        >
            <View>
                <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10}}>

                <Text style={{paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30, fontFamily: 'Manrope_700Bold'}}>
                My Car
                </Text>

                <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

        
          <Text style={{paddingLeft: 15, fontSize: 13, fontWeight: '600', color: 'black', fontFamily: 'Manrope_500Medium'}}>{user.email}</Text>

          <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

            </View>

                <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
            <Text style={{paddingLeft: 15, fontWeight: '600', color: 'black', fontFamily: 'Manrope_700Bold', fontSize: 14}}>
                Vehicle Details
            </Text>
            
            <View>
            <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 25, fontFamily: 'Manrope_500Medium' }}>
             Please input correct details to avoid suspension
            </Text>

            {/* guide */}
            <View style={{ marginBottom: 10, justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>

                {/* medium items */}
                <View style={{width: 100, height: 100,  borderRadius: 10, backgroundColor:'#e1e7e8', flexDirection: 'row', marginTop: 10, justifyContent:'space-evenly', alignItems: 'center'}}>
                   
                    <View style={{width: 60, height: 60}}>
                    <LottieView style={{width: 75, height: 75, marginLeft: -2, marginTop: -2}} source={require('../../assets/warning/9948-camera-pop-up.json')} autoPlay loop />

                    </View>
                </View>

               
            </View>

            {/* input details  */}
            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', marginLeft: 20, marginBottom: 5, fontFamily: 'Manrope_500Medium', fontSize: 13}} >Car Name</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', fontSize: 13, paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium'}}
                    onChangeText={val => setCarName(val)}
                    placeholder= {champUser?.carName || 'Mercedes Benz'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontFamily: 'Manrope_500Medium', fontSize: 13, marginLeft: 20, marginBottom: 5}} >Model</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setModel(val)}
                    // value={formState.surname}
                    placeholder= {champUser?.model || 'G63, 2021'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400',  marginLeft: 20, marginBottom: 5, fontFamily: 'Manrope_500Medium', fontSize: 13}} > Number Plate</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setPlate(val)}
                    placeholder= {champUser?.plate || 'JYD512GP'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

          

        </View>

            <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

     </View>
            
             

      


                    
            
                <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
           
 
            <View style={{ marginTop: 20,  backgroundColor: 'green', borderRadius: 5, alignItems:'center', alignContent: 'center'}}>
                    <TouchableOpacity style={{width: 250, height: 45, marginLeft: 50, marginRight: 50, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
                    // onPress={addTodo} 
                    // onPress={ ()=> updateTodo} 
                    onPress={()=> updateTodo()}
                    >
                        <Text style={{fontWeight: '500', color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Update Car</Text>
                    </TouchableOpacity>
            </View>
       

            

     </View>
            
            

            </View>
        </ScrollView>

    </SafeAreaView>
    )
        }
}

export default CarProfileScreen

const styles = StyleSheet.create({})
