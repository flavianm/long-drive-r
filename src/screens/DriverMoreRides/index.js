import * as React from 'react';
import { FlatList, Pressable, StyleSheet, Image } from 'react-native';

// import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../../components/Themed';
import ListItem from '../../components/ListItem';
import AppIcon from '../../components/AppIcon';
import AppScreen from '../../components/AppScreen';

import { useNavigation } from "@react-navigation/core";

const APP_COLORS = {
    primary: '#fc5c65',
    secondary: '#4ecdc4',
    black: '#000',
    white: '#fff',
    dark : '#0c0c0c',
};

const ListItemSeperator = () => {
    return (
        <View style={styles.seperator}></View>
    );
};

const MENU_ITEMS = [
    {
        title: 'New Trip',
        icon: {
            name: 'plus',
            backgroundColor: APP_COLORS.primary,
        },
        targetScreen: 'RiderDestDriver',
    },
    // {
    //     title: 'Search Request',
    //     icon: {
    //         name: 'map-search-outline',
    //         backgroundColor: APP_COLORS.primary,
    //     },
    //     targetScreen: 'GoodsOrdersDA',
    // },
    {
        title: 'My Posted Trips',
        icon: {
            name: 'format-list-bulleted',
            backgroundColor: APP_COLORS.primary,
        },
        targetScreen: 'RidesOrdersDriver',
        
    },
    {
        title: 'Active Trips',
        icon: {
            name: 'car-connected',
            backgroundColor: APP_COLORS.primary,
        },
        targetScreen: 'RidesDAO',
    },
    
    // {
    //     title: 'Wallet',
    //     icon: {
    //         name: 'cash',
    //         backgroundColor: APP_COLORS.secondary,
    //     },
    //     targetScreen: 'Pay',
    // },
   
];
export default function DriverMoreRides() {

   const navigation = useNavigation();

  return (
  
    <AppScreen style={styles.screen}>
      <View >
                
                <View style={{alignContent: 'center', alignItems: 'center'}}>

                    <Text style={{marginTop: 15}}>
                        
                    </Text>
                    <Text style={{marginTop: 5}}>
                       
                    </Text>
                    {/* <Text style={{fontSize: 30, fontWeight: '500', color: 'red'}}>
                       R1.00
                    </Text> */}

                    

                </View>
                
        </View>

            <View style={styles.container}>
                <FlatList
                    data={MENU_ITEMS}
                    keyExtractor={item => item.title}
                    ItemSeparatorComponent={ListItemSeperator}
                    renderItem={({ item }) => {
                        return (
                            <ListItem
                                title={item.title}
                                IconComponent={<AppIcon name={item.icon.name} backgroundColor={item.icon.backgroundColor}/>}
                                onPress={() => {
                                    navigation.navigate(item.targetScreen)}}
                            />
                        );
                    }}
                />
            </View>

    </AppScreen>


  );
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 10,
        borderRadius: 15,
        backgroundColor:'white'
    },
   seperator: {
        width: '100%',
        height: 1,
        color: 'white',
        backgroundColor: '#d6d6d6'

    },
      screen: {
        // borderRadius: 15,
        backgroundColor: 'white'
    }
});
