import React, {useState} from "react";
import {View, Text, Dimensions, Alert, ScrollView} from 'react-native';
import UberTypes from "../../components/UberTypes";
import RouteMap from "../../components/RouteMap";
import {API, graphqlOperation, Auth} from 'aws-amplify';
import { createOrder } from "../../graphql/mutations";

import { useNavigation, useRoute } from "@react-navigation/core";


const SearchResult = (props) =>{

    const typeState = useState(null);
    const route = useRoute();

    const navigation = useNavigation();

    const {originPlace, destinationPlace} = route.params

    const onSubmit = async () =>{
        const [type] = typeState;
        if(!type){
            return;
        }

        //submit to server

        try{    

            const userInfo =  await Auth.currentAuthenticatedUser();
            const date = new Date();
            const input = {
                createdAt: date.toISOString(),
                type: type, //type here can represent the carID below .. find a way to remove the car id. redirect all car id's to the type 
                originLatitude: originPlace.details.geometry.location.lat,
                originLongitude: originPlace.details.geometry.location.lng,
                destLatitude: destinationPlace.details.geometry.location.lat,
                destLongitude: destinationPlace.details.geometry.location.lng,
                userId: userInfo.attributes.sub,
                carId:"1",
                status: 'NEW', // will be used to filter the order on driver side. you can add the filtering here 

            }

            const response = await API.graphql(
                graphqlOperation(
                    createOrder,{
                        input: input
                    }
                )
            )
            console.log(response);
            navigation.navigate('OrderPage',{id:response.data.createOrder.id})

        }catch(e){
            console.error(e);
        }
    }

    return (
        <View>
           
           {/* <View style={{height: Dimensions.get('window').height - 450}}>
                <RouteMap origin={originPlace} destination = {destinationPlace}/>
           </View> */}
           
            <ScrollView>
                <UberTypes typeState={typeState} onSubmit={onSubmit}/>
            </ScrollView>
        </View>
    )
};

export default SearchResult;