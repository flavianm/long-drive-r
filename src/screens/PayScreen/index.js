import { StyleSheet, Image, TextInput, Pressable } from "react-native";
import {PayFastWebView} from "react-native-payfast-gateway";
import React, {useEffect} from "react";
import { useState } from "react";
import { View, Button, Text } from "react-native";
import { Modal } from "react-native";
import { useNavigation, useRoute } from "@react-navigation/core";
import { API, graphqlOperation } from "aws-amplify";
import { updateUser } from '../../graphql/mutations';
import {Auth} from 'aws-amplify';
import { getUser } from '../../graphql/queries';
import * as mutations from '../../graphql/mutations'
import { MaterialIcons } from "@expo/vector-icons";
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    // alignItems: 'center',
    // justifyContent: 'center',
    marginTop: 20,
    backgroundColor: 'white'
  },
  btnWrapper: {
    width: "100%",
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    margin: 15,
    backgroundColor: 'white',
    width: '90%',
    borderRadius: 15,
    height: 50,
    alignContent: 'center',
    alignContent: 'center',
    padding: 5
    
  },
  but:{
    fontSize: 500
  }
});



export default function PayScreen() {

  const navigation = useNavigation();
  const initialState = {id: userId , balance: newAmount}
  const [success, setSuccess] = useState(false)
  const [modalVisible, setModalVisible] = useState(false);
  const [paymentData, setPaymentData] = useState({});
  const [amount, setAmount] = useState();
  const [newAmount, setNewAmount] = useState();
  const [balancebf, setBalanceBf] = useState();

let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });




  const [userDetails, setUserDetails] = useState([])


  const route = useRoute();
  const {userId, email} = route.params
 

     console.log(success)



  const amountdetails = {
        id: userId,
        balance: newAmount,
    };

    const done = async ()=>{
           await API.graphql({ query: mutations.updateUser, variables: {input: amountdetails}});
          console.log('updated amount')
           
           navigation.navigate('home')
       
        }


  useEffect(()=>{
    const result = parseFloat(balancebf) + parseFloat(amount);
      setNewAmount(result)
  })      


  useEffect(() => {
    
    if (success === true){


      
      console.log('set db' + newAmount)

      // navigation.navigate('home')
      done();

    }

  }, [success])



const balanceAmount = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser({bypassCache: true});
            const userSub = userData.attributes;
            const userIdd = userSub.sub;
            console.log(userIdd)

            const todoData = await API.graphql(graphqlOperation(getUser, { id: userId}))
            const todos = todoData.data.getUser
            setBalanceBf(todos.balance)

            console.log('balance bf  ' + todos.balance)
            
    }

    useEffect(() => {
      
      balanceAmount();
    }, [])


  const addCalc = async()=>{

    const result = parseFloat(balancebf) + parseFloat(amount);
    console.log('answer   ' + result)

  }

useEffect(() => {
      
      addCalc();
    }, [])





  let onceOffPayment = {
    merchant_id : "10000100",
    merchant_key: '46f0cd694581a',
    amount: amount,
    item_name: email
  }

  let subscription = {
    subscription_type: 1,
    recurring_amount: "300.85",
    frequency: 3,
    cycles: 0
  }

  function handleOnceOffPayment() {
    setPaymentData(onceOffPayment);
    setModalVisible(true);
  }

  function handleSubscriptionPayment() {
    setPaymentData({ ...onceOffPayment, ...subscription });
    setModalVisible(true);
  }

  return (
    <View style={styles.container}>
        <Text style={{ fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30, marginLeft: 15, fontFamily: 'Manrope_700Bold'}}>
                    Deposit
                </Text>
      
      <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 80}}>
        <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    onChangeText={val => setAmount(val, 'amount')}
                    value={amount}
                    placeholder="Amount"
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
        
 <Pressable style={{width: 180, marginTop: 20, height: 40, borderRadius: 5, backgroundColor: 'green', flexDirection: 'row', alignItems: 'center', alignContent: 'center', justifyContent: 'center'}}
      onPress={() => handleOnceOffPayment()} 
      >
          
          <Text style={{fontSize: 15, fontWeight: '500', color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Pay</Text>
          <MaterialIcons name="keyboard-arrow-right" size={24} color="white" />
      </Pressable>



      </View>




     
      <View style={styles.btnWrapper}>
        {/* <View style={styles.btn}>
          <Button  title="Continue to Pay"
           color="#272B3A" 
          onPress={() => handleOnceOffPayment()} />
        </View> */}
        {/* <View style={[styles.btn]}>
          <Button style={styles.but} 
          title="R50 Monthly Contribution"
          color="#272B3A" 
          onPress={() => handleSubscriptionPayment()} />
        </View> */}
      </View>

      

     


      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}  
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <PayFastWebView sandbox={true} onClick={() => setModalVisible(false)} callback={setSuccess} signature={false} data={paymentData}  />
      </Modal>
    </View>
  );
}

