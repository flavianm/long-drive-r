import React from "react";
import {View, Text, Dimensions} from 'react-native';
import HomeMap from "../../components/HomeMap";
import CovidMessage from "../../components/CovidMessage";
import HomeSearch from "../../components/HomeSearch";
import TopMessage from '../../components/TopMessage/index' 

import { API, graphqlOperation } from "aws-amplify";
import {Auth} from 'aws-amplify'
import { updateUser } from '../../graphql/mutations';
import { getUser } from '../../graphql/queries';
import { useNavigation, useRoute } from "@react-navigation/core";


const HomeScreen = (props) =>{

const [champuser, setChampUser] = useState();
 const navigation = useNavigation();

const fetchUsers = async() =>{

    const userData = await Auth.currentAuthenticatedUser();
    const userSub = userData.attributes.sub;

    const userBalance = await API.graphql(graphqlOperation(getUser, { id: userSub}))
    const userBalanceInfo = userBalance.data.getUser

    // setChampUser(userBalanceInfo)

    if(userBalanceInfo.completeP === null ){
    navigation.navigate('UserProfile')
    }

}

useEffect(() => {
    
    fetchUsers()
}, [])


    return (
        <View style={{backgroundColor: 'white'}}>

            {/*covid message*/}
            <TopMessage/>
             {/*Search*/}
             <HomeSearch/>
        </View>
    )
};

export default HomeScreen;