import React, {useState, useEffect, useCallback, useMemo, useRef } from 'react'
import {View, Button, Platform, Text, Pressable, TextInput, Image, ActivityIndicator, TouchableOpacity} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { AntDesign, Entypo, Ionicons, MaterialIcons } from '@expo/vector-icons';
// import { useNavigation } from "@react-navigation/core";
import { Picker } from "@react-native-picker/picker";
import DateTimePicker from '@react-native-community/datetimepicker';
import { Feather, FontAwesome, MaterialCommunityIcons, Fontisto  } from '@expo/vector-icons';
import PostSearch from '../PostSearch';
import {Auth} from 'aws-amplify';
import {API, graphqlOperation} from 'aws-amplify';
import { createGoodsOrder, createRides } from '../../graphql/mutations';
import {listGoodsOrders} from '../../graphql/queries';
import { useNavigation, useRoute } from "@react-navigation/core";
import {LocaleConfig} from 'react-native-calendars';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import SelectDropdown from 'react-native-select-dropdown';
import Toast from 'react-native-toast-message';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getUser } from '../../graphql/queries';
import LottieView from 'lottie-react-native';

import AnimatedLoader from "react-native-animated-loader";

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';
import { color } from 'react-native-reanimated';

const RiderDriver = () => {

  let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

    const route = useRoute();
    const {originPlace, destinationPlace, destDesc, originDesc, originSec, destSec} = route.params

const [visible, setVisible] = useState(true);

    useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 3000);
  }, [])


    const navigation = useNavigation();

    const goToSearch = () =>{
       navigation.navigate('PostSearch');
    }


    const size = ["1", "2", "3"]
     const taxi = ["Yes", "No"]
     const luggage = ["Small", "Medium", "Large"]
    // STATES 

  const [user, setuser] = useState(null);
    const [loading, setLoading] = useState('false');
    const [seatsA, setSeatsA] = useState('0');


  const [date, setDate] = useState('');
  const [time, setTime] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

const initialState = { driverId: user, name: '', surname: '', imageUrl: '', tripFee: '0', message: '', totalSeats: seatsA, availSeats: seatsA, willTaxi: '', willTaxiPrice: '', luggageMax: '',    price: '', startDate: date, status: 'driver', active: '', originLatitude: originPlace.details.geometry.location.lat, originLongitude: originPlace.details.geometry.location.lng, destLatitude:destinationPlace.details.geometry.location.lat, destLongitude:destinationPlace.details.geometry.location.lng, originDesc: originDesc, originSec: originSec, destDesc: destDesc, destSec: destSec}
const [formState, setFormState] = useState(initialState)
const [todos, setTodos] = useState([]);

const [userDa, setUserDa] = useState();
const [messageD, setMessageD] = useState();
const [priceD, setPriceD] = useState('0');
const [luggageMaxD, setLuggageMaxD] = useState('null');
const [willTaxiD, setWillTaxiD] = useState('null');
const [willTaxiPriceD, SetWillTaxiPriceD] = useState('0');
const [DTD, setDTD] = useState();


             function setInput(key, value) {
    setFormState({ ...formState, [key]: value })
  }


  
  const geometry = async() =>{
    const originLat = originPlace.details.geometry.location.lat;
    const originLng = originPlace.details.geometry.location.lng;
    setInput('originLatitude', originLat)
    setInput('originLongitude', originLng)

    console.log(originLat)
    console.log(originLng)
  }

   useEffect(() => {
            
            geometry();

            },[])

//   FETCHING USER INFORMATION

const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setuser(userSub);
            console.log(userSub)
            setInput('driverId', userSub)

             const todoData = await API.graphql(graphqlOperation(getUser, { id: userSub}));
             const todos = todoData.data.getUser;
            setUserDa(todos)
          

    }

     useEffect(() => {
            
                fetchUsers();
            },[])
   

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };




 async function fetchTodos() {
    try {
        const todoData = await API.graphql(graphqlOperation(listGoodsOrders))
      const todos = todoData.data.listGoodsOrders.items
      setTodos(todos)
   
    } catch (err) { console.log('error fetching orders') }
    
  }

  useEffect(() => {
        
            fetchTodos();
        },[]);

        
  const topTodo = {

   driverId: user,
   name: userDa?.name,
   surname: userDa?.surname,
   imageUrl: userDa?.imageUrl,
   tripFee: '0',
   message: messageD,
   totalSeats: seatsA, 
   availSeats: seatsA, 
   willTaxi: willTaxiD, 
   willTaxiPrice: willTaxiPriceD, 
   luggageMax: luggageMaxD,
   carName: userDa?.carName,
   model: userDa?.model,
   plate: userDa?.plate,
   carImage: userDa?.carImage,    
   price: priceD, 
   startDate: date, 
   status: 'driver', 
   active: '', 
   originLatitude: originPlace.details.geometry.location.lat, 
   originLongitude: originPlace.details.geometry.location.lng, 
   destLatitude:destinationPlace.details.geometry.location.lat, 
   destLongitude:destinationPlace.details.geometry.location.lng, 
   originDesc: originDesc, 
   originSec: originSec, 
   destDesc: destDesc, 
   destSec: destSec

  }


  const SeatssOp = () => {


if(seatsA === '0'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSeatsA('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSeatsA('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        {/* thr */}
          <TouchableOpacity onPress={()=> setSeatsA('3')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>


        </View>
      )
    }


    
    if(seatsA === '1'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSeatsA('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        {/* thr */}
          <TouchableOpacity onPress={()=> setSeatsA('3')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>


        </View>
      )
    }
    
    if(seatsA === '2'){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSeatsA('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>

        {/* thr */}
          <TouchableOpacity onPress={()=> setSeatsA('3')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>


        </View>
      )
    }

    if(seatsA === '3'){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSeatsA('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSeatsA('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        {/* thr */}
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>


        </View>
      )
    }
    
    
  
  }

  const SDOp = () => {

if(willTaxiD === 'null'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity  onPress={()=> setWillTaxiD('Yes')}  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>Y E S</Text>
          </TouchableOpacity>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}>

          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setWillTaxiD('No')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>N O </Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

    

        </View>
      )
    }

    
    if(willTaxiD === 'Yes'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            {/* <Entypo name="thumbs-up" size={24} color="orange" /> */}
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'orange'}}>Y E S</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}>

          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setWillTaxiD('No')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>N O </Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

    

        </View>
      )
    }
    
    if(willTaxiD === 'No'){
      return(
       <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setWillTaxiD('Yes')}  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            {/* <Entypo name="thumbs-up" size={24} color="orange" /> */}
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>Y E S</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}>

          </View>

          {/* tw */}
          <TouchableOpacity  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'orange'}}>N O </Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

    

        </View>
      )
    }

    
    
    
  
  }

  const Lugp = () => {

if(luggageMaxD === 'null'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity  onPress={()=> setLuggageMaxD('Small')}  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>S M A L L</Text>
          </TouchableOpacity>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}>

          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setLuggageMaxD('Medium')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>M E D I U M</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}/>

           <TouchableOpacity onPress={()=> setLuggageMaxD('Large')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>L A R G E</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        </View>
      )
    }

    
    if(luggageMaxD === 'Small'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View   style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'orange'}}>S M A L L</Text>
          </View>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}>

          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setLuggageMaxD('Medium')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>M E D I U M</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}/>

           <TouchableOpacity onPress={()=> setLuggageMaxD('Large')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>L A R G E</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        </View>
      )
    }
    
    if(luggageMaxD === 'Medium'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setLuggageMaxD('Small')}  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>S M A L L</Text>
          </TouchableOpacity>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}>

          </View>

          {/* tw */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'orange'}}>M E D I U M</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}/>

           <TouchableOpacity onPress={()=> setLuggageMaxD('Large')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>L A R G E</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        </View>
      )
    }

    if(luggageMaxD === 'Large'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setLuggageMaxD('Small')}  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>S M A L L</Text>
          </TouchableOpacity>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}>

          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setLuggageMaxD('Medium')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>M E D I U M</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}/>

           <View  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'orange'}}>L A R G E</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>

        </View>
      )
    }

    
    
    
  
  }


  const DTDD = () =>{

    if(willTaxiD === 'Yes'){


      return(
         <View style={{backgroundColor: 'white', marginTop: 20, paddingBottom: 20}}>

           <View>

             <Text style={{fontFamily: 'Manrope_500Medium', fontSize: 13, color: 'grey', marginTop: 20}}>Enter Door 2 Door amount per seat</Text>
           </View>


            <View style={{flexDirection: 'row', marginTop: 25}}>
             {/*Date box*/}
                

             {/* input details  */}
            <View style={{flexDirection:'row',  marginBottom: 10}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 5, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Text style={{fontWeight: '600', color: 'white', fontSize: 20}} >R</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 40, width: 100, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium'}}
                    placeholder="30"
                    keyboardType='numeric'
                    placeholderTextColor="#7d7d7d"
                     onChangeText={val => SetWillTaxiPriceD(val)}
                    />
                    
                </View>

            </View>

            </View>


         </View>
      )

    } 

  }



    // Post order

  async function addTodo() {

    setVisible(!visible)

    if(seatsA === '0'){

      setVisible(!visible)

       Toast.show({
      type: 'error',
      text1: 'Seats Missing',
      text2: 'Please pick an option',
      visibilityTime: 5000,

    });
      return;
    }

    if(willTaxiD === 'null'){

  setVisible(!visible)
       Toast.show({
      type: 'error',
      text1: 'Door 2 Door',
      text2: 'Please pick an option',
      visibilityTime: 5000,

    });
      return;
    }

if(luggageMaxD === 'null'){

  setVisible(!visible)
       Toast.show({
      type: 'error',
      text1: 'Luggage Missing',
      text2: 'Please pick an option',
      visibilityTime: 5000,

    });
      return;
    }

if(date === ''){

  setVisible(!visible)
       Toast.show({
      type: 'error',
      text1: 'Date Missing',
      text2: 'Please pick an option',
      visibilityTime: 5000,

    });
      return;
    }

 if(priceD === '0' || priceD === '' || priceD === ' '  ){

  setVisible(!visible)
       Toast.show({
      type: 'error',
      text1: 'Price Missing',
      text2: 'Please enter an amount',
      visibilityTime: 5000,

    });
      return;
    }   


    try {
        setLoading('true') 
      const todo = { ...formState }
      setTodos([...todos, todo])
      setFormState(initialState)
      await API.graphql(graphqlOperation(createRides, {input: topTodo}))
     
        Toast.show({
      type: 'success',
      text1: 'Rides',
      text2: 'Your trip has been successfully placed',
      visibilityTime: 5000,

    });


     navigation.navigate('Home')
    } catch (err) {
      console.log('error creating Trip:', err)

      setVisible(!visible)
      
       Toast.show({
      type: 'error',
      text1: 'Rides Error',
      text2: 'Your trip could not be placed, please try again ',
      visibilityTime: 4000,
      
    });
    }

  }

 if (!fontsLoaded) {
            return(
               <View>
                  <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
               </View>

            )
        } else{

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
           <AnimatedLoader
              visible={visible}
              overlayColor="rgba(255,2555,255,7)"
              animationStyle={{width: 75, height: 75}}
              speed={1}
            >
          </AnimatedLoader>

        <ScrollView
        style={{backgroundColor: 'white', padding: 10}}
    showsVerticalScrollIndicator={false}
    showsHorizontalScrollIndicator={false}
    >
    
       

        <View style={{backgroundColor: 'white', padding: 10, paddingBottom: 20}}>

           <Text style={{paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 25, fontFamily: 'Manrope_700Bold'}}>
                Set Your Trip
            </Text>
  
        </View>

          <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 5, marginBottom: 20}} />

       <View style={{flexDirection: 'row'}}>
                        <View style={{width: 50, height: 50}}>
                            {/* <Ionicons name={'warning'} color="grey" size={40} /> */}
                            <LottieView style={{width: 50, height: 50, marginLeft: 0}} source={require('../../assets/warning/8750-alert.json')} autoPlay loop />

                        </View>
                            <View style={{width: 300}}>
                                <Text style={{color: 'grey', fontFamily: 'Manrope_400Regular', fontSize: 13}}>Please use the guide below to input correct details. Passenger may filter your trip based on your inputed data</Text>
                            </View>
             </View>

          <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />


       <View style={{backgroundColor: 'white',  padding: 10, paddingBottom: 20}}>

           <View>
                        <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>TRIP DETAILS</Text>
            </View>
            
            <View>
            
   
              <View style={{  marginBottom: 25, justifyContent: 'space-evenly'}}>

                   <View style={{marginBottom: 10}}>
                        <Text style={{ fontSize: 13, color: 'grey', marginTop: 20, marginBottom: 10, fontFamily: 'Manrope_500Medium'}} >Number Of Seats</Text>
                    </View>

            
                  {SeatssOp()}





                <View style={{alignItems: 'center', marginTop: 20}}>
                {/* <Text style={{fontStyle: 'italic', color: 'grey'}}>Selected Seats : {}</Text> */}
                </View>


                <View style={{ marginBottom: 10, justifyContent: 'space-evenly'}}>
            
                  <View style={{marginBottom: 10}}>
                            <Text style={{ fontSize: 13, color: 'grey', marginTop: 20, fontFamily: 'Manrope_500Medium'}} >Door2Door ? Will you be able to fetch and deliver the passenger to their exact location? Additional Cost is up to you</Text>
                  </View>

                 {SDOp()}

                    {DTDD()}


                 <View style={{alignItems: 'center'}}>
                {/* <Text style={{fontStyle: 'italic', color: 'grey'}}>Selected D2D : {}</Text> */}
                </View>
            
            
            </View>               


                <View style={{ marginBottom: 10, justifyContent: 'space-evenly'}}>
                
                <View style={{marginBottom: 10}}>
                        <Text style={{ fontSize: 13, color: 'grey', marginBottom: 10, marginTop: 20, fontFamily: 'Manrope_500Medium'}} >Maximum size luggage a passenger can carry ?</Text>
              </View>
                

                {Lugp()}

   
                </View>   

            </View>

             
             
         
        </View>

       <View style={{ marginBottom: 10, justifyContent: 'space-evenly'}}>
         
         <View style={{marginBottom: 10}}>
                        <Text style={{ fontSize: 13, color: 'grey', marginTop: 20, fontFamily: 'Manrope_500Medium'}} >Please pick precise Date of trip</Text>
              </View>
         
        <View style={{backgroundColor: 'white',  paddingBottom: 20}}>
         
            
            <View>
           
            <View style={{alignItems: 'center'}}>
            <Text style={{fontSize: 20, color: '#6e6e6e', marginTop: 5, marginBottom: 10 , fontFamily: 'Manrope_600SemiBold'}}>
              {date}
            </Text>
            </View>

             
        </View>

            <View style={{}}>
          <View>

            
            <View>
                 <Calendar
  // Initially visible month. Default = Date()
  current={Date()}
  // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
  minDate={Date()}
  // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
  maxDate={'2022-05-30'}
  // Handler which gets executed on day press. Default = undefined
//   onDayPress={(day) => {console.log('selected day', day)}}
//   onDayPress={(day) => setDate(day.dateString)}
  onDayPress={(day) => [setInput('startDate', day.dateString ), setDate(day.dateString)]}
  // Handler which gets executed on day long press. Default = undefined
  onDayLongPress={(day) => {console.log('selected day', day)}}
  // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
  monthFormat={'yyyy MM'}
  // Handler which gets executed when visible month changes in calendar. Default = undefined
  onMonthChange={(month) => {console.log('month changed', month)}}
  // Hide month navigation arrows. Default = false
  hideArrows={true}
  // Replace default arrows with custom ones (direction can be 'left' or 'right')
  renderArrow={(direction) => (<Arrow/>)}
  // Do not show days of other months in month page. Default = false
  hideExtraDays={true}
  // If hideArrows = false and hideExtraDays = false do not switch month when tapping on greyed out
  // day from another month that is visible in calendar page. Default = false
  disableMonthChange={true}
  // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday
  firstDay={1}
  // Hide day names. Default = false
  hideDayNames={true}
  // Show week numbers to the left. Default = false
  showWeekNumbers={true}
  // Handler which gets executed when press arrow icon left. It receive a callback can go back month
  onPressArrowLeft={subtractMonth => subtractMonth()}
  // Handler which gets executed when press arrow icon right. It receive a callback can go next month
  onPressArrowRight={addMonth => addMonth()}
  // Disable left arrow. Default = false
  disableArrowLeft={true}
  // Disable right arrow. Default = false
  disableArrowRight={true}
  // Disable all touch events for disabled days. can be override with disableTouchEvent in markedDates
  disableAllTouchEventsForDisabledDays={true}
  // Replace default month and year title with custom one. the function receive a date as parameter
  renderHeader={(date) => {/*Return JSX*/}}
  // Enable the option to swipe between months. Default = false
  enableSwipeMonths={true}
//   markedDates={
   
//     {
//             better: {selected: true, marked: true, selectedColor: 'blue'},
//   }}
/>
            </View>

           
        
         </View>


            </View>


     </View>
  
        
        
        </View>  


     </View>

      <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

       <View style={{backgroundColor: 'white',  padding: 10, paddingBottom: 20}}>
            <View>
                        <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>FEE'S</Text>
            </View>


          <View style={{  marginBottom: 25, justifyContent: 'space-evenly'}}>

                      <View style={{marginBottom: 10}}>
                          <Text style={{ fontSize: 13, color: 'grey', marginTop: 20, fontFamily: 'Manrope_500Medium'}} >Price Per Seat</Text>
                      </View>

            </View>

            
         <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
             {/*Date box*/}
                

             {/* input details  */}
            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 5, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Text style={{fontWeight: '600', color: 'white', fontSize: 20, fontFamily: 'Manrope_700Bold'}} >R</Text>
                        
                    </View>

                </View>
                
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 40, width: 200, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium'}}
                    placeholder="180"
                    keyboardType='numeric'
                    placeholderTextColor="#7d7d7d"
                     onChangeText={val => setPriceD(val)}
                    value={priceD}
                    />
                    
                </View>

            </View>

            </View>

       </View>
        
      <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

      <View style={{backgroundColor: 'white',  padding: 10, paddingBottom: 20}}>

          <View>
                      <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>PREFERENCES</Text>
          </View>


          <View style={{  marginBottom: 25, justifyContent: 'space-evenly'}}>

                      <View style={{marginBottom: 10}}>
                          <Text style={{ fontSize: 13, color: 'grey', marginTop: 20, fontFamily: 'Manrope_500Medium'}} >You can write a message you would like the passenger to see before booking</Text>
                      </View>

            </View>

          <View style={{backgroundColor:'#ebebeb',  height: 100, width: 300, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 100, fontFamily: 'Manrope_500Medium'}}
                    placeholder="No smoker please"
                    placeholderTextColor="#7d7d7d"
                     onChangeText={val => setMessageD(val)}
                    value={messageD}
                    />
                    
                </View>


      </View>



  

    {/* <View>
          <Pressable style={{ marginTop: 10}}
            // onPress={addTodo}
            onPress={
                ()=> addTodo()
            }
            >
                <View style={{backgroundColor: 'green', margin: 50, borderRadius: 10, height: 40}}>
                <View style={{alignItems:'center', flexDirection: 'row', alignContent:'space-around', padding: 10, paddingLeft: 60 }}>
                  <Text style={{color: 'white', fontWeight: '600', marginRight: 15}}  > Set Trip</Text>
                  <Feather name="arrow-right" color={'white'} size={20} />
                </View>
                
                </View>
            </Pressable>
           
    </View> */}



      <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

      <View style={{alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>

 <TouchableOpacity
         onPress={()=> addTodo()}
         style={{backgroundColor: '#5e675d', justifyContent: 'space-around', 
         alignItems:'center', width: 315, height: 45, marginTop: 15, marginBottom: 30,
         borderRadius: 5}}>
                     <View style={{justifyContent:'space-around', flexDirection:'row', alignContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontWeight: '500', color: 'white', marginLeft: 10}}>Set Trip</Text>
                     <Feather name="arrow-right" color={'white'} size={20} />
                     </View>
                </TouchableOpacity>

      </View>

      
     
       

    </ScrollView>
     </SafeAreaView>
    )
          }
}

export default RiderDriver

// const styles = StyleSheet.create({})
