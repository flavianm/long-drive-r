import React,{useState, useEffect} from 'react'
import { Dimensions, Pressable, Text, View } from 'react-native'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { useNavigation, useRoute } from "@react-navigation/core";
import { OpenMapDirections } from 'react-native-navigation-directions';

const GOOGLE_MAPS_APIKEY = 'AIzaSyAj8LKvEnwzBhZRycGM1yJJ96j7AA2snGE';

const ProgressScreen = () => {

    const typeState = useState(null);
    const route = useRoute();

 const {id, myId, theStatus, orLat, orLng, desLat, desLng} = route.params
      const origin = {
        latitude: orLat,
        longitude: orLng
    }

    const destination = {
        latitude: desLat,
        longitude: desLng
    }

    console.log(orLng)

    _callShowDirections = () => {

const startPoint = {
      longitude: '',
      latitude: ''
    } 

         const endPoint = {
      longitude: orLng,
      latitude: orLat
    }

    const transportPlan = 'd';

    // OpenMapDirections(startPoint, endPoint, transportPlan).then(res => {
    //   console.log(res)
    // });
    //  }

     _callShowDirections = () => {

        if(theStatus === 'Collect'){
            const startPoint = {
      longitude: '',
      latitude: ''
    } 

         const endPoint = {
      longitude: orLng,
      latitude: orLat
    }

    const transportPlan = 'd';

    OpenMapDirections(startPoint, endPoint, transportPlan).then(res => {
      console.log(res)
    });

    

     }

     if(theStatus === 'Delivering'){

    
    const startPoint = {
        longitude: '',
        latitude: ''
        } 

         const endPoint = {
      longitude: desLng,
      latitude: desLat
    }

    const transportPlan = 'd';

    OpenMapDirections(startPoint, endPoint, transportPlan).then(res => {
      console.log(res)
    });
        }
    }
     }

    return (
        <View>
            <MapView
            style={{height: Dimensions.get('window').height-200, width: '100%'}}
            provider={PROVIDER_GOOGLE}
                showsUserLocation={true}
                followsUserLocation={true}
                showsBuildings={true}
                showsTraffic={false}
                showsMyLocationButton={true}
                loadingEnabled={true}
                initialRegion={
                    {
                    latitude: -26.040340,
                    longitude: 27.924320,
                    latitudeDelta: 0.0222,
                    longitudeDelta: 0.0121,
            }}
  >
      <Marker
      coordinate={{latitude: orLat, longitude: orLng}}
      title={'Parcel Origin'}
      description={'Where Parcel is coming from'}
    />
    <Marker
      coordinate={{latitude: desLat, longitude: desLng}}
      title={'Parcel Destination'}
      description={'Where Parcel is going to'}
      pinColor={'blue'}
      
    />
    {/* <MapViewDirections
                    origin={{latitude: orLat, 
                      longitude: orLng,
                    }}
                    onReady={{}}
                    destination={{latitude: desLat, 
                                  longitude: desLng,}}
                    apikey={GOOGLE_MAPS_APIKEY}
                    strokeWidth={4}
                    strokeColor="black" 
                /> */}
  </MapView>
               <View>

                <Pressable style={{width:'100%', height: 50, backgroundColor: 'green', justifyContent:'space-around',
                alignItems:'center'}}
                onPress={() => { _callShowDirections() }}>
                        <Text style={{fontWeight: '600', color: 'white'}}> SET COLLECTED</Text>
                   </Pressable>

                   <Pressable style={{width:'100%', height: 50, backgroundColor: 'red', justifyContent:'space-around',
                alignItems:'center'}}
                onPress={() => { _callShowDirections() }}>
                        <Text style={{fontWeight: '600', color: 'white'}}> NAVIGATE</Text>
                   </Pressable>
               </View>
        </View>
    )
}

export default ProgressScreen

// const styles = StyleSheet.create({})
