import React from "react";
import {View, Text, Dimensions} from 'react-native';
import HomeMap from "../../components/HomeMap";
import CovidMessage from "../../components/CovidMessage";
import HomeSearch from "../../components/HomeSearch";
import HoScreen from '../../components/HoSearch';
import CovidMessageGoods from "../../components/CovidMessageGoods";
import HoSearchParcel from "../../components/HoSearchParcel";
import HoSearchGoods from "../../components/HoSearchGoods";
const SecScreenGoods = (props) =>{
    return (
        <View style={{backgroundColor: 'white'}}>
             {/*HomeMap*/}
             {/* <View style={{height: Dimensions.get('window').height - 400}}>
                <HomeMap/>
           </View> */}
            {/*covid message*/}
            <View style={{height: 80}}>
             <CovidMessageGoods />
            </View>
           
             {/*Search*/}
             <View style={{marginBottom: 60}}>
                <HoSearchGoods/>
             </View>
            
        </View>
    )
};

export default SecScreenGoods;