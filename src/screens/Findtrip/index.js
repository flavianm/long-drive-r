import React, { useEffect, useState } from "react";
import {View, Text, TextInput, SafeAreaView, ScrollView, Pressable, Image} from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import PlaceRow from "./PlaceRow";
import styles from "./styles";
import { installWebGeolocationPolyfill } from "expo-location";
import { useNavigation } from "@react-navigation/core";
import { Feather, FontAwesome5 } from "@expo/vector-icons";
import { Picker } from "@react-native-picker/picker";
import { DatePickerPick } from "../DatePick";


const GOOGLE_PLACES_API_KEY = 'AIzaSyAj8LKvEnwzBhZRycGM1yJJ96j7AA2snGE';

const homePlace = {
    description : 'Home',
    geometry: {location: {lat: -26.192910, lng: 28.035990 }},
};

const workPlace = {
    description : 'Work',
    geometry: {location: {lat: -26.193910, lng: 28.045990 }},
};


const FindSearch = (props) =>{

    const [fromText, setFromText] = useState('');
    const [destinationText, setDestinationText] = useState('');

    const [originPlace, setOriginPlace] = useState(null);
    const [destinationPlace, setDestinationPlace] = useState(null);

    const navigation = useNavigation();

    useEffect(() => {
        if(originPlace && destinationPlace){
            navigation.navigate('SearchResult', {
                originPlace,
                destinationPlace
            });
        }
    }, [originPlace, destinationPlace])



    return (
        <View style={{backgroundColor: 'white'}} >
            
             <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 15}}>
                 {/* <View style={{ marginTop: 30, marginRight: 10}}>
                     <FontAwesome5 name={'search'} size={20} />
                </View> */}
                
                <Text style={{ fontSize: 30, fontWeight: '600', color: 'black', marginTop: 30}}>
                    Find a trip
                </Text>
               
                
            </View> 

            

            <View style={styles.container}>

                   <GooglePlacesAutocomplete 

                        placeholder="Pick Up From ?"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za'
                        }}
                        enablePoweredByContainer={false}
                        suppressDefaultStyles
                        currentLocation={true}
                        currentLocationLabel='Current Location'
                        enableHighAccuracyLocation={true}
                        styles={{
                            textInput: styles.textInput,
                            container: styles.autoCompleteContainer,
                            listView: styles.listView,
                            separator: styles.separator
                            
                        }}
                        onPress={(data, details = null) =>{
                            setOriginPlace({data, details});
                        }}
                        textInputProps={{
                            leftIcon: {}
                        }}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                        renderRow={(data) => <PlaceRow data={data} />}
                        renderDescription={(data) => data.description || data.vicinity}
                        predefinedPlaces={[homePlace, workPlace]}
                    />

                    <GooglePlacesAutocomplete 

                        placeholder="Where To"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za'
                        }}
                        suppressDefaultStyles
                        styles={{
                            textInput: styles.textInput,
                            //  container:{
                            //     position: 'absolute',
                            //     top: 85,
                            //     left: 10,
                            //     right: 10,
                            // },
                            container:{
                                ...styles.autoCompleteContainer,
                                top: 85,
                                
                            },
                            // listView:{...styles.listView,
                            // top: 70},
                            separator: styles.separator
                        }}
                        onPress={(data, details = null) =>{
                            setDestinationPlace({data, details});
                        }}
                        enablePoweredByContainer={false}
                        renderRow={(data) => <PlaceRow
                            data={data} />}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                    />

                    {/* cricle near origin input */}
                        <View style={styles.circle} />

                    {/* long line connecting cirlces */}
                        <View style={styles.line} />

                    {/* square near destintaion input */}
                        <View style={styles.square} />

                        
                    <View>

                        {/* <Image
                        style={{marginLeft: 30, height: 300, width: 300, marginTop: 250, alignContent: 'center', alignItems:'center'}}
                        source={require('../../../assets/air.jpg')}
                        /> */}
                    </View>

                     
                       
            </View>

           

        </View>

       
    );
};

export default FindSearch;