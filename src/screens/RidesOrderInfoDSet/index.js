import React, {useState, useEffect, useCallback} from 'react'
import { StyleSheet, Image, Text, TouchableOpacity, View, SafeAreaView, ScrollView, RefreshControl, ActivityIndicator } from 'react-native'
import { useNavigation, useRoute } from "@react-navigation/core";
import {Auth} from 'aws-amplify';
// import * as mutations from './graphql/mutations';
import * as mutations from '../../graphql/mutations'
import * as queries from '../../graphql/queries';
import {API, graphqlOperation} from 'aws-amplify';
import { getGoodsOrder, getRequests, getRides, getUser } from '../../graphql/queries';
import { Feather, FontAwesome, FontAwesome5, Fontisto, Ionicons, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';
import { onParcelOrderUpdated, onRidesOrderUpdated, onRequestsUpdated } from '../../graphql/subscriptions';
import AnimatedLoader from "react-native-animated-loader";


import { listParcelOrders } from '../../graphql/queries';

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const RidesOrderInfoDSet = (props) => {

 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

   useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 3000);
  }, [])


const navigation = useNavigation();
 const [user, setuser] = useState(null);
 const [todo, setTodo] = useState([]);
 const [tripRide, setTripRide] = useState([]);
 const [loading, setLoading] = useState('true'); 
const [tempBal, setTempBal] = useState();
 const [userInfo ,setUserInfo] = useState();
  const [refreshing, setRefreshing] = useState(false);
    const [visible, setVisible] = useState(true);


 const haversine = require('haversine')

    const typeState = useState(null);
    const route = useRoute();

       const onRefresh = useCallback(() => {

    setRefreshing(true);

                fetchUsers();
                fetchOrderInfo();


                // pop up that sauys "you are upto date"
           


    wait(2000).then(() => setRefreshing(false));
  }, []);
    
    const {id, thestatus} = route.params


    const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setuser(userSub);

            // fetch passenger balance 

            
            
    }

    const fetchOrderInfo = async()=>{
      

            // get request 

              const todoData = await API.graphql(graphqlOperation(getRequests, { id: id}))
            // const todos = todoData.data.getParcelOrder.contents
              const todos = todoData.data.getRequests
              const rideIdd = todos.rideId
            setTodo(todos)
            setLoading('false')


            // getRide

             const RideData = await API.graphql(graphqlOperation(getRides, { id: rideIdd}))
             const rideIn = RideData.data.getRides
            //  const ridenew = rideIn.price
             setTripRide(rideIn)

             

            // getCustomer Info

            const UserData = await API.graphql(graphqlOperation(getUser, { id: todos.userId}))
            const userIn = UserData.data.getUser
            const namethe = userIn.name
            setUserInfo(userIn)

            // get LDTemp Info

            const TempData = await API.graphql(graphqlOperation(queries.getLdTemp, { id: '1'}))
            const TempQuery = TempData.data.getLdTemp
            setTempBal(TempQuery)
    }

     useEffect(() => {
            
                fetchUsers();
                fetchOrderInfo();
            },[todo])


     const theImage = 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/images/products/keyboard1.jpg'


let reqUpdate;

 function setUpSus(){

                 reqUpdate = API.graphql(graphqlOperation(onRequestsUpdated)).subscribe( {next: (bruu) => {
                    setTodo(bruu)

                }, }) 


            }


   useEffect(() =>{
                setUpSus();

                return() =>{
                    reqUpdate.unsubscribe();   
                };

            },[]);          

    const requestDeduct = {
        id: id,
        status: 'Accepted',
        active: 'yes',
    };

    const RideDeduct = {
        id: todo?.rideId,
        active: 'yes',
        availSeats: (parseFloat(tripRide?.availSeats) - parseFloat(todo?.seats)),
        tripFee: parseFloat(tripRide?.tripFee) + parseFloat(todo?.tripFee) 
    };

     const declineDetails = {
        id: id,
        status: 'declined',
        active: 'no',
    };

     const userBalUpdate = {
        id: userInfo?.id, 
        balance: (parseFloat(userInfo?.balance) + (parseFloat(todo?.tripFee) + (parseFloat(todo?.tripFee) * parseFloat('0.035')))) , // get current balance and get tripfee + booking fee
    };

     const ldTempBalUpdate = {
        id: '1',
        balance: (parseFloat(tempBal?.balance) - (parseFloat(todo?.tripFee) + (parseFloat(todo?.tripFee) * parseFloat('0.035')))),  // get current balance and get tripfee + booking fee
    };

  


    const Press = async ()=>{


        if(tripRide?.availSeats > 0){
           
            if(todo?.seats < tripRide?.availSeats || todo?.seats === tripRide?.availSeats){
                
                await API.graphql({query: mutations.updateRides, variables: {input: RideDeduct}});
                await API.graphql({query: mutations.updateRequests, variables: {input: requestDeduct}});
                
                 Toast.show({
                    type: 'success',
                    text1: 'Rides',
                    text2: 'You have accepted the trip',
                    visibilityTime: 5000,

                    });
                
                
                navigation.navigate('home')
            
            }else{
                Toast.show({
                    type: 'error',
                    text1: 'Seats',
                    text2: 'You do not have enough seats',
                    visibilityTime: 5000,

                    });
            }

        }else{
            Toast.show({
                    type: 'error',
                    text1: 'Seats',
                    text2: 'No seats available at all',
                    visibilityTime: 5000,

                    });
        }

  
        }


    const Decline = async ()=>{
           await API.graphql({ query: mutations.updateRequests, variables: {input: declineDetails}});
           
           // refund amount back to customer
           await API.graphql({ query: mutations.updateUser, variables: {input: userBalUpdate}}); 

           // deduct from Temp
           await API.graphql({ query: mutations.updateLdTemp, variables: {input: ldTempBalUpdate}});  

           navigation.navigate('home')
              Toast.show({
      type: 'success',
      text1: 'Trip Decline',
      text2: 'You have declined the trip',
      visibilityTime: 5000,
    });  
        
        }

const start = {
  latitude: todo?.originLatitude,
  longitude: todo?.originLongitude
}

const end = {
  latitude: todo?.destLatitude,
  longitude: todo?.destLongitude
}

if (!fontsLoaded) {
            return(
                <View>
                   <Text></Text>
                </View>
            )
        } else{

    return (

        <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>

            {/* <AnimatedLoader
                visible={visible}
                overlayColor="rgba(255,2555,255,7)"
                animationStyle={{width: 75, height: 75}}
                speed={1}
                >
                </AnimatedLoader> */}





            <ScrollView style={{backgroundColor: 'white'}} showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false}>


            <View style={{ backgroundColor: 'white', padding: 20}}>
               <View>
                    <Text style={{ paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 20, marginBottom: 15, fontFamily: 'Manrope_700Bold'}}>Request</Text>
                </View>

                <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}} />

                <View>

                    <View style={{ flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>

                            {/* image */}

                            <View>
                                    <View style={{height: 100, marginRight: 20, justifyContent: 'space-around'}} >
                                        <Image
                                            source={{uri: theImage}}
                                            style={{width: 50, height: 50, borderRadius: 10}}
                                        />
                                    </View>
                            </View>


                            {/* driven by */}
                            <View>
                                
                                <Text style={{fontWeight: '500', fontSize: 15, fontFamily: 'Manrope_600SemiBold'}}>{userInfo?.name || `Loading...`} {userInfo?.surname}</Text>

                            </View>


                     </View>


             <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}} />

                    <View>
                        <Text style={{fontWeight: '500', marginTop: 20, fontFamily: 'Manrope_700Bold'}}>ROUTE</Text>
                    </View>


                    <View>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10, backgroundColor:'green', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{fontSize: 13, fontWeight: '500', width: 280, fontFamily: 'Manrope_600SemiBold'}}>{todo?.originDesc || `Loading...`}</Text>
                                </View>
                            </View>
                     </View>
                     <View>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10,backgroundColor:'red', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{fontSize: 13, fontWeight: '500', width: 280, fontFamily: 'Manrope_600SemiBold'}}>{todo?.destDesc || `Loading...`}</Text>
                                </View>
                            </View>
                     </View>

                    <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />


                    <View style={{flexDirection: 'row'}}>
                        <View style={{width: 50, height: 50}}>
                            <Ionicons name={'warning'} color="grey" size={40} />
                        </View>
                            <View style={{width: 300}}>
                                <Text style={{color: 'grey', fontFamily: 'Manrope_500Medium', fontSize: 13}}>Please note that a passenger may book multiple seats for multiple people. Provided information is for the passenger that booked the trip</Text>
                            </View>
                    </View>

                    <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />


                    <View>
                        <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>TRIP ITINERARY</Text>
                    </View>

                     <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialCommunityIcons name='gender-male-female' size={14} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{userInfo?.gender}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='smoke-free' size={14} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{userInfo?.smoke}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <FontAwesome5 name='wheelchair' size={14} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{userInfo?.wheelchair}</Text>
                        </View>

                    </View>

                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={14} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{todo?.seats} Seats</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <FontAwesome name='taxi' size={14} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>D2D : {todo?.taxi || `No`}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='shopping-bag' size={14} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{todo?.luggageMax || `Small`}</Text>
                        </View>

                    </View>

                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={14} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>R{(parseFloat(todo?.tripFee)).toFixed(2)}</Text>
                        </View>


                    </View>


                </View>
               
               
                                    <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />


                <View style={{flexDirection: 'row'}}>

                <TouchableOpacity style={{margin: 15, backgroundColor: 'green', height: 35, width: 130, borderRadius: 5, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
            onPress={()=> Press()}
            >
                <View>
                    <Text style={{color: 'white', fontWeight: '500',  fontFamily: 'Manrope_600SemiBold'}}>Accept Order</Text>
                    
                </View>
            </TouchableOpacity>
            <TouchableOpacity style={{margin: 15, backgroundColor: 'red', height: 35, width: 130, borderRadius: 5, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
            onPress={()=> Decline()}
            >
                <View>
                    <Text style={{color: 'white', fontWeight: '500',  fontFamily: 'Manrope_600SemiBold'}}>Decline Order</Text>
                    
                </View>
            </TouchableOpacity>
        

                </View>




            </View>    

           
        
            <View style={{margin: 15, backgroundColor: 'white', borderRadius: 10, padding: 20}}>


                

            </View>



        <View style={{marginTop: 20}}>
            
           
        </View>
        <ActivityIndicator animating={loading} size="small"/>
         </ScrollView>
        </SafeAreaView>
    )
        }
}

export default RidesOrderInfoDSet

const styles = StyleSheet.create({})
