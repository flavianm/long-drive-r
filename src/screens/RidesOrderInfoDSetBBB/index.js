import React, {useState, useEffect, useCallback} from 'react'
import { StyleSheet, Text, Image, TouchableOpacity, View, SafeAreaView, ScrollView, ActivityIndicator, RefreshControl, Pressable, FlatList } from 'react-native'
import { useNavigation, useRoute } from "@react-navigation/core";
import {Auth} from 'aws-amplify';
// import * as mutations from './graphql/mutations';
import * as mutations from '../../graphql/mutations'
import * as queries from '../../graphql/queries';
import {API, graphqlOperation} from 'aws-amplify';
import { getGoodsOrder, getRides } from '../../graphql/queries';
import { Feather, FontAwesome, FontAwesome5, Fontisto, Ionicons, MaterialCommunityIcons, MaterialIcons, Zocial } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';
import Modal from "react-native-modal";

import AnimatedLoader from "react-native-animated-loader";
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

import { listParcelOrders } from '../../graphql/queries';

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const RidesOrderInfoDSetBBB = (props) => {

const navigation = useNavigation();
 const [user, setuser] = useState(null);
 const [todo, setTodo] = useState([]);
 const [passengers, setPassengers] = useState([]);
 const [refreshing, setRefreshing] = useState(false);
 const [loading, setLoading] = useState('true');
 const [accReq, setAccReq] = useState([]);
 const [userPlus, setUserPlus] = useState(null);
 const [userIdquery, setUserIdquery] = useState(null);
 const [awaitt, setAwaitt] = useState();

const [visible, setVisible] = useState(true);


 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });


  useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 3000);
  }, [])





 const onRefresh = useCallback(() => {


    setRefreshing(true);

                fetchUsers();
                 fetchOrderInfo();
    wait(2000).then(() => setRefreshing(false));
  }, []);

const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
    
  };


 const haversine = require('haversine')

    const typeState = useState(null);
    const route = useRoute();
    
    const {id, thestatus} = route.params


    const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setuser(userSub);
            
    }

    const fetchOrderInfo = async()=>{
        const todoData = await API.graphql(graphqlOperation(getRides, { id: id}))
            // const todos = todoData.data.getParcelOrder.contents
              const todos = todoData.data.getRides
            setTodo(todos)
            setLoading('false')
            
            
             const userplus = await Auth.currentAuthenticatedUser({bypassCache: true});

            const reqPlus = await API.graphql({query: queries.listRequests, variables:  
                {filter : 
                   {and: [{ rideId: 
                        {eq: 
                            (id)
                        }
                        },
                        { status: 
                        {eq: 
                            'Accepted'
                        }
                        }
                       
                    ]
                 }, 
                } 
        })
            const pend = reqPlus.data.listRequests.items
            setAccReq(pend)



        
        

                


    }

     useEffect(() => {
            
                fetchUsers();
                fetchOrderInfo();
            },[])


     const userGot = async()=>{

        console.log(userIdquery)



        const todoData = await API.graphql(graphqlOperation(queries.getUser, { id: userIdquery}))
        const todos = todoData.data.getUser
            setUserPlus(todos)
           

     }   
     
     
     useEffect(() => {
            
               userGot();
            },[userIdquery])




    const todoDetails = {
        id: id,
        driverId: user,
        status: 'Accepted',
        active: 'yes'
    };

     const declineDetails = {
        id: id,
        driverId: user,
        status: 'deleted',
        active: 'no',
    };

    const Press = async ()=>{
           await API.graphql({ query: mutations.updateGoodsOrder, variables: {input: todoDetails}});
            navigation.navigate('GoodsOrdersDA')
           console.log('done')
        
        }
         const Decline = async ()=>{
           await API.graphql({ query: mutations.updateGoodsOrder, variables: {input: declineDetails}});
            
            Toast.show({
            type: 'success',
            text1: 'Logistics',
            text2: 'The order has been declined successfully',
            visibilityTime: 5000,
    });

    navigation.navigate('GoodsOrdersDA')
        
        }

        const showthe = async () => {
            
            setModalVisible(true)

        }


const start = {
  latitude: todo.originLatitude,
  longitude: todo.originLongitude
}

const end = {
  latitude: todo.destLatitude,
  longitude: todo.destLongitude
}

     const theImage = 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/images/products/keyboard1.jpg'


    const ItemSeperatorView = () => {
                return(
                    <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}}
                    />
                )
            }

   const [zeName, setzeName] = useState();
   const placeholder = () =>{
       return(
           <View>
               <MaterialIcons name='person-add' size={20} />
           </View>

       )
   }

    const ItemView2 = ({item}) =>{   


                return(

              

              <View style={{ backgroundColor: 'white'}} >

                     <View style={{ flexDirection: 'row', marginLeft: 20, alignContent: 'center', alignItems: 'center'}}>

                <View>
                        <View style={{height: 100, marginRight: 20, justifyContent: 'space-around'}} >
                            <Image
                                source={{uri: item.imageUrl}}
                                style={{width: 50, height: 50, borderRadius: 10}}
                            />
                        </View>
                </View>

                 <View>
                        
                        <Text style={{fontWeight: '500', fontSize: 15, fontFamily: 'Manrope_600SemiBold'}}>{item.name || `Name`} {item.surname || `Surname`}</Text>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                            <FontAwesome name={'location-arrow'} color={'grey'} size={15} />
                            <Text style={{marginLeft: 10, color: 'grey',  fontFamily: 'Manrope_500Medium', fontSize: 12}}> {}KM From Your Location</Text>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                            <MaterialCommunityIcons name={'map-marker-distance'} color={'grey'} size={15} />
                            <Text style={{marginLeft: 10, color: 'grey', fontFamily: 'Manrope_500Medium', fontSize: 12}}>To {}KM From Your Destination</Text>
                        </View>
                        

                    </View>

                    


            </View>    

                    <View style={{margin: 10}}>

                     <View style={{marginLeft: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                                
                                       <View style={{width: 10, height: 10, backgroundColor:'green', borderRadius: 50, marginRight: 10}}>

                                </View>

                                     <View>
                                     <Text style={{fontSize: 13, fontWeight: '500', width: 280, fontFamily: 'Manrope_600SemiBold'}}>{item?.originDesc || `Loading...`}</Text>
                                    </View>
                             </View>

                             <View>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10,backgroundColor:'red', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{fontSize: 13, fontWeight: '500', width: 280, fontFamily: 'Manrope_600SemiBold'}}>{item?.destDesc || `Loading...`}</Text>
                                </View>
                            </View>
                     </View>
                    </View>


                    <View style={{flexDirection: 'row'}}>

                       

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>R {item.tripFee || `0.00`} Trip Total</Text>
                        </View>

                     <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 7}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.seats || `0`}  Booked</Text>
                        </View>



                    </View>


                     <View style={{flexDirection: 'row'}}>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.status || `Loading...`}</Text>
                        </View>


                         <View style={{flexDirection: 'row', marginTop: 15}}>
                                <View style={{alignItems: 'center', marginRight: 10, justifyContent: 'center', width: 110, height: 35, backgroundColor: 'red', borderRadius: 10}}>
                                    <Text style={{fontWeight: '500', color: 'white', fontSize: 12, fontFamily: 'Manrope_600SemiBold'}}>REPORT</Text>
                                </View>
                         </View>

                    </View>

                    </View>
                   
                    </View>


              
                 )
            }


if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{
    return (

        <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>

              <AnimatedLoader
                visible={visible}
                overlayColor="rgba(255,2555,255,7)"
                animationStyle={{width: 75, height: 75}}
                speed={1}
            >
            </AnimatedLoader>



            <Modal isVisible={isModalVisible}
         swipeDirection={'down'}
         onSwipeComplete={()=> setModalVisible(false)}
         style={{ backgroundColor: 'white', marginLeft: 0, marginRight: 0}}>

         <View style={{ flex: 1 }}>

            <View style={{width: '100%', marginTop: 20, alignContent: 'center', alignItems: 'center'}}> 
                 <Pressable onPress={toggleModal}>
               <View style={{flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
                   <Ionicons name="close-circle" size={30} color="red" />
                   <Text style={{fontWeight: '500'}}>Close</Text>
               </View>
           </Pressable>
            </View>

                <Text style={{fontSize: 25, fontWeight: '500'}}>{userPlus?.name || `not availble`} {userPlus?.surname || `not availble`}</Text>
                <Text>Gender : {userPlus?.gender || `not availble`}</Text>
                <Text>Does Passenger smoke during trips ? : {userPlus?.smoke}</Text>
                <Text>Phone Number : {userPlus?.phone_number || `not availble`}</Text>
                <Text>Use of Wheel Chair ? : {userPlus?.wheelchair || `not availble`}</Text>
                <Text>Chronic Illnesses you should be aware of ? : {userPlus?.chronic || `not availble`}</Text>
                <Text>Illness name : {userPlus?.chronic_name || `not availble`}</Text>
                <Text>Emergency contact : {userPlus?.emergency_name || `not availble`}</Text>
                <Text>Emergency Number : {userPlus?.emergency_number || `not availble`}</Text>
         </View>




         </Modal>


            <ScrollView 
            style={{backgroundColor: 'white'}}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            refreshControl={
            <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
            />
            }
            >


            <View style={{ backgroundColor: 'white', padding: 20}}>
                
               <View style={{flexDirection: 'row'}}>
                    <Text style={{ paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 20, marginBottom: 15, fontFamily: 'Manrope_700Bold'}}>Trip Details</Text>
                </View> 
                
                <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}} />
            

                <View>
                    <Text style={{fontWeight: '500', marginTop: 20, fontFamily: 'Manrope_700Bold'}}>ROUTE</Text>
                </View>

                    <View>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10, backgroundColor:'green', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{fontSize: 13, fontWeight: '500', width: 280, fontFamily: 'Manrope_600SemiBold'}}>{todo?.originDesc || `Loading...`}</Text>
                                </View>
                            </View>
                     </View>
                
                <View>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10,backgroundColor:'red', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{fontSize: 13, fontWeight: '500', width: 280, fontFamily: 'Manrope_600SemiBold'}}>{todo?.destDesc || `Loading...`}</Text>
                                </View>
                            </View>
                     </View>

                 <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

                <View style={{flexDirection: 'row'}}>
                        <View style={{width: 50, height: 50}}>
                            <Ionicons name={'warning'} color="grey" size={40} />
                        </View>
                        <View style={{width: 300}}>
                            <Text style={{color: 'grey', fontFamily: 'Manrope_400Regular', fontSize: 13}}>Please note that a passenger may book multiple seats for multiple people. Provided information is for the passenger that booked the trip</Text>
                        </View>
                </View>


             <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

                    <View>
                        <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>TRIP ITINERARY</Text>
                    </View>

                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Fontisto name='date' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>{todo?.startDate}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>{todo?.status}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>R{todo?.tripFee}</Text>
                        </View>

                        

                    </View>

                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>{todo.availSeats} / {todo.totalSeats}</Text>
                        </View>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialCommunityIcons name='map-marker-distance' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>{haversine(start, end).toFixed(1)}KM</Text>
                        </View>

                    </View>


              <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />
               

            <View>
                <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>PASSENGERS</Text>
            </View>  



                <FlatList
                    data={accReq}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView2}
                />





            </View>     




          
        


       

         </ScrollView>
        </SafeAreaView>
    )
        }
}

export default RidesOrderInfoDSetBBB

const styles = StyleSheet.create({

     todo: {  backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20},
  input: { height: 50, backgroundColor: '#ddd', marginBottom: 10, padding: 8 },
  todoName: { fontSize: 15 },
})
