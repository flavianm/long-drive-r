import React, {useState, useEffect, useCallback} from 'react'
import { StyleSheet, ActivityIndicator, RefreshControl, Text, View, TouchableOpacity, Pressable,  ScrollView, SafeAreaView, FlatList } from 'react-native'
import { useNavigation, useRoute } from "@react-navigation/core";
import * as mutations from '../../graphql/mutations'
import { API, graphqlOperation } from "aws-amplify";
import * as queries from '../../graphql/queries';
import {Auth} from 'aws-amplify';
import { OpenMapDirections } from 'react-native-navigation-directions';
import {getUser, getLdTemp, getLd, getRides } from '../../graphql/queries';
import { onLdTempUpdated, onRidesOrderUpdated } from '../../graphql/subscriptions';
import { Feather, FontAwesome5, Fontisto, Ionicons, MaterialCommunityIcons, MaterialIcons, Zocial } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';
import Modal from "react-native-modal";
import AnimatedLoader from "react-native-animated-loader";

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}
const RidesDAOInfo = (props) => {

const navigation = useNavigation();



 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });


const [visible, setVisible] = useState(true);

 useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 4000);
  }, [])



    const onRefresh = useCallback(() => {

    setRefreshing(true);

                fetchCustomer();

    wait(2000).then(() => setRefreshing(false));
  }, []);

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
    
  };

    const typeState = useState(null);
    const route = useRoute();

    const [custUser, setCustUser] = useState([])
     const [user, setuser] = useState(null);
     const [status, setStatus] = useState([]);
     const [active, setActive] = useState([]);
       const [refreshing, setRefreshing] = useState(false);
       const [loading, setLoading] = useState('true');
 const [accReq, setAccReq] = useState([]);
  const [userIdquery, setUserIdquery] = useState(null);
  
  const [reqId, setReqId] = useState(null);
    const [requestPlus, setRequestPlus] = useState(null);

 const [userPlus, setUserPlus] = useState(null);
 const [rideInfo ,setRideInfo] = useState(null);

 const [totalF, setTotalF] = useState(null);


  //wallet balances

    const [curBal, setCarBal] = useState();
    const [ldTempBal, setLdTempBal] = useState();
    const [ldBal, setLdBal] = useState();




    const {id, myId, originDesc, destDesc, price, theStatus, orLat, orLng, desLat, desLng, custId, date, type, breakage, flame, qty, contents, recName, recSurname, recCode, recNumber} = route.params

         

    async function fetchCustomer() {

             

            // const todoData = await API.graphql(graphqlOperation(getUser, { id: custId}))

            //   const todos = todoData.data.getUser
            // setCustUser(todos)
            setLoading('false')


            // my bank balance

            const gotData = await API.graphql(graphqlOperation(getUser, { id: myId}))
            const mydata = gotData.data.getUser
            setCarBal(mydata.balance)

        // fetch ldTemp Account

         const ldTempData = await API.graphql(graphqlOperation(getLdTemp, { id: '1'}))
            const ldTempBalance = ldTempData.data.getLdTemp
            setLdTempBal(ldTempBalance.balance)


        // fetch ld main account

            const ldData = await API.graphql(graphqlOperation(getLd, { id: '1'}))
            const ldBalance = ldData.data.getLd
            setLdBal(ldBalance.balance)

        // get ride information 

            const rideData = await API.graphql(graphqlOperation(getRides, { id: id}))
            const rideDataM = rideData.data.getRides
            setTotalF(rideDataM.tripFee)


        // List request items here

            const reqPlus = await API.graphql({query: queries.listRequests, variables:  
                {filter : 
                   {and: [{ rideId: 
                        {eq: 
                            (id)
                        }
                        },
                        { status: 
                        {eq: 
                            'Accepted'
                        }
                        }
                       
                    ]
                 }, 
                } 
        })
            const pend = reqPlus.data.listRequests.items
            setAccReq(pend)


        const todoData = await API.graphql(graphqlOperation(queries.getUser, { id: userIdquery}))
        const todos = todoData?.data.getUser
            setUserPlus(todos)

        // get specific request details

        const speReqData = await API.graphql(graphqlOperation(queries.getRequests, {id: reqId}))
        const speReqDataPlus = speReqData?.data.getRequests
        setRequestPlus(speReqDataPlus)

     }

     useEffect(() => {
            
                fetchCustomer();
            },[ldTempBal, userIdquery, reqId, rideInfo])

    useEffect(() => {
            
                setStatus(theStatus)
            },[])

  let subsUpdate;
  let rideUpd;
 function setUpSus(){
   

     subsUpdate = API.graphql(graphqlOperation(onLdTempUpdated)).subscribe( {next: (daraa) => {
         setLdTempBal(daraa) }})

     rideUpd = API.graphql(graphqlOperation(onRidesOrderUpdated)).subscribe( {next: (dara) => {
         setRideInfo(dara)    
     }, }) 

 }

  useEffect(() =>{
        setUpSus();

        return() =>{
            subsUpdate.unsubscribe();
            rideUpd.unsubscribe();
        };

    },[]);


      const todoDetails = {
        id: id,
        status: 'cancelled',
        active: 'no'
    };

    const fetchingPassengers = {
        id: id,
        status: 'fetchingPassengers',
        active: 'yes'
    };

    const ToDestination = {
        id: id,
        status: 'ToDestination',
        active: 'yes'
    };

    const DroppedOff = {
        id: id,
        status: 'DroppedOff',
        active: 'no'
    };

     const updateMyBank = {
        id: myId,
        balance: (parseFloat(totalF) - (parseFloat(totalF) * parseFloat('0.10'))) + parseFloat(curBal),
    };

    const updateTemp = {
        id: '1',
        balance:  (parseFloat(ldTempBal) - (parseFloat(totalF) + (parseFloat(totalF) * parseFloat('0.035')))),
    };

    const updateLdWallet = {
        id: '1',
        balance: (parseFloat(ldBal) + ((parseFloat(totalF) * parseFloat('0.10')) + (parseFloat(totalF) * parseFloat('0.035')))),
    };

    const Press = async ()=>{
           await API.graphql({ query: mutations.updateRides, variables: {input: todoDetails}});
           setStatus('cancelled')
           setActive('no')
            Toast.show({
      type: 'success',
      text1: 'Logistics',
      text2: 'Your trip has been successfully declined',
      visibilityTime: 5000,

    });
           navigation.navigate('home')
        console.log('done')
        }

        const CollectPress = async ()=>{
           await API.graphql({ query: mutations.updateRides, variables: {input: fetchingPassengers}});
           setStatus('fetchingPassengers')
           Toast.show({
      type: 'info',
      text1: 'Trip',
      text2: 'Trip started successfully, proceed to fetch passengers',
      visibilityTime: 5000,

    });
        console.log('done')
        }

         const ReadyToDeliver = async ()=>{
           await API.graphql({ query: mutations.updateRides, variables: {input: ToDestination}});
           setStatus('ToDestination')
           setActive('yes')
            Toast.show({
      type: 'info',
      text1: 'Order',
      text2: 'Passengers fetched, proceed to destination',
      visibilityTime: 5000,

    });
        console.log('collected, ready to deliver')
        }

          const DeliveredPress = async ()=>{
           await API.graphql({ query: mutations.updateRides, variables: {input: DroppedOff}});
           setStatus('DroppedOff')
           setActive('no')
           Toast.show({
      type: 'info',
      text1: 'Order',
      text2: 'Order updated to delivered, press Finish Trip to complete the trip',
      visibilityTime: 5000,

    });
        console.log('delivered')
        }

        const RequestCash = async ()=>{
         
           // update bank amount of driver
        await API.graphql({ query: mutations.updateUser, variables: {input: updateMyBank}});
            // update LdTemp
            await API.graphql({ query: mutations.updateLdTemp, variables: {input: updateTemp}});

            // update Ld Bank
            await API.graphql({ query: mutations.updateLd, variables: {input: updateLdWallet}});


            Toast.show({
      type: 'info',
      position: 'bottom',
      text1: 'Wallet',
      text2: 'R' + (parseFloat(totalF) - (parseFloat(totalF) * parseFloat('0.10'))) +' has been credited to you account',
      visibilityTime: 5000,

    });

        // console.warn( (parseFloat(price) - (parseFloat(price) * parseFloat('0.10'))) +' has been credited to you account')
        navigation.navigate('home')
        }
        const StatusStatus = () =>{

            
             if(status === 'driver'){
            return(
                 <TouchableOpacity onPress={
                 ()=>
                 CollectPress()
                } 
                style={{margin: 15, backgroundColor: 'green', height: 45, width: 130, borderRadius: 5, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}> 
                 <Text style={{fontSize: 15, fontWeight: '500', color: 'white'}}>
                    Start Trip
                   </Text>
                </TouchableOpacity>

            )
        }
          if(status === 'ToDestination'){
            return(
                     <View>

                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10}}>
                <TouchableOpacity onPress={
                 ()=>
                 DeliveredPress()
                } 
                style={{marginRight: 15, marginTop: 10, marginLeft: 15, backgroundColor: 'orange', height: 35, width: 130, borderRadius: 5, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}> 
                 <Text style={{fontSize: 15, fontWeight: '500', color: 'white'}}>
                    Dropped Off
                   </Text>
                </TouchableOpacity>

                <TouchableOpacity style={{marginLeft: 15, marginTop: 10, backgroundColor: 'red', height: 35, width: 130, borderRadius: 5, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
                onPress={()=> Press()}
                >
                    <View>
                        <Text style={{fontWeight: '500', color: 'white'}}>Cancel Order</Text>
                    </View>
                </TouchableOpacity>



                </View>

                
                    {/* <Pressable style={{width: 300, borderRadius: 10, height: 50, backgroundColor: 'green', justifyContent:'space-around',
                alignItems:'center', marginLeft: 20}}
                onPress={() =>  callShowDirections() }>
                        <Text style={{fontWeight: '600', color: 'white'}}> NAVIGATE D</Text>
                   </Pressable> */}

                </View>


            )

            
       
        }

            
            if(status === 'fetchingPassengers'){
            return(


                <View>

                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10}}>
                <TouchableOpacity onPress={
                 ()=>
                 ReadyToDeliver()
                } 
                style={{marginRight: 15, marginTop: 10, marginLeft: 15, backgroundColor: 'orange', height: 35, width: 130, borderRadius: 10, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}> 
                 <Text style={{fontSize: 15, fontWeight: '500', color: 'white'}}>
                    Fetched
                   </Text>
                </TouchableOpacity>

                <TouchableOpacity style={{marginLeft: 15, marginTop: 10, backgroundColor: 'red', height: 35, width: 130, borderRadius: 10, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
                onPress={()=> Press()}
                >
                    <View>
                        <Text style={{fontWeight: '500', color: 'white'}}>Cancel Order</Text>
                    </View>
                </TouchableOpacity>



                </View>

                
                    {/* <Pressable style={{width: 300, borderRadius: 10, height: 50, backgroundColor: 'green', justifyContent:'space-around',
                alignItems:'center', marginLeft: 20}}
                onPress={() =>  _callShowDirections() }>
                        <Text style={{fontWeight: '600', color: 'white'}}> NAVIGATE</Text>
                   </Pressable> */}

                </View>
                 

            )
        }


            // to destination




         if(status === 'DroppedOff'){
            return(

                 <TouchableOpacity onPress={
                 ()=>
                 RequestCash()
                } 
                style={{margin: 15, backgroundColor: 'green', height: 35, width: 130, borderRadius: 5, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}> 
                 <Text style={{fontSize: 15, fontWeight: '500', color: 'white'}}>
                    Finish Trip
                   </Text>
                </TouchableOpacity>

            )
        }

         if(status === 'cancelled'){
            return(
                <View>
                    <TouchableOpacity onPress={
                 ()=>
                //  RequestCash()
                console.log('pressed')
                } 
                style={{margin: 15, backgroundColor: 'green', height: 35, width: 130, borderRadius: 10, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}> 
                 <Text style={{fontSize: 15, fontWeight: '500', color: 'white'}}>
                    Report Trip
                   </Text>
                </TouchableOpacity>
                </View>
                
            )

            
        }

        }

        const ShDir = () => {


            if(requestPlus?.taxi === 'Yes'){

                    if(status === 'fetchingPassengers'){
                        return(
                        <View style={{width: '100%', marginTop: 20, alignContent: 'center', alignItems: 'center'}}>
                            <Pressable onPress={()=> _callShowDirections()}>
                                <View style={{width: 300, height: 45, alignItems: 'center', alignContent: 'center', justifyContent: 'space-around', backgroundColor: 'green', borderRadius: 10}}>
                                    <Text style={{fontWeight: '500', color: 'white'}}>Navigate To Passenger</Text>
                                </View>
                            </Pressable>
                            
                    </View>
                    )
                    
                    }

                    if(status === 'ToDestination'){
                        return(
                        <View style={{width: '100%', marginTop: 20, alignContent: 'center', alignItems: 'center'}}>
                            <Pressable onPress={()=> callShowDirections()}>
                                <View style={{width: 300, height: 45, alignItems: 'center', alignContent: 'center', justifyContent: 'space-around', backgroundColor: 'green', borderRadius: 10}}>
                                    <Text style={{fontWeight: '500', color: 'white'}}>Navigate To Passenger Destination</Text>
                                </View>
                            </Pressable>
                            
                    </View>
                    )
                    }




             } else{
                  return(
                 <View style={{width: '100%', marginTop: 20, alignContent: 'center', alignItems: 'center'}}>
                    <Pressable>
                        <View style={{width: 300, height: 45, alignItems: 'center', alignContent: 'center', justifyContent: 'space-around', backgroundColor: 'red', borderRadius: 10}}>
                            <Text style={{fontWeight: '500', color: 'white'}}>No Door To Door</Text>
                        </View>
                    </Pressable>
                    
            </View>
            )
             }
        }

        const ShDirTwo = () =>{
            return(
                <View>
                    <Text>llo</Text>
                </View>
            )
        }



         const ItemView2= ({item}) => {

                return(

              <Pressable style={{ backgroundColor: 'white'}} 
                     onPress={() =>
                    {
                    setUserIdquery(item.userId)
                    setReqId(item.id)
                    setModalVisible(true) 
                    }}
              >

                    <View style={{flexDirection: 'row'}}>


                    </View>

                    <View style={{margin: 10}}>

                     <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                    <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                        <FontAwesome5 name="user-alt" size={12} color="white" />
                                    </View>
                                    <Text style={{ marginLeft: 10, fontSize: 15, fontWeight: '500'}}>{item.name} {item.surname}</Text>
                            </View>   

                     <View style={{marginLeft: 10}}>

                            




                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                                
                                     <View style={{marginRight: 10}}>
                                        <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'green'}}/>
                                     </View>

                                    <Text style={{fontWeight: '500', marginLeft: 10, width: 280}}>{item.originDesc || `Loading...`} </Text>
                             </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                <View style={{marginRight: 10}}>
                                    <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'red'}}/>
                                </View>
                                <Text style={{fontWeight: '500', marginLeft: 10 , width: 280}}>{item.destDesc || `Loading...`}</Text>
                            </View>
                    </View>


                    <View style={{flexDirection: 'row'}}>

                       <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 7}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}>{item.seats || `0`} Seats Booked</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}>R {item.tripFee || `0.00`} Trip Total</Text>
                        </View>

                    </View>


                     <View style={{flexDirection: 'row'}}>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10, marginLeft: 8}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}>Status : {item.status || `Loading...`}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Fontisto name='taxi' size={11} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}> D2D :  {item.taxi || `No`}</Text>
                        </View>

                    </View>

                    </View>
                   
                    </Pressable>


              
                 )
            }

        const ItemSeperatorView = () => {
                return(
                    <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}}
                    />
                )
            }





        const ShowPassengers = () =>{

             if(status === 'fetchingPassengers'){
            return(
                 
                <View>
                     
                   
                <View>



                    <FlatList
                    data={accReq}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView2}
                />    


                </View>
            

                </View>

            )
        }

            if(status === 'ToDestination'){
              
                return(

               

               <View>
                   <FlatList
                    data={accReq}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView2}
                />      
                   
                <View>

                    
                </View>
            

                </View>

             )


            }
        }




       const _callShowDirections = () => {


        const startPoint = {
      longitude: '',
      latitude: ''
    } 

         const endPoint = {
      longitude: requestPlus?.originLongitude,
      latitude: requestPlus?.originLatitude
    }

    const transportPlan = 'd';

    OpenMapDirections(startPoint, endPoint, transportPlan).then(res => {
      console.log(res)
    });
    
    
     }

     const callShowDirections = () => {


    const startt = {
        longitude: '',
        latitude: ''
        } 

         const endd = {
      longitude: requestPlus?.destLongitude,
      latitude: requestPlus?.destLatitude,
    }

    const transportPlan = 'd';

    OpenMapDirections(startt, endd, transportPlan).then(res => {
      console.log(res)
    });
       
    
     }
       

        if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{

   
     return (

        <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>


             <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>



             <Modal isVisible={isModalVisible}
         swipeDirection={'down'}
         onSwipeComplete={()=> setModalVisible(false)}
         style={{ backgroundColor: 'white', marginLeft: 0, marginRight: 0}}>

         <View style={{ flex: 1 }}>

            <View style={{width: '100%', marginTop: 20, alignContent: 'center', alignItems: 'center'}}> 
                            <Pressable onPress={toggleModal}>
                        <View style={{flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
                            <Ionicons name="close-circle" size={30} color="red" />
                            <Text style={{fontWeight: '500'}}>Close</Text>
                        </View>
                    </Pressable>
            </View>

            {ShDir()}
        
             {/* {ShDirTwo()} */}

        <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

            <View style={{marginLeft: 20, marginRight: 20}}>
                    <View>
                        <Text style={{fontWeight: '500', fontFamily: 'Manrope_700Bold'}}>REQUEST DETAILS</Text>
                    </View> 

                 <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10, marginLeft: 5}}>
                        <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                            <FontAwesome5 name="user-alt" size={12} color="white" />
                        </View>
                        <Text style={{ marginLeft: 10, fontSize: 15, fontWeight: '500', fontFamily: 'Manrope_700Bold'}}>{userPlus?.name || `Loading`} {userPlus?.surname || `...`}</Text>
                
                </View>   

                <View style={{flexDirection: 'row'}}>

                         <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{requestPlus?.status || `Loading...`}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>R {requestPlus?.tripFee || `0.00`} Trip Total</Text>
                        </View>

                    </View>

                <View style={{flexDirection: 'row'}}>

                     <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons  name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{requestPlus?.seats || `0`} Seats</Text>
                        </View>


                     

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <FontAwesome5  name='door-open' size={11} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>D2D :  {requestPlus?.taxi || `No`}</Text>
                        </View>

                    </View>

                <View style={{marginLeft: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10, backgroundColor:'green', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{fontSize: 13, fontWeight: '500', width: 280, fontFamily: 'Manrope_500Medium'}}>{requestPlus?.originDesc || `Loading...`}</Text>
                                </View>
                            </View>
                     </View>


                <View style={{marginLeft: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10, backgroundColor:'red', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{ fontWeight: '500', width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>{requestPlus?.destDesc || `Loading...`}</Text>
                                </View>
                            </View>
                     </View>


                <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

                
                  
            



                </View>
               
         </View>




         </Modal>

            <View>
                    <Text style={{ paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30, marginBottom: 15, fontFamily: 'Manrope_700Bold'}}>Order Details</Text>
            </View>

            <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

            <View style={{flexDirection: 'row'}}> 
                {StatusStatus()}
                <ActivityIndicator animating={loading} size="small"/>
             
            </View>

            <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

             
            <ScrollView
             showsVerticalScrollIndicator={false}
             showsHorizontalScrollIndicator={false}
             style={{backgroundColor: 'white'}}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
             >

                
            
             <View style={{ backgroundColor: 'white', borderRadius: 10, padding: 20}}>

                <View>
                    <Text style={{fontWeight: '500', fontFamily: 'Manrope_700Bold'}}>ROUTE</Text>
                </View>



                    <View>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10, backgroundColor:'green', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{fontSize: 13, fontWeight: '500', width: 280, fontFamily: 'Manrope_600SemiBold'}}>{originDesc || `Loading...`}</Text>
                                </View>
                            </View>
                     </View>


                    <View>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10,backgroundColor:'red', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{fontSize: 13, fontWeight: '500', width: 280, fontFamily: 'Manrope_600SemiBold'}}>{destDesc || `Loading...`}</Text>
                                </View>
                            </View>
                     </View>

                    <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

                <View>
                    <Text style={{fontWeight: '500', fontFamily: 'Manrope_700Bold'}}>TRIP ITINERARY</Text>
                </View>
                

                <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Fontisto name='date' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{date}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{status}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 80, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>R{totalF || `0.00`}</Text>
                        </View>

                        

                    </View>

                 <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

                <View>
                    <Text style={{fontWeight: '500', fontFamily: 'Manrope_700Bold'}}>PASSENGERS</Text>
                </View>    



                {ShowPassengers() || <Text style={{ marginTop: 20, fontStyle: 'italic', fontFamily: 'Manrope_400Regular', fontSize: 13}}>Start trip to see passengers</Text>}
                

               <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

                

                </View>


        <View style={{marginTop: 20}}>
            
          

        </View>
            </ScrollView>
         </SafeAreaView>
    )
            }
}

export default RidesDAOInfo

const styles = StyleSheet.create({})
