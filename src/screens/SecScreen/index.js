import React, {useEffect, useState} from "react";
import {View, Text, Dimensions, SafeAreaView} from 'react-native';
import HomeMap from "../../components/HomeMap";
import CovidMessage from "../../components/CovidMessage";
import HomeSearch from "../../components/HomeSearch";
import HoScreen from '../../components/HoSearch';
import {Auth} from 'aws-amplify';
import {API, graphqlOperation} from 'aws-amplify';
import { getUser } from '../../graphql/queries';
import { useNavigation } from "@react-navigation/core";
import { onUserUpdated } from '../../graphql/subscriptions';




const SecScreen = (props) =>{


const [userDet, setUserDet] = useState();

const navigation = useNavigation();




 const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            // setuser(userSub);
            // console.log(userSub)

            


            const todoData = await API.graphql(graphqlOperation(getUser, { id: userSub}))
            const todos = todoData.data.getUser

            if (todos === null){
                <Onboarding/>
            }

            setUserDet(todos)



            if (todos.completeP === 'No'){
                navigation.navigate('UserProfile', {userId: userSub});
                return;
            }


            
    }

    useEffect(() => {
     fetchUsers()
    }, [])






    return (
        <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>
        <View style={{backgroundColor: 'white'}}>
   
             {/*HomeMap*/}
             {/* <View style={{height: Dimensions.get('window').height - 420}}>
                <HomeMap/>
           </View> */}
            {/*covid message*/}
            {/* <View style={{height: 50, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
             <CovidMessage />
            </View> */}
           
             {/*Search*/}
             <View style={{marginBottom: 60}}>
                <HoScreen/>
             </View>
            
        </View>

        </SafeAreaView>
        
    )
};

export default SecScreen;