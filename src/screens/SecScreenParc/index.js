import React from "react";
import {View, Text, Dimensions, SafeAreaView} from 'react-native';
import HomeMap from "../../components/HomeMap";
import CovidMessage from "../../components/CovidMessage";
import HomeSearch from "../../components/HomeSearch";
import HoScreen from '../../components/HoSearch';
import CovidMessageParcels from "../../components/CovidMessageParcels";
import HoSearchParcel from "../../components/HoSearchParcel";
const SecScreenParc = (props) =>{
    return (
        <SafeAreaView style={{backgroundColor: 'white', flex: 'white'}}>
             <View style={{backgroundColor: 'white'}}>
             {/*HomeMap*/}
             {/* <View style={{height: Dimensions.get('window').height - 400}}>
                <HomeMap/>
           </View> */}
            {/*covid message*/}
            {/* <View style={{height: 80}}>
             <CovidMessageParcels />
            </View> */}
           
             {/*Search*/}
             <View style={{marginBottom: 60, backgroundColor: 'white'}}>
                <HoSearchParcel/>
             </View>
            
        </View>
        </SafeAreaView>
       
    )
};

export default SecScreenParc;