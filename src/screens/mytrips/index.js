import { FlatList, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react';
import {Auth} from 'aws-amplify';
import {API, graphqlOperation} from 'aws-amplify';
import * as queries from '../../graphql/queries';
import { listGoodsOrders, listUsers, listRides } from '../../graphql/queries';

import { useNavigation, useRoute } from "@react-navigation/core";
import {  onRidesOrderUpdated, onRequestsUpdated  } from '../../graphql/subscriptions';
import { Feather, FontAwesome, FontAwesome5, Ionicons, MaterialCommunityIcons, MaterialIcons, Zocial } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';
const MyTrips = () => {

     const navigation = useNavigation();

  const [topOpt, setTopOpts] = useState('Rides')
const [botOpt, setBotOpt] = useState('Booked')
const [itred, setItRed] = useState('Booked')


  // STATES 

  const [user, setuser] = useState(null);
  const [todos, setTodos] = useState([])  
    const [todosTrips, setTodosTrips] = useState([])  
  const [todosAct, setTodosAct] = useState([])  
  const [Pending, setPending] = useState([]);
  const [filterTodo, setFilterTodo] = useState([]);
  const [orderId, setOrderId] = useState([]);
  const [getStatus, setGetStatus] = useState([]);
  const [loading, setLoading] = useState('true'); 
  const [refreshing, setRefreshing] = useState(false);
   const [custUser, setCustUser] = useState([])
const [todosParcPos,setTodosParcPos] = useState([]);
const [todosGoodsPos,setTodosGoodsPos] = useState([]);
const [pendingPen, setPendingPen] = useState([]);
const [filterTodora, setFilterTodora] = useState([]);
const [todoParcBook, setTodoParcBook] = useState([]);



 const [visible, setVisible] = useState(true);

 useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 3000);
  }, [])

     let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });


 const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setuser(userSub);
            
            
    }

     async function fetchCustomer() {

            const todoData = await API.graphql({query: queries.listUsers})
            const todos = todoData.data.listUsers.items
            setCustUser(todos)
    
     }

    async function fetchTodos() {

            const userplus = await Auth.currentAuthenticatedUser({bypassCache: true});

            const todoData = await API.graphql({query: queries.listRides, variables:  
                {filter : 
                   {and: [{ driverId: 
                        {eq: 
                            ( userplus.attributes.sub)
                        }
                        },
                        { status: 
                        {eq: 
                            'driver'
                        }
                        }
                    ]
                 }
                } 
        })
            const todos = todoData.data.listRides.items
            setTodos(todos)

            setLoading('false')

     }

      async function fetchTodosAct() {


            const todoData = await API.graphql({query: queries.listRides})
            const todos = todoData.data.listRides.items
            setTodosAct(todos)

                let filter = 
              
                {
                and: [{ driverId: {eq: user} },
                    { active: {eq:'yes'} }]
                 };
    
            const userplus = await Auth.currentAuthenticatedUser({bypassCache: true});
            const oneTodoData = await API.graphql({ query: listRides, variables: { filter : {and : [{driverId: {eq: 
                ( userplus.attributes.sub)
            }}, {active: {eq: 'yes'}}]}}});
            const oneTodos = oneTodoData.data.listRides.items
            setFilterTodo(oneTodos)

     }
      async function fetchTodosTrips() {
            const userplus = await Auth.currentAuthenticatedUser({bypassCache: true});

            const todoData = await API.graphql({query: queries.listRequests, variables:  {filter : { userId: {eq: 
                ( userplus.attributes.sub)
            }}} })
            const todos = todoData.data.listRequests.items
            setTodosTrips(todos)
            setLoading('false')

            console.log(todos)
     }


async function fetchPending() {

            const userplus = await Auth.currentAuthenticatedUser({bypassCache: true});

            const todoData = await API.graphql({query: queries.listRequests, variables:  
                {filter : 
                   {and: [{ driverId: 
                        {eq: 
                            ( userplus.attributes.sub)
                        }
                        },
                        { status: 
                        {eq: 
                            'pending'
                        }
                        }
                    ]
                 }, 
                } 
        })
            const pend = todoData.data.listRequests.items
            setPending(pend)


     }


useEffect(() => {
            
                fetchUsers();
                fetchCustomer();
                fetchTodos();
                fetchTodosTrips();
            },[todos])

useEffect(() => {
 
                fetchTodosAct()
            },[todosAct])


 useEffect(() => {
            
            
                fetchPending();
            },[Pending])

// Aother






            let subsUpdate;
            let reqUpdate;
            let actUpdate;
            function setUpSus(){
   
                subsUpdate = API.graphql(graphqlOperation(onRidesOrderUpdated)).subscribe( {next: (daraa) => {
                    setTodos(daraa)
                }, }) 

                 reqUpdate = API.graphql(graphqlOperation(onRequestsUpdated)).subscribe( {next: (bruu) => {
                    setPending(bruu)

                }, }) 

                actUpdate = API.graphql(graphqlOperation(onRidesOrderUpdated)).subscribe( {next: (acta) => {
                    setTodosAct(acta)
                }, }) 

            }

            useEffect(() =>{
                setUpSus();

                return() =>{
                    subsUpdate.unsubscribe();
                    reqUpdate.unsubscribe();  
                    actUpdate.unsubscribe();  
                };

            },[]);


            const ItemView= ({item}) => {

                return(

              

              <TouchableOpacity style={{ backgroundColor: 'white'}} onPress={() => 
                    navigation.navigate('RidesOrderInfoDSet',{id: item?.id, thestatus: item?.status})
                }>
            <View style={{margin: 10}}>

                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <FontAwesome5 name="user-alt" size={12} color="white" />
                                </View>
                        <Text style={{fontWeight: '500', marginLeft: 10, fontSize: 13, fontFamily: 'Manrope_600SemiBold'}}>{item?.name || `Loading...`} {item?.surname}</Text>
                        </View>



                     <View style={{marginLeft: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                
                                     <View style={{marginRight: 10}}>
                                        <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'green'}}/>
                                     </View>

                                    <Text style={{fontWeight: '500', marginLeft: 10, width: 280, fontSize: 12, fontFamily: 'Manrope_600SemiBold'}}>{item.originSec || `Loading...`} </Text>
                             </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                <View style={{marginRight: 10}}>
                                    <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'red'}}/>
                                </View>
                                <Text style={{fontWeight: '500', marginLeft: 10 , width: 280, fontSize: 12, fontFamily: 'Manrope_600SemiBold'}}>{item.destSec || `Loading...`}</Text>
                            </View>
                    </View>


                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name="date-range" size={13} color="white" />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.startDate}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>R{item.tripFee || `0.00`} P/S</Text>
                        </View>

                    </View>


                     <View style={{flexDirection: 'row'}}>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 7}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.seats || `0`} Seats Booked</Text>
                        </View>


                        <View style={{flexDirection: 'row', marginTop: 15}}>
                                    <View style={{alignItems: 'center', marginRight: 10, justifyContent: 'center', width: 70, height: 30, backgroundColor: 'green', borderRadius: 5}}>
                                            <Text style={{fontWeight: '500', color: 'white', fontSize: 12, fontFamily: 'Manrope_600SemiBold'}}>Accept</Text>
                                        </View>
                                        <View style={{alignItems: 'center', justifyContent: 'center', width: 70, height: 30, backgroundColor: 'red', borderRadius: 5}}>
                                            <Text style={{fontWeight: '500', color: 'white', fontSize: 12, fontFamily: 'Manrope_600SemiBold'}}>Decline</Text>
                                        </View>
                    </View>

                    </View>


                    </View>


                   
                    </TouchableOpacity>


              
                 )
            }

            const ItemView2= ({item}) => {

                return(

              

              <TouchableOpacity style={{ backgroundColor: 'white'}} onPress={() => 
                    navigation.navigate('RidesOrderInfoDSetBBB',{id: item?.id, thestatus: item?.status})
                }>

                    <View style={{flexDirection: 'row'}}>



                    </View>

                    <View style={{margin: 10}}>

                     <View style={{marginLeft: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                                
                                     <View style={{marginRight: 10}}>
                                        <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'green'}}/>
                                     </View>

                                    <Text style={{fontWeight: '500', marginLeft: 10, width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>{item.originDesc || `Loading...`} </Text>
                             </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                <View style={{marginRight: 10}}>
                                    <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'red'}}/>
                                </View>
                                <Text style={{fontWeight: '500', marginLeft: 10 , width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>{item.destDesc || `Loading...`}</Text>
                            </View>
                    </View>


                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name="date-range" size={13} color="white" />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.startDate}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>R {item.tripFee || `0.00`} Trip Total</Text>
                        </View>

                    </View>


                     <View style={{flexDirection: 'row'}}>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 7}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.availSeats || `0`} of {item.totalSeats || `0`} Seats Left</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.status || `Loading...`}</Text>
                        </View>

                    </View>

                    </View>
                   
                    </TouchableOpacity>


              
                 )
            }

            const ItemView3= ({item}) => {

                return(

              <TouchableOpacity style={{ backgroundColor: 'white'}} onPress={() => 
                    navigation.navigate('RidesDAOInfo',{id: item.id, date: item.startDate, contents: item.contents, qty: item.qty, price: item.price, breakage: item.breakage, flame: item.flameable, type: item.type, myId: user, custId: item.userId, theStatus: item.status, orLat: item.originLatitude, orLng: item.originLongitude, desLat: item.destLatitude, desLng: item.destLongitude, originDesc: item.originDesc, destDesc: item.destDesc, recName: item.recName, recSurname: item.recSurname, recCode: item.recCode, recNumber: item.recNumber})
                }>

                    <View style={{flexDirection: 'row'}}>



                    </View>

                    <View style={{margin: 10}}>

                     <View style={{marginLeft: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                                
                                     <View style={{marginRight: 10}}>
                                        <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'green'}}/>
                                     </View>

                                    <Text style={{fontWeight: '500', marginLeft: 10, width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>{item.originDesc || `Loading...`} </Text>
                             </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                <View style={{marginRight: 10}}>
                                    <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'red'}}/>
                                </View>
                                <Text style={{fontWeight: '500', marginLeft: 10 , width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>{item.destDesc || `Loading...`}</Text>
                            </View>
                    </View>


                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name="date-range" size={13} color="white" />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}>{item.startDate}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}>R {item.tripFee || `0.00`} Trip Total</Text>
                        </View>

                    </View>


                     <View style={{flexDirection: 'row'}}>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 7}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}>{item.availSeats || `0`} of {item.totalSeats || `0`} Seats Left</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}>Status : {item.status || `Loading...`}</Text>
                        </View>

                    </View>

                    </View>
                   
                    </TouchableOpacity>


              
                 )
            }

            const ItemView4 = ({item}) => {

                return(

              <TouchableOpacity style={{ backgroundColor: 'white'}} onPress={() => 
                    navigation.navigate('RidesOrderInfoCust',{driverId: item.driverId,  price: item.tripFee,  originSec: item.originDesc, destSec: item.destDesc,  type: item.seats, id: item.id, thestatus: item.status, date: item.startDate, luggageSize: item.luggage })
                }>

                    <View style={{flexDirection: 'row'}}>

                    </View>

                    <View style={{margin: 10}}>

                     <View style={{marginLeft: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                                
                                     <View style={{marginRight: 10}}>
                                        <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'green'}}/>
                                     </View>

                                    <Text style={{fontWeight: '500', marginLeft: 10, width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>{item.originDesc || `Loading...`} </Text>
                             </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                <View style={{marginRight: 10}}>
                                    <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'red'}}/>
                                </View>
                                <Text style={{fontWeight: '500', marginLeft: 10 , width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>{item.destDesc || `Loading...`}</Text>
                            </View>
                    </View>


                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name="date-range" size={13} color="white" />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.startDate}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>R {item.tripFee || `0.00`} Trip Total</Text>
                        </View>

                    </View>


                     <View style={{flexDirection: 'row', marginBottom: 10}}>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 7}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.seats || `0`} Seats </Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12, fontFamily: 'Manrope_500Medium'}}>{item.status || `Loading...`}</Text>
                        </View>

                    </View>

                    </View>
                   
                    </TouchableOpacity>
              
                 )
            }

           

           


            const ItemSeperatorView = () => {
                return(
                    <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}}
                    />
                )
            }






    const topArea = () =>{

if(topOpt === 'Rides'){

    if (!fontsLoaded) {
            return(
                <View style={{backgroundColor: 'white', flex: 1}}>
                   
                </View>
            )
        } else{
        return(
            <View>
              
                 {bottomArea()}


            </View>
        )
        }
    }



}

const bottomArea = () =>{


if(topOpt === 'Rides'){

    if(botOpt === 'Booked'){

    if (!fontsLoaded) {
            return(
                <View style={{backgroundColor: 'orange', flex: 1}}>
                   
                </View>
            )
        } else{
        return(
            <View style={{marginLeft: 20, marginRight: 20}}>
                <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-evenly'}}>
                    <TouchableOpacity style={{width: 100, height: 45, borderBottomWidth: 0.5, borderColor: 'orange', 
                    alignContent: 'center', alignItems: 'center', justifyContent: 'center', }}>
                         <Text style={{color: 'orange', fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>Booked</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> setBotOpt('Active')} style={{width: 100,height: 45, borderColor: 'grey', 
                    alignContent: 'center', alignItems: 'center', justifyContent: 'center',
                    
                    }}>
                         <Text style={{color: 'grey', fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>Active</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> setBotOpt('Posted')} style={{width: 100, height: 45,  borderColor: 'grey',
                    alignContent: 'center', alignItems: 'center', justifyContent: 'center', borderTopRightRadius: 5, borderBottomRightRadius: 5,
                    
                    }}>
                         <Text style={{color: 'grey', fontFamily: 'Manrope_600SemiBold', fontSize: 12, fontSize: 12}}>Posted</Text>
                    </TouchableOpacity>
                 

                </View>
                    {itemsRead()}

            </View>
        )
        }
    }

    if(botOpt === 'Active'){

    if (!fontsLoaded) {
            return(
                <View style={{backgroundColor: 'orange', flex: 1}}>
                   
                </View>
            )
        } else{
        return(
            <View style={{marginLeft: 20, marginRight: 20}}>
                <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-evenly'}}>
                    <TouchableOpacity onPress={()=> setBotOpt('Booked')} style={{width: 100, height: 45, borderColor: 'grey', 
                    alignContent: 'center', alignItems: 'center', justifyContent: 'center', borderTopLeftRadius: 5, borderBottomLeftRadius: 5}}>
                         <Text style={{color: 'grey', fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>Booked</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> setBotOpt('Active')} style={{width: 100,height: 45,  borderBottomWidth: 0.5, borderColor: 'orange', 
                    alignContent: 'center', alignItems: 'center', justifyContent: 'center',
                    
                    }}>
                         <Text style={{color: 'orange', fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>Active</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> setBotOpt('Posted')} style={{width: 100, height: 45,   borderColor: 'grey',
                    alignContent: 'center', alignItems: 'center', justifyContent: 'center', borderTopRightRadius: 5, borderBottomRightRadius: 5,
                    
                    }}>
                         <Text style={{color: 'grey', fontFamily: 'Manrope_600SemiBold', fontSize: 12, fontSize: 12}}>Posted</Text>
                    </TouchableOpacity>
                 

                </View>
                    {itemsRead()}

            </View>
        )
        }
    }

    if(botOpt === 'Posted'){

    if (!fontsLoaded) {
            return(
                <View style={{backgroundColor: 'orange', flex: 1}}>
                   
                </View>
            )
        } else{
        return(
            <View style={{marginLeft: 20, marginRight: 20}}>
                <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-evenly'}}>
                    <TouchableOpacity onPress={()=> setBotOpt('Booked')} style={{width: 100, height: 45, borderColor: 'grey', 
                    alignContent: 'center', alignItems: 'center', justifyContent: 'center', borderTopLeftRadius: 5, borderBottomLeftRadius: 5}}>
                         <Text style={{color: 'grey', fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>Booked</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> setBotOpt('Active')} style={{width: 100,height: 45,  borderColor: 'grey', 
                    alignContent: 'center', alignItems: 'center', justifyContent: 'center',
                    
                    }}>
                         <Text style={{color: 'grey', fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>Active</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> setBotOpt('Posted')} style={{width: 100, height: 45,   borderBottomWidth: 0.5, borderColor: 'orange',
                    alignContent: 'center', alignItems: 'center', justifyContent: 'center',
                    
                    }}>
                         <Text style={{color: 'orange', fontFamily: 'Manrope_600SemiBold', fontSize: 12, fontSize: 12}}>Posted</Text>
                    </TouchableOpacity>
                 

                </View>
                    {itemsRead()}

            </View>
        )
        }
    }

}





}

const itemsRead = () =>{

    if(botOpt === 'Booked' && topOpt === 'Rides'){

        return(
            <ScrollView style={{backgroundColor: 'white'}} showsVerticalScrollIndicator={false}
             showsHorizontalScrollIndicator={false}>
                
                 <FlatList
                    data={todosTrips}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView4}
                />

                <View style={{alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginTop: 30}}>
                    <Text style={{fontFamily: 'Manrope_500Medium', color: 'grey'}}>- No More Trips -</Text>
                </View>



            </ScrollView>
        )
    }

    if(botOpt === 'Active' && topOpt === 'Rides'){

        return(
            <ScrollView style={{backgroundColor: 'white'}} showsVerticalScrollIndicator={false}
             showsHorizontalScrollIndicator={false}>

                <FlatList
                    data={filterTodo}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView3}
                />

                 <View style={{alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginTop: 30}}>
                    <Text style={{fontFamily: 'Manrope_500Medium', color: 'grey'}}>- No More Trips -</Text>
                </View>

             </ScrollView>
           
        )
    }

     if(botOpt === 'Posted' && topOpt === 'Rides'){

        if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{

        return(
            <ScrollView style={{backgroundColor: 'white'}} showsVerticalScrollIndicator={false}
             showsHorizontalScrollIndicator={false}>
                

                <FlatList
                    data={Pending}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView}
                />

                 
                 {/* <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    /> */}

                 <FlatList
                    data={todos}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView2}
                />

                 <View style={{alignItems: 'center', alignContent: 'center', justifyContent: 'center', marginTop: 30, marginBottom: 200}}>
                    <Text style={{fontFamily: 'Manrope_500Medium', color: 'grey'}}>- No More Trips -</Text>
                </View>
                
            </ScrollView>
        )
        }
    }


}

    return (
        <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>
            <Text style={{margin: 20, fontSize: 25, fontFamily: 'Manrope_600SemiBold'}}>My Trips</Text>
            {topArea()}

        </SafeAreaView>
    )
}

export default MyTrips

const styles = StyleSheet.create({})
