import React,{useEffect, useState} from "react";
import {View, Text, Dimensions, Alert, Image} from 'react-native';


const CompleteScreen = (props) =>{

    return (
        <View style={{backgroundColor: 'white'}}>

           <View style={{alignItems: 'center', marginTop: 40}}>
            <Image
                   source={require('../../../src/assets/correct.png')}
                   style={{width: 300, height: 300}}
                    />
           </View>

           <View style={{alignItems: 'center'}}>
            <Text style={{fontSize: 30, fontWeight: '500', color: 'green', marginBottom: 250}}>Success !</Text>
           </View>

        </View>
    )
};

export default CompleteScreen;