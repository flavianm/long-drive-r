import React, {useState, useEffect, useCallback} from 'react'
import { StyleSheet, Text, TouchableOpacity, View, SafeAreaView, ScrollView, ActivityIndicator, RefreshControl } from 'react-native'
import { useNavigation, useRoute } from "@react-navigation/core";
import {Auth} from 'aws-amplify';
// import * as mutations from './graphql/mutations';
import * as mutations from '../../graphql/mutations'
import * as queries from '../../graphql/queries';
import {API, graphqlOperation} from 'aws-amplify';
import { getGoodsOrder } from '../../graphql/queries';
import { Feather, FontAwesome5, Fontisto, Ionicons, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';

import { listParcelOrders } from '../../graphql/queries';

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const RidesOrderInfoDSetBBB = (props) => {

const navigation = useNavigation();
 const [user, setuser] = useState(null);
 const [todo, setTodo] = useState([]);
 const [refreshing, setRefreshing] = useState(false);
 const [loading, setLoading] = useState('true'); 

 const onRefresh = useCallback(() => {


    setRefreshing(true);

                fetchUsers();
                 fetchOrderInfo();
    wait(2000).then(() => setRefreshing(false));
  }, []);

 const haversine = require('haversine')

    const typeState = useState(null);
    const route = useRoute();
    
    const {id, thestatus} = route.params


    const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setuser(userSub);
            console.log(userSub)
            
    }

    const fetchOrderInfo = async()=>{
        const todoData = await API.graphql(graphqlOperation(getGoodsOrder, { id: id}))
            // const todos = todoData.data.getParcelOrder.contents
              const todos = todoData.data.getGoodsOrder
            setTodo(todos)
            console.log(todos)
            setLoading('false') 
    }

     useEffect(() => {
            
                fetchUsers();
                fetchOrderInfo();
            },[])

    const todoDetails = {
        id: id,
        driverId: user,
        status: 'Accepted',
        active: 'yes'
    };

     const declineDetails = {
        id: id,
        driverId: user,
        status: 'deleted',
        active: 'no',
    };

    const Press = async ()=>{
           await API.graphql({ query: mutations.updateGoodsOrder, variables: {input: todoDetails}});
            navigation.navigate('GoodsOrdersDA')
           console.log('done')
        
        }
         const Decline = async ()=>{
           await API.graphql({ query: mutations.updateGoodsOrder, variables: {input: declineDetails}});
            
            Toast.show({
            type: 'success',
            text1: 'Logistics',
            text2: 'The order has been declined successfully',
            visibilityTime: 5000,
    });

    navigation.navigate('GoodsOrdersDA')
        
        }

const start = {
  latitude: todo.drPOLat,
  longitude: todo.drPOLng
}

const end = {
  latitude: todo.drPDLat,
  longitude: todo.drPDLng
}


    return (

        <SafeAreaView style={{flex: 1}}>
            <ScrollView 
            showsVerticalScrollIndicator={false}
            refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
            >

           <View>
                    <Text style={{ paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30, marginBottom: 15}}>Order Details.</Text>
            </View>
        
            <View style={{margin: 15, backgroundColor: 'white', borderRadius: 10, padding: 20}}>


                 <View>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Fontisto name={'date'}/>
                        <Text style={{marginLeft: 10}}>Date</Text>
                         
                    </View >
                    <View style={{borderWidth: 0.5, borderBottomColor: 'black', margin: 5}}>

                    </View> 
                    <View style={{flexDirection: 'row'}}>

                        <Text style={{fontSize: 15}}>{todo.pickupDate}</Text>
                         <Text style={{fontSize: 15, marginLeft: 50}}>Status :</Text>
                        <Text style={{marginLeft: 10, fontSize: 15, fontWeight: '600', color: 'green'}}>{todo.status}</Text>
                        </View>
                </View>

                 <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                        <View style={{width: 40, marginRight: 10}}>
                            <FontAwesome5 name='box-open' size={15} />
                        </View>
                        <View style={{width: 80, marginLeft: 10}}>
                         <Text>Size</Text>
                        </View>
                        <Text style={{width: 80, marginLeft: 10}}>{todo.type}</Text>
                </View>



                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5, marginLeft: 15}}>
                        <View style={{width: 40,marginRight: 10}}>
                            <MaterialIcons name='attach-money' size={18} />
                        </View>
                        <View style={{width: 80, marginLeft: 10}}>
                         <Text>Fare</Text>
                        </View>
                        <Text style={{width: 80, marginLeft: 10}}>R{todo.price}</Text>
                </View>

                 <View>
                    <Text style={{marginLeft: 10, fontWeight: '500', marginTop: 20}}>Locations</Text>
                </View>

                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5, marginLeft: 15, marginTop: 20}}>
                        <View style={{width: 40, marginRight: 10}}>
                            <Ionicons name='location-outline' color={'black'} size={18} />
                        </View>
                        <Text style={{width: 190, marginLeft: 10}}>{todo.drPODesc} </Text>
                </View>

                 <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5, marginLeft: 15, marginTop: 20}}>
                        <View style={{width: 40, marginRight: 10}}>
                            <Ionicons name='md-location-sharp' color={'red'} size={18} />
                        </View>
                        <Text style={{width: 190, marginLeft: 10}}>{todo.drPDDesc} </Text>
                </View>


                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5, marginLeft: 15, marginTop: 20}}>
                        <View style={{width: 40, marginRight: 10}}>
                            <MaterialCommunityIcons name='map-marker-distance'size={18} />
                        </View>
                        <Text style={{width: 190, marginLeft: 10}}>{haversine(start, end).toFixed(1)} KM</Text>
                </View>

                <View style={{flexDirection: 'row'}}>

                
            <TouchableOpacity style={{margin: 15, backgroundColor: 'red', height: 35, width: 130, borderRadius: 10, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
            onPress={()=> Decline()}
            >
                <View>
                    <Text style={{color: 'white', fontWeight: '500'}}>Cancel Order</Text>
                    
                </View>
            </TouchableOpacity>
        

                </View>



            </View>



        <View style={{marginTop: 20}}>
            
            
        </View>

        <ActivityIndicator animating={loading} size="small"/>
         </ScrollView>
        </SafeAreaView>
    )
}

export default RidesOrderInfoDSetBBB

const styles = StyleSheet.create({})
