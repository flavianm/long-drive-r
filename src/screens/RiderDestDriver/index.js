import React, { useEffect, useState } from "react";
import {View, Text, TextInput, SafeAreaView, ScrollView, Pressable, Image, TouchableOpacity} from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
// import PlaceRow from "./PlaceRow";
// import styles from "./styles";
// import PlaceRow from "./PlaceRow";
// import styles from "./styles";
import styles from "./styles";
import PlaceRow from "./PlaceRow";
import { installWebGeolocationPolyfill } from "expo-location";
import { useNavigation } from "@react-navigation/core";
import { Feather, Ionicons, MaterialIcons } from "@expo/vector-icons";
import { Picker } from "@react-native-picker/picker";
import { DatePickerPick } from "../DatePick";
import types from "../../data/types";
import LottieView from 'lottie-react-native';
import Toast from 'react-native-toast-message';
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';


const GOOGLE_PLACES_API_KEY = 'AIzaSyAj8LKvEnwzBhZRycGM1yJJ96j7AA2snGE';

// const homePlace = {
//     description : 'Home',
//     geometry: {location: {lat: -26.192910, lng: 28.035990 }},
// };

// const workPlace = {
//     description : 'Work',
//     geometry: {location: {lat: -26.193910, lng: 28.045990 }},
// };

const RiderDestDriver = (props) =>{

    let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

    const [fromText, setFromText] = useState('');
    const [destinationText, setDestinationText] = useState('');

    const [originPlace, setOriginPlace] = useState(null);
    const [destinationPlace, setDestinationPlace] = useState(null);

    const [originDesc, setOriginDesc] = useState(null);
    const [destDesc, setDestDesc] = useState(null);

    const [originSec, setOriginSec] = useState(null);
    const [destSec, setDestSec] = useState(null);

    const navigation = useNavigation();

   if(originPlace && destinationPlace){
            navigation.navigate('RiderDriver', {
                originPlace,
                destinationPlace,
                destDesc,
                originDesc,
                originSec,
                destSec
            });
        } else{
             Toast.show({
      type: 'info',
      text1: 'Locations',
      text2: 'Trip will be filtered by location',
      position: 'bottom',
      visibilityTime: 5000,
    });
            
        }


    const Press =()=>{
      
        if(originPlace && destinationPlace){
            navigation.navigate('RiderDriver', {
                originPlace,
                destinationPlace,
                destDesc,
                originDesc,
                originSec,
                destSec
            });
        } else{
             Toast.show({
      type: 'error',
      text1: 'Locations',
      text2: 'Enter both locations',
      visibilityTime: 5000,
    });
            
        }
    }

     if (!fontsLoaded) {
            return(
                <View style={{alignContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontFamily: 'Manrope_600SemiBold'}}>Please wait</Text>
                </View>
            )
        } else{

    return (
        <View style={{backgroundColor: 'white'}} >
            <View style={styles.container}>
                {/* <View style={{alignItems: 'center'}}>
                        

                           <TouchableOpacity style={{backgroundColor: '#5e675d', flexDirection: 'row',  borderRadius: 5, width: 300, height: 45, marginTop: 500, justifyContent: 'center', alignContent: 'center', alignItems: 'center'}}
                        onPress={Press}
                        >
                                <Text style={{ color: 'white', fontWeight: '500', fontFamily: 'Manrope_600SemiBold'}}>Continue</Text>
                                <MaterialIcons name='keyboard-arrow-right' size= {25} color={'white'} />
                            

                        </TouchableOpacity>
                    </View> */}

                   <GooglePlacesAutocomplete 

                        placeholder="Where are you coming from ?"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za',
                        }}
                        enablePoweredByContainer={false}
                        suppressDefaultStyles
                        
                        currentLocation={true}
                        currentLocationLabel='Current Location'
                        enableHighAccuracyLocation={true}
                        styles={{
                            textInput: styles.textInput,
                            container: styles.autoCompleteContainer,
                            listView: styles.listView,
                            separator: styles.separator

                        }}
                        onPress={(data, details = null) =>{
                            setOriginPlace({data, details});
                            setOriginDesc(data.description);
                            setOriginSec(data.structured_formatting.secondary_text);
                        }}
                        textInputProps={{
                            leftIcon: {}
                        }}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                        renderRow={(data) => <PlaceRow data={data} />}
                        renderDescription={(data) => data.description || data.vicinity}
                        
                        // predefinedPlaces={[homePlace, workPlace]}
                    />

                    <GooglePlacesAutocomplete 

                        placeholder="Where are you going ?"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za'
                        }}
                        suppressDefaultStyles
                        styles={{
                            textInput: styles.textInput,
                            //  container:{
                            //     position: 'absolute',
                            //     top: 85,
                            //     left: 10,
                            //     right: 10,
                            // },
                            container:{
                                ...styles.autoCompleteContainer,
                                top: 85,
                                
                            },
                            // listView:{...styles.listView,
                            // top: 70},
                            separator: styles.separator
                        }}
                        onPress={(data, details = null) =>{
                            console.log(originDesc)
                            setDestinationPlace({data, details});
                            setDestDesc(data.description);
                            setDestSec(data.structured_formatting.secondary_text);
                        }}
                        enablePoweredByContainer={false}
                        renderRow={(data) => <PlaceRow
                            data={data} />}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                    />

                    {/* cricle near origin input */}
                       <View style={styles.circle}>
                            {/* <Ionicons name='ios-location-outline' size={20}/> */}
                            <View style={{borderRadius: 50, width: 8, height: 8, backgroundColor: 'green', marginTop: 10}}/> 

                        </View>


                    {/* square near destintaion input */}
                        <View style={[styles.square, {alignContent: 'center', justifyContent: 'center'}]}>
                            <View style={{borderRadius: 50, width: 8, height: 8, backgroundColor: 'red', marginTop: 10}}/> 

                           
                        {/* <LottieView style={{width: 30, height: 30, marginLeft: -2}} source={require('../../assets/31078-pin-animation.json')} autoPlay loop /> */}
                        </View>
                        
                    <View>

                        {/* <Image
                        style={{marginLeft: 30, height: 300, width: 300, marginTop: 250, alignContent: 'center', alignItems:'center'}}
                        source={require('../../../assets/air.jpg')}
                        /> */}
                    </View>

                    
                       
            </View>
           
          



        </View>

       
    );
                    }
};



export default RiderDestDriver;