import { StyleSheet } from "react-native";

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const styles = StyleSheet.create({
    container:{
        padding: 10,
        height:'100%',
        backgroundColor: 'white'
        
    },

    textInput:{
    margin: 10,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 42,
    borderRadius: 20,
    // backgroundColor: '#e0e0e0',
    borderWidth: 0.4,
    borderColor: '#e0e0e0',
    height: 45,
    fontSize: 13,
    fontFamily: 'Manrope_500Medium',
    
    
},
row:{
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10
},
iconContainer:{
    backgroundColor: '#a2a2a2',
    padding: 5,
    borderRadius: 50,
    marginRight: 15

},
locationText:{

},
separator:{
    backgroundColor: '#efefef',
    height: 1
      },
listView:{
    position: 'absolute',
    top: 123,
    },
 autoCompleteContainer:{
    position: 'absolute',
    top: 30,
    left: 10,
    right: 10,
},
circle:{
     width: 20,
    height: 20,
    position: 'absolute',
    top: 50,
    left: 22,
    borderRadius: 5
},
line: {
    height: 51,
    width: 2,
    backgroundColor: '#919191',
    position: 'absolute',
    top: 67,
    left: 23.5,
},
square:{
      width: 20,
    height: 20,
    position: 'absolute',
    top: 100,
    left: 22
}

});

export default styles;