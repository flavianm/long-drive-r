import * as React from 'react';
import { FlatList, Pressable, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from "@react-navigation/core";
// import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../../components/Themed';
import {useEffect, useState}  from 'react';
import Amplify, { Auth, API, graphqlOperation } from 'aws-amplify';
import ListItem from '../../components/ListItem';
import AppIcon from '../../components/AppIcon';
import AppScreen from '../../components/AppScreen';
import { useRef, useMemo,useCallback } from 'react';
import { Feather } from '@expo/vector-icons';
import { getUser } from '../../graphql/queries';
import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';




const APP_COLORS = {
    primary: '#fc5c65',
    secondary: '#4ecdc4',
    black: '#000',
    white: '#fff',
    dark : '#0c0c0c',
};



const ListItemSeperator = () => {
    return (
        <View style={styles.seperator}></View>
    );
};

const MENU_ITEMS = [
    {
        title: 'My Profile',
        icon: {
            name: 'account',
            backgroundColor: 'orange',
        },
        targetScreen: 'UserProfile',
    },
 

    {
        title: 'My Vehicle',
        icon: {
            name: 'car-cog',
            backgroundColor: 'orange',
        },
        targetScreen: 'CarProfileScreen'
    },

    {
        title: 'Wallet',
        icon: {
            name: 'cash',
            backgroundColor: 'orange',
        },
        targetScreen: 'WalletHome'
    }

];


export default function TabTwoScreen() {

 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });



    

 const navigation = useNavigation();

 const [user, setuser] = useState();
const [userId, setUserId] = useState();
const [newId, setNewId] = useState();
const [userDetails, setUserDetails] = useState([]);

const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser({bypassCache: true});
            const userSub = userData.attributes;
            const userId = userSub.sub;
            console.log(userId)
            setUserId(userId)
            setuser(userSub);

            console.log('user id : '+userId)
            console.log('user sub : '+userSub)


            const todoData = await API.graphql(graphqlOperation(getUser, { id: userId}))
            const todos = todoData.data.getUser
            setUserDetails(todos)
           
            
    }

    useEffect(() => {
        
        fetchUsers();
    }, [])





  const onLogout = () =>{
    Auth.signOut();
  }

const imag = userDetails?.imageUrl


if (!fontsLoaded) {
            return(
                <View>
                    <Text>Loading...</Text>
                </View>
               
            )
        } else{


  return (
  
    <AppScreen style={styles.screen}>
     

            <View style={styles.container}>

                {/* <TouchableOpacity onPress={()=> navigation.navigate('UserProfile', userId)} style={{alignContent: 'center', alignItems: 'center'}}>
                        <Image 
                            source={{uri: imag}}
                            style={{width: 120, height: 120, borderRadius: 50}}
                        />
                </TouchableOpacity>
                 */}

                {/* <View style={{alignContent: 'center', marginBottom: 20, marginTop: 15, alignItems: 'center', }}> 
                    <Text style={{fontSize: 16, fontFamily: 'Manrope_600SemiBold'}}>{userDetails?.name || `Loading`} {userDetails?.surname || `...`}</Text>
                     <Text style={{fontSize: 15, fontFamily: 'Manrope_500Medium', color: 'grey'}}>R{userDetails?.balance || `Loading ...`}</Text>
                </View> */}


                <FlatList
                    data={MENU_ITEMS}
                    keyExtractor={item => item.title}
                    ItemSeparatorComponent={ListItemSeperator}
                    renderItem={({ item }) => {
                        return (
                            <ListItem
                                title={item.title}
                                IconComponent={<AppIcon name={item.icon.name} backgroundColor={item.icon.backgroundColor}/>}
                                onPress={() => navigation.navigate(item.targetScreen, {userId})}
                            />
                        );
                    }}
                />
            </View>

            

            <View style={{marginBottom: 3}}>
                    <ListItem
                title="Logout"
                onPress={onLogout}
                IconComponent={<AppIcon name="logout" backgroundColor="orange"/>}
            />
            </View>
    </AppScreen>

  );
                }
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 10,
        borderRadius: 15,
        backgroundColor:'white'
    },
   seperator: {
        width: '100%',
        height: 1,
        color: 'white',
        backgroundColor: '#d6d6d6'

    },
      screen: {
        // borderRadius: 15,
        backgroundColor: 'white'
    }
});
