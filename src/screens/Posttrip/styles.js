import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
title:{
    fontSize: 22,
    padding: 20,
    marginVertical: 10    
},
  container: {
   // flex: 1,
    alignItems: "center",
    // width: 10,
    // height:
  },
  inputBox:{
    backgroundColor:'#e7e7e7',
    margin: 10,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 5
},
inputText:{
    fontSize: 20,
    fontWeight: '600',
    color: '#6e6e6e',
},
timeContainer:{
flexDirection:'row',
width: 100,
justifyContent:'space-between',
padding:10
},
});

export default styles;