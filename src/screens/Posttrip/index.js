import React, {useState} from 'react';
import {View, Button, Platform, Text, Pressable, TextInput} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import styles from './styles';
import { AntDesign, Entypo, MaterialIcons } from '@expo/vector-icons';
import { useNavigation } from "@react-navigation/core";
import { Picker } from "@react-native-picker/picker";
import { ScrollView } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons';


export const Posttrip = () => {

    const navigation = useNavigation();

    const goToSearch = () =>{
       navigation.navigate('PostSearch');
    }

  const [date, setDate] = useState(new Date(1598051730000));
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);


  const [selectedValue, setSelectedValue] = useState("java");


  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };

  return (
    <ScrollView
    showsVerticalScrollIndicator={false}
    >            
      <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
                <Text style={{paddingLeft: 15, fontSize: 30, fontWeight: '600', color: 'black', marginTop: 30, marginBottom: 15}}>
               Set New Trip
              </Text>

            <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black', marginTop: 30}}>
               Ride Schedule
            </Text>
            <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5 }}>
              Enter a date and make sure to post the correct time of leaving. Once booked, it can not be edited
            </Text>

            <Pressable style={styles.inputBox} onPress={showDatepicker}>
                {/*Date box*/}
                <Text style={styles.inputText}> Travel Date</Text>
                 <View style={styles.timeContainer}>
                    <AntDesign color={'white'} name="calendar" size={16} color="#535353"/>
                </View>
            </Pressable>

           
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />

      )} 

      </View>
      

<View  style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>

          <View>
          <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black', marginTop: 30}}>
               Vehicle details
        </Text>
        <Text style={{  color:'#7d7d7d', padding: 10}}>This helps get more bookings and gives passengers a way to identify your Vehicle</Text>

        <View>

          <View style={{marginLeft: 50, flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
            <View style={{width: 100}}>
            <Text style={{ color:'black', fontWeight: '500' }}>Model</Text>
            </View>

            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, backgroundColor:'white', height: 40, width: 150, borderRadius: 5, padding: 10, borderWidth: 1, borderColor: '#b3b3b3'}}>
              <TextInput style={{ color:'#7d7d7d'}} placeholderTextColor='#7d7d7d' placeholder='e.g Toyota Corolla'></TextInput>
            </View>
          </View>

          <View style={{marginLeft: 50, flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
            
            <View style={{width: 100}}>
              <Text style={{ color:'black', fontWeight: '500' }}>Type</Text>
            </View>
            
            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, backgroundColor:'white', height: 40, width: 150, borderRadius: 5, padding: 10, borderWidth: 1, borderColor: '#b3b3b3'}}>
              <TextInput style={{ color:'black'}} placeholderTextColor='#7d7d7d' placeholder='e.g Sedan'></TextInput>
            </View>
          </View>
          <View style={{marginLeft: 50, flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
            <View style={{width: 100}}>
                    <Text style={{ color:'black', fontWeight: '500' }}>Colour</Text>

            </View>


            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, backgroundColor:'white', height: 40, width: 150, borderRadius: 5, padding: 10, borderWidth: 1, borderColor: '#b3b3b3'}}>
              <TextInput style={{ color:'black'}} placeholderTextColor='#7d7d7d' placeholder='e.g Red'></TextInput>
            </View>
          </View>

           <View style={{marginLeft: 50, flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
            
            <View style={{width: 100}}>
                    <Text style={{ color:'black', fontWeight: '500' }}>Year</Text>
            </View>
            
            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, backgroundColor:'white', height: 40, width: 150, borderRadius: 5, padding: 10, borderWidth: 1, borderColor: '#b3b3b3'}}>
              <TextInput style={{ color:'#7d7d7d'}} placeholderTextColor='#7d7d7d' placeholder='e.g 2006'></TextInput>
            </View>
          </View>


           <View style={{marginLeft: 50, flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
            <View style={{width: 100}}>
            <Text style={{ color:'black', fontWeight: '500' }}>Licence Plate</Text>
            </View>


            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, backgroundColor:'white', height: 40, width: 150, borderRadius: 5, padding: 10, borderWidth: 1, borderColor: '#b3b3b3'}}>
              <TextInput style={{ color:'#7d7d7d'}} placeholderTextColor='#7d7d7d' placeholder='e.g JS22DSGP'></TextInput>
            </View>
          </View>

        </View>
        </View>

        

</View>


      


        <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
          <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black', marginTop: 30}}>
               Empty Seats
        </Text>

         <View>
            <Text style={{paddingLeft: 15, fontSize: 16, fontWeight: '500', color: 'black', marginTop: 5}}>
                Number Of Seats
            </Text>

            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, backgroundColor:'#dadb8a', height: 80, borderRadius: 5, padding: 10}}>
                            
                            <View style={{flexDirection: 'row', alignItems: 'center',  alignContent: 'center', paddingRight: 10}}> 
                            <MaterialIcons name={'dangerous'} size={22} color='#7d7d7d'  />
                            <Text  style={{alignItems: 'center',  alignContent: 'center', marginLeft: 10}} style={{  color:'#7d7d7d'}} >Please try to keep seats empty to comply with COVID Regulations </Text>
                            </View>

            </View>


        </View>


             <View style={{margin: 10, marginBottom: 0, backgroundColor: 'white', borderRadius: 10}}>
            <View style={styles.container}>
                <Picker
                    selectedValue={selectedValue}
                    style={{width: 150 }}
                    onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                >
                    <Picker.Item label="1 Seat" value="1" />
                    <Picker.Item label="2 Seats" value="2" />
                    <Picker.Item label="3 Seats" value="3" />
                    <Picker.Item label="4 Seats" value="4" />
                </Picker>
            </View>
        </View>

          

        </View>



        <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10}}>
            <Text style={{paddingLeft: 15, marginTop: 30, fontSize: 20, fontWeight: '600', color: 'black'}}>
              Trip  Preferences
            </Text>
          <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 10 }}>
              This informs the passenger of how much space you have for their luggage and extras before they book
            </Text>

            
        </View>

          <View  style={{backgroundColor: 'white', borderRadius: 10, margin: 10}}>
            <Text style={{paddingLeft: 15, marginTop: 30, fontSize: 20, fontWeight: '600', color: 'black'}}>
                Pricing
            </Text>
          <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5 }}>
              We give you total freedom to make your own prices. Your trip may be filtered by price and time 
            </Text>

            <View style={{flexDirection: 'row', alignContent:'space-around', alignItems: 'center', padding: 20}}>
              <Text style={{  color:'black' , fontWeight: '500', fontSize: 20}} >R</Text>
               <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, backgroundColor:'white', height: 40, width: 100, borderRadius: 5, padding: 10, borderWidth: 1, borderColor: '#b3b3b3'}}>
                  <TextInput style={{ color:'#ffffff'}} placeholderTextColor='#7d7d7d' placeholder='259'></TextInput>
            </View>
                <Text style={{  color:'black' , fontWeight: '500', fontSize: 20}} >Per Seat</Text>

            </View>
           

        </View>

         <View  style={{backgroundColor: 'white', borderRadius: 10, margin: 10}}>
            <Text style={{paddingLeft: 15, marginTop: 30, fontSize: 20, fontWeight: '600', color: 'black'}}>
                Trip Description
            </Text>
          <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5 }}>
              Use this space to write any details relevant to your trip for passengers before they book 
            </Text>
            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, marginBottom: 40, backgroundColor:'white', height: 100, borderRadius: 5, padding: 10}}>
                            <TextInput style={{  color:'#6e6e6e', padding: 10, borderRadius: 5, borderWidth: 1, borderColor: '#b3b3b3', height: 100}} placeholderTextColor='#7d7d7d' placeholder='E.g Please note that I am a chain smoker'></TextInput>
            </View>

        </View>

        <View  style={{backgroundColor: 'white', borderRadius: 10, margin: 10}}>
            <Text style={{paddingLeft: 15, marginTop: 30, fontSize: 20, fontWeight: '600', color: 'black'}}>
                COVID-19 Safety Guidelines 
            </Text>
          <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 20 }}>
              To keep everyone safe, please make sure to follow the guidelines below 
            </Text>




        </View>

    <View>
          <Pressable style={{ marginTop: 10}}
            onPress={goToSearch}
            >
                <View style={{backgroundColor: '#E15F25', margin: 50, borderRadius: 50, height: 40}}>
                <View style={{alignItems:'center', flexDirection: 'row', alignContent:'space-around', padding: 10, paddingLeft: 60 }}>
                  <Text style={{color: 'white', fontWeight: '600', marginRight: 15}}  > Set Destination</Text>
                  <Feather name="arrow-right" color={'white'} size={20} />

                </View>
                
                </View>
            </Pressable>
           
    </View>
       

    </ScrollView>
  );
};
