import React, {useState, useEffect, useCallback, useMemo, useRef } from 'react'
import {View, Button, Platform, Text, Pressable, TextInput, Image, TouchableOpacity, SafeAreaView} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { AntDesign, Entypo, Ionicons, MaterialIcons } from '@expo/vector-icons';

import styles from './styles';
import { Feather, FontAwesome, MaterialCommunityIcons, Fontisto  } from '@expo/vector-icons';

import {Auth} from 'aws-amplify';
import {API, graphqlOperation} from 'aws-amplify';
import { createParcelOrder } from '../../graphql/mutations';
import {listParcelOrders} from '../../graphql/queries';
import { useNavigation, useRoute } from "@react-navigation/core";
import {LocaleConfig} from 'react-native-calendars';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import { updateParcelOrder } from '../../graphql/mutations';
import { updateGoodsOrder } from '../../graphql/mutations';
import { updateLdTemp } from '../../graphql/mutations';
import SelectDropdown from 'react-native-select-dropdown'
import { getUser, getLdTemp, getRides } from '../../graphql/queries';
import { getLd } from '../../graphql/queries';
import * as queries from '../../graphql/queries';
import Toast from 'react-native-toast-message';
import LottieView from 'lottie-react-native';

import AnimatedLoader from "react-native-animated-loader";

import * as mutations from '../../graphql/mutations'

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const RidesCustomerSearch = () => {

    const [visible, setVisible] = useState(true);

 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

 useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 3000);
  }, [])

    const route = useRoute();
    const seats = [noSeats]
    const taxii = ["Yes", "No"]

    const {id, price, driverId, willTaxiPrice, originPlace, destinationPlace, destDesc, originDesc, orSec, desSec, startDate, message} = route.params


    const navigation = useNavigation();



    // STATES 

  const [user, setuser] = useState();

  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [bookingFee] = useState('0.035');
  const [currentBalance, setCurrentBalance] = useState();
  const [totalFee, setTotalFee] = useState();
  const [totalNoFee, setTotalNoFee] = useState();
  const [dtd, setDtd] = useState('No');
  const [sts, setSts] = useState('0');
  const [tf, setTF] = useState();
  const [ldBalance, setLdBalance] = useState(); 
const [noSeats, setNoSeats] = useState([]);
const [willTaxiOpt ,setWillTaxiOpt] = useState('No');



  const [userDeto, setUserDeto] = useState('');

// const initialState = {rideId: id, userId: user, taxi: '', price: price,  status: 'pending', luggage: '', seats: '', pickupDate: datedate ,  originLatitude: originPlace.details.geometry.location.lat, originLongitude: originPlace.details.geometry.location.lng, destLatitude:destinationPlace.details.geometry.location.lat, destLongitude:destinationPlace.details.geometry.location.lng, originDesc: originDesc, originSec: orSec, destDesc: destDesc, destSec: desSec}
const initialState = {name: '', rideId: id, userId: '',  tripFee: '', taxi: 'Yes', startDate: startDate, status: 'pending', driverId: driverId, luggage: '', seats: '',  originLatitude: originPlace.details.geometry.location.lat, originLongitude: originPlace.details.geometry.location.lng, destLatitude:destinationPlace.details.geometry.location.lat, destLongitude:destinationPlace.details.geometry.location.lng, originDesc: originDesc, originSec: orSec, destDesc: destDesc, destSec: desSec}

const [formState, setFormState] = useState(initialState)
const [todos, setTodos] = useState([])

             function setInput(key, value) {
    setFormState({ ...formState, [key]: value })
  }


  
  const geometry = async() =>{
    const originLat = originPlace.details.geometry.location.lat;
    const originLng = originPlace.details.geometry.location.lng;
    setInput('originLatitude', originLat)
    setInput('originLongitude', originLng)

    const dedstLat = destinationPlace.details.geometry.location.lat;
    const destLng = destinationPlace.details.geometry.location.lng;
    setInput('destLatitude', dedstLat)
    setInput('destLongitude', destLng)

   
  }

   useEffect(() => {
            
            geometry();

            },[])



//   FETCHING USER INFORMATION

const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setuser(userSub);
            // setInput('userId', userSub)

            const todoData = await API.graphql(graphqlOperation(getUser, { id: userSub}))
            const todos = todoData.data.getUser
            const zeBalance = todos.balance
            
            setUserDeto(todos)
            setCurrentBalance(zeBalance)


            const todoDataB = await API.graphql(graphqlOperation(getLdTemp, { id: '1'}))
            const todosB = todoDataB.data.getLdTemp
            const zeBalanceB = todosB.balance
            setLdBalance(zeBalanceB)

            // console.warn(zeBalanceB)


            setInput('userId', userSub)

            
            
    }



     useEffect(() => {
            
                fetchUsers();
            },[])

            // END USER INFORMATION


     const fetchSeats = async() =>{

            const todoData = await API.graphql(graphqlOperation(getRides, { id: id}))
            const todos = todoData.data.getRides
            const zeBalance = todos.availSeats
            const willTaxi = todos.willTaxi
            setNoSeats(zeBalance)
            setWillTaxiOpt(willTaxi)


           const userData = await Auth.currentAuthenticatedUser();
           const userSub = userData.attributes.sub;

            const userS = await API.graphql(graphqlOperation(getUser, { id: userSub}))
            const subsa = userS.data.getUser;

            

            setInput('userId', userSub);

            

            
     }       

     useEffect(() => {
            
                fetchSeats();
            },[])

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };





     const updateUserBalance = {
        id: user,
        balance: parseFloat(currentBalance) - parseFloat(totalFee),
    };

     const updateLdBalance = {
        id: '1',
        balance: parseFloat(ldBalance) + parseFloat(totalFee),
    };

    const SeatssOp = () => {


if (noSeats === '1'){

    if(sts === '0'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>
     
        </View>
      )
    }


    if(sts === '1'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>
        </View>
      )
    }


}

if (noSeats === '2'){

    if(sts === '0'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

     
        </View>
      )
    }




    if(sts === '1'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
          </TouchableOpacity>

        </View>
      )
    }
    
    if(sts === '2'){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange"/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>
        </View>
      )
    }
}

if (noSeats === '3'){

   if(sts === '0'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        {/* thr */}
          <TouchableOpacity onPress={()=> setSts('3')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>


        </View>
      )
    }
   
   if(sts === '1'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
          </TouchableOpacity>

        </View>
      )
    }
    
    if(sts === '2'){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange"/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>
        </View>
      )
    }
   
    if(sts === '3'){
      return(
        <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setSts('1')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}} />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          {/* tw */}
          <TouchableOpacity onPress={()=> setSts('2')} style={{flexDirection: 'row', justifyContent: 'space-evenly', borderRightWidth: 0.5, borderColor: 'grey'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="grey" style={{marginRight: 30}}/>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

        {/* thr */}
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            <MaterialIcons name="airline-seat-recline-extra" size={24} color="orange" />
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </View>


        </View>
      )
    }

}


  }


const willTaxiOptTwo = ()=>{

    if (willTaxiOpt == 'Yes'){
        
        if(dtd === 'null'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity  onPress={()=> setDtd('Yes')}  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>Y E S</Text>
          </TouchableOpacity>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}>

          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setDtd('No')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>N O </Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

    

        </View>
      )
    }

    
    if(dtd === 'Yes'){
      return(
         <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <View  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            {/* <Entypo name="thumbs-up" size={24} color="orange" /> */}
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'orange'}}>Y E S</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </View>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}>

          </View>

          {/* tw */}
          <TouchableOpacity onPress={()=> setDtd('No')} style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>N O </Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

    

        </View>
      )
    }
    
    if(dtd === 'No'){
      return(
       <View style={{width: '100%', borderWidth: 0.5, borderColor: 'grey', height: 60, borderRadius: 5, justifyContent: 'space-evenly', flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
          
          {/* one */}
          <TouchableOpacity onPress={()=> setDtd('Yes')}  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            {/* <Entypo name="thumbs-up" size={24} color="orange" /> */}
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'grey'}}>Y E S</Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>1</Text> */}
          </TouchableOpacity>

          <View style={{width: 0.5, backgroundColor: 'grey', height: 23}}>

          </View>

          {/* tw */}
          <TouchableOpacity  style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Text style={{fontFamily: 'Manrope_600SemiBold', color: 'orange'}}>N O </Text>
            {/* <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 20, color: 'grey'}}>2</Text> */}
          </TouchableOpacity>

    

        </View>
      )
    }


    }

    // if (willTaxiOpt == 'No'){
        
    //   setDtd('No')

    // }




}







    

    const totalSeatss = () =>{

        if(noSeats === '3'){
            return(
                <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                    {/* yes */}
                    
                        <Pressable style={{justifyContent: 'space-evenly', alignItems: 'center', marginRight: 40}} 
                            onPress={()=> setSts('1')
                            }
                        >
                            <View style={{width: 60, height: 60, justifyContent: 'space-evenly', alignItems: 'center', alignContent: 'center', backgroundColor: 'green', borderRadius: 100}}>
                                <MaterialIcons name="event-seat" size={24} color="white" />
                            </View>
                            <Text style={{marginTop: 10, fontWeight: '500'}}>1</Text>
                        </Pressable>

                        {/* no */}
                        <Pressable style={{justifyContent: 'space-evenly',  alignItems: 'center' , marginRight: 40}}
                        onPress={()=> 
                            setSts('2')
                        }
                        >
                            <View style={{width: 60, height: 60, justifyContent: 'space-evenly', alignItems: 'center', alignContent: 'center', backgroundColor: 'green', borderRadius: 100}}>
                                <MaterialIcons name="event-seat" size={24} color="white" />
                            </View>
                            <Text style={{marginTop: 10, fontWeight: '500'}}>2</Text>
                        </Pressable>
                    
                        <Pressable style={{justifyContent: 'space-evenly', alignItems: 'center'}}
                        onPress={()=> setSts('3')}
                        >
                            <View style={{width: 60, height: 60, justifyContent: 'space-evenly', alignItems: 'center', alignContent: 'center', backgroundColor: 'green', borderRadius: 100}}>
                                <MaterialIcons name="event-seat" size={24} color="white" />
                            </View>
                            <Text style={{marginTop: 10, fontWeight: '500'}}>3</Text>
                        </Pressable>
                        
                    
                </View>
                </View>
            )
        }

        if(noSeats === '2'){
            return(
                <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                    {/* yes */}
                    
                        <Pressable style={{justifyContent: 'space-evenly', alignItems: 'center', marginRight: 40}} 
                            onPress={()=> setSts('1')}
                        >
                            <View style={{width: 60, height: 60, justifyContent: 'space-evenly', alignItems: 'center', alignContent: 'center', backgroundColor: 'green', borderRadius: 100}}>
                                <MaterialIcons name="event-seat" size={24} color="white" />
                            </View>
                            <Text style={{marginTop: 10, fontWeight: '500'}}>1</Text>
                        </Pressable>

                        {/* no */}
                        <Pressable style={{justifyContent: 'space-evenly',  alignItems: 'center' , marginRight: 40}}
                        onPress={()=> setSts('2')}
                        >
                            <View style={{width: 60, height: 60, justifyContent: 'space-evenly', alignItems: 'center', alignContent: 'center', backgroundColor: 'green', borderRadius: 100}}>
                                <MaterialIcons name="event-seat" size={24} color="white" />
                            </View>
                            <Text style={{marginTop: 10, fontWeight: '500'}}>2</Text>
                        </Pressable>
                    
                </View>
                </View>
            )
        }

        if(noSeats === '1'){
            return(
                <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                    {/* yes */}
                    
                        <Pressable style={{justifyContent: 'space-evenly', alignItems: 'center', marginRight: 40}} 
                            onPress={()=> setSts('1')}
                        >
                            <View style={{width: 60, height: 60, justifyContent: 'space-evenly', alignItems: 'center', alignContent: 'center', backgroundColor: 'green', borderRadius: 100}}>
                                <MaterialIcons name="event-seat" size={24} color="white" />
                            </View>
                            <Text style={{marginTop: 10, fontWeight: '500'}}>1</Text>
                        </Pressable>   
                </View>
                </View>
            )
        }



    }

useEffect(() => {

        if (sts === '3'){
          
            setInput('seats', '3')
  
        } else{
            if (sts === '2'){
           
        setInput('seats', '2')
        }
        else{
            if (sts === '1'){
           
        setInput('seats', '1')
        }
        }
    }

    }, [sts])
    
 useEffect(() => {

        if (dtd === 'Yes'){
            const selct = (parseFloat(willTaxiPrice)) + (parseFloat(price) * parseFloat(sts))
            // setTF(selct)
            // setInput('tripFee', selct)
            setTF(selct)

           

  
        } else{
            if (dtd === 'No'){
            const ifNo =  (parseFloat(price) * parseFloat(sts))
            // setInput('tripFee', ifNo)
            setTF(ifNo)
            
        
        }
        }

    }, [dtd])





const toput = {
    name: userDeto.name, 
    surname: userDeto.surname,
    imageUrl: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/images/products/keyboard1.jpg', 
    rideId: id, 
    userId: user,  
    tripFee: tf, 
    taxi: 'Yes', 
    startDate: startDate, 
    status: 'pending', 
    driverId: driverId, 
    luggage: '', 
    seats: sts,  
    originLatitude: originPlace.details.geometry.location.lat, 
    originLongitude: originPlace.details.geometry.location.lng, 
    destLatitude:destinationPlace.details.geometry.location.lat, 
    destLongitude:destinationPlace.details.geometry.location.lng, 
    originDesc: originDesc, 
    originSec: orSec, 
    destDesc: destDesc, 
    destSec: desSec
};


  async function updateTodo() {

    

     if(sts === '0'){

        Toast.show({
        type: 'error',
        text1: 'Seats',
        text2: 'Pick an option',
        visibilityTime: 10000,
        position: 'top'
        
        });

        return;

    }

    if(dtd === 'null'){

        Toast.show({
        type: 'error',
        text1: 'Door To Door',
        text2: 'Pick an option',
        visibilityTime: 10000,
        position: 'top'
        });
        return;
    }


   
    if (dtd === 'Yes'){

        const ifYes = ((((parseFloat(willTaxiPrice)) + (parseFloat(price) * parseFloat(sts))) * parseFloat(bookingFee))  + ( parseFloat(willTaxiPrice)) + (parseFloat(price) * parseFloat(sts)))
                           
        if( parseFloat(currentBalance) > parseFloat(ifYes) ){
                        try {

                               setInput('name', userDeto)

                                     const updateUserBal = {
                                    id: user,
                                    balance: parseFloat(currentBalance) - parseFloat(ifYes),
                                };

                                const updateLdBal = {
                                    id: '1',
                                    balance: parseFloat(ldBalance) + parseFloat(ifYes),
                                };


                                await API.graphql({ query: mutations.updateUser, variables: {input: updateUserBal}});
                                
                                Toast.show({
                                type: 'info',
                                text1: 'Wallet',
                                text2: 'R' + parseFloat(ifYes).toFixed(2) + ' deducted from your wallet',
                                visibilityTime: 10000,
                                position: 'bottom'
                                
                                });
                        
                                
                            
                                 await API.graphql({ query: mutations.updateLdTemp, variables: {input: updateLdBal}});

                                    const todo = { ...formState }
                                    setTodos([...todos, todo])
                                    setFormState(initialState)
                                  
                                    await API.graphql(graphqlOperation(mutations.createRequests, {input: toput}))

                                    Toast.show({
                                    type: 'success',
                                    text1: 'Rides',
                                    text2: 'Your seats have successfully been booked. Driver will accept or decline',
                                    visibilityTime: 5000,
                                    
                                    });


                                        navigation.navigate('Home')
                                        
                                        } catch (err) {
                                        Toast.show({
                                        type: 'error',
                                        text1: 'Logistics Error',
                                        text2: 'Your booking could not be placed, please try again ',
                                        visibilityTime: 4000,
                                        
                                        });
                                        }
                        
                    }else{
                            Toast.show({
                        type: 'error',
                        text1: 'Wallet',
                        text2: 'You have insufficient funds, please top up your wallet to continue',
                        visibilityTime: 8000,
                        
                        });
                    }
   
    } else{
        if(dtd === 'No'){

            const ifNo = ((parseFloat(bookingFee) * (parseFloat(price) * parseFloat(sts))) + ((parseFloat(price) * parseFloat(sts))))


             if( parseFloat(currentBalance) > parseFloat(ifNo) ){

       try {

                                const updateUserBala = {
                                    id: user,
                                    balance: parseFloat(currentBalance) - parseFloat(ifNo),
                                };

                                const updateLdBala = {
                                    id: '1',
                                    balance: parseFloat(ldBalance) + parseFloat(ifNo),
                                };


    await API.graphql({ query: mutations.updateUser, variables: {input: updateUserBala}});
    
    Toast.show({
      type: 'info',
      text1: 'Wallet',
      text2: 'R' + parseFloat(ifNo).toFixed(2) + ' deducted from your wallet',
      visibilityTime: 10000,
      position: 'bottom'
      
    });
    
    

        
    await API.graphql({ query: mutations.updateLdTemp, variables: {input: updateLdBala}});
       



      const todo = { ...formState }
      setTodos([...todos, todo])
      setFormState(initialState)
      await API.graphql(graphqlOperation(mutations.createRequests, {input: toput}))


      
      Toast.show({
      type: 'success',
      text1: 'Rides',
      text2: 'Your booking has successfully been placed. Driver will accept or decline',
      visibilityTime: 5000,
      
    });


      navigation.navigate('Home')
     
    } catch (err) {
       Toast.show({
      type: 'error',
      text1: 'Logistics Error',
      text2: 'Your booking could not be placed, please try again ',
      visibilityTime: 4000,
      
    });
    }
       
   }else{
        Toast.show({
      type: 'error',
      text1: 'Wallet',
      text2: 'You have insufficient funds, please top up your wallet to continue',
      visibilityTime: 8000,
      
    });
   }
   
        }
    }





  



    

  }

//    let selct = (parseFloat(willTaxiPrice)) + (parseFloat(price) * parseFloat(sts));
    const orderInv = () =>{

        if (dtd === 'Yes'){
            
            
            return(
            <View>
                 {/* AMOUNT CONTAINER */}
               

                    {/* Driver fare  */}
                    <View style={{flexDirection:'row', marginLeft: 20, justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
               
               {/* icons */}
                <View>
                    <View style={{width: 100}}>
                        <Text style={{ fontSize: 13, fontFamily: 'Manrope_600SemiBold'}} >Driver Fare</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{ marginLeft: 20, width: 200, borderRadius: 5}}>
                    <Text style={{ color: '#6e6e6e',paddingLeft: 15, paddingRight: 15,fontSize: 13, fontFamily: 'Manrope_500Medium'}}
                  
                    >
                         R{ parseFloat(price) * parseFloat(sts)}
                    </Text>
                    
                </View>

            </View>


            {/* Door to door */}

                 <View style={{flexDirection:'row', marginLeft: 20, justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
               
               {/* icons */}
                <View>
                    <View style={{width: 100}}>
                        <Text style={{fontSize: 13, fontFamily: 'Manrope_600SemiBold'}} >Door2Door</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{ marginLeft: 20, width: 200, borderRadius: 5}}>
                    <Text style={{fontSize: 13, fontFamily: 'Manrope_500Medium', color: '#6e6e6e',paddingLeft: 15, paddingRight: 15}}
                  
                    >
                        R{parseFloat(willTaxiPrice)}
                         {/* R{price} */}
                    </Text>
                    
                </View>

            </View>



             {/* Booking Fee  */}
                    <View style={{flexDirection:'row', marginLeft: 20, justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
               
               {/* icons */}
                <View>
                    <View style={{width: 100}}>
                        <Text style={{ fontSize: 13, fontFamily: 'Manrope_600SemiBold'}} >Booking Fee</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{ marginLeft: 20, width: 200, borderRadius: 5}}>
                    <Text style={{fontSize: 13, fontFamily: 'Manrope_500Medium', color: '#6e6e6e',paddingLeft: 15, paddingRight: 15}}
                  
                    >
                         R{(((parseFloat(willTaxiPrice)) + (parseFloat(price) * parseFloat(sts))) * parseFloat(bookingFee)).toFixed(2)}
                    </Text>
                    
                </View>

            </View>

                  {/* Booking Fee  */}
                    <View style={{flexDirection:'row', marginTop: 10, marginLeft: 20, justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
               
               {/* icons */}
                <View>
                    <View style={{width: 100}}>
                        <Text style={{ fontSize: 13, fontFamily: 'Manrope_600SemiBold'}} >Total Payment</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{ marginLeft: 20, width: 200, borderRadius: 5}}>
                    <Text style={{fontSize: 13, fontFamily: 'Manrope_500Medium', color: 'red',paddingLeft: 15, paddingRight: 15}}
                  
                    >
                         R{((((parseFloat(willTaxiPrice)) + (parseFloat(price) * parseFloat(sts))) * parseFloat(bookingFee))  + ( parseFloat(willTaxiPrice)) + (parseFloat(price) * parseFloat(sts))).toFixed(2)}
                    </Text>
                    
                </View>

            </View>

                 
            </View>
           
        )
        
        } else {
            if (dtd === 'No'){
                return (
                    <View>
                 {/* AMOUNT CONTAINER */}
           

                    {/* Driver fare  */}
                    <View style={{flexDirection:'row', marginLeft: 20, justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
               
               {/* icons */}
                <View>
                    <View style={{width: 100}}>
                        <Text style={{ fontSize: 15}} >Driver Fare</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{ marginLeft: 20, width: 200, borderRadius: 5}}>
                    <Text style={{fontSize: 15, color: '#6e6e6e',paddingLeft: 15, paddingRight: 15}}
                  
                    >
                         R{parseFloat(price) * parseFloat(sts)}
                    </Text>
                    
                </View>

            </View>


            {/* Door to door */}

                 <View style={{flexDirection:'row', marginLeft: 20, justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
               
               {/* icons */}
                <View>
                    <View style={{width: 100}}>
                        <Text style={{ fontSize: 15}} >Door To Door</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{ marginLeft: 20, width: 200, borderRadius: 5}}>
                    <Text style={{fontSize: 15, color: '#6e6e6e',paddingLeft: 15, paddingRight: 15}}
                  
                    >
                         R{'0.00'}
                    </Text>
                    
                </View>

            </View>



             {/* Booking Fee  */}
                    <View style={{flexDirection:'row', marginLeft: 20, justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
               
               {/* icons */}
                <View>
                    <View style={{width: 100}}>
                        <Text style={{ fontSize: 15}} >Booking Fee</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{ marginLeft: 20, width: 200, borderRadius: 5}}>
                    <Text style={{fontSize: 15, color: '#6e6e6e',paddingLeft: 15, paddingRight: 15}}
                  
                    >
                         R{(parseFloat(bookingFee) * (parseFloat(price) * parseFloat(sts))).toFixed(2)}
                    </Text>
                    
                </View>

            </View>

                  {/* Booking Fee  */}
                    <View style={{flexDirection:'row', marginTop: 10, marginLeft: 20, justifyContent: 'space-evenly', alignContent: 'center', alignItems: 'center'}}>
               
               {/* icons */}
                <View>
                    <View style={{width: 100}}>
                        <Text style={{ fontSize: 15}} >Total Payment</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{ marginLeft: 20, width: 200, borderRadius: 5}}>
                    <Text style={{fontSize: 15, color: 'red',paddingLeft: 15, paddingRight: 15}}
                  
                    >
                         R{((parseFloat(bookingFee) * (parseFloat(price) * parseFloat(sts))) + (parseFloat(price) * parseFloat(sts))).toFixed(2)}
                    </Text>
                    
                </View>

            </View>


            </View>
        
                )
            }
        }


        
    }
if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{

    return (

        <SafeAreaView style={{backgroundColor: 'white', flex: 1}} >

        <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>


        <ScrollView style={{backgroundColor: 'white', padding: 10}}
    showsVerticalScrollIndicator={false}
    showsHorizontalScrollIndicator={false}
    >
    
       

        <View style={{backgroundColor: 'white', padding: 10, paddingBottom: 20}}>

           <Text style={{paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30, fontFamily: 'Manrope_700Bold'}}>
                Book Seats
            </Text>


       
        </View>
         <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />


           <View style={{flexDirection: 'row'}}>
                        <View style={{width: 50, height: 50}}>
                            {/* <Ionicons name={'warning'} color="grey" size={40} /> */}
                            <LottieView style={{width: 50, height: 50, marginRight: 0}} source={require('../../assets/warning/8750-alert.json')} autoPlay loop />

                        </View>
                            <View style={{width: 300, marginLeft: 10}}>
                                <Text style={{color: 'grey', fontFamily: 'Manrope_400Regular', fontSize: 13}}>Please use the guide below to input correct details. Driver may filter your request based on your inputed data</Text>
                            </View>
             </View>
         <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />


        {/* PACKAGE DETAILS */}

        <View style={{backgroundColor: 'white',  padding: 10, paddingBottom: 20}}>

          

           
            <View>
                        <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>TRIP DETAILS</Text>
            </View>

            
            <View>
        
            <View style={{  marginBottom: 25, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View style={{marginBottom: 10}}>
                        <Text style={{ fontSize: 13, color: 'grey', marginTop: 20, marginBottom: 10, fontFamily: 'Manrope_500Medium'}} >Number Of Seats</Text>
                    </View>


                

                {/* map the number of available seats here, set to state sts when clicked */}

                {/* {totalSeatss()} */}

                {SeatssOp()}
             
                

            </View>

            <View style={{ marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               

                <View style={{marginBottom: 20}}>
                        <Text style={{fontSize: 13, color: 'grey', marginTop: 20, fontFamily: 'Manrope_500Medium'}} >Door2Door ? Do you need the driver to fetch and take you to your exact location? Additional Costs is R{willTaxiPrice}</Text>
                    </View>



                
                    {willTaxiOptTwo()}




               
                

            </View>


        </View>


     </View>

  <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

                <View style={{flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
                        <View style={{width: 50, height: 50}}>
                            {/* <Ionicons name={'warning'} color="grey" size={40} /> */}
                        <LottieView style={{width: 50, height: 50, marginRight: 20}} source={require('../../assets/warning/8750-alert.json')} autoPlay loop />

                        </View>
                            <View style={{width: 280, marginLeft: 10}}>
                                <Text style={{fontSize: 13, color: 'grey', marginTop: 20, fontFamily: 'Manrope_500Medium'}}>The date and time is subject to the driver's schedule. Please make sure it is correct. If Door2Door is selected, driver will take you to your exact location </Text>
                            </View>
             </View>

  <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />



        <View style={{backgroundColor: 'white', padding: 10, paddingBottom: 20}}>
            

            

            <View>
                        <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>DEPARTURE DATE</Text>
            </View>
            
            <View>
            <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 10 , fontSize: 13, fontFamily: 'Manrope_500Medium'}}>
              Time will be subject to availability. Date will be precise
            </Text>
            <View style={{alignItems: 'center'}}>
            <Text style={{fontSize: 25, color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 10 }}>
              {startDate}
            </Text>
            </View>

             
        </View>

       


     </View>

                 <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

  
             {/* LOCATIONS RECIEVER */}
        <View style={{backgroundColor: 'white', padding: 10, paddingBottom: 20}}>
           

        

            <View>
                        <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>SET ROUTE</Text>
            </View>
            
            <View>
            <Text style={{color: '#6e6e6e', paddingRight: 15, marginTop: 5, marginBottom: 10, fontFamily: 'Manrope_500Medium', fontSize: 13 }}>
             Please confirm where you are leaving from and your destination
            </Text>

        </View>


            <View style={{margin: 15}}>

                <View style={{flexDirection: 'row', alignItems: 'center'}}>

                    <View style={{width: 10, height: 10, backgroundColor:'green', borderRadius: 50, marginRight: 10}}>

                                </View>

                    <View style={{ height: 45, width: 250, borderRadius: 5}}>
                        <Text style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_600SemiBold', fontSize: 13}}
                        >
                            {originDesc}
                            
                        </Text>
                    </View>
                    

                </View>



                <View style={{flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>

                    <View style={{width: 10, height: 10,backgroundColor:'red', borderRadius: 50, marginRight: 10}}>

                                </View>

                     <View style={{ height: 45, width: 250, borderRadius: 5, marginTop: 10}}>
                    <Text style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_600SemiBold', fontSize: 13}}
                    >
                        {destDesc}
                    </Text>
                </View>

                </View>
                 
                
            </View>

     </View>

                


            <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

  
         {/* SET OFFER */}
        <View style={{backgroundColor: 'white', padding: 10,}}>
            

            <View>
                        <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>PRICE</Text>
            </View>
            
            <Text style={{color: '#6e6e6e', paddingRight: 15, marginTop: 5, marginBottom: 10 , fontFamily: 'Manrope_500Medium', fontSize: 13}}>
             Driver has requested a seat fare of :
            </Text>

       
            {/* AMOUNT CONTAINER */}
            <View style={{flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 10}}>
             {/*Date box*/}
                

             {/* input details  */}
            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 5, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Text style={{fontWeight: '600', color: 'white', fontSize: 20}} >R</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{ marginLeft: 20, height: 45, width: 200, borderRadius: 5}}>
                    <Text style={{fontSize: 25, color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 10, fontFamily: 'Manrope_500Medium'}}
                  
                    >
                         {price}
                    </Text>
                    
                </View>

            </View>

                {/* Booking Invoice */}
                



            </View>


     </View>
  
  
          {/* INVOICE */}
        <View style={{backgroundColor: 'white', padding: 10, paddingBottom: 20}}>
            <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

            <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black', fontFamily: 'Manrope_600SemiBold'}}>
                Order Invoice
            </Text>
            
            <View>
            <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 10, fontFamily: 'Manrope_500Medium' }}>
              The order is broken down as follows : 
            </Text>
            {orderInv()}

        </View>

       
           

     </View>
  
         


    <View>
         
           
    </View>

                     <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

      
         <TouchableOpacity style={{backgroundColor: 'green', marginLeft: 50, marginRight: 50, marginBottom: 50, borderRadius: 5, height: 45, flexDirection: 'row', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}
             onPress={ () => updateTodo()}
            >
                
                   
                    <Text style={{color: 'white', fontWeight: '600'}}  > Complete Booking</Text>
                    <Feather name="arrow-right" color={'white'} size={20} />

                  
               
            </TouchableOpacity>
    </ScrollView>
   </SafeAreaView>
    )
        }
}

export default RidesCustomerSearch

// const styles = StyleSheet.create({})
