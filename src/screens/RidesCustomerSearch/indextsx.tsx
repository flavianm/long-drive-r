import React, {useState, useEffect, useCallback, useMemo, useRef } from 'react'
import {View, Button, Platform, Text, Pressable, TextInput, Image} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { AntDesign, Entypo, MaterialIcons } from '@expo/vector-icons';
import { useNavigation } from "@react-navigation/core";
import { Picker } from "@react-native-picker/picker";
import DateTimePicker from '@react-native-community/datetimepicker';
import styles from './styles';
import { Feather, FontAwesome, MaterialCommunityIcons, Fontisto  } from '@expo/vector-icons';
import PostSearch from '../PostSearch';
import {Auth} from 'aws-amplify';
import {API, graphqlOperation} from 'aws-amplify';
import { createParcelOrder } from '../../graphql/mutations';
import {listParcelOrders} from '../../graphql/queries';


import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import PlaceRow from "./PlaceRow";

import BottomSheet from '@gorhom/bottom-sheet';

const GOOGLE_PLACES_API_KEY = 'AIzaSyAj8LKvEnwzBhZRycGM1yJJ96j7AA2snGE';

const homePlace = {
    description : 'Home',
    geometry: {location: {lat: -26.192910, lng: 28.035990 }},
};

const workPlace = {
    description : 'Work',
    geometry: {location: {lat: -26.193910, lng: 28.045990 }},
};

const ParcelCustomer = () => {

    const bottomSheetRef = useRef<BottomSheet>(null);

      // variables
  const snapPoints = useMemo(() => [-1, '40%'], []);

    const handleSheetChanges = useCallback((index: number) => {
    console.log('handleSheetChanges', index);
  }, []);

    const navigation = useNavigation();

    const goToSearch = () =>{
       navigation.navigate('PostSearch');
    }

    // STATES 

  const [user, setuser] = useState(null);







  const [date, setDate] = useState(new Date());
  const [time, setTime] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

const initialState = { userId: '', type: '', contents: '', qty: '', breakage: '',flame: '', recName: '', recSurname: '', recNumber: '', recCode: '', pickupDate: '',pickupTime: '', createdAt: '', }
const [formState, setFormState] = useState(initialState)
const [todos, setTodos] = useState([])



  const [selectedValue, setSelectedValue] = useState("java");



             function setInput(key, value) {
    setFormState({ ...formState, [key]: value })
  }


//   FETCHING USER INFORMATION

const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setuser(userSub);
            console.log(userSub)
            setInput('userId', userSub)
            
    }

     useEffect(() => {
            
                fetchUsers();
            },[])
   


            // END USER INFORMATION




  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    setInput('pickupDate', currentDate.toISOString())
                    
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };


    // TRYING HERE



 async function fetchTodos() {
    try {
        const todoData = await API.graphql(graphqlOperation(listParcelOrders))
    //   const todoData = await API.graphql({query: queries.listParcelOrders})
      const todos = todoData.data.listParcelOrders.items
      setTodos(todos)
      console.log(todos)

    //   const oneTodo = await API.graphql({ query: queries.getTodo, variables: { id: 'e75d6d99-0caa-43c2-9b05-c2a5ed598b32' }});
     
    
    //     let filter = {
    //     name: {
    //         eq: 'Exe' // filter priority = 1
    //     }
    // };
    
    // const oneTodoData = await API.graphql({ query: listTodos, variables: { filter : filter }});
    // const oneTodos = oneTodoData.data.listTodos.items
    // setFilterTodo(oneTodos)
    //   console.log(oneTodo)  
    } catch (err) { console.log('error fetching todos') }
    
  }

  useEffect(() => {
        
            fetchTodos();
        },[]);

        




    // Post order

  async function addTodo() {
    try {
      const todo = { ...formState }
      setTodos([...todos, todo])
      setFormState(initialState)
      await API.graphql(graphqlOperation(createParcelOrder, {input: todo}))
    } catch (err) {
      console.log('error creating parcelOrder:', err)
    }
  }



    // LOCATION SERVICES


    const [fromText, setFromText] = useState('');
    const [destinationText, setDestinationText] = useState('');

    const [originPlace, setOriginPlace] = useState(null);
    const [destinationPlace, setDestinationPlace] = useState(null);


    return (
        <ScrollView
    showsVerticalScrollIndicator={false}
    >
    
        

        <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>

           <Text style={{paddingLeft: 15, fontSize: 30, fontWeight: '600', color: 'black', marginTop: 30, marginBottom: 15}}>
                Send Parcel
            </Text>

        
          {/* <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black'}}>{user.attributes.email}</Text> */}
      
      
             
       
       
        </View>

        {/* PACKAGE DETAILS */}

        <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
            <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black'}}>
                Package Details
            </Text>
            
            <View>
            <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 25 }}>
             Please use the guide below to input correct details
            </Text>

            {/* guide */}
            <View style={{marginLeft: 15, marginBottom: 10, justifyContent: 'space-evenly' }}>

                {/* small items */}
                <View style={{width: 300, height: 100, borderRadius: 10, backgroundColor:'#e1e7e8', flexDirection: 'row'}}>
                   
                    <View style={{width: 60, height: 60, marginTop: 10, marginBottom: 5, marginRight: 10}}>
                        <MaterialCommunityIcons name="kettle" size={60} color="black" />
                    </View>
                   
                   <View>

                          <View style={{marginTop: 10, marginLeft: 5}}>
                                <Text style={{fontWeight: '500', fontSize: 20, marginBottom: 5, }}>Small Items</Text>
                            </View>
                                <View style={{flexDirection: 'row'}}>
                            <View style={{alignItems: 'center'}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5}}>
                                        <AntDesign name="checkcircle" size={20} color="green" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12, marginBottom: 10}}> - 5kg</Text>
                                </View>
                            <View style={{alignItems: 'center', marginLeft: 10}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5, }}>
                                        <AntDesign name="checkcircle" size={20} color="green" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12}}>Laptop</Text>
                                </View>
                        
                            <View style={{alignItems: 'center', marginLeft: 10}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5, }}>
                                        <AntDesign name="checkcircle" size={20} color="green" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12}}>Laptop</Text>
                                </View>
                        <View style={{alignItems: 'center', marginLeft: 10}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5, justifyContent: 'space-around' }}>
                                        <Entypo name="circle-with-cross" size={20} color="red" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12}}>Firearm</Text>
                                </View>
                        

                            </View>
                   </View>

                          
                                
               
                </View>

                {/* medium items */}
                <View style={{width: 300, height: 100, borderRadius: 10, backgroundColor:'#e1e7e8', flexDirection: 'row', marginTop: 10}}>
                   
                    <View style={{width: 60, height: 60, marginTop: 10, marginBottom: 5, marginRight: 10}}>
                        <Feather name="codesandbox" size={60} color="black" />
                    </View>
                   
                   <View>

                          <View style={{marginTop: 10, marginLeft: 5}}>
                                <Text style={{fontWeight: '500', fontSize: 20, marginBottom: 5, }}>Medium Items</Text>
                            </View>
                                <View style={{flexDirection: 'row'}}>
                            <View style={{alignItems: 'center'}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5}}>
                                        <AntDesign name="checkcircle" size={20} color="green" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12, marginBottom: 10}}> - 10kg</Text>
                                </View>
                            <View style={{alignItems: 'center', marginLeft: 10}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5, }}>
                                        <AntDesign name="checkcircle" size={20} color="green" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12}}>Blankets</Text>
                                </View>
                        
                            <View style={{alignItems: 'center', marginLeft: 10}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5, }}>
                                        <AntDesign name="checkcircle" size={20} color="green" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12}}>TV</Text>
                                </View>
                        <View style={{alignItems: 'center', marginLeft: 10}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5, justifyContent: 'space-around' }}>
                                        <Entypo name="circle-with-cross" size={20} color="red" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12}}>Marijuana</Text>
                                </View>
                        

                            </View>
                   </View>

                          
                                
               
                </View>

                {/* large items */}
                <View style={{width: 300, height: 100, borderRadius: 10, backgroundColor:'#e1e7e8', flexDirection: 'row', marginTop: 10}}>
                   
                    <View style={{width: 60, height: 60, marginTop: 10, marginBottom: 5, paddingLeft: 5, marginRight: 10}}>
                        <Fontisto name="suitcase-alt" size={60} color="black" />
                    </View>
                   
                   <View>

                          <View style={{marginTop: 10, marginLeft: 5}} >
                                <Text style={{fontWeight: '500', fontSize: 20, marginBottom: 5, }}>Large Items</Text>
                            </View>
                                <View style={{flexDirection: 'row'}}>
                            <View style={{alignItems: 'center'}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5}}>
                                        <AntDesign name="checkcircle" size={20} color="green" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12, marginBottom: 10}}> - 25kg</Text>
                                </View>
                            <View style={{alignItems: 'center', marginLeft: 10}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5, }}>
                                        <AntDesign name="checkcircle" size={20} color="green" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12}}>Laptop</Text>
                                </View>
                        
                            <View style={{alignItems: 'center', marginLeft: 10}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5, }}>
                                        <AntDesign name="checkcircle" size={20} color="green" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12}}>Laptop</Text>
                                </View>
                        <View style={{alignItems: 'center', marginLeft: 10}}>
                                    <View style={{width: 20, height: 20, backgroundColor: 'white', borderRadius: 50, marginBottom: 5, justifyContent: 'space-around' }}>
                                        <Entypo name="circle-with-cross" size={20} color="red" />
                                    </View>

                                    <Text style={{fontWeight: '500', fontSize: 12}}>Firearm</Text>
                                </View>
                        

                            </View>
                   </View>

                          
                                
               
                </View>

            </View>

            {/* input details  */}
            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 50, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Text style={{fontWeight: '600', color: 'white', fontSize: 20}} >1</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 45, width: 200, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    onChangeText={val => setInput('type', val)}
                    value={formState.type}
                    placeholder="S | M | L"
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 50, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Text style={{fontWeight: '600', color: 'white', fontSize: 20}} >2</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 45, width: 200, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    placeholder="What is in the package ?"
                    onChangeText={val => setInput('contents', val)}
                    value={formState.contents}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 50, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Text style={{fontWeight: '600', color: 'white', fontSize: 20}} >3</Text>
                    </View>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 45, width: 200, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    onChangeText={val => setInput('qty', val)}
                    value={formState.qty}
                    placeholder="Number Of Items"
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 50, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Text style={{fontWeight: '600', color: 'white', fontSize: 20}} >4</Text>
                    </View>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 45, width: 200, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    placeholder="Does It Break?"
                    placeholderTextColor="#7d7d7d"
                     onChangeText={val => setInput('breakage', val)}
                    value={formState.breakage}
                    />
                </View>

            </View>

            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 50, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Text style={{fontWeight: '600', color: 'white', fontSize: 20}} >5</Text>
                    </View>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 45, width: 200, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    onChangeText={val => setInput('flame', val)}
                    value={formState.flame}
                    placeholder="Is It Flame able?"
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>


        </View>

       

     </View>

           
      {/* PACKAGE RECIEVER */}
        <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
            <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black'}}>
                Package Receiver
            </Text>
            
            <View>
            <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 25 }}>
             Package will not be given to anyone else. Please be sure to send receiver secret code
            </Text>

            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 50, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Feather name='user' size={20} color={'white'}
                        />
                    </View>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 45, width: 200, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    placeholder="First Name / Company"
                    placeholderTextColor="#7d7d7d"
                     onChangeText={val => setInput('recName', val)}
                    value={formState.recName}
                    />
                </View>

            </View>

            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 50, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Feather name='user-plus' size={20} color={'white'}
                        />
                    </View>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 45, width: 200, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    placeholder="Last Name (If Applicable)"
                    placeholderTextColor="#7d7d7d"
                    onChangeText={val => setInput('recSurname', val)}
                    value={formState.recSurname}
                    />
                </View>

            </View>

            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 50, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Feather name='phone-incoming' size={20} color={'white'}
                        />
                    </View>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 45, width: 200, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    placeholder="Mobile Number"
                    placeholderTextColor="#7d7d7d"
                    onChangeText={val => setInput('recNumber', val)}
                    value={formState.recNumber}
                    />
                </View>

            </View>

            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 50, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Feather name='lock' size={20} color={'white'}/>
                    </View>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 45, width: 200, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    placeholder="Secret Code"
                    placeholderTextColor="#7d7d7d"
                    onChangeText={val => setInput('recCode', val)}
                    value={formState.recCode}
                    />
                </View>

            </View>

        </View>

       

     </View>



         {/* DATE AND TIME */}
        <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
            <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black'}}>
                Set Date and Time
            </Text>
            
            <View>
            <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 10 }}>
              Time will be subject to availability. Date will be precise
            </Text>

        </View>

       
            {/* DATE AND TIME CONTAINER */}
            <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
             {/*Date box*/}
          <View>

           
          <Pressable style={[styles.inputBox,{width: 60, height: 60, borderRadius: 50, backgroundColor: 'red', alignContent: 'center', justifyContent: 'space-around' }]} onPress={showDatepicker}>
                
                 <View style={[styles.timeContainer], {color: 'white'}}>
                    <AntDesign  name="calendar" size={30} color="white"/>
                </View>
        </Pressable>

          {show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode='date'
            is24Hour={true}
            display="default"
            onChange={onChange}
            
          />

        )} 
         </View>


              {/* Time Show */}

          <View style={{marginLeft: 40}}>

            {/*Date box*/}
          <Pressable style={[styles.inputBox,{width: 60, height: 60, borderRadius: 50, backgroundColor: 'blue', alignContent: 'center', justifyContent: 'space-around' }]} onPress={showTimepicker}>
                
                 <View style={[styles.timeContainer], {color: 'white'}}>
                    <AntDesign  name="clockcircleo" size={30} color="white"/>
                </View>
        </Pressable>

          {show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode='time'
            is24Hour={true}
            display="default"
            onChange={onChange}
          />

        )} 
         </View>


            </View>


     </View>
  
             {/* LOCATIONS RECIEVER */}
        
         {/* SET OFFER */}
        <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
            <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black'}}>
                Set Offer
            </Text>
            
            <View>
            <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 10 }}>
              How much are you offering the driver ? Driver may counter the offer
            </Text>

        </View>

       
            {/* DATE AND TIME CONTAINER */}
            <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
             {/*Date box*/}
                


          

             {/* input details  */}
            <View style={{flexDirection:'row', marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                    <View style={{height: 40, width:40, borderRadius: 50, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'space-around'}}>
                        <Text style={{fontWeight: '600', color: 'white', fontSize: 20}} >R</Text>
                        
                    </View>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, height: 45, width: 200, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    placeholder="180"
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

            </View>


     </View>
  
  
    <View>
          <Pressable style={{ marginTop: 10}}
            // onPress={goToSearch}
            onPress={
                console.log('')
            }
            >
                <View style={{backgroundColor: '#E15F25', margin: 50, borderRadius: 50, height: 40}}>
                <View style={{alignItems:'center', flexDirection: 'row', alignContent:'space-around', padding: 10, paddingLeft: 60 }}>
                  <Text style={{color: 'white', fontWeight: '600', marginRight: 15}}  > Set Destination</Text>
                  <Feather name="arrow-right" color={'white'} size={20} />

                </View>
                
                </View>
            </Pressable>
           
    </View>
       <View style={{width: 150, height: 40, backgroundColor: 'orange', borderRadius: 20}}>
                    <Button title="Create Order" onPress={addTodo}  />
                </View>


<BottomSheet
        ref={bottomSheetRef}
        index={1}
        snapPoints={snapPoints}
        onChange={handleSheetChanges}
        enableContentPanningGesture={false}
        animateOnMount={true}
      >
        <View style={styles.contentContainer}>
          <Text>Set Parcel Route</Text>
          <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
            <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black'}}>
                Set Parcel Route
            </Text>
            
            <View>
            <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 10 }}>
             Set where the parcel should be fetched from and destination
            </Text>

        </View>

       
            {/* DATE AND TIME CONTAINER */}
            
             {/*Date box*/}
            {/* <View style={{marginBottom: 400}}>
                <PostSearch/>
            </View> */}

            <View style={{margin: 15}}>
                
                <View style={styles.container}>

                   <GooglePlacesAutocomplete 

                        placeholder="Pick Up From ?"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za'
                        }}
                        enablePoweredByContainer={false}
                        suppressDefaultStyles
                        currentLocation={true}
                        currentLocationLabel='Current Location'
                        enableHighAccuracyLocation={true}
                        styles={{
                            textInput: styles.textInput,
                            container: styles.autoCompleteContainer,
                            listView: styles.listView,
                            separator: styles.separator

                        }}
                        onPress={(data, details = null) =>{
                            setOriginPlace({data, details});
                        }}
                        textInputProps={{
                            leftIcon: {}
                        }}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                        renderRow={(data) => <PlaceRow data={data} />}
                        renderDescription={(data) => data.description || data.vicinity}
                        predefinedPlaces={[homePlace, workPlace]}
                    />

                    <GooglePlacesAutocomplete 

                        placeholder="Where To"
                        query={{
                        key: GOOGLE_PLACES_API_KEY,
                        language: 'en', // language of the results
                        components: 'country:za'
                        }}
                        suppressDefaultStyles
                        styles={{
                            textInput: styles.textInput,
                            //  container:{
                            //     position: 'absolute',
                            //     top: 85,
                            //     left: 10,
                            //     right: 10,
                            // },
                            container:{
                                ...styles.autoCompleteContainer,
                                top: 85,
                                
                            },
                            // listView:{...styles.listView,
                            // top: 70},
                            separator: styles.separator
                        }}
                        onPress={(data, details = null) =>{
                            setDestinationPlace({data, details});
                        }}
                        enablePoweredByContainer={false}
                        renderRow={(data) => <PlaceRow
                            data={data} />}
                        onFail={(error) => console.error(error)}
                        fetchDetails
                        requestUrl={{
                        url:
                            'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
                        useOnPlatform: 'web',
                        }}
                    />

                    {/* cricle near origin input */}
                        <View style={styles.circle} />

                    {/* long line connecting cirlces */}
                        <View style={styles.line} />

                    {/* square near destintaion input */}
                        <View style={styles.square} />

                        
                    <View>

                        {/* <Image
                        style={{marginLeft: 30, height: 300, width: 300, marginTop: 250, alignContent: 'center', alignItems:'center'}}
                        source={require('../../../assets/air.jpg')}
                        /> */}
                    </View>
                       
            </View>
           
           
            </View>


           


     </View>
  
        </View>
      </BottomSheet>

    </ScrollView>
  
    )
}

export default ParcelCustomer

// const styles = StyleSheet.create({})
