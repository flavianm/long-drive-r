import * as React from 'react';
import { FlatList, Pressable, StyleSheet, Image } from 'react-native';

// import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../../components/Themed';
import ListItem from '../../components/ListItem';
import AppIcon from '../../components/AppIcon';
import AppScreen from '../../components/AppScreen';

import { useNavigation } from "@react-navigation/core";

const APP_COLORS = {
    primary: '#fc5c65',
    secondary: '#4ecdc4',
    black: '#000',
    white: '#fff',
    dark : '#0c0c0c',
};

const ListItemSeperator = () => {
    return (
        <View style={styles.seperator}></View>
    );
};



const MENU_ITEMS = [
    {
        title: 'Seat Orders',
        icon: {
            name: 'account',
            backgroundColor: APP_COLORS.primary,
        },
        targetScreen: 'SeatOrder',
    },
    {
        title: 'Parcel Orders',
        icon: {
            name: 'account',
            backgroundColor: APP_COLORS.primary,
        },
        targetScreen: 'ParcelOrders',
    },
    {
        title: 'Driver Parcel Orders',
        icon: {
            name: 'account',
            backgroundColor: APP_COLORS.primary,
        },
        targetScreen: 'ParcelOrdersDA',
    },
    {
        title: 'Driver Active Trips',
        icon: {
            name: 'head-question',
            backgroundColor: APP_COLORS.primary,
        },
        targetScreen: 'PDAO',
    },
    {
        title: 'Recent Trips',
        icon: {
            name: 'bell',
            backgroundColor: APP_COLORS.primary,
        },
        targetScreen: 'Empty',
       
    },
    {
        title: 'Settle Payments',
        icon: {
            name: 'cash',
            backgroundColor: APP_COLORS.secondary,
        },
        targetScreen: 'Pay',
        
    },
    {
        title: 'Payouts',
        icon: {
            name: 'cash-refund',
            backgroundColor: APP_COLORS.secondary,
        },
        targetScreen: 'Empty',
    },
   
];
export default function MoreScreen() {

   const navigation = useNavigation();

  return (
  
    <AppScreen style={styles.screen}>
      <View >
                
                <View style={{alignContent: 'center', alignItems: 'center'}}>

                    <Image
                        style={{width: 120, height: 120, borderRadius: 100}}
                    source={require('../../assets/images/charles.jpg')} />
                    <Text style={{marginTop: 15}}>
                        ACCOUNT : 9989854875
                    </Text>
                    <Text style={{marginTop: 5}}>
                        Your Account Balance : 
                    </Text>
                    <Text style={{fontSize: 30, fontWeight: '500', color: 'red'}}>
                       -R300.85
                    </Text>
                     

                    
                </View>
                
        </View>

            <View style={styles.container}>
                <FlatList
                    data={MENU_ITEMS}
                    keyExtractor={item => item.title}
                    ItemSeparatorComponent={ListItemSeperator}
                    renderItem={({ item }) => {
                        return (
                            <ListItem
                                title={item.title}
                                IconComponent={<AppIcon name={item.icon.name} backgroundColor={item.icon.backgroundColor}/>}
                                onPress={() => {
                                    navigation.navigate(item.targetScreen)}}
                            />
                        );
                    }}
                />
            </View>

    </AppScreen>


  );
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 10,
        borderRadius: 15,
        backgroundColor:'white'
    },
   seperator: {
        width: '100%',
        height: 1,
        color: 'white',
        backgroundColor: '#d6d6d6'

    },
      screen: {
        // borderRadius: 15,
        backgroundColor: 'white'
    }
});
