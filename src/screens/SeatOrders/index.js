import { Feather } from '@expo/vector-icons'
import React from 'react'
import { View, Text, Pressable, Image } from 'react-native'

export default function SeatOrder() {
    return (
        <View>
            <Text style={{ fontSize: 30, fontWeight: '600', color: 'black', marginTop: 30, marginLeft: 15}}>
                    Seat Orders
            </Text>

        <Pressable style={{backgroundColor:'white', margin: 20, borderRadius: 10}}>
            
           <View  style={{ margin: 10}}>
           
            <View style={{backgroundColor:'white',  flexDirection: 'row'}}>

                <View style={{backgroundColor: 'white', margin: 10}}>
                
                <View style={{flexDirection: 'row', justifyContent:'space-between', alignContent: 'center', alignItems: 'baseline'}}>
                    <Text>
                            09 : 01  
                    </Text>

                    <View style={{width: 10, height: 10, borderWidth: 1, borderColor: 'black', borderRadius: 50}}></View>

                </View>
                

                <View style={{flexDirection: 'row', justifyContent:'space-between', alignContent: 'center', alignItems: 'baseline'}}>
                        <Text style={{marginTop: 30, paddingRight: 20}}>
                        11 : 00 
                        </Text>
                        <View style={{width: 10, height: 10, backgroundColor: 'black', borderRadius: 50}}></View>
                </View>
                

            </View>

            <View style={{paddingLeft: 5, margin: 10}}>
                <View>
                            <Text>
                        24 Mandela  
                        </Text>
                        <Text style={ {marginTop: 30}}>
                        7th Hani St
                        </Text>
                </View>

            
            </View>

            <View style={{padding: 10, height:'100%'}}>
                        <Text style={{fontWeight: '400'}}>31/08/2021</Text>
                         <Image
                        style={{borderRadius: 10, height: 70, width: 70, marginTop: 20 }} 
                        source={require('../../assets/images/muzi.jpg')}
                        /> 
            </View>


            </View>

         <View style={{ padding: 10, marginTop: -50}}>
                <Text style={{fontSize: 20}}>Ginger McDonald</Text>
                <Text style={{marginTop: 5}}>1 Seat(s)</Text>
                <Text style={{marginTop: 5}}>Small Luggage</Text>
                <Text style={{marginTop: 5}}>0 Stops</Text>
            </View>

             <View style={{flexDirection: 'row', padding: 10, justifyContent:'space-evenly'}}>
                
                <Pressable style={{ flexDirection: 'row', alignItems: 'center', padding: 5}}>
                    <View style={{backgroundColor:'green', padding: 10, borderRadius: 50, marginRight: 5}}>
                        <Feather name={'twitch'} size={20} color={'white'} />
                    </View>
                    <Text>Accept</Text>
                </Pressable>

                  <View style={{ flexDirection: 'row', alignItems: 'center', padding: 5}}>
                    <View style={{backgroundColor:'red', padding: 10, borderRadius: 50, marginRight: 5}}>
                        <Feather name={'twitch'} size={20} color={'white'} />
                    </View>
                    <Text>Decline</Text>
                </View>

            </View>
        </View>
            
        
            </Pressable>

        </View>
    )
}
