import React,{useEffect, useState} from "react";
import {View, Text, Dimensions, Alert, Image} from 'react-native';
import HomeMap from "../../components/HomeMap";
import OrderMap from "../../components/OrderMap";
import { useRoute } from "@react-navigation/core";
import {API, graphqlOperation} from 'aws-amplify';
import { getOrder, getCar } from "../../graphql/queries";
import { onOrderUpdated, onCarUpdated } from "./subscriptions";
import { Entypo, Feather, FontAwesome5, MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import { useNavigation} from "@react-navigation/core";

const OrderScreen = (props) =>{
   
    const [car, setCar] = useState(null);
    const [order, setOrder] = useState(null);

    const route = useRoute();

    const navigation = useNavigation();
    console.log(route.params.id);

    useEffect(() =>{
        
        if(!order?.carId || order.carId === '1'){ // if it has an id of 1 then dont do anything. we set new cars to have 1 when registered without being assigned
            return;
        }

        const fetchCar = async () =>{
            try{
                const carData = await API.graphql(
                    graphqlOperation(
                        getCar, {id: order.carId})
                );
                setCar(carData.data.getCar);
            }catch(e){

            }
        }
        fetchCar();
    },[order]) //call the car whenever we update the order

        // fetch order on inital render
    useEffect(() =>{
        const fetchOrder = async () =>{
            try{
                const orderData = await API.graphql(
                    graphqlOperation(
                        getOrder, {id: route.params?.id})
                );
                setOrder(orderData.data.getOrder);
            }catch(e){

            }
        }
        fetchOrder();
    },[])

    // subscribe to order updates

    useEffect(() => {

       
        const subscription = API.graphql(
            graphqlOperation(
                onOrderUpdated, {id: route.params?.id })
        ).subscribe({
            next: ({value}) => setOrder(value.data.onOrderUpdated),
            error: error => console.warn(error)
        })
        return ()=> subscription.unsubscribe();
    },[])


        // SUBSCRIBE TO CAR UPDATES
     useEffect(() => {
       
          if(!order?.carId || order.carId === '1'){
            return;
        }

        const subscription = API.graphql(
            graphqlOperation(
                onCarUpdated, {id: order.carId })
        ).subscribe({
            next: ({value}) => setCar(value.data.onCarUpdated),
            error: error => console.warn(error)
        })
        return ()=> subscription.unsubscribe();
    },[order])


    const alert = ()=>{
        Alert.alert('Cancel Trip?', 'Funds will be reversed minus booking fee in 12 hours')
    }

    const tomessage =()=>{
        navigation.navigate('Chat');
    }

    return (
        <View style={{backgroundColor: 'white'}}>

             {/*HomeMap*/}
             {/* <View style={{height: Dimensions.get('window').height - 600}}>
                <OrderMap car={car}/>
           </View> */}

           <View style={{alignItems: 'center'}}>
            <Image
                   source={require('../../../src/assets/correct.png')}
                   style={{width: 200, height: 200}}
                    />
           </View>

           <View style={{alignItems: 'center'}}>
            <Text style={{fontSize: 20, fontWeight: '500', color: 'green'}}>Successfully Booked !</Text>
           </View>

           <View style={{alignItems: 'center'}}>
                
               <Text style={{color: 'grey', marginTop: 10}}>Status: {order?.status}</Text>
               {/* <Text style={{color: 'black', fontSize: 30, fontWeight: '400', marginTop: 10}}> Sibusiso Dlamini</Text>
                <Text style={{color: 'black', fontSize: 20,  marginTop: 10}}>EJD221GP</Text>
                <Text style={{color: 'black', fontSize: 18, marginTop: 10}}>Toyota Etios 2008</Text>
                <Text style={{color: 'black', marginTop: 10}}>Departure: 18 / 08 / 2021</Text>
                <Text style={{color: 'black', marginTop: 10}}>Time: 18: 52</Text> */}

                {/* <View style={{flexDirection: 'row', paddingTop: 20}} >
                    <Feather style={{margin: 5}} 
                    name="phone-call"
                    size={28}
                    color='green'/>
                    <Feather style={{margin: 5, marginLeft: 40}}
                    name="message-circle"
                    size={28}
                    color='black'/>
                      <Feather style={{margin: 5, marginLeft: 40}}
                    name="x-circle"
                    size={28}
                    onPress={alert}
                    color='red'/>
                
                </View> */}
           </View>

            <View style={{alignItems: 'center'}}>
                {/* image */}
                <View style={{ alignItems: 'center',marginTop: 10 }}>
                    <View>
                        <Image
                        style={{borderRadius: 100, height: 70, width: 70}} 
                        source={require('../../../src/assets/images/muzi.jpg')}
                        />
                        
                    </View>
                     {/* name */}
                    <View>
                        <Text style={{fontSize: 20, fontWeight: '500', paddingLeft: 10, paddingTop: 5}}>Muzi Dladla</Text>
                    </View>
                </View>

                <View>
                    <View style={{ flexDirection: 'row'}}>
                    <Text style={{fontSize: 18,}}>Car : </Text>
                    <Text style={{fontSize: 18,}}>Mahindra M4</Text>
                    </View>
                </View>

            </View>  


            <View style={{alignItems: 'center', width: 300,}}>

                <View style={{width: 100, flexDirection: 'row', alignContent: 'center', alignItems: 'baseline'}}>
                    <Text style={{marginTop: 30, paddingRight: 30}} >09 : 00</Text>

                    <View style={{width: 10, height: 10, borderWidth: 1, borderColor: 'black', borderRadius: 50}}></View>
                            <Text style={{width: 150, marginTop: 30, paddingLeft: 30}}>24 Mandela Av</Text>
                </View>
                

                <View style={{width: 100,flexDirection: 'row', alignContent: 'center', alignItems: 'baseline'}}>
                        <Text style={{marginTop: 30, paddingRight: 30 }}>12 : 05</Text>
                        <View style={{width: 10, height: 10, backgroundColor: 'black', borderRadius: 50}}></View>
                         <Text style={{width: 150, marginTop: 30 , paddingLeft: 30}}>7th Hani St</Text>
                </View>

            </View>

            <View style={{ marginTop: 20, flexDirection: 'row', width: '100%', justifyContent:'space-evenly'}}>
                <View style={{flexDirection: 'row', padding: 10, alignContent: 'center', alignItems: 'center'}}>
                    
                    <View style={{borderRadius: 50, borderWidth: 2, height: 30, width: 30, alignItems: 'center', justifyContent: 'space-around'}}>
                        <MaterialCommunityIcons name='car-seat' color='black' size={15} />
                    </View>
                    <Text style={{fontWeight: '500', fontSize: 15, marginLeft: 10}}>3</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, alignContent: 'center', alignItems: 'center'}}>
                    
                    <View style={{borderRadius: 50, borderWidth: 2, height: 30, width: 30, alignItems: 'center', justifyContent: 'space-around'}}>
                        <Entypo name='price-tag' color='black' size={15} />
                    </View>
                    <Text style={{fontWeight: '500', fontSize: 15, marginLeft: 10}}>ZAR300</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, alignContent: 'center', alignItems: 'center'}}>
                    
                    <View style={{borderRadius: 50, borderWidth: 2, height: 30, width: 30, alignItems: 'center', justifyContent: 'space-around'}}>
                        <MaterialIcons name='pets' color='black' size={15} />
                    </View>
                    <Text style={{fontWeight: '500', fontSize: 15, marginLeft: 10}}>NO</Text>
                </View>
                <View style={{flexDirection: 'row', padding: 10, alignContent: 'center', alignItems: 'center'}}>
                    
                    <View style={{borderRadius: 50, borderWidth: 2, height: 30, width: 30, alignItems: 'center', justifyContent: 'space-around'}}>
                        <FontAwesome5 name='suitcase-rolling' color='black' size={15} />
                    </View>
                    <Text style={{fontWeight: '500', fontSize: 15, marginLeft: 10}}>S</Text>
                </View>
               
            </View>

            <View style={{justifyContent:'space-around', alignContent: 'center', alignItems: 'center'}}>
               <View style={{flexDirection: 'row', padding: 10, alignContent: 'center', alignItems: 'center'}}>
                    <View style={{borderRadius: 50, backgroundColor:'red', borderColor: 'red', borderWidth: 2, height: 30, width: 30, alignItems: 'center', justifyContent: 'space-around'}}>
                            <MaterialCommunityIcons name='delete' color='white' size={15} />
                    </View>

                <Text style={{fontWeight: '500', fontSize: 15, marginLeft: 10, color: 'red'}}>DELETE ORDER</Text>
            
               </View>
                
            </View>
              
        </View>
    )
};

export default OrderScreen;