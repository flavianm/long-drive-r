import React, {useState, useEffect, useCallback} from 'react'
import { FlatList, Pressable, StyleSheet, RefreshControl, Image, TextInput, ScrollView, SafeAreaView} from 'react-native'
import {Auth} from 'aws-amplify';
import { getUser } from '../../graphql/queries';
import { API, graphqlOperation } from "aws-amplify";
import { updateUser } from '../../graphql/mutations';
import { onUpdateUser, onUserUpdated, onUserWalletUpdated } from '../../graphql/subscriptions';
import Modal from "react-native-modal";
import { useNavigation } from "@react-navigation/core";


import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

// import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../../components/Themed';
import ListItem from '../../components/ListItem';
import AppIcon from '../../components/AppIcon';
import AppScreen from '../../components/AppScreen';
import { Feather, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const ListItemSeperator = () => {
    return (
        <View style={styles.seperator}></View>
    );
};



const APP_COLORS = {
    primary: '#fc5c65',
    secondary: '#4ecdc4',
    black: '#000',
    white: '#fff',
    dark : '#0c0c0c',
};

const MENU_ITEMS = [
     {
        title: 'Deposit',
        icon: {
            name: 'plus',
            backgroundColor: 'orange',
        },
        targetScreen: 'Pay',
    },
    {
        title: 'Cash Out',
        icon: {
            name: 'minus',
            backgroundColor: 'orange',
        },
        targetScreen: 'cashout',
    },
    {
        title: 'Banking Details',
        icon: {
            name: 'format-list-bulleted',
            backgroundColor: 'orange',
        },
        targetScreen: 'BankingDetails',
    },   
];

const WalletHome = () => {


    const navigation = useNavigation();


 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });



const [isModalVisible, setModalVisible] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

const [user, setuser] = useState([]);
const [email, setEmail] = useState([]);
const [userId, setUserId] = useState();
const [BB, setBB] = useState([]);
const [userDetails, setUserDetails] = useState([]);
const initialState = {id: userId , bank: '', account: '', branch: ''}
const [formState, setFormState] = useState(initialState)
const [todos, setTodos] = useState([]);
const [balancebf, setBalancebf] = useState([]);


const toggleModal = () => {
    setModalVisible(!isModalVisible);
    
  };

 const onRefresh = useCallback(() => {

    setRefreshing(true);

                 fetchUsers();
                
           


    wait(2000).then(() => setRefreshing(false));
  }, []);


const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser({bypassCache: true});
            const userSub = userData.attributes;
            const userId = userSub.sub;
            setUserId(userId)
            setEmail(userSub.email);
            setuser(userSub);

            const todoData = await API.graphql(graphqlOperation(getUser, { id: userId}))
            const todos = todoData.data.getUser
            setUserDetails(todos)
            setBalancebf(todos.balance)
            
    }


     useEffect(() => {
                
                fetchUsers();
            },[BB])






let subsUpdate;

 function setUpSus(){
    //  subs = API.graphql(graphqlOperation(onUserUpdated, 
    //     {id: '2f6df6e4-9b36-4c6e-8a2c-dd344c0d2daa'})).subscribe({ next: (thedata) => {
    //         setBB(thedata.data.onUserUpdated)
    //     }});

     subsUpdate = API.graphql(graphqlOperation(onUserWalletUpdated)).subscribe( {next: (daraa) => {
         setBB(daraa)
     }, }) 

 }

    useEffect(() =>{
        setUpSus();

        return() =>{
           
            subsUpdate.unsubscribe();
        };

    },[]);

          
        
        // console.log('mo',BB)
  
    




 function setInput(key, value) {
    setFormState({ ...formState, [key]: value })
  }
             async function updateTodo() {
    try {
      const todo = { ...formState }
      setTodos([...todos, todo])
      setFormState(initialState)
      await API.graphql(graphqlOperation(updateUser, {input: todo}));
    //    await API.graphql(graphqlOperation(updateUser, todo));
      console.warn('Successful')
    } catch (err) {
      console.log('error update User:', err)
    }

  }


    return (
        <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>

        {/* Banking details Modal */}

             <Modal isVisible={isModalVisible}
         swipeDirection={'down'}
         onSwipeComplete={()=> setModalVisible(false)}
         style={{ backgroundColor: 'white', marginLeft: 0, marginRight: 0}}>

        <Text style={{paddingLeft: 15, fontSize: 30, fontWeight: '600', color: 'black', marginTop: 30, marginBottom: 15}}>
                Banking Details
                </Text>

           <Text style={{paddingLeft: 15, fontSize: 20, fontWeight: '600', color: 'black'}}>{user.email}</Text>
     
            <View style={{marginLeft: 15, marginTop: 20, marginBottom: 10, justifyContent: 'center', flexDirection: 'row' }}>

                {/* medium items */}
                <View style={{width: 50, height: 50,  borderRadius: 50, backgroundColor:'blue', flexDirection: 'row', marginTop: 10, justifyContent:'space-evenly', alignItems: 'center'}}>
                   
                        <MaterialCommunityIcons name="bank-outline" size={30} color="white" />
                    
                </View>
                <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontSize: 15, marginLeft: 20, marginBottom: 5}} >Bank Name</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    onChangeText={val => setInput('bank', val)}
                    value={formState.bank}
                    placeholder={userDetails.bank}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

                
                

            </View>

               
            </View>

            <View style={{marginLeft: 15, marginTop: 20, marginBottom: 10, justifyContent: 'center', flexDirection: 'row' }}>

                {/* medium items */}
                <View style={{width: 50, height: 50,  borderRadius: 50, backgroundColor:'blue', flexDirection: 'row', marginTop: 10, justifyContent:'space-evenly', alignItems: 'center'}}>
                   
                        <MaterialCommunityIcons name="account-cash-outline" size={30} color="white" />
                    
                </View>
                <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontSize: 15, marginLeft: 20, marginBottom: 5}} >Account Number</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    onChangeText={val => setInput('account', val)}
                    value={formState.account}
                    placeholder={userDetails?.account?.toString()}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
                

            </View>

               
            </View>

            <View style={{marginLeft: 15, marginTop: 20, marginBottom: 10, justifyContent: 'center', flexDirection: 'row' }}>

                {/* medium items */}
                <View style={{width: 50, height: 50,  borderRadius: 50, backgroundColor:'blue', flexDirection: 'row', marginTop: 10, justifyContent:'space-evenly', alignItems: 'center'}}>
                   
                        <MaterialCommunityIcons name="account-cash-outline" size={30} color="white" />
                    
                </View>
                <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontSize: 15, marginLeft: 20, marginBottom: 5}} >Branch Code</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40}}
                    onChangeText={val => setInput('branch', val)}
                    value={formState.branch}
                    placeholder={userDetails.branch}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
                

            </View>

               
            </View>

<View style={{marginLeft: 15, marginTop: 20, marginBottom: 10, justifyContent: 'center', flexDirection: 'row' }}>

                {/* medium items */}
               
                <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               
                

              </View>

               
            </View>



             <View style={{marginLeft: 30, marginTop: 20, width: 200, height: 40, backgroundColor: 'green', borderRadius: 10, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}>
                    <Pressable style={{ alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
                    // onPress={addTodo} 
                    // onPress={ ()=> updateTodo} 
                    onPress={()=> updateTodo()}
                    >
                        <Text style={{fontWeight: '500', color: 'white'}}>Update Profile</Text>
                    </Pressable>
            </View>







         </Modal>    
            <View>
                <Text style={{ paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30, marginBottom: 15, fontFamily: 'Manrope_700Bold'}}>Wallet</Text>
            </View>

        <ScrollView 
        
        refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
        
        >
            <View style={{alignContent: 'center', alignItems: 'center'}}>
            <View style={{width: 300, height: 150, backgroundColor: '#91d0eb', borderRadius: 10, marginLeft: 30, marginRight: 30, marginTop: 15, marginBottom: 30}}>
                <Text style={{marginLeft: 10, color: 'white', marginTop: 10, fontWeight: '500', fontSize: 18, fontFamily: 'Manrope_500Medium'}}>Available Balance</Text>
                {/* <Text style={{marginLeft: 10, marginTop: 5, fontSize: 13}}>{user.sub}</Text> */}
                <View style={{alignContent: 'center', backgroundColor: '#91d0eb', alignItems: 'center', marginTop: 20}}>
                 <Text style={{marginLeft: 10, marginTop: 0, fontWeight: '600', fontSize: 50, color: 'white', fontFamily: 'Manrope_600SemiBold'}}>R{parseFloat(userDetails.balance).toFixed(2) || `0.00`}</Text>
                </View>
          
           </View>
           <View style={{ marginBottom: 30, flexDirection: 'row', alignItems: 'center'}}>
               <MaterialIcons name="keyboard-arrow-down" size={24} color="black" />
                <Text style={{fontStyle: 'italic', fontFamily: 'Manrope_400Regular'}}>Pull to refresh balance</Text>
           </View>
           
            </View>

            
        </ScrollView>    
            
              <View style={[styles.container]}>
                <FlatList
                    data={MENU_ITEMS}
                    keyExtractor={item => item.title}
                    ItemSeparatorComponent={ListItemSeperator}
                    renderItem={({ item }) => {
                        return (
                            <ListItem
                                title={item.title}
                                IconComponent={<AppIcon name={item.icon.name} backgroundColor={item.icon.backgroundColor}/>}
                                onPress={() => {
                                    navigation.navigate(item.targetScreen, {userId, balancebf, email})}}
                            />
                        );
                    }}
                />
            </View>

        </SafeAreaView>
    )
}



export default WalletHome

const styles = StyleSheet.create({

     container: {
        marginVertical: 10,
        borderRadius: 15,
        backgroundColor:'white'
    },
   seperator: {
        width: '100%',
        height: 1,
        color: 'white',
        backgroundColor: '#d6d6d6'

    },
      screen: {
        // borderRadius: 15,
        backgroundColor: 'white'
    }
})
