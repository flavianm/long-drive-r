export const onUserUpdated = /* GraphQL */ `
  subscription OnUserUpdated($id: ID!) {
    onUserUpdated(id: $id) {
      id
      username
      email
      name
      surname
      idno
      dob
      phone_number
      emergency_name
      emergency_number
      chronic
      chronic_name
      wheelchair
      gender
      completeP
      imageUrl
      smoke
      balance
      account
      bank
      branch
      orders {
        items {
          id
          createdAt
          type
          status
          originLatitude
          originLongitude
          destLatitude
          destLongitude
          userId
          carId
          updatedAt
        }
        nextToken
      }
      car {
        id
        type
        latitude
        longitude
        heading
        isActive
        orders {
          nextToken
        }
        userId
        user {
          id
          username
          email
          name
          surname
          idno
          dob
          phone_number
          emergency_name
          emergency_number
          chronic
          chronic_name
          wheelchair
          gender
          completeP
          imageUrl
          smoke
          balance
          account
          bank
          branch
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;