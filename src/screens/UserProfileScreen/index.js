import React, {useState, useEffect} from 'react'
import { ScrollView, StyleSheet, Text, View, TextInput, Pressable, Image, SafeAreaView, Platform , TouchableOpacity } from 'react-native'
import { useNavigation, useRoute } from "@react-navigation/core";
import { AntDesign, Entypo, MaterialIcons } from '@expo/vector-icons';import { Feather, FontAwesome, MaterialCommunityIcons, Fontisto  } from '@expo/vector-icons';
import { API, graphqlOperation } from "aws-amplify";
import {Auth, Storage} from 'aws-amplify';
import { updateUser } from '../../graphql/mutations';
import { getUser } from '../../graphql/queries';
import Toast from 'react-native-toast-message';
import AnimatedLoader from "react-native-animated-loader";
import LottieView from 'lottie-react-native';
import * as ImagePicker from 'expo-image-picker';

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';


const UserProfile = () => {

 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

 const [visible, setVisible] = useState(true);


     useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 5000);
  }, [])


const [user, setUser] = useState([])

const [champUser, setChampUser] = useState();

const [image, setImage] = useState(null);

const [name, setName] = useState(user?.name);
const [surname, setSurname] = useState(user?.surname);
const [idNo, setIdNo] =useState(champUser?.idno);
const [gender, setGender] =useState(champUser?.gender);
const [emerName, setEmerName] =useState(champUser?.emergency_name);
const [emerNumber, setEmerNumber] =useState(champUser?.emergency_number);
const [smoke, setSmoke] =useState(champUser?.smoke);
const [wheelchair, setWheelchair] =useState(champUser?.wheelchair);
const [chronic, setChronic] =useState(champUser?.chronic);
const [chronic_name, setChronic_name] =useState(champUser?.chronic_name);




 const route = useRoute();
  const {userId} = route.params;

  

      const navigation = useNavigation();

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

 console.log(result);

  if (!result.cancelled) {
      setImage(result.uri);
    }
};

const fetchUsers = async() =>{

        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setUser(userData.attributes);
            console.log(userData.attributes.email)
            // setInput('userId', userSub)


        // if balance is equal to null, put in a 0.01 value

        // fetch balance amount

            const userBalance = await API.graphql(graphqlOperation(getUser, { id: userSub}))
            const userBalanceInfo = userBalance.data.getUser
            setChampUser(userBalanceInfo)
            const zeBalance = userBalanceInfo.balance
            // setCurrentBalance(zeBalance)
            console.log('USER BALANCE  ::' + zeBalance)


            const bankupdate = {
                id: userSub,
                balance: '0.01'
            }


            if (zeBalance === null){
                console.log('null value here')
                await API.graphql(graphqlOperation(updateUser, {input: bankupdate}))
                console.log('done')
            } else{
                console.log('balance is :' + zeBalance )
            }


            
    }

 useEffect(() => {

   fetchUsers();
              
            },[])


const uploadImage = async () =>{
    try {
        const response = await fetch(image);

        const blob = await response.blob();


        const urlParts = image.split('.')


        const extension = urlParts[urlParts.length - 1];

        const frnt = champUser?.id + 'profile'

        const key =   `${frnt}.${extension}`; 
        
        await Storage.put(key, blob);

        return key;



    }catch(e){
        console.log(e)
    }

    return '';
}





const initialState = {id: userId, name: '', surname: '', idno: '', emergency_name: '', emergency_number: '', chronic: '',chronic_name: '', wheelchair: '', gender: '', completeP: 'Yes', imageUrl: 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/images/products/keyboard1.jpg', smoke: ''}
const [formState, setFormState] = useState(initialState)
const [todos, setTodos] = useState([])

    function setInput(key, value) {
    setFormState({ ...formState, [key]: value })
  }
   

const todoDetails = {
        id: user.sub,
        status: 'dcancelled',
        active: 'no'
    };

     async function updateTodo() {
        let imagee;

        if(!!image){
          imagee = await uploadImage();
        }

        const userData = await Auth.currentAuthenticatedUser();
        const userSub = userData.attributes.sub;


        const bluza = {
            id: userSub,
            name: name, 
            surname: surname, 
            idno: idNo, 
            emergency_name: emerName, 
            emergency_number: emerNumber, 
            chronic: chronic,
            chronic_name: chronic_name, 
            wheelchair: wheelchair, 
            gender: gender, 
            completeP: 'Yes', 
            imageUrl: 'https://thumbs.dreamstime.com/b/default-avatar-profile-icon-social-media-user-vector-default-avatar-profile-icon-social-media-user-vector-portrait-176194876.jpg', 
            smoke: smoke
        }

    try {
      const todo = { ...formState }
      console.log('1')
      setTodos([...todos, todo])
       console.log('2')
      setFormState(initialState)
       console.log('3')
      await API.graphql(graphqlOperation(updateUser, {input: bluza}))
    //   console.warn('Successful')

       Toast.show({
      type: 'success',
      text1: 'Profile',
      text2: 'Profile is Set'
    }); 

    navigation.navigate('Home')



    } catch (err) {
      console.log('error update User:', err)
      Toast.show({
      type: 'error',
      text1: 'Profile',
      text2: 'Profile Update Failed'
    }); 
    }

  }

  const zeImage = ()=>{

    if (image === null){


        return(
            <TouchableOpacity  onPress={pickImage} style={{width: 100, height: 100, borderRadius: 5, backgroundColor: '#e1e7e8', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
             <Image  source={{ uri: champUser?.imageUrl}}
            style={{ width: 100, height: 100, marginBottom: -94.5, borderRadius: 5 }}
            />
            
             <LottieView style={{width: 75, height: 75, marginBottom: 20}} source={require('../../assets/warning/9948-camera-pop-up.json')} autoPlay loop />

            </TouchableOpacity>

        )
    } else{
        return(
            <TouchableOpacity onPress={pickImage} >

            <Image  source={{ uri: image}}
            style={{ width: 100, height: 100, borderRadius: 5 }}
            />

            </TouchableOpacity>
            

            )
    }


     
  }


   if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{

    return (

         <SafeAreaView style={{ backgroundColor: 'white', flex: 1}}>


             <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>

        <ScrollView
         showsVerticalScrollIndicator={false}
        >
            <View>
                <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10}}>

                <Text style={{paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30, fontFamily: 'Manrope_700Bold'}}>
                My Profile
                </Text>

                <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

        
          <Text style={{paddingLeft: 15, fontSize: 13, fontWeight: '600', color: 'black', fontFamily: 'Manrope_500Medium'}}>{user.email}</Text>

          <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

            </View>

                <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
            <Text style={{paddingLeft: 15, fontWeight: '600', color: 'black', fontFamily: 'Manrope_700Bold', fontSize: 14}}>
                Personal Details
            </Text>
            
            <View>
            <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 25, fontFamily: 'Manrope_500Medium' }}>
             Please input correct details to avoid suspension
            </Text>

            {/* guide */}
           
            <View style={{alignContent: 'center', alignItems: 'center', marginBottom: 30}}>

            { zeImage()}
            </View>

              

            {/* input details  */}
            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', marginLeft: 20, marginBottom: 5, fontFamily: 'Manrope_500Medium', fontSize: 13}} >First Name</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', fontSize: 13, paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium'}}
                    onChangeText={val => setName(val)}
                    // value={formState.name}
                    placeholder= {champUser?.name || 'First Name'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontFamily: 'Manrope_500Medium', fontSize: 13, marginLeft: 20, marginBottom: 5}} >Last Name</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setSurname(val)}
                    // value={formState.surname}
                    placeholder= {champUser?.surname || 'Surname'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400',  marginLeft: 20, marginBottom: 5, fontFamily: 'Manrope_500Medium', fontSize: 13}} > SA ID Number / Passport</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setIdNo(val)}
                    placeholder= {champUser?.idno || 'ID Number / Passport'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

           <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', marginLeft: 20, marginBottom: 5, fontFamily: 'Manrope_500Medium', fontSize: 13}} >Gender</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setGender( val)}
                    placeholder= {champUser?.gender || 'Male | Female'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

        </View>

            <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

     </View>
            
                <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
            <Text style={{paddingLeft: 15, fontWeight: '600', color: 'black', fontFamily: 'Manrope_700Bold', fontSize: 14}}>
                Contact Details
            </Text>
            
            <View>
            <Text style={{color: '#6e6e6e',paddingLeft: 15, paddingRight: 15, marginTop: 5, marginBottom: 25, fontFamily: 'Manrope_500Medium', fontSize: 13 }}>
             Please input correct details to avoid suspension
            </Text>

          

            {/* input details  */}
            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontFamily: 'Manrope_500Medium', fontSize: 13, marginLeft: 20, marginBottom: 5}} >Your Phone Number</Text>

                </View>
                {/* TextInput */}
                <View style={{justifyContent: 'space-evenly', backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <Text style={{color:'#7d7d7d', paddingLeft: 10, paddingTop: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    >
                        {user.phone_number}
                </Text>
                </View>

            </View>

            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontFamily: 'Manrope_500Medium', fontSize: 13, marginLeft: 20, marginBottom: 5}} >Your e-mail</Text>

                </View>
                {/* TextInput */}
                 <View style={{justifyContent: 'space-evenly', backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <Text style={{color:'#7d7d7d', paddingLeft: 10, paddingTop: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    >
                        {user.email}
                </Text>
                </View>

            </View>

            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontFamily: 'Manrope_500Medium', fontSize: 13, marginLeft: 20, marginBottom: 5}} >Emergency Contact Name</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setEmerName(val)}
                    placeholder= {champUser?.emergency_name || 'Emergency Name'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

       
            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontSize: 15, marginLeft: 20, marginBottom: 5, fontFamily: 'Manrope_500Medium', fontSize: 13}} >Emergency Contact Number</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setEmerNumber(val)}
                    placeholder={champUser?.emergency_number || 'emergency Number'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>


        </View>

            

     </View>

      <View style={{marginLeft: 20, marginRight: 20}}>
                    <View   
                    style={{ height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />



      </View>


                    
            
                <View style={{backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20}}>
            <Text style={{paddingLeft: 15,  fontWeight: '600', color: 'black', fontFamily: 'Manrope_700Bold', fontSize: 14}}>
                More Info
            </Text>
            
            <View>
            <Text style={{color: '#6e6e6e', fontFamily: 'Manrope_500Medium', fontSize: 13, paddingLeft: 15, marginTop: 5, marginBottom: 25 }}>
             Please input correct details to make your journey more satisfying
            </Text>

          

            {/* input details  */}
            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontFamily: 'Manrope_500Medium', fontSize: 13, marginLeft: 20, marginBottom: 5}} >Are you a chain smoker?</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setSmoke(val)}
                    placeholder={champUser?.smoke || 'Yes | No'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontFamily: 'Manrope_500Medium', fontSize: 13, marginLeft: 20, marginBottom: 5}} >Do you use a wheel chair ?</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setWheelchair(val)}
                    placeholder={champUser?.wheelchair || 'Yes | No'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

            <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontFamily: 'Manrope_500Medium', fontSize: 13, marginLeft: 20, marginBottom: 5}} >Do you have any chronic sicknesses that may require immediate care on the way</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setChronic(val)}
                    placeholder={champUser?.chronic || 'Yes | No'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>

           <View style={{marginLeft: 10, marginBottom: 10, justifyContent: 'space-evenly'}}>
               
               {/* icons */}
                <View>
                        <Text style={{fontWeight: '400', fontFamily: 'Manrope_500Medium', fontSize: 13, marginLeft: 20, marginBottom: 5}} >If yes, Please provide brief description and how to get emergency help to you</Text>

                </View>
                {/* TextInput */}
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 80, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setChronic_name(val)}
                    numberOfLines={4}
                    // value={chronic_name}
                    placeholder={champUser?.smoke || 'Asthma, Give me my pump. Sit me up right and plenty of water'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>

            </View>
<View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />
            

            <View style={{ marginTop: 20,  backgroundColor: 'green', borderRadius: 5, alignItems:'center', alignContent: 'center'}}>
                    <TouchableOpacity style={{width: 250, height: 45, marginLeft: 50, marginRight: 50, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
                    // onPress={addTodo} 
                    // onPress={ ()=> updateTodo} 
                    onPress={()=> updateTodo()}
                    >
                        <Text style={{fontWeight: '500', color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Update Profile</Text>
                    </TouchableOpacity>
            </View>
        </View>

            

     </View>
            
            

            </View>
        </ScrollView>

    </SafeAreaView>
    )
        }
}

export default UserProfile

const styles = StyleSheet.create({})
