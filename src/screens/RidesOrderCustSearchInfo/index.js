import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, ScrollView, SafeAreaView } from 'react-native'
import { useNavigation, useRoute } from "@react-navigation/core";
import { API, graphqlOperation} from "aws-amplify";
import * as mutations from '../../graphql/mutations'
import { FontAwesome, FontAwesome5, Fontisto, Ionicons, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';
import { getUser } from '../../graphql/queries';
import AnimatedLoader from "react-native-animated-loader";
import LottieView from 'lottie-react-native';

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const RidesOrderCustSearchInfo = (props) => {

    let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

const navigation = useNavigation();

const [visible, setVisible] = useState(true);

 useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 3000);
  }, [])

    const typeState = useState(null);
    const route = useRoute();
        const [status, setStatus] = useState([]);
        const [driverDet, setDriverDet] = useState();

    const {id, toEnd, blu, dName, dSurname, imageUrl, carImage, model, carName, startDate, message, willTaxi, willTaxiPrice, luggageMax, driverId, type, totalSeats, availSeats, price, thestatus, orSec, desSec, datedate, originPlace, destinationPlace, destDesc, originDesc,} = route.params

    useEffect(() => {
            
                setStatus(thestatus)
            },[])


            // get driver details

         const fetchDriver = async ()=>{
         const todoData = await API.graphql(graphqlOperation(getUser, { id: driverId}))
            const todos = todoData.data.getUser
            setDriverDet(todos)
        
        }


        useEffect(() => {
            
                fetchDriver()
            },[])


 const restart = {
        id: id,
        status: 'Looking',
        active: 'no'
    };

    const collect = {
        id: id,
        status: 'Collect'
    };

    const delivered = {
        id: id,

        status: 'Delivered'
    };





            const whenCancelled = async ()=>{
           await API.graphql({ query: mutations.updateParcelOrder, variables: {input: restart}});
           setStatus('looking')
            Toast.show({
      type: 'success',
      text1: 'Logistics',
      text2: 'Your trip has been successfully declined',
      visibilityTime: 5000,

    });
           navigation.navigate('ParcelOrders')
        console.log('done')
        }

        const whenCollect = async ()=>{
           await API.graphql({ query: mutations.updateParcelOrder, variables: {input: restart}});
           setStatus('uCancel')
           navigation.navigate('ParcelOrders')
           // write logic to deduct for cancelation + 50% of order, pay driver 25% of order fee
       
        }



        const whenLooking = async ()=>{
        //    await API.graphql({ query: mutations.updateParcelOrder, variables: {input: restart}});
        //    setStatus('uCancel')
           navigation.navigate('RidesCustomerSearch', {id, willTaxiPrice, startDate, type, thestatus, orSec, desSec, datedate, originPlace, destinationPlace, destDesc, originDesc, price, driverId})
           // no cancellation fee. write logic to delete order
        
        }

         const whenAccepted = async ()=>{
           await API.graphql({ query: mutations.updateRides, variables: {input: restart}});
           setStatus('uCancel')
            Toast.show({
      type: 'success',
      text1: 'Logistics',
      text2: 'Your trip has been successfully declined',
      visibilityTime: 5000,

    });
           
           navigation.navigate('ParcelOrders')
           // write logic to deduct amount for cancelation only
     
        }

    
            const theImage = 'https://notjustdev-dummy.s3.us-east-2.amazonaws.com/images/products/keyboard1.jpg'

    


     const StatusStatus = () =>{

            
             if(status === 'Accepted'){
            return(
           

                 <TouchableOpacity style={{margin: 15, backgroundColor: 'red', height: 35, width: 130, borderRadius: 10, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
            onPress={()=> whenAccepted()}
            >
                <View>
                    <Text style={{color: 'white'}}>Cancel Trip</Text>
                    
                </View>
            </TouchableOpacity>

            )
        }
            
            

         if(status === 'dcancelled'){
            return(
         
                
                <TouchableOpacity style={{margin: 15, backgroundColor: 'green', height: 35, width: 130, borderRadius: 10, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
            onPress={()=> whenCancelled()}
            >
                <View>
                    <Text style={{color: 'white'}}>RE-BOOK</Text>
                    
                </View>
            </TouchableOpacity>
                
                
                
                )

            }

                if(status === 'driver'){
            return(
        

                <TouchableOpacity style={{margin: 15, backgroundColor: 'green', height: 45, width: 300, borderRadius: 5, alignItems:'center', alignContent: 'center', justifyContent:'space-around'}}
                    onPress={()=> whenLooking()}
                    >
                <View>
                    <Text style={{color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Book Trip</Text>
                    
                </View>
            </TouchableOpacity>
                
                )}

        }


         if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{



    return (
        <SafeAreaView style={{backgroundColor: 'white'}} >


        <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>



            <ScrollView style={{marginTop: 20}} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
            
            
             <View style={{ backgroundColor: 'white', padding: 20}}>

                  <View>
                    <Text style={{ paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 20, marginBottom: 15, fontFamily: 'Manrope_700Bold'}}>Book Trip</Text>
                </View>
               
                <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}} />

                 <View>

                   <View style={{ flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>

                    {/* image */}

                    <View>
                            <View style={{height: 100, marginRight: 20, justifyContent: 'space-around'}} >
                                <Image
                                    source={{uri: driverDet?.imageUrl}}
                                    style={{width: 50, height: 50, borderRadius: 10}}
                                />
                            </View>
                    </View>


                    {/* driven by */}
                    <View>
                        
                        <Text style={{fontWeight: '500', fontSize: 15, fontFamily: 'Manrope_600SemiBold'}}>{dName || `Loading...`} {dSurname}</Text>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                            <FontAwesome name={'location-arrow'} color={'grey'} size={15} />
                            <Text style={{marginLeft: 10, color: 'grey', fontSize: 12, fontFamily: 'Manrope_500Medium'}}> {(blu)}KM From Your Location</Text>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                            <MaterialCommunityIcons name={'map-marker-distance'} color={'grey'} size={15} />
                            <Text style={{marginLeft: 10, color: 'grey', fontSize: 12, fontFamily: 'Manrope_500Medium'}}>To {toEnd}KM From Your Destination</Text>
                        </View>
                        

                    </View>



                   </View>


                    <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}} />

                    <View>
                        <Text style={{fontWeight: '500', marginTop: 20, fontFamily: 'Manrope_700Bold'}}>ROUTE</Text>
                    </View>
                     <View>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10, backgroundColor:'green', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{fontSize: 13, fontWeight: '500', fontFamily: 'Manrope_600SemiBold'}}>{orSec}</Text>
                                </View>
                            </View>
                     </View>
                     <View>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
                                {/* dot */}
                                <View style={{width: 10, height: 10,backgroundColor:'red', borderRadius: 50, marginRight: 10}}>

                                </View>
                                {/* location */}
                                <View>
                                     <Text style={{fontSize: 13, fontWeight: '500', fontFamily: 'Manrope_600SemiBold'}}>{desSec}</Text>
                                </View>
                            </View>
                     </View>

                    <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

                    <View style={{flexDirection: 'row'}}>
                        <View style={{width: 50, height: 50}}>
                            <Ionicons name={'warning'} color="grey" size={40} />
                        </View>
                            <View style={{width: 300}}>
                                <Text style={{color: 'grey', fontFamily: 'Manrope_500Medium', fontSize: 13}}>You may be sharing your ride with others going your way. Other passengers may be added to the trip</Text>
                            </View>
                    </View>
                    

                    <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />


                     <View>
                        <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>VEHICLE</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignContent: 'center', alignItems: 'center'}}>
                        {/* image */}

                    <View>
                            <View style={{height: 100, marginRight: 20, justifyContent: 'space-around'}} >
                                <Image
                                    source={{uri: driverDet?.imageUrl}}
                                    style={{width: 50, height: 50, borderRadius: 10}}
                                />
                            </View>
                    </View>


                    {/* driven by */}
                    <View>
                        
                        <Text style={{fontWeight: '500', fontSize: 14, fontFamily: 'Manrope_600SemiBold'}}>{carName}, {model}</Text>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                            <FontAwesome5 name={'trailer'} color={'grey'} size={15} />
                            <Text style={{marginLeft: 10, color: 'grey', fontSize: 12, fontFamily: 'Manrope_500Medium'}}>No Trailer</Text>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                            <MaterialCommunityIcons name={'van-utility'} color={'grey'} size={19} />
                            <Text style={{marginLeft: 10, color: 'grey', fontSize: 12, fontFamily: 'Manrope_500Medium'}}>No Sail / Canopy</Text>
                        </View>
                        
                        </View>

                    </View>

                     <View style={{flexDirection: 'row'}}>
                        <View style={{width: 50, height: 50}}>
                            {/* <FontAwesome name={'question-circle'} color="grey" size={35} /> */}
                <LottieView style={{width: 60, height: 60, marginLeft: -2, marginTop: -4}} source={require('../../assets/warning/61735-message.json')} autoPlay loop />

                        </View>
                            <View style={{width: 300, marginLeft: 20}}>
                                <Text style={{fontFamily: 'Manrope_600SemiBold'}}>Driver Message : </Text>
                                <Text style={{color: 'grey', fontFamily: 'Manrope_500Medium', fontSize: 13}}>{message || `no message from driver`}</Text>
                            </View>
                           
                            
                    </View>


                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={15} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>{availSeats} of {totalSeats} Seats Avail.</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={15} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>R{price} Per Seat</Text>
                        </View>

                    </View>

                     <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <FontAwesome5 name='car-alt' size={15} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>Door2Door ? : {willTaxi}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={15} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>R{willTaxiPrice} Per Seat</Text>
                        </View>

                    </View>

                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='luggage' size={14} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>{luggageMax || `No Luggage`}</Text>
                        </View>
                    </View>

                    <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />


                    <View>
                        <Text style={{fontWeight: '500', marginTop: 5, fontFamily: 'Manrope_700Bold'}}>ABOUT DRIVER</Text>
                    </View>

                     <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 0}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialCommunityIcons name='gender-male-female' size={15} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>{driverDet?.gender}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='smoke-free' size={15} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontFamily: 'Manrope_500Medium', fontSize: 12}}>{driverDet?.smoke}</Text>
                        </View>

                    </View>

                    <View  style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 20, marginBottom: 20}} />

                {StatusStatus()}
                 </View>
           






            </View>

            

             

            

              
        </ScrollView>
        </SafeAreaView>
        
    )
        }
}

export default RidesOrderCustSearchInfo

const styles = StyleSheet.create({})
