import React, {useState, useEffect, useCallback} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Pressable, SafeAreaView, ScrollView, RefreshControl, ActivityIndicator, FlatList } from 'react-native'
import {Auth} from 'aws-amplify'
import { API } from "aws-amplify";
import { useNavigation, useRoute } from "@react-navigation/core";
import { listGoodsOrders, listUsers, listRides } from '../../graphql/queries';
import { Feather, FontAwesome5, Ionicons, MaterialCommunityIcons, MaterialIcons, Zocial } from '@expo/vector-icons';
import { onRidesOrderUpdated } from '../../graphql/subscriptions';
import * as queries from '../../graphql/queries';
import * as mutations from '../../graphql/mutations'
import { graphqlOperation } from 'aws-amplify';
import AnimatedLoader from "react-native-animated-loader";

import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}
const RidesDAO = () => {


    const navigation = useNavigation();


     let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

const [visible, setVisible] = useState(true);

 useEffect(() => {
     
     setInterval(() => {
      setVisible(!visible)
    }, 3000);
  }, [])


 const [user, setuser] = useState([]);
 const [custUser, setCustUser] = useState([])
const [todos, setTodos] = useState([])  
  const [filterTodo, setFilterTodo] = useState([]);
    const [refreshing, setRefreshing] = useState(false);
   const onRefresh = useCallback(() => {

    setRefreshing(true);

                 fetchUsers();
                fetchCustomer();
           


    wait(2000).then(() => setRefreshing(false));
  }, []);

 
    const fetchUsers = async() =>{
        
            const userData = await Auth.currentAuthenticatedUser();
            const userSub = userData.attributes.sub;
            setuser(userSub);
            
    }


    async function fetchCustomer() {

            const todoData = await API.graphql({query: queries.listUsers})
            const todos = todoData.data.listUsers.items
            setCustUser(todos)
    
     }

     useEffect(() => {
            
                fetchUsers();
                fetchCustomer();
            },[])

     async function fetchTodos() {


            const todoData = await API.graphql({query: queries.listRides})
            const todos = todoData.data.listRides.items
            setTodos(todos)

                let filter = 
              
                {
                and: [{ driverId: {eq: user} },
                    { active: {eq:'yes'} }]
                 };
    
            const userplus = await Auth.currentAuthenticatedUser({bypassCache: true});
            const oneTodoData = await API.graphql({ query: listRides, variables: { filter : {and : [{driverId: {eq: 
                ( userplus.attributes.sub)
            }}, {active: {eq: 'yes'}}]}}});
            const oneTodos = oneTodoData.data.listRides.items
            setFilterTodo(oneTodos)

     }

     useEffect(() => {
        
            fetchTodos();
            
        },[todos]);

        // Subscribing to rides change

             let subsUpdate;
            function setUpSus(){
   
                subsUpdate = API.graphql(graphqlOperation(onRidesOrderUpdated)).subscribe( {next: (daraa) => {
                    setTodos(daraa)
                }, }) 

            }

            useEffect(() =>{
                setUpSus();

                return() =>{
                    subsUpdate.unsubscribe();
                    // reqUpdate.unsubscribe();   
                };

            },[]);

        const ItemView2= ({item}) => {

                return(

              <Pressable style={{ backgroundColor: 'white'}} onPress={() => 
                    navigation.navigate('RidesDAOInfo',{id: item.id, date: item.startDate, contents: item.contents, qty: item.qty, price: item.price, breakage: item.breakage, flame: item.flameable, type: item.type, myId: user, custId: item.userId, theStatus: item.status, orLat: item.originLatitude, orLng: item.originLongitude, desLat: item.destLatitude, desLng: item.destLongitude, originDesc: item.originDesc, destDesc: item.destDesc, recName: item.recName, recSurname: item.recSurname, recCode: item.recCode, recNumber: item.recNumber})
                }>

                    <View style={{flexDirection: 'row'}}>



                    </View>

                    <View style={{margin: 10}}>

                     <View style={{marginLeft: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                                
                                     <View style={{marginRight: 10}}>
                                        <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'green'}}/>
                                     </View>

                                    <Text style={{fontWeight: '500', marginLeft: 10, width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>{item.originDesc || `Loading...`} </Text>
                             </View>

                            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
                                <View style={{marginRight: 10}}>
                                    <View style={{width: 8, height: 8, borderRadius: 50, backgroundColor: 'red'}}/>
                                </View>
                                <Text style={{fontWeight: '500', marginLeft: 10 , width: 280, fontFamily: 'Manrope_600SemiBold', fontSize: 12}}>{item.destDesc || `Loading...`}</Text>
                            </View>
                    </View>


                    <View style={{flexDirection: 'row'}}>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 5}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name="date-range" size={13} color="white" />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}>{item.startDate}</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='attach-money' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}>R {item.tripFee || `0.00`} Trip Total</Text>
                        </View>

                    </View>


                     <View style={{flexDirection: 'row'}}>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 7}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <MaterialIcons name='airline-seat-recline-normal' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}>{item.availSeats || `0`} of {item.totalSeats || `0`} Seats Left</Text>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 15}}>
                                <View style={{ backgroundColor: 'orange', borderRadius: 50, width: 24, height: 24, justifyContent: 'space-evenly', alignItems: 'center'}}>
                                    <Zocial name='statusnet' size={13} color={'white'} />
                                </View>
                                <Text style={{width: 118, marginLeft: 10, fontSize: 12}}>Status : {item.status || `Loading...`}</Text>
                        </View>

                    </View>

                    </View>
                   
                    </Pressable>


              
                 )
            }

        const ItemSeperatorView = () => {
                return(
                    <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8'}}
                    />
                )
            }


if (!fontsLoaded) {
            return(
                <View>
                    <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>
                </View>
            )
        } else{

    return (

        <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>

            <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,2555,255,7)"
        
        animationStyle={{width: 75, height: 75}}
        speed={1}
      >
      </AnimatedLoader>

             <ScrollView style={{backgroundColor: 'white', padding: 5}}
             showsHorizontalScrollIndicator={false}
             showsVerticalScrollIndicator={false}
              refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
             >

            <View style={{flexDirection: 'row'}}>
                    <Text style={{ paddingLeft: 15, fontSize: 25, fontWeight: '600', color: 'black', marginTop: 30, fontFamily: 'Manrope_700Bold'}}>Active Trips</Text>

            </View>

                     <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />

                    <View>
                        <Text style={{fontWeight: '500', marginLeft: 20, fontFamily: 'Manrope_700Bold'}}>TRIPS</Text>
                    </View>

                    <View 
                    style={{height: 0.5, width: '100%', backgroundColor: '#c8c8c8', marginTop: 10, marginBottom: 10}}
                    />


                <FlatList
                    data={filterTodo}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={ItemSeperatorView}
                    renderItem={ItemView2}
                />    

                <View style={{alignContent: 'center', alignItems: 'center', marginTop: 30}}>
                    <Text style={{fontFamily: 'Manrope_400Regular'}}>- No More Trips -</Text>
                </View>
           

        
         <View >
           



        </View>
          </ScrollView>
     </SafeAreaView>
    )
            }
}

export default RidesDAO


const styles = StyleSheet.create({

    container: { flex: 1, justifyContent: 'center', padding: 20 },
  todo: {  backgroundColor: 'white', borderRadius: 10, margin: 10, padding: 10, paddingBottom: 20},
  input: { height: 50, backgroundColor: '#ddd', marginBottom: 10, padding: 8 },
  todoName: { fontSize: 15 },

})
