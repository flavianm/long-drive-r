const aws = require('aws-sdk');
const ddb = new aws.DynamoDB();

exports.handler = async(event, context) =>{

  if (!event.request.userAttributes.sub){
    console.log("Error: No User Found")
    context.done(null, event);
    return;
  }

  // save user to dynamo db

  const date = new Date();

  const params ={
    Item: {
      'id' : {S: event.request.userAttributes.sub}, // we put S because its a string. for number its I
      '__typename' : {S: 'User'},
      'username': {S: event.userName},
      'email': {S: event.request.userAttributes.email} ,
      'createdAt': {S: date.toISOString()},
      'updatedAt': {S: date.toISOString()} ,
    },
    TableName: process.env.USERTABLE, 
  }

     //submit to dynamo bd
  try{
    await ddb.putItem(params).promise();
    console.log("success"); 
  } catch (e){
    console.log("Error", e);
  }
  context.done(null, event);

}