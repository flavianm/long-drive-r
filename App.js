import iconSet from '@expo/vector-icons/build/Fontisto';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {useState, useEffect, useRef } from 'react';
import { Dimensions, Platform, StyleSheet, Text, TouchableOpacity, View, Image, Pressable, ScrollView, TextInput, SafeAreaView, ImageBackground } from 'react-native';
import * as Location from 'expo-location';
import Constants from 'expo-constants';
import { installWebGeolocationPolyfill } from "expo-location";
import Router from './src/navigation/Root';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeNavigator from './src/navigation/Home';
import ParcelsNavigator from './src/navigation/Parcels';
import { createDrawerNavigator } from '@react-navigation/drawer';
import CustomDrawer from './src/navigation/CustomDrawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
//import stacktrace from 'react-native-stacktrace';
import GoodsNavigator from './src/navigation/Goods';
import WalletNavigator from './src/navigation/Wallet';
import MenuNavigator from './src/navigation/menu';
import Toast from 'react-native-toast-message';
import * as Notifications from 'expo-notifications';
import {Auth} from 'aws-amplify';
import {API, graphqlOperation} from 'aws-amplify';
import * as mutations from './src/graphql/mutations';
import * as queries from './src/graphql/queries';
import {getUser } from './src/graphql/queries';
import { onUserrUpdated, onUserUpdated } from './src/graphql/subscriptions';
import LottieView from 'lottie-react-native';


import * as Permissions from 'expo-permissions';

import { withAuthenticator } from 'aws-amplify-react-native';


import {
  useFonts,
  Manrope_200ExtraLight,
  Manrope_300Light,
  Manrope_400Regular,
  Manrope_500Medium,
  Manrope_600SemiBold,
  Manrope_700Bold,
  Manrope_800ExtraBold,
} from '@expo-google-fonts/manrope';


import Amplify from 'aws-amplify'
import config from './src/aws-exports'
import { Entypo, Feather, FontAwesome, FontAwesome5, Ionicons, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import Animated from 'react-native-reanimated';
Amplify.configure(config)

stacktrace.init(function onError(err, isFatal) {
  return new Promise((resolve, reject) => {
    // log to your own logging service
    return doAsyncThingWithError(err, isFatal)
      .then(resolve)
      .catch(reject);
  });
});

//Location.installWebGeolocationPolyfill();

const Stack = createStackNavigator();
import plus from './assets/plus.png';
//import { Chat } from './src/screens/ChatScreen';
//import Profile from './src/screens/ProfileScreen';
//import Parcel from './src/screens/ParcelScreen';
//import ParcelCounter from './src/screens/ParcelCounter';
import TabTwoScreen from './src/screens/Profile';
// import ShopScreen from './src/screens/ShopScreen';
//import FindSearch from './src/screens/Findtrip';
//import MoreScreen from './src/screens/moreopt';
//import HomeScreen from './src/screens/HomeScreen';
//import ParcelInput from './src/screens/Parcels';
//import ParcelCustomer from './src/screens/ParcelCustomer';
//import ParcelDest from './src/screens/ParcelDest/index';
import Onboarding from './src/components/Onboarding';
import OnBProfile from './src/components/OnBprofile';
//import SignIn from './src/components/Authentications/SignIn';
//import SignUp from './src/components/Authentications/SignUp';
import MyTrips from './src/screens/mytrips';
//import RiderDestDriver from './src/screens/RiderDestDriver';
import TripsNavigator from './src/navigation/Trips';
import NewtripNavigator from './src/navigation/NewtripNavigator';

const Drawer = createDrawerNavigator();



// export default function  App() {
function  App() {

  
    
Location.installWebGeolocationPolyfill();

  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  useEffect(() => {
    (async () => {
      if (Platform.OS === 'android' && !Constants.isDevice) {
        setErrorMsg(
          "Oops, this will not work on Snack in an Android emulator. Try it on your device!"
        );
        return;
      }
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, []);


registerForPushNotificationsAsync = async () => {
  if (Constants.isDevice) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    const token = (await Notifications.getExpoPushTokenAsync()).data;
    console.log(token);
    this.setState({ expoPushToken: token });
  } else {
    alert('Must use physical device for Push Notifications');
  }

  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FF231F7C',
    });
  }
  };

useEffect(() => {
  registerForPushNotificationsAsync
}, [])



  let text = 'Waiting..';
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify(location);
  }

 const Tab = createBottomTabNavigator();
 const tabOffsetValue = useRef(new Animated.Value(0)).current;


 let [fontsLoaded] = useFonts({
    Manrope_200ExtraLight,
    Manrope_300Light,
    Manrope_400Regular,
    Manrope_500Medium,
    Manrope_600SemiBold,
    Manrope_700Bold,
    Manrope_800ExtraBold,
  });

  const [fullLoad, setFullLoad] = useState(true);

 const [userDet, setUserDet] = useState();
    const [userSubv, setUserSubv] = useState(null);
    const [onBoarded, setOnBoarded] = useState(null);
    const [onProfiled, setOnProfiled] = useState(null);
  const [todosab, setTodosab] = useState([]);
  const [notAuthed, setNotAuthed] = useState(false);


  const [username, setUsername] = useState('null');
  const [password, setPassword] = useState('null');
  const [phone_number, setPhone_number] = useState('null');
  const [email, setEmail] = useState('null');
  const [confirmationCode, setConfirmationCode] = useState('null');
  const [confirmationCodeLog, setConfirmationCodeLog] = useState('null');
  const [userr, setUserr] = useState('null');
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const [choose, setChoose] = useState('main');
  // const [signPage, setSignPage] = useState('sgn')
  // const [regPage, setRegPage] = useState('reg') 

  const [loginError, setLoginError] = useState([]);
  const [errored, setErrored] = useState('null');


useEffect(() => {
     
     setInterval(() => {
      setFullLoad(!fullLoad)
    }, 8000);
  }, [])



    const fetchUsers = async() =>{
        

          // return;

          try{

        

            const userData = await Auth.currentAuthenticatedUser();
          
           setUserSubv(userSub)
            const userSub = userData.attributes.sub;
            const todoData = await API.graphql(graphqlOperation(getUser, { id: userSub}))
            const todos = todoData.data.getUser
            setUserDet(todos)


            setOnBoarded(todos?.onBoard)
            setOnProfiled(todos?.completeP)




            } catch(e){
              setNotAuthed(true)
              return;
            }
          
    }

const fetchUserInfo = async()=>{
        
  
  try{

      const todoDatasss = await API.graphql({query: queries.listUsers})
      const todosss = todoDatasss.data.listUsers.items
      setTodosab(todosss)

    
  }catch(e){
    setNotAuthed(true)

  }
  


        

    }

 const checkdata = async()=>{

      const userData = await Auth.currentAuthenticatedUser();
      const userSub = userData.attributes.sub;

      const todoData = await API.graphql(graphqlOperation(getUser, { id: userSub}))
      const todos = todoData.data.getUser

      if (todos === null){
        

         const tede = {
                id: userSub,
                email: userData.attributes.email,
                username: userSub,
                balance: '0.01'
                
            }

             await API.graphql(graphqlOperation(mutations.createUser, { input: tede}));

      }else{
        return;
      }

    }


      useEffect(() => {
   checkdata()

     fetchUsers()
     fetchUserInfo()
    }, [todosab])


  let subsUpdate;
  function setUpSus(){
    // return;
      subsUpdate = API.graphql(graphqlOperation(onUserrUpdated)).subscribe( {next: (daraa) => {
          setTodosab(daraa)
      }, }) 

  }

useEffect(() =>{
                setUpSus();
// return;
                return() =>{
                    subsUpdate.unsubscribe();
                };

            },[]);   

            


   async function putUp() {

    const userData = await Auth.currentAuthenticatedUser();
    const userSub = userData.attributes.sub;

    const date = new Date();


    const userInput = {
              id: userSub,
              username: userSub,
              createdAt: date.toISOString(),
              updatedAt: date.toISOString(),
              email: userData.attributes.email

            }

     await API.graphql({ query: mutations.createUser, variables: {input: userInput}});


   }         


  async function authenticate(isAuthenticated){
        setIsAuthenticated(isAuthenticated)

    }          


    render = () => {
        if(isAuthenticated){
            console.log('Auth: ', Auth)
            return(
                <View>
                    <Text>Authenticated </Text>
                </View>
            )
        }
    }

    async function signUp() {
    Auth.signUp({
        username: username,
        password: password,
        attributes: {
            email: email,
            phone_number: phone_number,
        }
    }).then(()=>{ setChoose('conf')}).catch(err => setErrored('signup'))


    
  }

  async function confirmSignUp(){

    Auth.confirmSignUp(username, confirmationCode)
    .then(()=> 
      // setNotAuthed(false),
      // putUp()
      setChoose('log')

    ).catch(err => setErrored('code'))
  }


  async function signIn() {
      Auth.signIn( username, password).then(user => {
          setUserr({user})
      console.log('Successfully Logged In')
      setIsAuthenticated(true)
      setNotAuthed(false)

      }).catch(err => 
       setLoginError(err),
       setErrored('login')
  )

    }


    async function confirmSignIn(){

    Auth.confirmSignIn(username, confirmationCodeLog)
    .then(()=> {  
        console.log('Success ')     
    setIsAuthenticated(true)
        console.log('authenticated true ')     

}).catch(err => console.log('Confirm error !:', err))
  }


 
if(fullLoad === true){

  return(
    <View style={{alignItems: 'center', alignContent: 'center', justifyContent:'center', marginTop: 200}}>
        
         <LottieView style={{width: 100, height: 100, }} source={require('./src/assets/9809-loading.json')} autoPlay loop />

    </View>

  )
}


  if(errored === 'login'){
    return(
      <View style={{alignItems: 'center', alignContent: 'center', justifyContent: 'center', flex: 1}}>
           <LottieView style={{width: 100, height: 100, }} source={require('./src/assets/warning/74896-alert-caution')} autoPlay loop />

        <Text style={{width: 200, marginTop: 30, fontFamily: 'Manrope_600SemiBold'}} >Error Signing In. Please check your Username / Password</Text>

        <TouchableOpacity style={{marginTop: 30}} onPress={()=> setErrored('null')} >
         <Text style={{fontFamily: 'Manrope_500Medium', color: 'orange'}}>Retry</Text>
         
          </TouchableOpacity>
      </View>
    )
  }

  if(errored === 'signup'){
    return(
      <View style={{alignItems: 'center', alignContent: 'center', justifyContent: 'center', flex: 1}}>
           <LottieView style={{width: 100, height: 100, }} source={require('./src/assets/warning/74896-alert-caution')} autoPlay loop />

        <Text style={{width: 200, marginTop: 30, fontFamily: 'Manrope_600SemiBold'}} >Error Signing Up. Please check : </Text>
        <Text style={{width: 200, marginTop: 30, fontFamily: 'Manrope_500Medium'}} >1. Is your phone number correct, Does it have a country code ? </Text>
                <Text style={{width: 200, marginTop: 30, fontFamily: 'Manrope_500Medium'}} >2. Check your email address. Ensure no spaces or Caps between characters </Text>
                <Text style={{width: 200, marginTop: 30, fontFamily: 'Manrope_500Medium'}} >4. Ensure your password is more than 8 characters </Text>

        <TouchableOpacity style={{marginTop: 30}} onPress={()=> setErrored('null')} >
         <Text style={{fontFamily: 'Manrope_500Medium', color: 'orange'}}>Retry</Text>
         
          </TouchableOpacity>
      </View>
    )
  }

  if(errored === 'code'){
    return(
      <View style={{alignItems: 'center', alignContent: 'center', justifyContent: 'center', flex: 1}}>
           <LottieView style={{width: 100, height: 100, }} source={require('./src/assets/warning/74896-alert-caution')} autoPlay loop />

        <Text style={{width: 200, marginTop: 30, fontFamily: 'Manrope_600SemiBold'}} >Your Confirmation Code is Incorrect ! </Text>
                
        <TouchableOpacity style={{marginTop: 30}} onPress={()=> setErrored('null')} >
         <Text style={{fontFamily: 'Manrope_500Medium', color: 'orange'}}>Retry</Text>
         
          </TouchableOpacity>
      </View>
    )
  }
 
  
  if(notAuthed === true){

        

    if(choose === 'main'){


     

      if (!fontsLoaded) {
                  return(
                      <View>
                        <Text>Loading</Text>
                      </View>
                  )
              } else{

          
      return(

        <SafeAreaView style={{backgroundColor: '#1E1B33', flex: 1}}>

          <View style={{backgroundColor: '#1E1B33'}}>

            <View style={{alignContent: 'center', justifyContent: 'center', alignItems: 'center', flex: 0.7, marginTop: 150}}>
                    <Image style={{width: 130, height: 130}}
                     source={require('./src/assets/oot.png')}/>
                    
            </View>

            <View style={{alignContent: 'center', justifyContent: 'center', alignItems: 'center', flex: 0.7}}>


            </View>

             <View style={{flexDirection: 'row', justifyContent: 'space-evenly', flex: 0.3, marginTop: 300}}> 

               <TouchableOpacity onPress={()=> setChoose('log')} style={{width: 150, height: 50, borderRadius: 5, borderWidth: 1, borderColor: '#E15F25', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{ color: '#E15F25', fontFamily: 'Manrope_600SemiBold', fontSize: 13}}>LogIn</Text>
              </TouchableOpacity>

              <TouchableOpacity  onPress={()=> setChoose('sup')} style={{width: 150, height: 50, borderRadius: 5, backgroundColor: '#E15F25', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{color: 'white', fontFamily: 'Manrope_600SemiBold', fontSize: 13}}>SignUp</Text>
              </TouchableOpacity>

          </View>


          </View>
          
         

       


        </SafeAreaView>
      )
              }

    }

    if(choose === 'sup'){

        if (!fontsLoaded) {
            return(
                <View>
                   <Text>Loading</Text>
                </View>
            )
        } else{

      return(

      <SafeAreaView style={{backgroundColor: 'white', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>


        <ScrollView>
          <View style={{ marginBottom: 15, marginTop: 10}}>
                <Text style={{fontSize: 30, fontFamily: 'Manrope_600SemiBold' }}>Sign Up</Text>
            </View>


            <View style={{alignContent: 'center', justifyContent: 'center', alignItems: 'center'}}>
                    <Image style={{width: 250, height: 250, resizeMode: 'center'}}
                     source={require('./src/assets/reg.png')}/>
                    
               </View>

          <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setUsername(val)}
                    placeholder={'E-mail Address'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setPassword(val)}
                    secureTextEntry={true}
                    placeholder={'Password'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
                 <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setPhone_number(val)}
                    placeholder={'Phone Number | +27676837717'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>
                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setEmail(val)}
                    placeholder={'Confirm E-mail'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>


                <View style={{ margin: 20}}>
                    <TouchableOpacity onPress={()=> signUp()} style={{width: '100%', width: 280, borderRadius: 5, height: 45, backgroundColor: '#E15F25', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Sign Up</Text>
                    </TouchableOpacity>
                </View>

                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20}}>

                      <TouchableOpacity onPress={()=> setChoose('log')}>
                        <Text  style={{fontFamily: 'Manrope_500Medium', color: 'grey', fontSize: 13}}>Sign In</Text>
                      </TouchableOpacity>

                    <TouchableOpacity onPress={()=> setChoose('conf')}>
                        <Text  style={{fontFamily: 'Manrope_500Medium', color: 'grey', fontSize: 13, marginRight: 20}}>Confirmation Code</Text>
                      </TouchableOpacity>
                    </View>

                

  
        </ScrollView>
        </SafeAreaView>
           )
        }

    }

    if(choose === 'log'){


      if (!fontsLoaded) {
            return(
                <View>
                   <Text>Loading</Text>
                </View>
            )
        } else{

      return(
    <SafeAreaView style={{backgroundColor: 'white', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>




          <View style={{alignContent: 'center', alignItems: 'center', backgroundColor: 'white'}}>

          <ScrollView  showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false} style={{alignContent: 'center'}}>


                <View style={{marginTop: 30, marginBottom: 30}}>
                    <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 30}}>Sign In</Text>
                </View>

             <View style={{alignContent: 'center', justifyContent: 'center', alignItems: 'center'}}>
                    <Image style={{width: 250, height: 250, resizeMode: 'center'}}
                     source={require('./src/assets/log.png')}/>
                    
               </View>

                    <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                        <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                        onChangeText={val => setUsername(val)}
                        placeholder={'E-mail Address'}
                        placeholderTextColor="#7d7d7d"
                        />
                    </View>

                    <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                        <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                        onChangeText={val => setPassword(val)}
                        secureTextEntry={true}
                        placeholder={'Password'}
                        placeholderTextColor="#7d7d7d"
                        />
                    </View>

                    <View style={{width: 280, marginTop: 20,  marginLeft: 20, alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>
                        <TouchableOpacity onPress={()=> signIn()} style={{width: '100%', borderRadius: 5, height: 45, backgroundColor: 'orange', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Sign In</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 20}}>

                      <TouchableOpacity onPress={()=> setChoose('sup')}>
                        <Text  style={{fontFamily: 'Manrope_500Medium', color: 'grey', fontSize: 13}}>No account ? Sign Up</Text>
                      </TouchableOpacity>

                   
                    </View>

                   

                              



                </ScrollView>

          </View>

      
    </SafeAreaView>
      )
        }

    }
    
    if(choose === 'conf'){

      if (!fontsLoaded) {
            return(
                <View>
                   <Text>Loading</Text>
                </View>
            )
        } else{

      return(

            <SafeAreaView style={{backgroundColor: 'white', alignContent: 'center', alignItems: 'center', justifyContent: 'center'}}>

            <View>

                <View style={{marginTop: 30, marginBottom: 30}}>
                    <Text style={{fontFamily: 'Manrope_600SemiBold', fontSize: 30}}>Confirm Code</Text>
                </View>

                <View style={{alignContent: 'center', justifyContent: 'center', alignItems: 'center'}}>
                    <Image style={{width: 250, height: 250, resizeMode: 'center'}}
                     source={require('./src/assets/undraw_Forgot_password_re_hxwm.png')}/>
                    
               </View>


                <View style={{backgroundColor:'#ebebeb', marginLeft: 20, marginTop: 20, marginBottom: 5, height: 45, width: 280, borderRadius: 5}}>
                    <TextInput style={{color:'black', paddingLeft: 10, height: 40, fontFamily: 'Manrope_500Medium', fontSize: 13}}
                    onChangeText={val => setConfirmationCode(val)}
                    placeholder={'Confirmation Code'}
                    placeholderTextColor="#7d7d7d"
                    />
                </View>


                <View style={{width: 280, marginTop: 20, margin: 20}}>
                  <TouchableOpacity onPress={()=> confirmSignUp()} style={{width: '100%', borderRadius: 5, height: 45, backgroundColor: '#E15F25', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{color: 'white', fontFamily: 'Manrope_600SemiBold'}}>Confirm Code</Text>
                  </TouchableOpacity>
                </View>



                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20}}>

                      <TouchableOpacity onPress={()=> setChoose('log')}>
                        <Text  style={{fontFamily: 'Manrope_500Medium', color: 'grey', fontSize: 13}}>Already have an account ? Sign In</Text>
                      </TouchableOpacity>

                    <TouchableOpacity onPress={()=> setChoose('sup')}>
                        <Text  style={{fontFamily: 'Manrope_500Medium', color: 'grey', fontSize: 13, marginRight: 20}}>Sign Up</Text>
                      </TouchableOpacity>
                    </View>

            </View>

            </SafeAreaView>


      )}

    }
 
    }









if (onBoarded === null || onBoarded === 'No'){


  return(
    <View style={{flex: 1, alignContent: 'center', justifyContent: 'center', alignItems: 'center'}}>
      <Onboarding/>
    </View>
  )
}



if (onProfiled === null || onProfiled === 'No'){


  return(
    <View style={{flex: 1}}>
      <OnBProfile />
    </View>
  )
}




 





  return (

    <NavigationContainer>

        <Tab.Navigator tabBarOptions={{
        showLabel: false,
        style: {
                backgroundColor: 'white',
                
                height: 60,
                borderRadius: 0,
                shadowColor: '#000',
                shadowOpacity: 0.06,
                shadowOffset: {
                  width: 10,
                  height: 10
          },
          paddingHorizontal: 20,
        }
      }}>


      <Tab.Screen name={"Home"} component={HomeNavigator} options={{
          tabBarIcon: ({ focused }) => (
            <View style={{
              position: 'absolute',
              top: 20
            }}>
              <FontAwesome5 
                name="suitcase-rolling"
                size={20}
                color={focused ? 'orange' : 'gray'}
              ></FontAwesome5>
            </View>
          )
        }} listeners={({ navigation, route }) => ({
          tabPress: e => {
            Animated.spring(tabOffsetValue, {
              toValue: 0,
              useNativeDriver: true
            }).start();
          }
        })}></Tab.Screen>

      


   
   
  

         <Tab.Screen name={"ActionButton"} component={FindScreen} options={{
          tabBarIcon: ({ focused }) => (
            <View style={{
              position: 'absolute',
              top: 20
            }}>
              <FontAwesome5 
                name="car"
                size={20}
                color={focused ? 'orange' : 'gray'}
              ></FontAwesome5>
            </View>
          )
        }} listeners={({ navigation, route }) => ({
          tabPress: e => {
            Animated.spring(tabOffsetValue, {
              toValue: getWidth() * 3,
              useNativeDriver: true
            }).start();
          }
        })}></Tab.Screen>

        {/* <Tab.Screen name={"Add"} component={WalletNavigator} options={{
          tabBarIcon: ({ focused }) => (
            <View style={{
              position: 'absolute',
              top: 20
            }}>
              <Ionicons
                name="add-circle"
                size={18}
                color={focused ? 'orange' : 'gray'}
                // color={focused ? '#E15F25' : 'gray'}

              ></Ionicons>
            </View>
          )
        }} listeners={({ navigation, route }) => ({
          tabPress: e => {
            Animated.spring(tabOffsetValue, {
              toValue: getWidth() * 3,
              useNativeDriver: true
            }).start();
          }
        })}></Tab.Screen> */}

 <Tab.Screen name={"listScreen"} component={TripsNavigator} options={{
          tabBarIcon: ({ focused }) => (
            <View style={{
              // centring Tab Button...
              position: 'absolute',
              top: 20
            }}>
              <Entypo 
                name="add-to-list"
                size={20}
                color={focused ? 'orange' : 'gray'}
              ></Entypo>
            </View>
          )
        }} listeners={({ navigation, route }) => ({
          // Onpress Update....
          tabPress: e => {
            Animated.spring(tabOffsetValue, {
              toValue: getWidth() * 4,
              useNativeDriver: true
            }).start();
          }
        })}></Tab.Screen>

 <Tab.Screen name={"Settings"} component={MenuNavigator} options={{
          tabBarIcon: ({ focused }) => (
            <View style={{
              // centring Tab Button...
              position: 'absolute',
              top: 20
            }}>
              <Entypo  
                name="menu"
                size={20}
                color={focused ? 'orange' : 'gray'}
              ></Entypo>
            </View>
          )
        }} listeners={({ navigation, route }) => ({
          // Onpress Update....
          tabPress: e => {
            Animated.spring(tabOffsetValue, {
              toValue: getWidth() * 4,
              useNativeDriver: true
            }).start();
          }
        })}></Tab.Screen>

      </Tab.Navigator>

     

        <Toast ref={(ref) => Toast.setRef(ref)} />

    </NavigationContainer>

  );



}

function getWidth() {
  let width = Dimensions.get("window").width

  // Horizontal Padding = 20...
  width = width - 80

  // Total five Tabs...
  return width / 5
}


function FindScreen() {
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <NewtripNavigator/>
    </View>
    
  );
}

function SettingsScreen() {
  return (
    <View style={{ flex: 1}}>
      <TabTwoScreen/>
    </View>
  );
}

function listScreen(){
  return (
    <View style={{ flex: 1, backgroundColor: 'white'}}>
      <MyTrips/>
    </View>
  );
}




const styles = StyleSheet.create({
 paragraph: {
    fontSize: 18,
    textAlign: 'center',
  },
});

export default App;
// export default withAuthenticator(App);